<?php
include('includes/header.php');
?>


    <div class="unit-5 overlay" style="background-image: url('<?php echo $base_url;?>/images/hero_bg_1.jpg');">
      <div class="container text-center">
        <h2 class="mb-0">Cell Number Database</h2>
        <p class="mb-0 unit-6"><a href="">Products</a> <span class="sep">></span> <span>Cell Number Database</span></p>
      </div>
    </div>


  
    <div class="site-section bg-light">
     <div class="container">  
    <div class="row justify-content-center">
        
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      
            <img src="<?php echo $base_url;?>Smss-database.png" class="img img-responsive">
        </div>
        
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h4><strong>SMSS DATABASES</strong></h4>
            <ul>
                <li>Marketing to <strong>Mobile Databases</strong> increased significantly once companies realised <strong>Mobile Technology</strong> was fast becoming the new number <strong>one marketing</strong> strategy.</li>
                <li>This inevitably also created an opportunity for illegal and unsolicited marketing hence the increase of spam activities.</li>
                <li>Privacy advocates and government regulators were subsequently forced to amongst other marketing strategies increase scrutiny regarding the legitimacy and validity of mobile databases.</li>
                <li>Added as a result, data protection rules in South Africa have been established to regulate the utilization of customer and consumer’s data and the retention thereof.</li>
                <li>SMSS provides verified and legally acquired databases only with accurate and reliable data. Our Databases have mostly been acquired through reliable opt-in practises.</li>
            </ul>
        </div>
        <ul>
            <li>Local and International consumer data protection laws and regulations are strictly adhered to.</li>
            <li>With zero-tolerance policies in place SMSS takes a firm stand against any form of unsolicited database harvesting, irresponsible data distribution and/or illegal marketing methods.</li>
            <li>With an SMSS Database, companies eliminate the pitfalls and legal implications normally associated with unregulated Mobile Databases which most likely results in unsuccessful direct marketing campaigns.</li>
        </ul>
        
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 justify-content-center">
        <h4><strong>Database Pricing</strong></h4> 
        <picture>
        <img src="<?php echo $base_url;?>/img_41.jpg" class="img-fluid">    
        </picture>     
       
        <p>Prices exclude VAT</p>     
        </div>
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 justify-content-center">
        <picture>
            <img src="<?php echo $base_url;?>/check1.png" class="img-fluid">
        </picture>
        </div>
        <div class="justify-content-center text-center">
        
       <h4><strong>Terms &amp; Conditions</strong></h4>
        <p>SMSS does not provide warranty for completeness and accuracy of the information and content of the database as numbers might change or become invalid.<br>
        The same applies for the accuracy, completeness and / or topicality of the directions for use.<br>
        SMSS does not provide warranty for damages resulting from errors of stored data or errors occurring with the reproduction of data<br></p> 
        </div>
        
    </div>
                    </div>  
      
      
      </div>
      



<?php
include('includes/footer.php');
?>