
<?php
include('includes/header.php');
?>
  
    <div class="unit-5 overlay" style="background-image: url('<?php echo $base_url;?>images/hero_bg_1.jpg');">
      <div class="container text-center">
        <h2 class="mb-0">Bulk MMS Service</h2>
        <p class="mb-0 unit-6"><a href="<?php echo $base_url;?>">Products</a> <span class="sep">></span> <span>Bulk MMS Service</span></p>
      </div>
    </div>


      
    <div class="site-section">
      <div class="container">
      <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade" >
          <center><h3>A <strong>picture</strong> is worth a <strong>1000</strong> words. A <strong>video</strong> is worth a <strong>1000</strong> pictures</h3></ceznter>
      <center><h2><strong>Bulk MMS Examples</strong></h2></center><br><br>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <center>
                <h4>Marketing</h4>
                <img src="<?php echo $base_url;?>images/marketing.gif" class="img img-responsive">
            </center>
        </div>
        
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <center>
                <h4>Funny Videos</h4>
                <img src="<?php echo $base_url;?>images/funny.gif" class="img img-responsive">
            </center>
        </div>
        
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <center>
                <h4>Debt Collecting</h4>
                <img src="<?php echo $base_url;?>images/data_collecting.gif" class="img img-responsive">
            </center>
        </div>
        
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <center>
                <h4>Account Statements</h4>
                <img src="<?php echo $base_url;?>images/data.gif" class="img img-responsive">
            </center>
        </div>
          
          </div>  
        </div>
          </div>   

                           
                           
    <div class="py-5 bg-primary">
      <div class="container">
        <div class="row align-items-center">
         <div class="col-lg-6 mb-5 mb-lg-0">
            <div>
             <h2 class="text-black">Why Bulk MMS Services?</h2>

    <ul class="text-white">
        <li><p>MMS Campaign Management from start to end</p></li>
        <li><p>An MMS includes a large amount of information in a single message</p></li>
        <li><p>Text messages up to 5000 characters are supported</p></li>
        <li><p>Sound and video files are supported</p></li>
        <li><p>Provide clients a preview of products or services with visually attractive ads</p></li>
        <li><p>Cost effective and delivered to a wider audience</p></li>
        <li><p>It gives target marketing a new meaning</p></li>
        <li><p>All MMS messages are tracked regarding delivery and open rate</p></li>
        <li><p>Viral marketing becomes inevitable since MMS messages are visually attractive and forwarded</p></li>
        <li><p>Excellent method to ensure improved customer relations and interaction</p></li>
        <li><p>It creates constant brand awareness for your company</p></li>
        <li><p>Send customers a voucher, coupon or ticket via an MMS</p></li>
        <li><p>Send reminder messages with your Company Logo as picture..sure to hit the target and not your budget</p></li>
    </ul>   
                             
            </div>
          </div> 
  
            
        
          <div class="col-lg-6 ml-md-auto">
            <div>
                
                
       <h2 class="text-black">Procedure to send bulk MMS messages</h2>

    <ul class="text-white">
        <li><p>Simply email your list of contacts to us and we will take care of the sending and make sure it’s delivered</p></li>
        <li><p>Verification of your database per network – regarding handset compatibility, are done free of charge</p></li>
        <li><p>The MMS may contain more than one page, like a PowerPoint slide show</p></li>
        <li><p>Each page may contain text, audio files or video clips</p></li>
        <li><p>Recommended formats: (Image format – jpg / gif) (Text file – .txt) (Movie – 3gp)</p></li>
        <li><p>Suggested image dimensions for a MMS is around 175 x 145</p></li>
        <li><p>Bulk MMS projects (hundreds – thousands) are provided, as MMS messages are supported by all the networks</p></li>
        <li><p>Free creation of an MMS animation if quantity justifies the project, otherwise a separate creation-fee might be applicable</p></li>
        <li><p>Cost per MMS: From R0.99 to R1.65per MMS sent, depending on the project (Max size: 150KB)</p></li>
        <li><p>Once-off actioning fee of R500 ex Vat, might be applicable, depending on the project</p></li>
    </ul>
              
              
            </div>
          </div>
        </div>
      </div>
    </div>                           

      <div class="site-section">
      <div class="container">
      <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade" >
            <center><a class="btn btn-danger btn-lg" href="<?php echo $base_url;?>app/index.php?join"><strong>Register New Completely Free</strong></a></center>
          </div>
        </div>
          </div>
      </div>


    <picture>        
    <img class="img-fluid" src="Bulk_MMS_Pricing-01.jpg"> 
    </picture>


    
<?php
include('includes/footer.php');
?>