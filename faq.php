<?php
include('includes/header.php');
?>


  
    <div class="unit-5 overlay" style="background-image: url('<?php echo $base_url;?>images/hero_bg_1.jpg');">
      <div class="container text-center">
        <h2 class="mb-0">FAQ</h2>
        <p class="mb-0 unit-6"><a href="<?php echo $base_url;?>">FAQ</a> <span class="sep">></span> <span>Frequently Asked Questions</span></p>
      </div>
    </div>


      
    <div class="site-section">
      <div class="container">
      <div class="row">     
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            
            <img src="<?php echo $base_url;?>faq.jpg" class="img img-responsive" style="display: inline-block;" width="260" height="260" />
        </div>
        
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <p><span style="color: #3366ff;"><strong>How do I start sending SMS messages?</strong></span><br />
To send SMS messages you need a (Free) SMS Account to send from<br />
<a href="https://www.mojasms.com/app/index.php?join" target="_blank" rel="noopener"><strong>Register</strong></a> SMS messages to test our services.<br />
A typical SMS message is only 160 characters (spaces included) long. One SMS credit is charged per typical SMS<br />
You can send SMS messages using our web-based (online) <strong>SMS Platform</strong>, our <strong>SMS API</strong> (HTTP or SMPP) or from <strong>your own software</strong> after integration with ours<br />
SMS are connected to several premium high priority bulk SMS gateways worldwide.</p>
<p><span style="color: #3366ff;"><span style="color: #3366ff;"><strong>How do I buy SMS credits?<br />
</strong><span style="color: #333333;">SMS works on a prepaid basis. SMS credits can be purchased online via Credit or Debit card, PayPal, or Electronic Funds Transfer<br />
To pay via any of the above mentioned methods click on the following link and scroll down<a href="https://mojasms.com/app/" target="_blank" rel="noopener"><strong> BUY SMS CREDITS</strong></a></span><strong><br />
</strong></span></span><br />
            
            </div>
    
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

<strong><span style="color: #3366ff;">Does SMSS offer discounted messaging?</span></strong><br />
Yes, we offer the lowest discounted prices for large monthly volumes<br />
For non-profit organizations, NGO’s, Schools and Religious organizations we offer special SMS rates irrespective of monthly volumes</p>
<p><span style="color: #3366ff;"><strong>Can SMS messages with more than 160 characters be sent ?</strong></span><br />
Yes. SMSS allow you to send up to 6 SMS text messages (918 characters) as one SMS<br />
The message gets split into 6 parts when sent, but arrives at the mobile phone as a single SMS message.<br />
This process is called a concatenated SMS</p>
<p><span style="color: #3366ff;"><strong>Are we able to send SMS messages to Local and International numbers? If so what is the cost difference?</strong></span><br />
<em>There are 2 main factors which determines the cost of sending an SMS:</em><br />
The <a href="https://mojasms.com/bulk_sms_service.php/" target="_blank" rel="noopener"><strong>cost per SMS</strong></a> as per the bundle when SMS credits were purchased<br />
To which <a href="https://mojasms.com/bulk_sms_service.php" target="_blank" rel="noopener"><strong>country</strong> </a>or countries the SMS messages are being sent to</p>
<p><strong><span style="color: #3366ff;">How long does it take for my sent SMS messages to be delivered?</span></strong><br />
SMS messages sent via the SMSS gateway are typically delivered to a mobile phone number within 10 -15 seconds<br />
In some cases delivery may take longer (up to 3 minutes) depending on the Mobile Operator routing the SMS messages<br />
The number of SMS messages sent in a batch or very large volumes are factors that could have an influence on delivery speed</p>
<p><strong><span style="color: #3366ff;">Do I get charged for failed messages and why?</span></strong><br />
Yes, unfortunately Mobile Operators charge SMSS for all submissions regardless of the message status (not delivered, delayed or expired)<br />
More resources are actually expended by handling failed messages as our system re-try sending these messages to the network before reporting it as a failed message</p>
<p><strong><span style="color: #3366ff;">How many messages can I send at the same time?</span></strong><br />
You can send 1, 1000, 100000, or any number of messages at the same time</p>
<p><span style="color: #3366ff;"><strong>Are there any <em>other</em> costs involved when using SMSS?</strong><br />
<span style="color: #333333;">No. SMSS operates on a prepaid basis. Simply purchase SMS credit bundles and use them as and when you need to.<br />
There is no monthly fee, no set up fee, and no contract fee for using our SMS Platform</span><br />
</span></p>
<p><span style="color: #3366ff;"><strong>Can people reply to the SMS messages I&#8217;ve sent them?<br />
</strong></span>Yes, in South Africa and most other countries. Some International countries may charge a fee for reply SMS messages received.<br />
You (the sender) will not be charged for any reply SMS messages received</p>

<p><span style="color: #3366ff;"><strong>Do SMSS credits expire when not used?</strong></span><br />
No, SMS credits never expire and can be used if and when you need them</p>
<p><span style="color: #3366ff;"><strong>Can I personalize the SMS messages I want to send?</strong></span><br />
Yes, our SMS software allows each SMS message to start with a different recipient&#8217;s name if they need to be personalized</p>
<p><strong><span style="color: #3366ff;">Can multiple SMS messages be sent with <em>different content</em> for each SMS message?</span></strong><br />
Yes. Multiple SMS messages with different content for each SMS message can be sent from the SMSS Platform</p>
<p><span style="color: #3366ff;"><strong>Can SMS messages be set-up or scheduled to be sent at a later time and/or date?</strong></span><br />
Yes. SMS messages can be scheduled to be sent at any specific time and date in the future</p>
<p><span style="color: #3366ff;"><strong>Can scheduled SMS messages be edited or cancelled?</strong></span><br />
Yes. Scheduled SMS messages can be edited and cancelled at any time before the scheduled date and time.</p>

<p><span style="color: #3366ff;"><strong>How safe are my client contact lists?</strong></span><br />
SMSS will not intercept, monitor, copy or disclose any client&#8217;s messages or personal information, including Phone Books, Contact Groups or Contact Lists</p>
<p><span style="color: #3366ff;"><strong>Can various Contact Groups be set-up within the SMS database?</strong></span><br />
Yes, all database numbers are uploaded onto the platform via an Excel CSV file into groups or individually</p>
<p><span style="color: #3366ff;"><strong>Why can SMS recipients not reply to messages where an alphanumeric Sender ID was used?</strong></span><br />
Sending SMS messages with a alpha sender-ID is not allowed in South Africa and cannot be replied to<br />
The only way a Sender ID can be replied to (Not South Africa) is when the Sender-ID is set to a mobile number.<br />
Replies would then go to that particular mobile number<br />
SMSS does however allow any of your recipients (South Africa) to reply as our software automatically replaces the sender-id to enable replies</p>
<p><strong><span style="color: #3366ff;">Does SMSS support Unicode and foreign language characters?</span></strong><br />
Yes, SMS messages using Unicode for non-English characters (such as Arabic, Chinese or Cyrillic characters) are supported<br />
This will however limit character length per message to about 70 characters</p>
<p><strong><span style="color: #3366ff;">Can multiple devices be used to access and send SMS messages?</span> </strong><br />
Yes, sending SMS messages can be done via a browser on a laptop or smartphone, an app on a smartphone or even remotely via any mobile phone<br />
Since our SMS system is web-based, there is no need for any installations or software downloads</p>
<p><span style="color: #3366ff;"><strong>Can low SMS credit level notifications be set and edited?</strong></span><br />
Yes and whenever you need to</p>
<p><strong><span style="color: #3366ff;">Can multiple user accounts be created within my SMS account?</span> </strong><br />
Yes, multiple users can be created in your account and credits towards those accounts are controlled from your account&#8217;s admin panel</p>

         
     <a href="<?php echo $base_url;?>app/index.php?join" class="btn btn-success btn-lg"><strong>Register Now</strong></a>     
     </div>
      

     </div>
      </div>
      </div>
    


<?php
include('includes/footer.php');
?>