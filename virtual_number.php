<?php
include('includes/header.php');
?>


    <div class="unit-5 overlay" style="background-image: url('<?php echo $base_url;?>images/hero_bg_1.jpg');">
      <div class="container text-center">
        <h2 class="mb-0">Virtual Number</h2>
        <p class="mb-0 unit-6"><a href="<?php echo $base_url;?>">Products</a> <span class="sep">></span> <span>Virtual Number</span></p>
      </div>
    </div>
 <div class="site-section bg-light">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade">
            <h2 style="color: forestgreen">Get <hr style="background-color: green">
            Virtual Number of your choice</h2>
              <h3 class="bg-dgreen text-white">for your Business</h3>
          </div>
        </div>
        <div class="row hosting">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">

            <div class="unit-2 text-center border py-5 h-100 bg-dgreen">
              
              <h3 class="h4 text-white">NO MONTHLY<br> PAYMENT</h3>
              
            </div>

          </div>

          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-2 text-center border py-5 h-100 bg-brown">
              
              <h3 class="h4 text-white">ONCE OFF<br> PAYMENT</h3>
              
            </div>

          </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-2 text-center border py-5 h-100 bg-orange">
              
              <h3 class="h4 text-white">USE IT<br> ON YOUR <br>OWN PHONE</h3>
              
            </div>              
                

            </div>
        </div>
      <div class="row justify-content-center text-center">
          
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
              
             <div class="unit-2 text-center border py-5 h-100 bg-blue text-brown"> 
             <h3>ONLY 60 MINUTES TO SETUP</h3>  
              </div>     
          </div>  
            </div> 
      </div>
    </div>

 <div class="site-section bg-green">
      <div class="container">
          
   <div class="row text-white mb-5">
<h5> In order to purchase a virtual number or any other phone service you may complete the next steps:<hr class="bg-danger"></h5>       
       
     <ol>
<li><strong><a style="color:white;" href="<?php echo $base_url;?>/app/">Sign up</a></strong> to our site or <strong><a style="color:white;" href="<?php echo $base_url;?>/app/index.php?join">login </a></strong>if you already have an account;</li>
<li>Recharge your balance</li>
<li>Choose and order virtual number or other phone service you need</li>
<li>Make all settings and setup needed functions</li>    
</ol>            
 </div>
  <div class="row justify-content-center text-center mb-5">
    <a href="#" class="btn btn-danger rounded-0 py-2 px-4">Choose And Buy Virtual Phone Number Online!</a>       
  </div>   
    
    <div class="row text-white mb-5">
          
    <h4>Additional phone services for virtual number</h4>
    <br><br>
        <p>There are some useful and cheap phone services which are additional services to your phone numbers:- </p>
        <p>
            <ul>
                <li>Voicemail; </li>
                <li>Call recording; </li>
                <li>Conditional call forwarding; </li>
                <li>IVR menu; </li> 
                <li>Call history; </li>        
                <li>Greeting message;</li>
                <li>Hold a music.</li>
            </ul>
        </p>
      </div>      
          
          
          
        <div class="row justify-content-center text-center mb-5">

            <h5 class="text-white"><hr class="bg-primary">CHOOSE FROM ANY S.A COUNTRY CODE</h5>
                 
          </div>
     </div>
      </div>



<?php
include('includes/footer.php');
?>