<?php
include('includes/header.php');
?>


  
    <div class="unit-5 overlay" style="background-image: url('<?php echo $base_url;?>images/hero_bg_1.jpg');">
      <div class="container text-center">
        <h2 class="mb-0">SMS Keywords</h2>
        <p class="mb-0 unit-6"><a href="<?php echo $base_url;?>">Keywords</a> <span class="sep">></span> <span>SMS Keywords</span></p>
      </div>
    </div>


      
    <div class="site-section">
      <div class="container">
      <div class="row">     
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <img src="<?php echo $base_url;?>/images-old/Test.gif" class="img img-responsive" style="display: inline-block;" width="160" height="148" />
            <img src="<?php echo $base_url;?>/images-old/key.png" class="img img-responsive" style="display: inline-block;" width="160" height="148" />
        </div>
        
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    
            <p>We provide an online SMS solution aimed at consumers, SME’s and corporates. In fact it has been designed to meet the needs of anyone who has a potential use for sms. The only limit is your imagination.</p>
        </div>
    
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <br>
        <li>We offer <strong>2</strong> types of Short Codes: <strong>Dedicated</strong> and <strong>Shared</strong> (On a Shared Short Code, you will need a Keyword)</li>
        <li><strong>SHARED SHORT CODES</strong></li>
        <ul>
            <li>1. On a SHARED Short Code, you need a keyword to identify your campaign</li>
            <li>2. When a person sms to a Short Code (e.g. 45509) it’s called a premium sms (Examples)</li>
            <li>3. Cell phone users send sms messages to your unique keyword</li>
            <li>4. The sender pays a certain price e.g. R1.95 per sms</li>
            <li>5. Mostly used to run SMS-Competitions, database creation or SMS-voting, etc.</li>
            <li>6. Sender receives an automated reply SMS</li>
            <li>7. Revenue share is accumulated every time a message is received</li>
            <li>8. A database of all these incoming SMS messages can be found in your Short Code Portfolio on our website</li>
            <li>9. Our server also sends you a notification email upon every incoming SMS</li>
            <li>10. This email notification contains the sender’s cell phone number, the message and time & date it was sent</li>
            <li>11. Keywords are not case sensitive</li>
            <li>12. It’s a good idea to keep your keywords as short as possible.</li>
        </ul>
        
         
        <div class="row bg-primary text-white">
            <ul>
            <li>If you would like to know if your preferred <strong>keyword</strong> is still available on any of our Short Codes, please use our <strong>keyword checker</strong></li>
            <li><strong>It is possible that a keyword could appear not to be available or even available, using the keyword checker</strong></li>
            <li>To make <strong>absolutely</strong> sure, use the <strong>keyword-checker</strong> again; (<i><strong>This</strong> time however, do the following:</i>)</li>
            <li>Type your preferred <strong>keyword +</strong> your <strong>email</strong> address, click <strong>check</strong>, and voilà! You will be notified within minutes!</li>
            <li>Should the keyword be available, you may test it <strong>instantly</strong> for <strong>FREE</strong> on any of our available <strong>Short Codes</strong></li>
            <li>Your <strong>keyword</strong> is activated on registration </li>
          </ul>
        </div>            
     </div>
      
     <div class="col-lg-6 mb-5 mb-lg-0"><p>The <strong>Bulk SMS account</strong> will remain <strong>free</strong> and <strong>active</strong> – whether you keep the keyword or not</p>
          
          
          <a href="<?php echo $base_url;?>app/index.php?join" class="btn btn-danger btn-lg"><strong>Register Now</strong></a>
          </div> 
      
      <div class="col-lg-6 ml-md-auto">
      
      <p>Now, simply invite customers to SMS your <strong>keyword</strong> to the <strong>Short Code</strong> of your choice</p>
        <p>The keyword will be made available for a period of <strong>7(seven)</strong> days, pending receipt of proof of payment from you. At the end of this period, the keyword will be removed from your account and will again be available to anyone wishing to purchase them, and with NO obligation to you</p>
        
        
        
        
      
      
      </div> 
      <div class="col-lg-6 mb-5 mb-lg-0"><h4><strong>DEDICATED SHORT CODES</strong></h4>
        
        <ul>
            <li>On a dedicated short code, you have unlimited usage of keywords</li>
            <li>Unlike a shared short code, a DEDICATED short code is used by your business exclusively</li>
            <li>Normally used by businesses with multiple branches or outlets</li>
        </ul></div>     
      <div class="col-lg-6 ml-md-auto"><h4><strong>CHECK SHORT CODE AVAILABILITY INSTANTLY!</strong></h4>
        <p>If you would like to know if your preferred 5-digit Short Code is still available, please click <a href="<?php echo $base_url;?>/app/index.php?join"><strong>HERE</strong></a><br>
        (Some of our Short Codes may indicate any company other than mzansi student affairs and we hereby declare that all our Short Codes are ligitimate, whether our company name is displayed or any other company that is in alliance with mzansi student affairs.</p>          
          
          </div>
      
      
      
      
      
      
          </div>
     </div>
      </div>


<?php
include('includes/footer.php');
?>