<?php
include('includes/header.php');
?>


    <div class="unit-5 overlay" style="background-image: url('<?php echo $base_url;?>images/hero_bg_1.jpg');">
      <div class="container text-center">
        <h2 class="mb-0">API</h2>
        <p class="mb-0 unit-6"><a href="<?php echo $base_url;?>">Products</a> <span class="sep">></span> <span>API</span></p>
      </div>
    </div>
 <div class="site-section">
<div class="container">
   <div class="row justify-content-center">
     <div class="col col-lg-2">
      <ul class="list-group list-group-flush">
  <li class="list-group-item active">Our Software Development Solutions</li>
  <li class="list-group-item">Application Development</li>
  <li class="list-group-item">Application Maintenance</li>
  <li class="list-group-item">Backup and Disaster Recovery</li>
  <li class="list-group-item">Implementation and Deployment</li>
  <li class="list-group-item">Infrastructure Support</li>
  <li class="list-group-item">Migrations and Upgrades</li>
</ul>
    </div>
        <div class="col-md-auto justify-content-center">
      
      <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
  <div class="card-header"></div>
  <div class="card-body">
    <h5 class="card-title"></h5>
    <p class="card-text"> <ul class="list-style-circle">
<li>Product Lifecycle Management</li>
<li>Quality Assurance</li>
<li>SLA Support</li>
<li>Software Security</li>
<li>Software Integration</li>
      
  </ul></p>
  </div>

    </div>
      <div class="col text-center">
      
      
      
      
    </div> 
    
  </div>
</div>
</div>
</div>




   <div class="site-section">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade" >
            <h2>Extending your app's reach, one API at a time</h2>
             <p class="mb-5">It's with this understanding of enterprise technology that we approach our custom API services.</p>
              <p> 
          </div>
        </div>
        <div class="row hosting">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="100">

            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-equalization3"></span>
                </div>
                <h2 class="h5">Custom API</h2>
              </div>
              <div class="unit-3-body">
                <p>Our custom Application Programming Interface (API) solutions cover the development, integration, publishing, documentation, deployment, and continuous maintenance of APIs. We develop robust API architectures and security protocols, like custom key encryptions, access control dashboards, single sign-on and scalable caching proxies. We also build API call management platforms for logging, authenticating and throttling API calls.</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="200">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-cube29"></span>
                </div>
                <h2 class="h5">API Configurations</h2>
              </div>
              <div class="unit-3-body">
                <p>We configure APIs for mobile, desktop, console and browser apps, as well as databases, search engines and intranet systems. We implement internal and external APIs while leveraging exposed third-party web services. Our industry-specific approach to API management preemptively solves issues related to the sharing of data, business logic, content, microservices and communications.</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-airplane86"></span>
                </div>
                <h2 class="h5">API Development</h2>
              </div>
              <div class="unit-3-body">
                <p>MojaSMS develops specialized APIs for new and legacy enterprise applications. Our APIs facilitate access to application data, functionality and business logic, as well as web services. We create APIs for desktop, mobile, and cloud apps, web sockets, Service Oriented Architectures (SOA), firmware/middleware, browsers, databases and operating systems. We also design Hardware Platform Interfaces (HPI) for managing computer systems.</p>
              </div>
            </div>

          </div>

          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="400">

            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-hot67"></span>
                </div>
                <h2 class="h5">API Intergration</h2>
              </div>
              <div class="unit-3-body">
                <p>We emphasize networking best practices when integrating and implementing original, open-source and third-party APIs. We interface disparate business systems and processes, add web service functionality to existing applications and synchronize data across applications.</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="500">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-headphones32"></span>
                </div>
                <h2 class="h5">API As A Service</h2>
              </div>
              <div class="unit-3-body">
                <p>We develop web APIs and APIs as a Service (APIaaS) using common web services and protocols, including HTTP/HTTPS, XML, XHTML, JSON, Java, REST, SOAP, EDI, AJAX, and TCP/IP. We use Docker containers and HTTP/SOAP interfaces for executing API-based microservices with custom business logic.</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="600">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-user143"></span>
                </div>
                <h2 class="h5">API Testing Automation</h2>
              </div>
              <div class="unit-3-body">
                <p>We design and configure API integration testing platforms to automate validation, functional, UI, load, runtime, security, penetration and fuzz testing. We use API testing platforms like Postman, Rest-Assured, and HttpMaster to perform exploratory API tests, manage testing automation protocols and visualize tests.</p>
              </div>
            </div>

          </div>

        </div>
      
      </div>
    </div>




<div class="jumbotron text-center bg-white text-dark">
<!--  <h1 class="display-4">Contact Us</h1> -->
  <p class="lead">Drop us a line or give us ring about payment gateway and payment processing software development.</p>
  <hr class="my-4">
  <p>We love to hear from you and are happy to answer any questions</p>
  <a class="btn btn-success btn-lg" href="#" role="button">Subscribe</a>
</div>



<?php
include('includes/footer.php');
?>