<?php
define('ENVIRONMENT', 'production');
ob_start();
session_start();
$URL = 'dashboard';
if(isset($_GET['url']))
{ 
	$URL = $_GET['url'];
}

echo '<span style="display: none;">'.$_SESSION['MOBIKETA'].'</span>';
if(isset($_GET['logout'])) {
	setcookie("MOBIKETA", '', $timeout, '/'); // clear password;
	unset($_COOKIE['MOBIKETA']);
	unset($_SESSION['MOBIKETA']);
	
  header("location: index.php");
  exit;
}

if (defined('ENVIRONMENT'))
{
	switch (ENVIRONMENT)
	{
		case 'development':
			error_reporting(E_ALL);
		break;
	
		case 'testing':
		case 'production':
			error_reporting(0);
		break;

		default:
			exit('The application environment is not set correctly.');
	}
}
//$timezone = "Asia/Bangkok";
//if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone); 
$system_path = 'system';
$application_folder = 'hooks';

	// Set the current directory correctly for CLI requests
	if (defined('STDIN'))
	{
		chdir(dirname(__FILE__));
	}

	if (realpath($system_path) !== FALSE)
	{
		$system_path = realpath($system_path).'/';
	}

	// ensure there's a trailing slash
	$system_path = rtrim($system_path, '/').'/';

	// Is the system path correct?
	if ( ! is_dir($system_path))
	{
		exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
	}
	// The name of THIS file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
	// The PHP file extension
	// this global constant is deprecated.
	define('EXT', '.php');
	// Path to the system folder
	define('BASEPATH', str_replace("\\", "/", $system_path));
	// Path to the front controller (this file)
	define('FCPATH', str_replace(SELF, '', __FILE__));
	// Name of the "system folder"
	define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));

	// The path to the "application" folder
	if (is_dir($application_folder))
	{
		define('APPPATH', $application_folder.'/');
	}
	else
	{
		if ( ! is_dir(BASEPATH.$application_folder.'/'))
		{
			exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
		}

		define('APPPATH', BASEPATH.$application_folder.'/');
	}
define('COR', BASEPATH.'core/');	
define('LIB', BASEPATH.'libraries/');	
define('HELP', BASEPATH.'helpers/');	
require_once 'root.php';
require_once BASEPATH.'core/Config.php';
foreach (glob(COR."/*.php") as $filename) { include $filename; }
foreach (glob(HELP."/*.php") as $filename) { include $filename; }
foreach (glob(LIB."/*.php") as $filename) { include $filename; }
$Config = new CI_Config();
setTimeZone();

$trans_ref='';

function converRate($id) {
	global $server;
	$result = mysqli_query($server,"SELECT * FROM currencies WHERE id = '$id'"); 
	$row = mysqli_fetch_assoc($result);
	return $currency_rate = $row['rate'];	
}
if(isset($_POST["token"])) {
	//to do 
} else {	
//do for other gateways
global $server;
	if(!empty($_REQUEST['refNo'])&&!empty($_REQUEST['transactionId']))$trans_ref=$_REQUEST['transactionId'];
	elseif(!empty($_POST['gtpay_tranx_id']))$trans_ref=$_POST['gtpay_tranx_id'];
	elseif(!empty($_REQUEST['txnref']))$trans_ref=$_REQUEST['txnref']; //&&!empty($_REQUEST['payRef'])
	elseif(isset($_POST['cart_order_id']))$trans_ref=$_POST['cart_order_id'];
	elseif(isset($_GET['trans_ref']))$trans_ref=$_GET['trans_ref'];
	elseif(isset($_REQUEST['ORDER_NUM']))$trans_ref=$_REQUEST['ORDER_NUM'];
	elseif(!empty($_GET['OrderID']))$trans_ref=$_GET['OrderID'];
	//$_POST['transaction_id'] //voguepay::internal
	$trans_ref=addslashes($trans_ref);
	
	if(!empty($trans_ref))
	{	
	global $server; 
		$result = mysqli_query($server,"SELECT * FROM transactions WHERE reference = '$trans_ref'"); 
		$transaction = mysqli_fetch_assoc($result) or die(mysqli_error($server));
		if(empty($transaction))$Error="<h3>Transaction record not found!</h3>";
		elseif($transaction['status']!='Completed')
		{
			$approved_amount='0.00';
			$expected_deposit=$transaction['amount'] ;
			$gtpay_amount=$expected_deposit*converRate(1)*100;
			$simplepay_deposit = $simplepay_amount=$expected_deposit*converRate(1);
			
			$paypal_amount=round( ($expected_deposit*converRate(2)),2);
			$new_status=0;
			$payment_method = gatewayData($transaction['gateway'],'alias');
			$transaction_id = $transaction['reference'];

			//treate GTPay
			if($payment_method=='gtpay')
			{
				if(isset($_POST['gtpay_tranx_id'])&&($_POST['gtpay_tranx_status_code']!='00'||!empty($gtpay_demo)))
				{
					$new_status=($_POST['gtpay_tranx_status_code']!='00')?-1:1;
					$json_data=array(
						'response_description'=>$_POST['gtpay_tranx_status_msg'],
						'response_code'=>$_POST['gtpay_tranx_status_code'],
						'approved_amount'=>(empty($_POST['gtpay_tranx_amt'])&&$new_status==1)?0:($_POST['gtpay_tranx_amt']/100)
									);
				}
				else
				{
					$hash=hash("sha512",$payment_params['gtpay_merchant_id'].$trans_ref.$payment_params['gtpay_hash_key']);
					
					$url="https://ibank.gtbank.com/GTPayService/gettransactionstatus.json?mertid={$payment_params['gtpay_merchant_id']}&amount=$gtpay_amount&tranxid=$trans_ref&hash=$hash";
					
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);			
					curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					//curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; MS Web Services Client Protocol 4.0.30319.239)" );
					curl_setopt($ch, CURLOPT_URL, $url);			
					$response = @curl_exec($ch);
					$response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					if($response_code != 200)$response=curl_error($ch);
					curl_close($ch);
			
					$json=null;			
					if($response_code == 200)$json=@json_decode($response,true);
					else $response="HTTP Error $response_code: $response. ";
					
					if(!empty($json))
					{		
						$new_status=($json['ResponseCode']=='00')?1:-1;
						$json_data=array(
										'response_description'=>$json['ResponseDescription'],
										'response_code'=>$json['ResponseCode'],
										'approved_amount'=>empty($json['Amount'])?0:$json['Amount']
									);
					}
					else $json_data=array(
										'info'=>$response,
										'response_code'=>$response_code,
									);
				}
			}
			elseif($payment_method=='interswitch')
			{
				$transaction_reference=$transaction['transaction_reference'];
				
					$interswitch_product_id=$payment_params['interswitch_product_id'];
					$interswitch_mac_key=$payment_params['interswitch_mac_key'];
					
				if($payment_params['interswitch_demo']==1)
				{
					$interswitch_url='https://stageserv.interswitchng.com/test_paydirect/api/v1/gettransaction.json';
				}
				else
				{
					$interswitch_url='https://webpay.interswitchng.com/paydirect/api/v1/gettransaction.json';
				}
				
				$hash=strtoupper(hash("sha512",$interswitch_product_id.$transaction_reference.$interswitch_mac_key));
				$url="$interswitch_url?productid=$interswitch_product_id&transactionreference=$transaction_reference&amount=$gtpay_amount";
				
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_HTTPHEADER,array("Hash: $hash"));
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);			
				curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL, $url);
				
				$response = @curl_exec($ch);
				$returnCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				if($returnCode != 200)$response=curl_error($ch);
				curl_close($ch);	
				$json=null;
				
				if($returnCode == 200)$json=@json_decode($response,true);
				else $response="HTTP Error $returnCode: $response. ";

				if(!empty($json))
				{				
					$json_data=array(
										'response_description'=>$json['ResponseDescription'],
										'response_code'=>$json['ResponseCode'],
										'approved_amount'=>empty($json['Amount'])?0:$json['Amount']
									);
			
					$new_status=($json['ResponseCode']=='00')?1:-1;	
				}
				else $json_data= $json_data=array(
										'info'=>$response,
										'response_code'=>$returnCode,
									);		
			}
			elseif($payment_method=='ucollect')
			{
				$ubaurl="https://ucollect.ubagroup.com/cipg-payportal/confirmation/verify?cipgtxnref=$trans_ref&mytxnref={$transaction['time']}&cipgid={$payment_params['ucollect_mert_id']}";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $ubaurl);
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$response = @curl_exec($ch);
				$response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				if($response_code != 200)$response=curl_error($ch);
				curl_close($ch);
				
				$json_data['response_code']=$response_code;
				
				if ($response_code != 200)$json_data['info']=$response;
				else
				{
					$json_data['response_description']=$response;
					
					if($response=="Approved Transaction")
					{	
						$json_data['approved_amount']=$expected_deposit;
						$new_status=1;
					}
					else 	$new_status=-1;
				}
			}
			elseif($payment_method=='2checkout')
			{
				$ipnData=array();
				foreach ($_POST as $field=>$value)$ipnData["$field"] = $value;
				
				$vendorNumber   = ($ipnData["vendor_number"] != '') ? $ipnData["vendor_number"] : $ipnData["sid"];
				$orderNumber    = $ipnData["order_number"];
				$orderTotal     = $ipnData["total"];
				// If demo mode, the order number must be forced to 1
				$demo="";
				
				if($ipnData['demo'] == 'Y')
				{
					$orderNumber = "1";
					$demo = "Y";
				}
				// Calculate md5 hash as 2co formula: md5(secret_word + vendor_number + order_number + total)
				$key = strtoupper(md5($payment_params['2checkout_secret'] . $vendorNumber . $orderNumber . $orderTotal));
				
				$new_status=-1;
				$json_data=$ipnData;
				$json_data['approved_amount']=$orderTotal;
				
				if(floatval($orderTotal)<$paypal_amount)$json_data['response_description'] = "Incorrect deposit amount ($paypal_amount USD was expected, but $PAYMENT_AMOUNT USD found).";
				elseif($vendorNumber!=$payment_params['2checkout_seller_id'])$json_data['response_description']="The payment was made into a different 2checkout sid $vendorNumber.";
				elseif($demo == "Y"&&empty($payment_params['2checkout_demo']))$json_data['response_description'] = "This is a demo payment.";
				elseif($ipnData["key"] == $key || $ipnData["x_MD5_Hash"] == $key)
				{
					$new_status=1;
					$json_data['response_description'] = "Transaction Successfully Completed.";
				}
				else $json_data['response_description'] = "Verification failed: MD5 does not match!";
			}
			elseif($payment_method=='perfectmoney')
			{					
				$new_status=-1;
				$PAYMENT_ID = input_post('PAYMENT_ID');
				$PAYEE_ACCOUNT  = input_post('PAYEE_ACCOUNT');
				$PAYMENT_AMOUNT=input_post('PAYMENT_AMOUNT');
				$PAYMENT_UNITS=input_post('PAYMENT_UNITS');
				$PAYMENT_BATCH_NUM=input_post('PAYMENT_BATCH_NUM');
				$PAYER_ACCOUNT=input_post('PAYER_ACCOUNT');
				$TIMESTAMPGMT=input_post('TIMESTAMPGMT');
				$V2_HASH=input_post('V2_HASH');
				$pm_pass=strtoupper(md5($payment_params['perfectmoney_paraphrase']));
				
				$string=
					  $PAYMENT_ID.':'.$PAYEE_ACCOUNT.':'.$PAYMENT_AMOUNT.':'.$PAYMENT_UNITS.':'.
					  $PAYMENT_BATCH_NUM.':'.$PAYER_ACCOUNT.':'.$pm_pass.':'.$TIMESTAMPGMT;
					  
				$hash=strtoupper(md5($string));
				$json_data=@$_POST;
				$json_data['approved_amount']=$PAYMENT_AMOUNT;
				
				if($hash!=$V2_HASH)$json_data['response_description']="Perfectmoney payment validation failed.";
				elseif($pm_acc!=$PAYEE_ACCOUNT)$json_data['response_description']="The payment was made into another perfectmoney account $PAYEE_ACCOUNT not ours {$payment_params['perfectmoney_account']}";
				elseif($PAYMENT_UNITS!="USD")$json_data['response_description']="Contradicting payment unit/currency. (USD was expected but '$PAYMENT_UNITS' was found).";
				elseif(floatval($PAYMENT_AMOUNT)<$paypal_amount)$json_data['response_description']="Incorrect deposit amount ($expected_deposit USD was expected, but $PAYMENT_AMOUNT USD found). ";
				else 
				{
					$new_status=1;
					$json_data['response_description']="Transaction successfully completed.";
				}
			}
			elseif($payment_method=='paypal')
			{
				$new_status = -1;
				$req = 'cmd=_notify-validate';
				foreach ($_POST as $key => $value) {
					$value = urlencode(stripslashes($value));
					$req .= "&$key=$value";
				}
				
				$url = "https://www.paypal.com/cgi-bin/webscr";
				$curl_result = $curl_err = '';
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded", "Content-Length: " . strlen($req)));
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_VERBOSE, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				$curl_result = @curl_exec($ch);
				$curl_err = curl_error($ch);
				curl_close($ch);
				
				$req = str_replace("&", "\n", $req);  
				if (strpos($curl_result, "VERIFIED") !== false) {
					$new_status=1;
					$json_data['response_description'] = 'Transaction Successfully Completed';
				} else {
					$new_status=-1;
					$json_data['response_description'] = 'We are unable to verify your payment with PayPal';
				}									
			}
			elseif($payment_method=="skye"||$payment_method=="firstpay"||$payment_method=="stanbic")
			{
				$stanbicibtc_merchant_id = $payment_params['stanbic_merchant_ngn'];
				
				if($payment_method=="skye")$url="https://skyecipg.skyebankng.com:5443/MerchantServices/UpayTransactionStatus.ashx?MERCHANT_ID={$payment_params['skye_merchant_id']}&ORDER_ID=$trans_ref";
				elseif($payment_method=="stanbic")$url="https://cipg.stanbicibtcbank.com/MerchantServices/UpayTransactionStatus.ashx?MERCHANT_ID=$stanbicibtc_merchant_id&ORDER_ID=$trans_ref";
				elseif($payment_method=="firstpay")$url="https://firstpaylink.firstbanknigeria.com:553/MerchantServices/UpayTransactionStatus.ashx?MERCHANT_ID={$payment_params['firstpay_merchant_id']}&ORDER_ID=$trans_ref";
				
				$xml_string=file_get_contents($url); //,false,$context
				$xml=@simplexml_load_string($xml_string,"SimpleXMLElement",LIBXML_NOCDATA);
				
				if($xml!==false)$xml=json_decode(str_replace('{}', '""', json_encode($xml)));
				
				if($xml===false)$json_data['response_description']="Error accessing the gateway confirmation page.";
				elseif(!isset($xml->StatusCode))$json_data['response_description']="Error interpreting response from confirmation page.";
				else
				{					
					$new_status=-1;
					$json_data['Transaction Reference']=$xml->TransactionRef;
					$json_data['Transaction Status']=$xml->Status;
					$json_data['Payment Reference']=$xml->PaymentRef;
					$json_data['Order ID']=$xml->OrderID;
					//$json_data['Response Description']=$xml->ResponseDescription;
					$json_data['response_description']=@$xml->ResponseDescription;
					$json_data['approved_amount']=@$xml->Amount;
					$json_data['response_code']=$xml->StatusCode;
					if($xml->StatusCode=='00')
					{
						$new_status=1;
					}
				}
			}
			elseif($payment_method=='zenith_globalpay')
			{				

				error_reporting (E_ALL ^ E_DEPRECATED);
				ini_set ('soap.wsdl_cache_enabled', 0);
				include($_payment_base_path.'lib/nusoap.php');
				$client = new nusoap_client('https://demo.globalpay.com.ng/GlobalpayWebService_demo/service.asmx?wsdl', true);
				
				$soapaction = "http://www.eazypaynigeria.com/globalpay_demo/getTransactions";
				$namespace = "http://www.eazypaynigeria.com/globalpay_demo/";
				$client->soap_defencoding = 'UTF-8';
				
				$txnref = $trans_ref; // $_GET["txnref"];
				
				$merch_txnref=$txnref; 
				$channel="" ; 
				//change the merchantid to the one sent to you
				$merchantID=$payment_params['globalpay_merchant_id']; 
				$start_date=""; 
				$end_date=""; 
				//change the uid and pwd to the one sent to you
				$uid=$payment_params['globalpay_user_id']; 
				$pwd=$payment_params['globalpay_password']; 
				$payment_status="" ; 
				
				$err = $client->getError();
				
				if ($err) $json_data['response_description']='<h2>Constructor error</h2><pre>' . $err . '</pre>';
				else
				{
					// Doc/lit parameters get wrapped
					$MethodToCall= "getTransactions";
					//$MethodToCall= "Checkcenter";
					
					$param = array(
						'merch_txnref' => $merch_txnref, 
						'channel' => $channel,
						'merchantID' => $merchantID,
						'start_date' => $start_date,
						'end_date' => $end_date,
						'uid' => $uid,
						'pwd' => $pwd,
						'payment_status' => $payment_status
					);

					$result = $client->call(
						'getTransactions', 
						array('parameters' => $param), 
						'http://www.eazypaynigeria.com/globalpay_demo/', 
						'http://www.eazypaynigeria.com/globalpay_demo/getTransactions', 
						false,  
						true
					);

					// Check for a fault
					if ($client->fault) $json_data['response_description']='SOAP Fault: '.print_r($result,true);
					else 
					{
						// Check for errors
						$err = $client->getError();
						if ($err)$json_data['response_description']='Error: ' . $err . '</pre>';
						else 
						{
							//This gives getTransactionsResult
							$WebResult=$MethodToCall."Result";
							// Pass the result into XML
							$xml = simplexml_load_string($result[$WebResult]);
							
							//echo $xml;
							
							$amount = $xml->record->amount;
							$txn_date = $xml->record->payment_date;
							$pmt_method = $xml->record->channel;
							$pmt_status = $xml->record->payment_status;
							$pmt_txnref = $xml->record->txnref;
							$currency = $xml->record->field_values->field_values->field[2]->currency;
						   // $pnr = 'PNR';
							$trans_status = $xml->record->payment_status_description;
							}
							
						 $ujson_details=get_json($transaction['json_details']);
						  //Get information from your database
						 $merch_amt = $expected_deposit;
						 $merch_name = "{$ujson_details['customer_info']['customer_firstname']} {$ujson_details['customer_info']['customer_lastname']}";
						 $merch_phoneno  = $ujson_details['customer_info']['customer_phone'];
						 
						 
						$json_data['approved_amount']=$amount;
						$json_data['response_code']=$pmt_status;

						 //Format and display the necessary parameters including the one above
						  $raw_info = "Name : ". $merch_name ;
						  $raw_info .= "<br/>Phone number : ".$merch_phoneno;
						  
						if ($pmt_status == 'successful')
						{
							if ($amount >= $merch_amt)
							{
								$raw_info .= "<br/>Amount : ". $amount ;
								$raw_info .= "<br/>Transaction Date : ".$txn_date;
								$raw_info .= "<br/>Payment Method : ".$pmt_method;
								$raw_info .= "<br/>Payment Status : ".$pmt_status;
								$raw_info .= "<br/>Transaction Reference Number : ".$pmt_txnref;
								$raw_info .= "<br/>Currency : ".$currency;
								$raw_info .= "<br/>Transaction Status : ".$trans_status;
								$new_status=1;
							}
							else
							{
								$raw_info .= "<br/>Transaction Amount : ".$merch_amt;
								$raw_info .= "<br/>Debited Amount : ". $amount ;
								$raw_info .= "<br/>Transaction Date : ".$txn_date;
								$raw_info .= "<br/>Payment Method : ".$pmt_method;
								$raw_info .= "<br/>Payment Status : ".$pmt_status. " ( Amount does not match and no service will be rendered)";
								$raw_info .= "<br/>Transaction Reference Number : ".$pmt_txnref;
								$raw_info .= "<br/>Currency : ".$currency;
								$raw_info .= "<br/>Transaction Status : ".$trans_status;
								$new_status=-1;
							}
							
						}       
						 else
						{
						   if ($pmt_status == 'pending')
						   {
								//Please display a message telling the user to check back at a later time. 
								//You should have a background agent that will query globalpay web service intermittently to update the transactions on your site.
								$raw_info .= "<br/>Amount : ". $amount ;
								$raw_info .= "<br/>Transaction Date : ".$txn_date;
								$raw_info .= "<br/>Payment Method : ".$pmt_method;
								$raw_info .= "<br/>Payment Status : ".$pmt_status;
								$raw_info .= "<br/>Transaction Reference Number : ".$pmt_txnref;
								$raw_info .= "<br/>Currency : ".$currency;
								$raw_info .= "<br/>Transaction Status : ".$trans_status;
						   }
						   else
						   {
								$raw_info .= "<br/>Amount : ". $amount ;
								$raw_info .= "<br/>Transaction Date : ".$txn_date;
								$raw_info .= "<br/>Payment Method : ".$pmt_method;
								$raw_info .= "<br/>Payment Status : ".$pmt_status;
								$raw_info .= "<br/>Transaction Reference Number : ".$pmt_txnref;
								$raw_info .= "<br/>Currency : ".$currency;
								$raw_info .= "<br/>Transaction Status : ".$trans_status;
								$new_status=-1;
						   }
						}
						
						$json_data['response_description']=$trans_status;
						$json_data['raw_info']=$raw_info;
					}
				}
			}
			elseif($payment_method=='simplepay')
			{
				$comments= $_POST["comments"];

				if(empty($payment_params['simplepay_demo']))$simplepay_url="https://simplepay4u.com/processverify.php";
				else $simplepay_url= "http://sandbox.simplepay4u.com/processverify.php";
				
				$curldata["cmd"]="_notify-validate";
				foreach ($_REQUEST as $key =>  $value)
				{
					if ($key!='view'&&$key!='layout')$curldata[$key]=urlencode ($value);
				}
				$handle=curl_init();
				curl_setopt($handle, CURLOPT_URL, $simplepay_url);
				curl_setopt($handle, CURLOPT_POST, 1);
				curl_setopt($handle, CURLOPT_POSTFIELDS, $curldata);
				curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($handle, CURLOPT_TIMEOUT, 90);
				$result=curl_exec($handle);
				
				$returnCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
				$total=floatval($_POST["total"]);
				$json_data=$_POST;
				$json_data['approved_amount']=$total;
				
				$new_status=-1;
				
				if ($returnCode != 200)
				{
					$json_data['info'] = "HTTP ERROR while trying to verify your payment at simplepay4u: ".curl_error($ch);
					$new_status=0;
				}
				elseif($total<$simplepay_deposit)$json_data['response_description']="The amount deposited ($total) is less thant the expect amount of ($simplepay_deposit).";
				elseif( 'VERIFIED' != $result )$json_data['response_description']="Payment verification from simplepay4u failed.";
				else 
				{
					$new_status=1;
					$json_data['response_description']="Transaction successfully completed.";
				}
				
				curl_close($handle);
			}
			elseif($payment_method=='voguepay'&&!empty($_POST['transaction_id']))
			{
				$vjson = file_get_contents('https://voguepay.com/?v_transaction_id='.$_POST['transaction_id'].'&type=json');
				//create new array to store our transaction detail
				$vtransaction = json_decode($vjson, true);

				$new_status=0;
				$json_data=$vtransaction;
				$json_data['approved_amount']=$vtransaction['total'];
				if(empty($vtransaction))$json_data['info']="Invalid response from voguepay api";
				else 
				{
					$new_status=-1;
					if($vtransaction['status'] != 'Approved')$json_data['response_description']='Failed transaction';
					elseif($vtransaction['merchant_id'] != $payment_params['voguepay_merchant_id'])$json_data['response_description']='Invalid merchant: '.$vtransaction['merchant_id'];
					elseif($vtransaction['total'] <$simplepay_deposit)$json_data['response_description']="Invalid total {$vtransaction['total']}, the expected amount is $simplepay_deposit";
					else
					{
						$new_status=1;
						$json_data['response_description']="Transaction successfully completed.";
					}
				}
			}
			
			if(!empty($json_data))
			{
				$error_details=empty($json_data['response_description'])?addslashes($json_data['info']):addslashes($json_data['response_description']);
				
				$json_info_str=json_encode($json_data);
				$json_info_str=addslashes($json_info_str);				

				if($new_status!=1)
				{
					$time=time();
					//fail transaction
					$_SESSION['comment'] = @$json_data['response_description'];
					mysqli_query($server,"UPDATE transactions SET status = 'Completed' WHERE reference = '$transaction_id6' LIMIT 1 ");
					header('location: index.php?urlorder&failed=1');
				}
				else
				{
					//process payment
					processTransaction($transaction_id);
					header('location: index.php?url=order&success=1');
				}

				$transaction['json_info']=$json_info_str;
				$transaction['status']=$new_status;
			}
		}
	} else {
		header('location: index.php');			
	}
}
?>
