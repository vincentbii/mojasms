<?php
include('../../con.php');

global $con;
$con=$conn;


$key=filters($_REQUEST['key']);
$phone=filters($_REQUEST['phone']);
$message=filters($_REQUEST['text']);
$senderid=filters($_REQUEST['senderid']);

if(isset($_REQUEST['key']) && checkAPIKey($key)){
    
    headers("200 OK");
    
    if(checkAPIKeySMSBalance($key) > 0){
        
        //send sms now
        checkMissing();
        $client=checkAPIUser($key);
        
        $response=array(
            'code'=>200,
            'message'=>sendSMS($senderid,$phone,$message,$client)
            );
        echo json_encode($response);
        
    }else{
        
        headers("200 OK");
        
        $response=array(
            'code'=>200,
            'message'=>'SMS BALANCE IS ZERO'
            );
        echo json_encode($response);
    }
    
}else{
    
    headers("401 Unauthorized");
    
    $response=array(
        'code'=>401,
        'message'=>'API KEY IS INVALID'
        );
        
    echo json_encode($response);
}


//----------------functions----------------------------//
function checkAPIKey($key) {
    
    global $con;
    
    $sqler="select * from users where api_key = '$key'";
    $result = $con->query($sqler);
    
    if($result->num_rows == 0){
        return false;
    }else{
        return true;
    }   

}

function checkAPIKeySMSBalance($key) {
    
    global $con;
    
    $sqler="select * from users where api_key = '$key' LIMIT 1";
    $result = $con->query($sqler);
    
    if($result->num_rows == 0){
        return false;
    }else{
        
        while($row = $result->fetch_assoc()){
                
                $ty=$row;
        }
        
        return $ty['balance'];
    }   

}

function checkAPIUser($key) {
    
    global $con;
    
    $sqler="select * from users where api_key = '$key' LIMIT 1";
    $result = $con->query($sqler);
    
    if($result->num_rows == 0){
        return false;
    }else{
        
        while($row = $result->fetch_assoc()){
                
                $ty=$row;
        }
        
        return $ty['id'];
    }   

}

function headers($code){
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("HTTP/1.1 $code");
}

function filters($input){
    return filter_var(trim($input), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
}

function checkMissing(){
    
    $phone=filters($_REQUEST['phone']);
    $message=filters($_REQUEST['text']);
    $senderid=filters($_REQUEST['senderid']);
    $true=true;
    if(empty($phone)){
        $true= false;
    }else if(empty($message)){
        $true = false;
    }else if(empty($senderid)){
        $true = false;
    }
    
    if($true==false){
        $response=array(
            'code'=>200,
            'message'=>'Please supply all Phone,Text and SenderID'
            );
        echo json_encode($response); die();
    }
    
}

function sendSMS($senderID,$recipientList,$textMessage,$client=0) {
	global $con; 
	//replace dynamic firlds 
	$textMessage = str_replace("[", '%', $textMessage);
	$textMessage = str_replace("]", '%', $textMessage);
	$textMessage = str_replace("%NUMBER%", $recipientList, $textMessage);
	$recipientList = str_replace('+', '', $recipientList);
	
	$active=activeApi();
	
	     
        if($active=='global'){
            //global
            //$textMessage=urlencode($textMessage);
            $textMessage=urlencode($textMessage.' '.sms_signature());
            $global="http://148.251.196.36/app/smsapi/index.php?key=1XfUGDAM&type=text&contacts=$recipientList&senderid=$senderID&msg=$textMessage";
            
            $redffd=url_get_contents($global);
            $samunjo=!empty(explode(',',$redffd)[1]) ? explode(',',$redffd)[1] : '';
            
            $saved=1;
            $return = $redffd;
            
        }else/*if($active=='bulk')*/{
            //bulk
            
            $messages = array(
              array('to'=>$recipientList, 'body'=>$textMessage)
            );
    
            $result = send_message_bulk_sms( json_encode($messages) );
    
            if ($result['http_status'] > 203) {
                $return = 'Message sending failed';
            } else {
              $return = 'Message Sent Successfully';
            }
    
        } 
        /*}else{
            //zoom
        }*/
      
	 
	if($saved != 1) { 	
		$date = date('Y-m-d H:i:s');
		$error = str_replace("'",'',$error);
		$samunjo=!empty($samunjo) ? $samunjo : '';
		$add = $con->query("INSERT INTO sentmessages (`id`, `campaign_id`, `job_id`, `recipient`, `status`, `gateway_id`, `error`, `date`,`message`, `customer_id`,`m_sent_id`) 
		VALUES (NULL, '0', '0', '$recipientList', '$status', '$activeGateway', '$error', '$date', '$textMessage', '$client','$samunjo');") or die('connection error');	
	}
	return $return; 
}

function activeApi() {
    
	global $con;
	
	$result=$con->query("select * from apis WHERE status='1'");
	
	while($row = $result->fetch_assoc()){
                
            $ty=$row;
    }
	
	$api = $ty['api_name'];	
	
	return $api;
}

function url_get_contents ($Url) {
    if (!function_exists('curl_init')){ 
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function send_message_bulk_sms( $post_body ) {
    
  $url='https://api.bulksms.com/v1/messages';
  $username = 'Mojasms';
  $password = 'Sithole@14';
    
  $ch = curl_init( );
  $headers = array(
  'Content-Type:application/json',
  'Authorization:Basic '. base64_encode("$username:$password")
  );
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt ( $ch, CURLOPT_URL, $url );
  curl_setopt ( $ch, CURLOPT_POST, 1 );
  curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
  curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_body );
  // Allow cUrl functions 20 seconds to execute
  curl_setopt ( $ch, CURLOPT_TIMEOUT, 20 );
  // Wait 10 seconds while trying to connect
  curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
  $output = array();
  $output['server_response'] = curl_exec( $ch );
  $curl_info = curl_getinfo( $ch );
  $output['http_status'] = $curl_info[ 'http_code' ];
  $output['error'] = curl_error($ch);
  curl_close( $ch );
  return $output;
  
}

function sms_signature(){
    global $con;
    $result=$con->query("select * from sms_signature");
    $status=0;$signature="";
  	while ($row = $result->fetch_assoc()) {
  	    $status=$row['status'];
  	    $signature=$row['signature'];
  	}
      	
    if($status==0){
      return ' \n'.$signature;
    }else{
        return '';
    }
}