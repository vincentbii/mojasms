<?php 
error_reporting(0);
ob_start();

include_once('root.php');
if(isset($_POST['finish'])) {
	if(file_exists($_POST['path'].'/index.php')) {
		$pat = $_POST['path'];
	
		function deleteDir($dir) {
		   $iterator = new RecursiveDirectoryIterator($dir);
		   foreach (new RecursiveIteratorIterator($iterator,RecursiveIteratorIterator::CHILD_FIRST) as $file)
		   {
			  if ($file->isDir()) {
				 rmdir($file->getPathname());
			  } else {
				 unlink($file->getPathname());
			  }
		   }
		   rmdir($dir);
		}
	deleteDir($pat);	
	}
	
header('location: index.php');	
exit();
}
?>