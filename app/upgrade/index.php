<?php 
define('ENVIRONMENT', 'production');
define('PRODUCT', 'Mobiketa');
ob_start();
ini_set('max_execution_time', 120); 
error_reporting(0);

if (defined('ENVIRONMENT'))
{
	switch (ENVIRONMENT)
	{
		case 'development':
			error_reporting(E_ALL);
		break;
	
		case 'testing':
		case 'production':
			error_reporting(0);
		break;

		default:
			exit('The application environment is not set correctly.');
	}
}
$timezone = "Asia/Bangkok";
if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone); 
$system_path = '../system';
$application_folder = 'hooks';

	// Set the current directory correctly for CLI requests
	if (defined('STDIN'))
	{
		chdir(dirname(__FILE__));
	}

	if (realpath($system_path) !== FALSE)
	{
		$system_path = realpath($system_path).'/';
	}

	// ensure there's a trailing slash
	$system_path = rtrim($system_path, '/').'/';

	// Is the system path correct?
	if ( ! is_dir($system_path))
	{
		exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
	}
	// The name of THIS file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
	// The PHP file extension
	// this global constant is deprecated.
	define('EXT', '.php');
	// Path to the system folder
	define('BASEPATH', str_replace("\\", "/", $system_path));
	// Path to the front controller (this file)
	define('FCPATH', str_replace(SELF, '', __FILE__));
	// Name of the "system folder"
	define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));

	// The path to the "application" folder
	if (is_dir($application_folder))
	{
		define('APPPATH', $application_folder.'/');
	}
	else
	{
		if ( ! is_dir(BASEPATH.$application_folder.'/'))
		{
			exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
		}

		define('APPPATH', BASEPATH.$application_folder.'/');
	}

include_once('../root.php');

include_once('../system/core/Config.php');
$state=$configinstalled;
if($state=='INSTALLED') { header('location: ../index.php');	}

function getCryptedPassword($plaintext, $salt = '', $encryption = 'md5-hex', $show_encrypt = false)
	{
global $confighost;
global $configdatabase;
global $configuser;
global $configpassword;
global $configinstalled;
global $configapp_store;
global $configverssion_id;
global $configapp_version;
global $configapp_name;
global $configversion_date;		
		// Get the salt to use.
		$salt = getSalt($encryption, $salt, $plaintext);

		// Encrypt the password.
		switch ($encryption)
		{
			case 'plain':
				return $plaintext;

			case 'sha':
				$encrypted = base64_encode(mhash(MHASH_SHA1, $plaintext));
				return ($show_encrypt) ? '{SHA}' . $encrypted : $encrypted;

			case 'crypt':
			case 'crypt-des':
			case 'crypt-md5':
			case 'crypt-blowfish':
				return ($show_encrypt ? '{crypt}' : '') . crypt($plaintext, $salt);

			case 'md5-base64':
				$encrypted = base64_encode(mhash(MHASH_MD5, $plaintext));
				return ($show_encrypt) ? '{MD5}' . $encrypted : $encrypted;

			case 'ssha':
				$encrypted = base64_encode(mhash(MHASH_SHA1, $plaintext . $salt) . $salt);
				return ($show_encrypt) ? '{SSHA}' . $encrypted : $encrypted;

			case 'smd5':
				$encrypted = base64_encode(mhash(MHASH_MD5, $plaintext . $salt) . $salt);
				return ($show_encrypt) ? '{SMD5}' . $encrypted : $encrypted;

			case 'aprmd5':
				$length = strlen($plaintext);
				$context = $plaintext . '$apr1$' . $salt;
				$binary = _bin(md5($plaintext . $salt . $plaintext));

				for ($i = $length; $i > 0; $i -= 16)
				{
					$context .= substr($binary, 0, ($i > 16 ? 16 : $i));
				}
				for ($i = $length; $i > 0; $i >>= 1)
				{
					$context .= ($i & 1) ? chr(0) : $plaintext[0];
				}

				$binary = _bin(md5($context));

				for ($i = 0; $i < 1000; $i++)
				{
					$new = ($i & 1) ? $plaintext : substr($binary, 0, 16);
					if ($i % 3)
					{
						$new .= $salt;
					}
					if ($i % 7)
					{
						$new .= $plaintext;
					}
					$new .= ($i & 1) ? substr($binary, 0, 16) : $plaintext;
					$binary = _bin(md5($new));
				}

				$p = array();
				for ($i = 0; $i < 5; $i++)
				{
					$k = $i + 6;
					$j = $i + 12;
					if ($j == 16)
					{
						$j = 5;
					}
					$p[] = _toAPRMD5((ord($binary[$i]) << 16) | (ord($binary[$k]) << 8) | (ord($binary[$j])), 5);
				}

				return '$apr1$' . $salt . '$' . implode('', $p) . _toAPRMD5(ord($binary[11]), 3);

			case 'md5-hex':
			default:
				$encrypted = ($salt) ? md5($plaintext . $salt) : md5($plaintext);
				return ($show_encrypt) ? '{MD5}' . $encrypted : $encrypted;
		}
	}
	
function getSalt($encryption = 'md5-hex', $seed = '', $plaintext = '')
	{
		// Encrypt the password.
		switch ($encryption)
		{
			case 'crypt':
			case 'crypt-des':
				if ($seed)
				{
					return substr(preg_replace('|^{crypt}|i', '', $seed), 0, 2);
				}
				else
				{
					return substr(md5(mt_rand()), 0, 2);
				}
				break;

			case 'crypt-md5':
				if ($seed)
				{
					return substr(preg_replace('|^{crypt}|i', '', $seed), 0, 12);
				}
				else
				{
					return '$1$' . substr(md5(mt_rand()), 0, 8) . '$';
				}
				break;

			case 'crypt-blowfish':
				if ($seed)
				{
					return substr(preg_replace('|^{crypt}|i', '', $seed), 0, 16);
				}
				else
				{
					return '$2$' . substr(md5(mt_rand()), 0, 12) . '$';
				}
				break;

			case 'ssha':
				if ($seed)
				{
					return substr(preg_replace('|^{SSHA}|', '', $seed), -20);
				}
				else
				{
					return mhash_keygen_s2k(MHASH_SHA1, $plaintext, substr(pack('h*', md5(mt_rand())), 0, 8), 4);
				}
				break;

			case 'smd5':
				if ($seed)
				{
					return substr(preg_replace('|^{SMD5}|', '', $seed), -16);
				}
				else
				{
					return mhash_keygen_s2k(MHASH_MD5, $plaintext, substr(pack('h*', md5(mt_rand())), 0, 8), 4);
				}
				break;

			case 'aprmd5': /* 64 characters that are valid for APRMD5 passwords. */
				$APRMD5 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

				if ($seed)
				{
					return substr(preg_replace('/^\$apr1\$(.{8}).*/', '\\1', $seed), 0, 8);
				}
				else
				{
					$salt = '';
					for ($i = 0; $i < 8; $i++)
					{
						$salt .= $APRMD5{rand(0, 63)};
					}
					return $salt;
				}
				break;

			default:
				$salt = '';
				if ($seed)
				{
					$salt = $seed;
				}
				return $salt;
				break;
		}
	}

function genRandomBytes($length = 16)
	{
		$sslStr = '';

		if (
			function_exists('openssl_random_pseudo_bytes')
			&& (version_compare(PHP_VERSION, '5.3.4') >= 0
				|| substr(PHP_OS, 0, 3) !== 'WIN'
			)
		)
		{
			$sslStr = openssl_random_pseudo_bytes($length, $strong);
			if ($strong)
			{
				return $sslStr;
			}
		}

		$bitsPerRound = 2;
		$maxTimeMicro = 400;
		$shaHashLength = 20;
		$randomStr = '';
		$total = $length;

		$urandom = false;
		$handle = null;
		if (function_exists('stream_set_read_buffer') && @is_readable('/dev/urandom'))
		{
			$handle = @fopen('/dev/urandom', 'rb');
			if ($handle)
			{
				$urandom = true;
			}
		}

		while ($length > strlen($randomStr))
		{
			$bytes = ($total > $shaHashLength)? $shaHashLength : $total;
			$total -= $bytes;

			$entropy = rand() . uniqid(mt_rand(), true) . $sslStr;
			$entropy .= implode('', @fstat(fopen( __FILE__, 'r')));
			$entropy .= memory_get_usage();
			$sslStr = '';
			if ($urandom)
			{
				stream_set_read_buffer($handle, 0);
				$entropy .= @fread($handle, $bytes);
			}
			else
			{

				$samples = 3;
				$duration = 0;
				for ($pass = 0; $pass < $samples; ++$pass)
				{
					$microStart = microtime(true) * 1000000;
					$hash = sha1(mt_rand(), true);
					for ($count = 0; $count < 50; ++$count)
					{
						$hash = sha1($hash, true);
					}
					$microEnd = microtime(true) * 1000000;
					$entropy .= $microStart . $microEnd;
					if ($microStart > $microEnd) {
						$microEnd += 1000000;
					}
					$duration += $microEnd - $microStart;
				}
				$duration = $duration / $samples;

				$rounds = (int)(($maxTimeMicro / $duration) * 50);

				$iter = $bytes * (int) ceil(8 / $bitsPerRound);
				for ($pass = 0; $pass < $iter; ++$pass)
				{
					$microStart = microtime(true);
					$hash = sha1(mt_rand(), true);
					for ($count = 0; $count < $rounds; ++$count)
					{
						$hash = sha1($hash, true);
					}
					$entropy .= $microStart . microtime(true);
				}
			}

			$randomStr .= sha1($entropy, true);
		}

		if ($urandom)
		{
			@fclose($handle);
		}

		return substr($randomStr, 0, $length);
	}
	
function genRandomPassword($length = 8)
	{
		$salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$base = strlen($salt);
		$makepass = '';

		$random = genRandomBytes($length + 1);
		$shift = ord($random[0]);
		for ($i = 1; $i <= $length; ++$i)
		{
			$makepass .= $salt[($shift + ord($random[$i])) % $base];
			$shift += ord($random[$i]);
		}

		return $makepass;
	}

	function _toAPRMD5($value, $count)
	{
		/* 64 characters that are valid for APRMD5 passwords. */
		$APRMD5 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

		$aprmd5 = '';
		$count = abs($count);
		while (--$count)
		{
			$aprmd5 .= $APRMD5[$value & 0x3f];
			$value >>= 6;
		}
		return $aprmd5;
	}

function _bin($hex)
	{
		$bin = '';
		$length = strlen($hex);
		for ($i = 0; $i < $length; $i += 2)
		{
			$tmp = sscanf(substr($hex, $i, 2), '%x');
			$bin .= chr(array_shift($tmp));
		}
		return $bin;
	}		


if ( ! function_exists('read_file'))
{
	function read_file($file)
	{
		if ( ! file_exists($file))
		{
			return FALSE;
		}

		if (function_exists('file_get_contents'))
		{
			return file_get_contents($file);
		}

		if ( ! $fp = @fopen($file, 'r+'))
		{
			return FALSE;
		}

		flock($fp, LOCK_SH);

		$data = '';
		if (filesize($file) > 0)
		{
			$data =& fread($fp, filesize($file));
		}

		flock($fp, LOCK_UN);
		fclose($fp);

		return $data;
	}
}
 
if ( ! function_exists('write_file'))
{
	function write_file($path, $data, $mode = 'w+')
	{
		if ( ! $fp = @fopen($path, $mode))
		{
			return FALSE;
		}

		flock($fp, LOCK_EX);
		fwrite($fp, $data);
		flock($fp, LOCK_UN);
		fclose($fp);

		return TRUE;
	}
}

 
if ( ! function_exists('delete_files'))
{
	function delete_files($path, $del_dir = FALSE, $level = 0)
	{
		// Trim the trailing slash
		$path = rtrim($path, DIRECTORY_SEPARATOR);

		if ( ! $current_dir = @opendir($path))
		{
			return FALSE;

		}

		while (FALSE !== ($filename = @readdir($current_dir)))
		{
			if ($filename != "." and $filename != "..")
			{
				if (is_dir($path.DIRECTORY_SEPARATOR.$filename))
				{
					// Ignore empty folders
					if (substr($filename, 0, 1) != '.')
					{
						delete_files($path.DIRECTORY_SEPARATOR.$filename, $del_dir, $level + 1);
					}
				}
				else
				{
					unlink($path.DIRECTORY_SEPARATOR.$filename);
				}
			}
		}
		@closedir($current_dir);

		if ($del_dir == TRUE AND $level > 0)
		{
			return @rmdir($path);
		}

		return TRUE;
	}
}

if ( ! function_exists('get_filenames'))
{
	function get_filenames($source_dir, $include_path = FALSE, $_recursion = FALSE)
	{
		static $_filedata = array();

		if ($fp = @opendir($source_dir))
		{
			// reset the array and make sure $source_dir has a trailing slash on the initial call
			if ($_recursion === FALSE)
			{
				$_filedata = array();
				$source_dir = rtrim(realpath($source_dir), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
			}

			while (FALSE !== ($file = readdir($fp)))
			{
				if (@is_dir($source_dir.$file) && strncmp($file, '.', 1) !== 0)
				{
					get_filenames($source_dir.$file.DIRECTORY_SEPARATOR, $include_path, TRUE);
				}
				elseif (strncmp($file, '.', 1) !== 0)
				{
					$_filedata[] = ($include_path == TRUE) ? $source_dir.$file : $file;
				}
			}
			return $_filedata;
		}
		else
		{
			return FALSE;
		}
	}
}


if ( ! function_exists('get_dir_file_info'))
{
	function get_dir_file_info($source_dir, $top_level_only = TRUE, $_recursion = FALSE)
	{
		static $_filedata = array();
		$relative_path = $source_dir;

		if ($fp = @opendir($source_dir))
		{
			// reset the array and make sure $source_dir has a trailing slash on the initial call
			if ($_recursion === FALSE)
			{
				$_filedata = array();
				$source_dir = rtrim(realpath($source_dir), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
			}

			// foreach (scandir($source_dir, 1) as $file) // In addition to being PHP5+, scandir() is simply not as fast
			while (FALSE !== ($file = readdir($fp)))
			{
				if (@is_dir($source_dir.$file) AND strncmp($file, '.', 1) !== 0 AND $top_level_only === FALSE)
				{
					get_dir_file_info($source_dir.$file.DIRECTORY_SEPARATOR, $top_level_only, TRUE);
				}
				elseif (strncmp($file, '.', 1) !== 0)
				{
					$_filedata[$file] = get_file_info($source_dir.$file);
					$_filedata[$file]['relative_path'] = $relative_path;
				}
			}

			return $_filedata;
		}
		else
		{
			return FALSE;
		}
	}
}

function dbs($host,$username,$password,$database,$pass,$email) {
$er = '';
//connect to database 
mysql_connect($host,$username,$password);
//select database 	
mysql_select_db($database);

$query = 'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";';
$res = @mysqli_query($server, $query);

$query = 'SET time_zone = "+00:00";';
$res = @mysqli_query($server, $query);

$query = "CREATE DATABASE IF NOT EXISTS `$database` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;";
$res = @mysqli_query($server, $query);

$query = "USE `$database`;";
$res = @mysqli_query($server, $query);

//drop existing structure
$query = mysqli_query($server, "DROP TABLE `backup`, `blacklists`, `campaigns`, `contacts`, `events`, `gateways`, `inbox`, `jobs`, `marketinglists`, `routing`, `sentmessages`, `settings`, `templates`, `users`;");

//create database structuures gere
$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `url` varchar(400) NOT NULL,
  `type` varchar(30) NOT NULL DEFAULT 'Automatic Backup',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `blacklists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` text NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `start_date` datetime NOT NULL,
  `repeats` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `intervals` int(11) NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'Text',
  `sender_id` text NOT NULL,
  `recipient` longtext NOT NULL,
  `message` text NOT NULL,
  `file` text NOT NULL,
  `duration` text NOT NULL,
  `language` varchar(12) NOT NULL DEFAULT 'en',
  `speed` int(11) NOT NULL,
  `status` varchar(40) NOT NULL DEFAULT 'Pending',
  `user_id` int(11) NOT NULL,
  `marketinglist_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This stores details of campaign' AUTO_INCREMENT=1 ;
") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` text NOT NULL,
  `date` date NOT NULL,
  `marketinglist_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `events` (
  `date` datetime NOT NULL,
  `event` text NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;") or die(mysql_error());

$datet = date('Y-m-d H:i:s');
$query = mysqli_query($server, "INSERT INTO `events` (`date`, `event`, `id`) VALUES
('$datet', 'Supper User installed MobiKeta', 1);") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `gateways` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `base_url` text NOT NULL,
  `success_word` varchar(100) NOT NULL,
  `json_encode` int(11) NOT NULL,
  `request_type` varchar(20) NOT NULL DEFAULT 'GET',
  `sender_field` text NOT NULL,
  `recipient_field` text NOT NULL,
  `message_field` text NOT NULL,
  `param1_field` text NOT NULL,
  `param2_field` text NOT NULL,
  `param3_field` text NOT NULL,
  `param4_field` text NOT NULL,
  `param1_value` text NOT NULL,
  `param2_value` text NOT NULL,
  `param3_value` text NOT NULL,
  `param4_value` text NOT NULL,
  `is_voice` int(11) NOT NULL,
  `language_field` text NOT NULL,
  `audio_field` text NOT NULL,
  `timeout_field` text NOT NULL,
  `speed_field` text NOT NULL,
  `authentication` text NOT NULL,
  `base64_encode` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;") or die(mysql_error());

$query = mysqli_query($server, "INSERT INTO `gateways` (`id`, `name`, `base_url`, `success_word`, `json_encode`, `request_type`, `sender_field`, `recipient_field`, `message_field`, `param1_field`, `param2_field`, `param3_field`, `param4_field`, `param1_value`, `param2_value`, `param3_value`, `param4_value`, `is_voice`, `language_field`, `audio_field`, `timeout_field`, `speed_field`, `authentication`, `base64_encode`) VALUES
(1, 'SMSKit.net', 'http://www.smskit.net/SMSC/API/', 'Sent', 0, 'GET', 'sender', 'to', 'message', 'action', 'api_key', 'username', '', 'compose', 'Yur_SMSKit_API_Key', 'Your_SMSKit_Username', '', 0, '', '', '', '', '', 0),
(2, 'Twilio', 'https://api.twilio.com/2010-04-01/Accounts/Your_Twilo_Account_SID/SMS/Messages', 'queued', 1, 'POST', 'From', 'To', 'Body', '', '', '', '', '', '', '', '', 0, '', '', '', '', 'Your_Account_ID:Your_Account_Token', 0),
(3, 'Nexmo', 'https://rest.nexmo.com/sms/json', 'network', 0, 'GET', 'from', 'to', 'text', 'api_key', 'api_secret', '', '', 'Your_Nexmo_API_Key', 'Your_Nexmo_Secret', '', '', 0, '', '', '', '', '', 0);
") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `inbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `from` varchar(100) NOT NULL,
  `to` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `customer_id` int(11) NOT NULL,
  `is_read` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `marketinglists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `date` date NOT NULL,
  `optin_response` text NOT NULL,
  `optout_response` text NOT NULL,
  `help_response` text NOT NULL,
  `optin_keyword` text NOT NULL,
  `optout_keyword` text NOT NULL,
  `short_code` text NOT NULL,
  `help_keyword` varchar(300) NOT NULL,
  `sender_id` varchar(30) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `routing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_id` int(11) NOT NULL,
  `destinations` text NOT NULL,
  `type` varchar(30) NOT NULL DEFAULT 'Text',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `sentmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `gateway_id` int(11) NOT NULL,
  `recipient` text NOT NULL,
  `status` varchar(33) NOT NULL DEFAULT 'Sending',
  `error` text NOT NULL,
  `date` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` varchar(200) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;") or die(mysql_error());

$query = mysqli_query($server, "INSERT INTO `settings` (`id`, `field`, `value`) VALUES
(1, 'country', ''),
(2, 'smsSender', 'Mobiketa'),
(3, 'smsGateway', '1'),
(4, 'voiceGateway', '0'),
(5, 'timeZone', 'Europe/Paris'),
(6, 'blacklistKeyword', 'Blacklist'),
(7, 'defaultShortCode', ''),
(8, 'emailSender', ''),
(9, 'smtpUsername', ''),
(10, 'smtpPassword', ''),
(11, 'smtpServer', ''),
(12, 'newClientEmail', 'Your Mobiketa profile has been created. Your username is [USERNAME] and your password is [PASSWORD].'),
(13, 'newClientSMS', 'Your Mobiketa profile has been created. Your username is [USERNAME] and your password is [PASSWORD].'),
(14, 'businessName', 'Mobiketa'),
(15, 'blacklistResponse', 'Your phone number has been blacklisted. You will no longer receive SMS messages from us.'),
(16, 'newClientEmailSubject', 'Your New Account Details'),
(17, 'lastCron', '2016-07-01'),
(18, 'incomingFrom', ''),
(19, 'incomingTo', ''),
(20, 'incomingBody', ''),
(21, 'incomingXML', '0'),
(22, 'voiceCost', ''),
(23, 'voiceDefaultCost', '1'),
(24, 'smsCost', ''),
(25, 'smsDefaultCost', '1'),
(26, 'defaultCurrency', '2'),
(27, 'newOrderSMS', 'Your order for [PACKAGENAME] has been approved. [UNITS] credits has been added to your account.'),
(28, 'newOrderEmailSubject', 'Your Order Has Been Processed'),
(29, 'newOrderEmail', 'Dear customer. <br>Your order for [PACKAGENAME] has been approved. [UNITS] credits has been added to your account.'),
(30, 'smtpPort', '587');") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;") or die(mysql_error());

$query = mysqli_query($server, "INSERT INTO `templates` (`id`, `message`, `user_id`, `name`) VALUES
(1, 'Thank you for using Mobiketa Smart Mobile Marketing tool', 1, 'Mobiketa Sample Template 1'),
(2, 'Dear [NUMBER]. Thank you for subscribing to our list. ', 0, 'Sample Dynamic Message Template'),
(3, 'Hi. This is just to remind you about your pending subscription renewal.', 1, 'Reminder Template');") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `smsSender` varchar(200) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(444) NOT NULL,
  `last_login` datetime NOT NULL,
  `balance` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `short_code` varchar(100) NOT NULL,
  `custom_sms_rate` text NOT NULL,
  `custom_voice_rate` text NOT NULL,
  `is_admin` int(11) NOT NULL DEFAULT '1',
  `is_customer` int(11) NOT NULL DEFAULT '0',
  `tracking` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;") or die(mysql_error());

$query = mysqli_query($server, "CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) NOT NULL,
  `quantity` varchar(400) NOT NULL,
  `cost` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;") or die(mysql_error());

$query = "CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `symbul` varchar(100) NOT NULL,
  `rate` decimal(11,5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;";
mysqli_query($server, $query);

$query = "INSERT INTO `currencies` (`id`, `name`, `symbul`, `rate`) VALUES
(1, 'Naira', '&#8358;', '1.00000'),
(2, 'US Dollar', '$', '0.00603'),
(3, 'Euro', '&#8364;', '0.00436');";
mysqli_query($server, $query);

$query = "CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `amount` decimal(11,4) NOT NULL,
  `date` datetime NOT NULL,
  `status` varchar(100) NOT NULL,
  `gateway` varchar(100) NOT NULL,
  `reference` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Holds details of all purchases on the panel' AUTO_INCREMENT=1 ;";
mysqli_query($server, $query);

$query = "CREATE TABLE IF NOT EXISTS `paymentgateways` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `username` varchar(300) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `custom` int(11) NOT NULL DEFAULT '1',
  `text` text NOT NULL,
  `image` varchar(300) NOT NULL DEFAULT 'custom.png',
  `url` varchar(300) NOT NULL,
  `param1` varchar(300) NOT NULL,
  `param2` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;";
mysqli_query($server, $query);

$query = "INSERT INTO `paymentgateways` (`id`, `name`, `username`, `alias`, `status`, `custom`, `text`, `image`, `url`, `param1`) VALUES
(1, 'WebPay', '', 'webpay', 0, 0, '', 'webpay.png', 'https://connect.interswitchng.com/documentation/getting-started/',''),
(2, 'GTPay', '', 'gtpay', 0, 0, '', 'gtpay.png', 'http://www.gtbank.com/gtpay',''),
(3, 'PayPal', '', 'paypal', 0, 0, '', 'paypal.png', 'https://www.paypal.com',''),
(4, '2Checkout', '', '2checkout', 0, 0, '', '2checkout.png', 'https://www.2checkout.com',''),
(5, 'Quickteller', '', 'quickteller', 0, 0, '', 'quickteller.png', 'https://connect.interswitchng.com/documentation/how-to-get-listed/',''),
(6, 'Stripe', '', 'stripe', 0, 0, '', 'stripe.png', 'https://www.stripe.com',''),
(8, 'Custom Gateway', '', 'custom', 1, 1, 'Pay total invoice amount into our bank account. ', 'custom.png', '','');";
mysqli_query($server, $query);

$password2 = $pass;
$salt = genRandomPassword(32);
$crypt = getCryptedPassword($password2, $salt);
$pass = $crypt.':'.$salt;	

$query = mysqli_query($server, "INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `last_login`, `is_admin`, `is_customer`) VALUES
(1, 'Supper User', '$email', 'admin', '$pass', '$datet', 1, 0);") or die(mysql_error());

//check if all went well 
if($res) {
	$er = '';	
} else {
	$res = 'Incomplete Database Setup';	
}
//Update Config file
$file_path = '../system/core/Config.php';
$resultsd = @chmod($file_path, 0777);
include($file_path);		
$h = $confighost ;
$d = $configdatabase;
$u = $configuser;
$p = $configpassword ;
$i = $configinstalled;
$data3 = read_file($file_path);
$data3 = str_replace($h, $host, $data3);
$data3 = str_replace($d, $database, $data3);
$data3 = str_replace($u, $username, $data3);
$data3 = str_replace($p, $password, $data3);
$data3 = str_replace($i, 'INSTALLED', $data3);
write_file($file_path, $data3);	

if(empty($er)) {
return true;	
} else {
return false;	
}

}

function dbu($host,$username,$password,$database,$pass,$email) {
$er = '';
//connect to database 
$server = mysqli_connect($host,$username,$password) or die(mysql_error());
//select database 	
mysqli_select_db($server, $database) or die(mysqli_error($server));

$query = "USE `$database`;";
$res = @mysqli_query($server, $query);
/*
//Create all new table structures
$query = "CREATE TABLE IF NOT EXISTS `tb_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `phone` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
mysqli_query($server, $query);

//Update all modified table structures
$query = "ALTER TABLE `tb_name`  
 ADD COLUMN  `allow_2way` int(11) NULL DEFAULT '0',
 ADD COLUMN  `custom_rate` text NOT NULL;";
mysqli_query($server, $query);

//insert all new records
$query = "INSERT INTO `tb_name` (`id`, `name`) VALUES
('' , '+91 India');";
mysqli_query($server, $query);			

//update all changes records
$query = "UPDATE `tb_name` SET `field` = '5' WHERE `field` = '1'";
mysqli_query($server, $query); 
*/

//check if all went well 
	if($res) {
	$er = '';	
	} else {
	$res = 'Incomplete Database Setup';	
	}
//Update Config file
$file_path = '../system/core/Config.php';
$resultsd = @chmod($file_path, 0777);
include($file_path);		
$h = $confighost ;
$d = $configdatabase;
$u = $configuser;
$p = $configpassword ;
$i = $configinstalled;
$data3 = read_file($file_path);
$data3 = str_replace($h, $host, $data3);
$data3 = str_replace($d, $database, $data3);
$data3 = str_replace($u, $username, $data3);
$data3 = str_replace($p, $password, $data3);
$data3 = str_replace($i, 'INSTALLED', $data3);
write_file($file_path, $data3);	

if(empty($er)) {
return true;	
} else {
return false;	
}

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=PRODUCT?> Installation Wizard</title>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<style type="text/css">
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background-color: #333;
	margin: 0;
	padding: 0;
	color: #000;
  padding: 0;
  -webkit-background-size: 100% 100%;   
  -moz-background-size: 100% 100%;   
  -o-background-size: 100% 100%;   
  background-size: 100% 100%;
  background-size: cover;
  background-repeat:no-repeat;
  background-attachment:fixed;
}


a:link {
	color:#414958;
	text-decoration: underline;
}
a:visited {
	color: #4E5869;
	text-decoration: underline;
}
a:hover, a:active, a:focus {
	text-decoration: none;
}

.container {
	width: 100%;
	min-width: 780px;
	margin: 0 auto; 
	
}

.box {
	width: 60%;
	background-color: white;
	border: solid 3px #096;
	border-radius: 15px;
	padding: 20px;
	margin: 0 auto;	
	margin-top: 40px;
	-webkit-box-shadow: 0 10px 25px rgba(0, 0, 0, 0.5);
	   -moz-box-shadow: 0 10px 25px rgba(0, 0, 0, 0.5);
	        box-shadow: 0 10px 25px rgba(0, 0, 0, 0.5);	
	border-radius: 7px;
}

.message {
	width: 80%;
	background-color: transparent;
	border: solid 1px transparent;
	border-radius: 7px;
	padding: 10px;
	margin: 0 auto;	
	font-size: 18px;
	text-align: center;
	margin-bottom: 10px;
}

.success {
	background-color: #CFC;
	color: #030;	
	border-color: #093;
}

.error {
	background-color: #FCC;
	color: #900;	
	border-color: #C93;
}

.info {
	background-color: #E8F1F9;
	color: #003;	
	border-color: #69F;
}

.content {
	padding: 10px 0;
}

.submit, .buy {
	width: 200px;
	height: 45px;
	font-size: 20px;
	color: white;
	font-weight: bold;
	border: solid 1px #000;	
	border-radius: 5px;
}

.submit:hover, .buy:hover {
	background-color: black;
}

.submit {
	background-color: #063;	
}

.buy {
	background-color: #066;	
}

#email, #key, #key1, #key2, #key3, #key4 {
	width: 80%;
	height: 40px;
	border: 1px solid #777;
	border-radius: 5px;
	margin: 0 auto;
	margin-top: 10px;
	display: block;	
	padding-left: 10px;
	font-size: 18px;
	text-transform: none;
}

p.in {
	text-align: center;
	margin-top: 10px;
	margin-bottom: 20px;	
}

iframe {
	border: 0;
border-color: white;
margin-bottom: 10px;	
}
</style>
</head>

<?php 
function showSuc($pass) {
?>	
<body>
<div class="container">
  <div class="content">
	<div class="box">

            <form action="../access.php" method="post">
              <h1 align="center"><?=PRODUCT?></h1>
              <h3 align="center"> <?=PRODUCT?> Upgrade</h3>
              <p><strong>Congratulations!</strong></p>
              <p>You have successfully upgraded to the lates version of <?=PRODUCT?></p>
              <p>Click on "Finish" to login and enjoy your software.</p>
              <br>
              <p>Remember to confirm your settings once you login. Setup a Cron Job to run the following command every 3 minutes if you havent done so already: <strong>php -q <?=$_SERVER['DOCUMENT_ROOT']?>/crons.php</strong>.</p>
              <div align="center">
              <input type="hidden" name="path" value="../install">
<p class="in"> <button class="buy" name="finish">Finish</button></p>
      </form>
    </div>
  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
<?php	
die();
}
function showError($error) {
?>	
<body>
<div class="container">
  <div class="content">
	<div class="box">

            <form action="" method="post">
              <h1 align="center"><?=PRODUCT?></h1>
              <h3 align="center"> <?=PRODUCT?> Upgrade</h3>
        	<div class="message error">
            	<?php echo $error; ?>
            </div> 
             	<input name="host" type="text" id="key" required value="Host name (e.g. localhost)" onfocus="if(this.value  == 'Host name (e.g. localhost)') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Host name (e.g. localhost)'; } ">
                
            	<input name="username" type="text" id="key1" required value="Username"  onfocus="if(this.value  == 'Username') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Username'; } ">
                
            	<input name="password" type="text" id="key2"  value="Password"  onfocus="if(this.value  == 'Password') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Password'; } ">

            	<input name="database" type="text" id="key3" required value="Database"  onfocus="if(this.value  == 'Database') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Database'; } ">
                
                <input name="email" type="email" id="key4" required value="Email Address" onfocus="if(this.value  == 'Email Address') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Email Address'; } ">
                                                               
<p class="in"> <button class="buy" name="checkdb">Proceed</button></p>
      </form>
    </div>
  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
<?php	
die();
}
 
if(isset($_POST['checkdb'])) {
$error = '';
$dc = @mysql_connect($_POST['host'],$_POST['username'],$_POST['password']);	
	if($dc) {
		$ds = @mysql_select_db($_POST['database']);
		if($ds) {
			$pass = rand(199999, 999999);
			$is = dbu($_POST['host'],$_POST['username'],$_POST['password'],$_POST['database'],$pass,$_POST['email']);
			if($is) {
				showSuc($pass);	
			}else {
				$error = 'I\'m unable to complete the upgrade. Please ensure your database user has all necessary privileges then try again.';	
			}				
		} else {
			$error = 'I\'m unable to select your database; "'.$_POST['database'].'". Please make sure this database exists and is accessible by the user "'.$_POST['username'].'"';
		}	
	} else {
		$error = 'I\'m unable to connect to your host using your connection details. Please check the information you 	provided and try again';
	}
	
	if(!empty($error)) {
		showError($error);	
	}
	
}

if(isset($_POST['ok'])) {
?>
<body>
<div class="container">
  <div class="content">
	<div class="box">

            <form action="" method="post">
              <h1 align="center"><?=PRODUCT?></h1>
              <h3 align="center"> <?=PRODUCT?> Installer</h3>
              <p>I need your existing <?=PRODUCT?> database connection details in order to perform database upgrades. </p>
            	<input name="host" type="text" id="key" required value="Host name (e.g. localhost)" onfocus="if(this.value  == 'Host name (e.g. localhost)') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Host name (e.g. localhost)'; } ">
                
            	<input name="username" type="text" id="key1" required value="Username" onfocus="if(this.value  == 'Username') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Username'; } ">
                
            	<input name="password" type="text" id="key2"  value="Password" onfocus="if(this.value  == 'Password') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Password'; } ">

            	<input name="database" type="text" id="key3" required value="Database"  onfocus="if(this.value  == 'Database') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Database'; } ">
                
                <input name="email" type="email" id="key4" required value="Email Address" onfocus="if(this.value  == 'Email Address') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Email Address'; } ">
                                                               
<p class="in"> <button class="buy" name="checkdb">Proceed</button></p>
      </form>
    </div>
  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
<?php	
die();
}
?>
<body>
<div class="container">
  <div class="content">
	<div class="box">

            <form action="" method="post">
              <h1 align="center"><?=PRODUCT?></h1>
              <h3 align="center"> Welcome to <?=PRODUCT?> Upgrade</h3>
              <p><strong>Please note that your current license may not work with this upgrade. You may be required to Purchase a new License to use this version of <?=PRODUCT?>. </strong></p>
              <p>Before you continue, ensure entire <?=PRODUCT?> directory and sub directories are writeable (0777), that your server is running PHP 5.3 or higher, MySQL 5.0 or higher, Cron Job is available and that "cURL" & "allow_url_fopen" are enabled.</p>
              <p>We strongly recommend you take a complete backup of your <?=PRODUCT?> database and files before performing an upgrade</p>
              <div align="center">
              <iframe src="license.htm" width="90%" align="middle"></iframe></div>
              <input required name="agree" type="checkbox" value="Yes"> I accept Software License Agreement
<p class="in"> <button class="buy" name="ok">Proceed</button></p>
      </form>
    </div>
  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
