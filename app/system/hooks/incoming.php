<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) { header('location: index.php'); } 

?>
    <section class="content-header">
      <h1>
        Incoming SMS Setup
        <small>Setup incoming SMS gateway</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> System Tools</a></li>
        <li class="active">Incoming SMS Settings</li>
      </ol>
    </section>

    <section class="content">
    <div class="row">
<?php    
if(isset($_POST['save'])){
	global $server;
	foreach ($_POST as $key => $value ){
		${$key} = $value = str_replace('\'', '', $value);
		mysqli_query($server, "update settings set `value` = '$value' where field = '$key'")or die(mysqli_error($server));
	}	
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                Your incoming SMS settings was successfully saved and applied. Contact your 2-way message service provider for instructions on how to link MOJASMS with their service to enable you start receiving your incoming messages on Mobiketa
              </div>    
     </div>         
    <?php
	
}
?>  
       <form class="form-vertial" action="index.php?url=incoming" method="post" role="form">           
		<div class="col-md-7">
          <div class="box box-success" >
            <div class="box-header with-border"><h4>Incoming SMS Settings</h4></div>
            
            <div class="box-body">
                <div class="form-group">
                  <label for="name">Sender Parameter Name<br /><small class="text-yellow">Which parameter in your gateway API contains the sender's phone number</small></label>
                  <input type="text" required class="form-control" id="incomingFrom" name="incomingFrom" value="<?=getSetting('incomingFrom') ?>" placeholder="Eg. from, source, etc">
                </div>
                
                <div class="form-group">
                  <label for="name">Recipient Parameter Name<br /><small class="text-yellow">Which parameter in your gateway API contains the recipient's phone number</small></label>
                  <input type="text" required class="form-control" id="incomingTo" name="incomingTo" value="<?=getSetting('incomingTo') ?>" placeholder="Eg. to, destination, etc">
                </div>

                <div class="form-group">
                  <label for="name">Message Body Parameter Name<br /><small class="text-yellow">Which parameter in your gateway API contains the message body</small></label>
                  <input type="text" required class="form-control" id="incomingBody" name="incomingBody" value="<?=getSetting('incomingBody') ?>" placeholder="Eg. text, body, etc">
                </div>
                
                <div class="form-group">
                  <label for="name">Enable XML Headers<br /><small class="text-yellow">Set this to Yes only if your gateway requires such.</small></label>
                  <select name="incomingXML" id="incomingXML" class="form-control" style="width: 100%;">
                    <option <?php if(getSetting('incomingXML') == 1) echo 'selected';?> value="1">Yes</option>			
                    <option <?php if(getSetting('incomingXML') == 0) echo 'selected';?> value="0">No</option>                  
                   </select>
                </div>                
            </div>

            <div class="box-footer">
              <button type="submit" name="save" class="btn btn-success">Update Settings</button>
            </div>
                          
        </div>
       </div> 
            </form>

		<div class="col-md-5">
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Integration Information</h3>
            </div>

            <div class="box-body">
            	<p>Some features on MOJASMS such as Auto-responders, Blacklisting, etc, needs 2-Way messaging.</p>
                <p>This page allows you to quickly integrate MOJASMS with your 2-Way messaging service provider.</p>
				<p>Most 2-Way SMS gateways POST your incoming messages to a URL on your server. MOJASMS provides the reqired script to retrieve these messages and store on your server.</p>
                <p>MOJASMS incoming message handler script is located at <strong><?=home_base_url();?>inbox.php</strong>. You need to configure your account with your 2-Way SMS service providers using this URL before you can recieve messages on your MOJASMS.</p>
                <p></p>
                <p>Refer to MOJASMS documentation for further help.</p>
           </div>
         </div> 
             
		</div>          
    </section>