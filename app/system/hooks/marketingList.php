<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('DATATABLE', true);
?>
    <section class="content-header">
      <h1>
        Marketing List
        <small>Manage marketing list and contacts</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Marketing List</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	if(!isAdmin(getUser())) {
		$user_id = getUser();
		$ch=mysqli_query($server, "select * from marketinglists where id = '$_GET[delete]' and customer_id = '$user_id' limit 1");
	}
	if(mysqli_num_rows($ch) < 1) {
		header('location: index.php?url=marketingList');	
	}
	$sql=mysqli_query($server, "delete from marketinglists where id='$_GET[delete]' ")or die(mysqli_error($server));
	$sql=mysqli_query($server, "delete from contacts where marketinglist_id='$_GET[delete]'")or die(mysqli_error($server));

	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected marketing list with all subscribers has been successfully deleted from the system.
              </div>    
     </div>         
    <?php
}
?>     
        <div class="col-md-12">
               
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=marketingList" method="get">
                <input type="hidden" name="url" value="marketingList" />
                <div class="input-group input-group-sm" style="width: 350px;">
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>

              </div>
                <a href="index.php?url=newMarketingList">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Create New List</button></a>
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th>
                  <th>Name</th>
                  <th>Assigned to</th>
                  <th>Created on</th>
                  <th>Subscribers</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
$user_id = getUser();
	$ch=mysqli_query($server, "select * from marketinglists where customer_id = '$user_id' order by id desc limit 1000");
		if(isAdmin(getUser())) { 
		$ch=mysqli_query($server, "select * from marketinglists order by id desc limit 1000");
		}
	
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "name LIKE '%$term%' OR short_code LIKE '%$term%' OR id LIKE '%$term%'";
	    } else {
	         $clauses[] = "name LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$user_id = getUser();
	$ch=mysqli_query($server, "select * from marketinglists where ".$filter." and customer_id = '$user_id'");	
		if(isAdmin(getUser())) { 
			$ch=mysqli_query($server, "select * from marketinglists where ".$filter);
		}
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
		$owner = 'Not Assigned';
		if($row['customer_id'] > 0) 
		$owner = userName($row['customer_id']);
?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo $owner; ?></td>
                  <td><?php echo $row['date']; ?></td>
                  <td><?php echo countSubscribers($row['id']); ?> subscribers</td>
                  <td>
                  	<div class="btn-group">
                        <a href="index.php?url=viewContact&view=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button></a>
                        <?php if(isAdmin(getUser()) || $row['customer_id'] == getUser()) { ?>
                        <a href="index.php?url=newSMSCampaign&list=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-envelope"></i></button></a>
                        <?php } ?>
                        <?php if(isAdmin(getUser())) { ?>
                        <a href="index.php?url=marketingList&edit=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                        <a href="index.php?url=marketingList&delete=<?php echo $row['id'];?>" onclick="confirm('Are you sure you want to delete this list?');">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                        <?php } ?>
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S/N</th>
                  <th>Name</th>
                  <th>Created on</th>
                  <th>Assigned to</th>
                  <th>Subscribers</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
        
    </section>
