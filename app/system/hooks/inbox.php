<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('DATATABLE', true);

?>
    <section class="content-header">
      <h1>
        SMS Inbox
        <small>View Incoming SMS</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Inbox</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	$user_id = getUser();
	if(isAdmin(getUser())) {
		$sql=mysqli_query($server, "update inbox set `deleted` = '1' WHERE id='$_GET[delete]'")or die(mysqli_error($server));	
	}else {
		$sql=mysqli_query($server, "update inbox set `deleted` = '1' WHERE id='$_GET[delete]' and customer_id = '$user_id'")or die(mysqli_error($server));
	}
	//show mesage
	?>
    <div class="col-xs-12">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected message has been successfully deleted from your inbox.
              </div>    
     </div>         
    <?php
}
?>     
        <div class="col-xs-12">
               
          <div class="box"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=campaigns" method="get">
                <input type="hidden" name="url" value="inbox" />
                <div class="input-group input-group-sm">
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Message</th>
                  <th>Date</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
$user_id = getUser();
$ch=mysqli_query($server, "select * from inbox where customer_id = '$user_id' AND deleted='0' order by id desc limit 1000");
if(isAdmin(getUser())) {
	$ch=mysqli_query($server, "select * from inbox WHERE deleted='0' order by id desc limit 1000");
}
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "from LIKE '%$term%' OR message LIKE '%$term%' OR to LIKE '%$term%' OR date LIKE '%$term%'";
	    } else {
	         $clauses[] = "from LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$user_id = getUser();
		$ch=mysqli_query($server, "select * from inbox where deleted='0' AND customer_id = '$user_id' AND ".$filter);
		if(isAdmin(getUser())) {
			$ch=mysqli_query($server, "select * from inbox where deleted='0' AND customer_id = '$user_id' AND ".$filter);	
		}
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
		mysqli_query($server, "update inbox set `is_read` = '1' where id='$row[id]'")or die(mysqli_error($server));
?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $row['from']; ?></td>
                 <td><?php echo !empty($row['to']) ? $row['to'] : 'MOJASMS'; ?></td> 
                  <td><?php echo $row['message']; ?></td>
                  <td><?php echo $row['date']; ?></td>
                  <td>
                  	<div class="btn-group">
                        <a href="index.php?url=inbox&delete=<?php echo $row['id'];?>" onclick="confirm('Are you sure you want to delete this message?');">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                	</div>
                  </td>

<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S/N</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Message</th>
                  <th>Date</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>

      </div>   
    
    </section>