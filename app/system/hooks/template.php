<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('DATATABLE', true); 
if(isset($_GET['edit']) && !empty($_GET['edit'])) {
	global $server;
	$user_id = getUser();
	$ch=mysqli_query($server, "select * from templates where id = '$_GET[edit]' and user_id = '$user_id' limit 1");
	if(isAdmin(getUser())) {
		$ch=mysqli_query($server, "select * from templates where id = '$_GET[edit]' limit 1");
	}
	$rows = mysqli_fetch_assoc($ch);
}
?>
    <section class="content-header">
      <h1>
        Message Templates
        <small>View Saved Message Templates</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Message Templates</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	$user_id = getUser();
	if(isAdmin(getUser())) {
		$sql=mysqli_query($server, "delete from templates where id='$_GET[delete]'")or die(mysqli_error($server));	
	}else {
		$sql=mysqli_query($server, "delete from templates where id='$_GET[delete]' and user_id = '$user_id'")or die(mysqli_error($server));
	}		
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected message template has been successfully deleted from your system.
              </div>    
     </div>         
    <?php
}

if(isset($_POST['save'])){
	global $server;
		$user_id = getUser();
		$add = mysqli_query($server, "INSERT INTO templates (`user_id`, `message`, `name`) 
		VALUES ('$user_id', '$_POST[message]', '$_POST[name]');") or die (mysql_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                Your new message template was successfully created.
              </div>    
     </div>         
    <?php
}

if(isset($_POST['update'])){
	global $server;
	mysqli_query($server, "update templates set `name` = '$_POST[name]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update templates set `message` = '$_POST[message]' where id='$_POST[id]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected message template has been successfully updated.
              </div>    
     </div>         
    <?php
}
?>     
        <div class="col-md-7">
               
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=template" method="get">
                <div class="input-group input-group-sm" style="width: 350px;">
                <input type="hidden" name="url" value="template" />
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Message</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$user_id = getUser();
	$ch=mysqli_query($server, "select * from templates where user_id = '$user_id' order by id desc limit 1000");
	if(isAdmin(getUser())) {
		$ch=mysqli_query($server, "select * from templates order by id desc limit 1000");
	}
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "name LIKE '%$term%' OR message LIKE '%$term%' OR id LIKE '%$term%'";
	    } else {
	         $clauses[] = "name LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$user_id = getUser();
	$ch=mysqli_query($server, "select * from templates where user_id = '$user_id' and ".$filter);	
	if(isAdmin(getUser())) {
		$ch=mysqli_query($server, "select * from templates where ".$filter);	
	}
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
?>
                <tr>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo $row['message']; ?></td>
                  <td >
                  	<div class="btn-group">
                    <?php if(isAdmin(getUser()) || $row['customer_id'] == getUser()) { ?>
                        <a title="Edit" href="index.php?url=template&edit=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                        <a title="Delete" href="index.php?url=template&delete=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                    <?php } ?>    
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Message</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
 

		<div class="col-md-5">
          <div class="box box-primary">
         <?php if(!isset($_GET['edit'])) { ?>
            <div class="box-header with-border"><h4>Create New Message Template</h4></div>
            <?php } else { ?>
            <div class="box-header with-border"><h4>Update <?=$rows['name']?></h4></div>
            <?php } ?>

            <form class="form-horizontal" action="index.php?url=template" method="post" role="form">           
            <div class="box-body" style="width: 90%; margin:0 auto;">
           		<div class="form-group">
                  <label for="type">Template Type</label>
                  <select name="type" id="type" class="form-control" style="width: 100%;">
                    <option  value="Text">Text & Voice Template</option>                    
                  </select>
                </div>
                
                <div class="form-group">
                  <label for="name">Template Name</label>
                  <input type="text" required class="form-control" id="name" name="name" value="<?=@$rows['name'] ?>" placeholder="Name your new template">
                </div>
                
                <div class="form-group">
                  <label for="message">Message</label>
                  <textarea onBlur="count2(this,this.form.countBox2,1000);" onKeyUp="count2(this,this.form.countBox2,1000);"  class="form-control" rows="6" id="messageText" name="message" placeholder="Type your message text here... You can dynamically insert recipient's phone number in your template using [NUMBER]"><?=@$rows['message']?></textarea>
                  <p class="help-block text-blue" id="countBox2">0 Characters Used</p>

<script>
//count message lenght 
	function count2(field,countfield,maxlimit) {
	if (field.value.lenght > 1000) {
		field.value = field.value.substring(0,1000);
		field.blur();
		return false;
	} else {
		var smslenght = 160;
		if(field.value.length > 160){
			var smslenght = 145;
		}
		var pages = field.value.length /smslenght;
			if(pages < 1) { var page = '1';	}
			if(pages == 1) { var page = '1';}
			if(pages > 1) { var page = '2';	}
			if(pages > 2) {	var page = '3';	}
			if(pages > 3) {	var page = '4';	}
			if(pages > 4) {	var page = '5';	}
			if(pages > 5) {	var page = '6';	}
			if(pages > 6) {	var page = '7';	}
															
		document.getElementById('countBox2').innerHTML = field.value.length + " of 1000 Characters Used ("+page+" SMS)";
		}
	}
</script>                   
                </div>    
                
            </div>

            <div class="box-footer">
              <button type="reset" class="btn btn-danger">Reset Form</button>
               <?php if(!isset($_GET['edit'])) { ?>
              <button type="submit" name="save" class="btn btn-success">Create Template</button>
              <?php } else { ?>
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update Template</button>
              <?php }?>
            </div>
            </form>
                          
        </div>
       </div> 
                     
      </div>      
    
    </section>