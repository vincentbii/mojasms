<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('DATATABLE', true); 
if(!isAdmin(getUser())) { header('location: index.php'); } 
?>
    <section class="content-header">
      <h1>
        Login pop notes
        <small>logins</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> System Tools</a></li>
        <li class="active">logins notes</li>
      </ol>
    </section>

    <section class="content">
    <div class="row">


<style>
/* Dotted red border */
hr {
  border-top: 1px dotted red;
}
</style>
<?php 

global $server;


if(isset($_POST['message'])){ $message=$_POST['message'];

    
    $sql="UPDATE `welcome_popup` SET note='$message' WHERE id='".$_GET['edit']."'";
		           
if(mysqli_query($server, $sql)){
		          
?>
<div class="col-md-12">
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Cool</h4>
    Message updated successfully 
  </div>    
 </div> 
<?php
		           }else{
?>
<div class="col-md-12">
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Error!</h4>
    Try to update once again. 
  </div>    
 </div> 
<?php
		           }
}

if(isset($_GET['delete'])){
	
	$user_id = getUser();
	if(isAdmin(getUser())) {
		$sql=mysqli_query($server, "delete from welcome_popup where id='$_GET[delete]'")or die(mysqli_error($server));	
	}
	//show mesage
	?>
    <div class="col-xs-12">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected welcome popup has been successfully deleted from the list.
              </div>    
     </div>         
    <?php
}

if(isset($_GET['edit'])){
?>


       <form class="form-vertial" action="index.php?url=popup_note&edit=<?php echo $_GET['edit'];?>" method="post" role="form" enctype="multipart/form-data">           
		<div class="col-md-12">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Update Login Welcome Notes</h4></div>
            
                <div class="box-body">
                    <div class="form-group">
                      <label>Message</label>
                      
                      <?php 
                      
                      $chu=mysqli_query($server, "select * from welcome_popup WHERE id='".$_GET['edit']."'");
                      $message="";
                      	while ($rower = mysqli_fetch_assoc($chu)) {
                      	    $message=$rower['note'];
                      	}
                      ?>
                      <textarea class="form-control" name="message" required rows="6" placeholder="Write Message"><?php echo $message;?></textarea>
                    </div> 
                </div>
                 <div class="box-footer">
                  <button type="submit" name="save" class="btn btn-success">Update</button>
                </div>
                
          </div>
        </div>
       </form>
<?php
}
?>
     </div>
     <div class="row">
         <div class="col-md-12">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Saved Login Welcome Notes</h4></div>
            
                <div class="box-body">
                          <table id="example1" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>Message</th>
                                      <th>For</th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                   <?php 
                                      $i=1;
                                      
                                      $ch=mysqli_query($server, "select * from welcome_popup");
                                      	while ($row = mysqli_fetch_assoc($ch)) {
                                      ?>
                                      <tr>
                                          <td><?php echo $i++;?></td>
                                          <td><?php echo $row['note'];?></td>
                                          <td><?php echo ($row['status']==1) ? 'New ' : 'Returning';?></td>
                                          <td>
                                              <a href="index.php?url=popup_note&edit=<?php echo $row['id'];?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                          </td>
                                      </tr>
                                      <?php
                                      	}
                                      ?>
                              </tbody>
                          </table>
                </div>
          </div>
        </div> 
     </div>
    </section>













