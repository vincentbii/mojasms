<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(isAdmin(getUser())) {
	header('location: index.php');
}
define('DATATABLE', true); 


//process Stripe
if ($_POST['stripeToken']) {
	global $server;
  $transaction_id = $_SESSION['referrence'];  
  \Stripe\Stripe::setApiKey($payment_params['stripe_api_key']);
  $error = $success = '';
  try {
    if (!isset($_POST['stripeToken']))
      throw new Exception("The Stripe Token was not generated correctly");
      \Stripe\Charge::create(array("amount" => strtr($_POST['amount'], array('.' => '')),
                                "currency" => "usd",
                                "card" => $_POST['stripeToken']));
    $success = 'Your payment was successful.';
	//process transactioon
	processTransaction($transaction_id);
	header('location: index.php?url=order&success=1');
  }
  catch (Exception $e) {
    $error = $e->getMessage();
	$_SESSION['comment'] = @$error;
	mysqli_query($server,"UPDATE transactions SET status = 'Failed' WHERE reference = '$referrence' LIMIT 1 ");
	header('location: Order.php?url=order&failed=1');
  }
}
?>
    <section class="content-header">
      <h1>
        Buy VMS Credit
        <small>Fund your VMS account.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Buy Credit</li>
      </ol>
    </section>


<?php 
if(isset($_GET['failed'])){
	global $server;
	//show mesage
	?>
    <div class="pad margin no-print">
       <div class="alert alert-danger alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
         <h4><i class="icon fa fa-info"></i> Sorry!</h4>
         Your payment was not successful. Please try again or try a different payment method if this error persists.<br />
         Here are your transaction details;<br />
         VMS Package: <?=packageName($_SESSION['package_id'])?><br />
         Quantity: <?=packageQuantity($_SESSION['package_id'])?></div>
         Billed Amount: <?=currencySymbul(getSetting('defaultCurrency'))?><?=$_SESSION['amount']?><br />
         Transaction Ref: <?=$_SESSION['referrence']?><br />
       </div>    
     </div>         
    <?php
}


if(isset($_GET['success'])){
	global $server;
	mysqli_query($server, "update paymentgateways set `status` = '1' where id = '$_GET[activate]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="pad margin no-print">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Congratulations!</h4>
                Your payment was successful. The purchased VMS credits has been added to your account.<br />
                Here are your transaction details;<br />
                VMS Package: <?=packageName($_SESSION['package_id'])?><br />
                Quantity: <?=packageQuantity($_SESSION['package_id'])?></div>
                Billed Amount: <?=currencySymbul(getSetting('defaultCurrency'))?><?=$_SESSION['amount']?><br />
                Transaction Ref: <?=$_SESSION['referrence']?><br />
              </div>    
     </div>         
    <?php
}


?>     
<!-- Invoice and others --->
<?php 
if(isset($_POST['review']) && !empty($_POST['quantity']) && (check_cell_number_pending_due_ordervms(getUser()) == 0)) {         
//display invoice and pay action
	global $server;
	foreach ($_POST as $key => $value ){
		${$key} = $value = str_replace('\'', '', $value);
	}
	
	$quantity=(int)trim($_POST['quantity']);
	
	$date = date('Y-m-d H:i:s');
	$description = 'Order for '.packageName($package_id);
	$_SESSION['referrence'] = $referrence = time();
	$_SESSION['amount'] = $amount = packageCost($package_id)*$quantity;
	$_SESSION['amount']=$_SESSION['amount']*115/100;
	$_SESSION['package_id'] = $package_id = $package_id;
	$status = 'Pending';
	$customer_id = getUser();
	$_SESSION['gateway'] = $gateway = $gateway;
	$gatewayName = gatewayName($gateway);
	//create main transaction
	$add = mysqli_query($server, "INSERT INTO transactions (`date`, `description`, `reference`, `amount`, `package_id`, `status`, `customer_id`, `gateway`, `package_type`,`package_qty`) 
	VALUES ('$date', '$description', '$referrence', '".$_SESSION['amount']."', '$package_id', '$status', '$customer_id', '$gateway','vms','$quantity');") or die (mysqli_error($server));

	$ch=mysqli_query($server, "select * from paymentgateways where id = '$gateway' limit 1");
	$rows = mysqli_fetch_assoc($ch);
	$image = $rows['image'];
	$alias = $rows['alias'];
	$text = $rows['text'];
?>

    <section class="invoice">
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Invoice <img src="/logo.jpeg" class="img-responsive" width="150px"/>
            <small class="pull-right">Date: <?= date('d/m/Y', strtotime($date))?></small>
          </h2>
        </div>
      </div>

      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong><?=getSetting('businessName')?></strong><br>
            Email: <?=getSetting('emailSender')?><br>
            Mobile: +27 79 479 4811<br>
            Website: www.mojasms.com<br>
            VAT: <?=currencySymbul(getSetting('defaultCurrency')).number_format($amount*15/100, 2)?>
          </address>
        </div>

        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?=userData('name', getUser())?></strong><br>
            Phone: <?=userData('phone', getUser())?><br>
            Email: <?=userData('email', getUser())?>
          </address>
        </div>
        
        <div class="col-sm-4 invoice-col">
          <b>Invoice #<?=$referrence?></b><br>
          <br>
          
          <b>Payment Due:</b> <?= date('d/m/Y', strtotime($date))?><br>
          <b>Amount:</b> <?=currencySymbul(getSetting('defaultCurrency')).number_format($amount,2)?>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Item</th>
              <th>Quantity</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td><?=packageName($package_id)?></td>
              <td><?=packageQuantity($package_id)?></td>
              <td><?=currencySymbul(getSetting('defaultCurrency')).number_format($amount, 2)?></td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>


      <div class="row">
        <div class="col-md-6">
          <p class="lead">Payment Methods:</p>
          
          <img src="system/includes/gateway/<?=$alias?>/<?=$alias?>.png" alt="<?=$rows['name']?>" style="height: 40px;">
          <h4><strong><?=$rows['name']?></strong> </h4>
          
		<?php if(!empty($text)) { ?>	
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            <?=$text?> <br><span style="color:red;"><b> Reference No:</b> <?=$referrence?></span>
          </p>
        <?php } ?> 
        <br>
        </div>

        <div class="col-md-6">
          <p class="lead">Amount Due <?= date('d/m/Y', strtotime($date))?></p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td><?=currencySymbul(getSetting('defaultCurrency')).number_format($amount, 2)?></td>
              </tr>
              <tr>
                <th style="width:50%">VAT:</th>
                <td><?=currencySymbul(getSetting('defaultCurrency')).number_format($amount*15/100, 2)?></td>
              </tr>
              <tr>
                <th>Total:</th>
                <td><?=currencySymbul(getSetting('defaultCurrency')).number_format($amount*115/100, 2)?></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
	<?php if(empty($text)) { ?>	
	<?php include('system/includes/gateway/'.$alias.'/'.$alias.'.php'); ?>	
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="#" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="submit" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Proceed with Payment
          </button>
          <a href="index.php">
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-close"></i> Cancel Order
          </button></a>
        </div>
      </div>
    </form>
    <?php } else {?>
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="#" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <a href="index.php">
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-home"></i> Return to Dashboard
          </button></a>
        </div>
      </div>
	<?php } ?>
<?php
} else {
?>
    <section class="content">
       <div class="row">
<!-- Order Form --->
        <div class="col-md-8">

          <div class="box box-info">

         <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
          <div class="box-body">
            <div class="box-header with-border">
              <h3 class="box-title"> Buy VMS Credit</h3>
            </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Choose a Package </label>
                  <div class="col-sm-9">
                  <select name="package_id" id="package_id" class="form-control" style="width: 100%;">
					<?php global $server;
                            $ch=mysqli_query($server, "select * from packages where type='vms' order by id asc");
                            while ($rows = mysqli_fetch_assoc($ch)) {
                    ?>                    
                    <option value="<?=$rows['id']?>"><?=$rows['name']?>&nbsp;&nbsp;&nbsp; [<?=currencySymbul(getSetting('defaultCurrency')).$rows['cost']?>]</option>
                    <!--<option value="<?=$rows['id']?>"><?=$rows['name']?>&nbsp;&nbsp;&nbsp; [<?php $R_099 = round($exchange_rate * $rows['cost'] * 0.069, 2); echo $curr_symbol.$R_099 ;  ?>]</option>-->
					<?php } ?>                       
                  </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Enter Quantity </label>
                  <div class="col-sm-9">
                    <input type="number" name="quantity" required placeholder="Enter Quantity" style="width:100%;border-radius:5px!important;padding:5px;border-color:#888;"/>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Choose Payment Option </label>
                  <div class="col-sm-9">
                  <select name="gateway" id="gateway" class="form-control" style="width: 100%;">
					<?php global $server;
                           $ch=mysqli_query($server, "select * from paymentgateways where status = '1' order by id desc");
                            while ($rows = mysqli_fetch_assoc($ch)) {
                    ?>                    
                   			<option value="<?=$rows['id']?>"><?=$rows['name']?></option>
					<?php } ?>                       
                  </select>
                  </div>
                </div>

              </div>                  
              
              <div class="box-footer">
              
                <a href="index.php">
                  <button type="button" class="btn btn-warning pull-right" style="margin-right: 5px;">
                    <i class="fa fa-close"></i> Back
                  </button></a>
                 <?php
                if(check_cell_number_pending_due_ordervms(getUser()) == 0){
                ?>
                <button type="submit" name="review" class="btn btn-success">Continue</button>
                
                <?php
                }else{
                ?>
                <br><br>
                <div class="alert alert-danger">Please Complete the pending transactions before placing another one..</div>
                <?php
                }
                ?>
              </div>
            </form>
          </div>
           
        </div> 
        
<!-- Packages -->
		<div class="col-md-4">
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Our Packages</h3>
            </div>
            <div class="box-body" style="width: 90%; margin:0 auto;">
			<!-- List packages -->
 			<?php global $server;
                  $ch=mysqli_query($server, "select * from packages where type='vms' order by id desc");
                  while ($rows = mysqli_fetch_assoc($ch)) {
            ?>                    
                    <h3 class="text-primary"><?=$rows['name']?></h3>
                    <h4><b><?=$rows['quantity']?></b> Credits at <b><?=currencySymbul(getSetting('defaultCurrency')).$rows['cost']?><!--<?php $R_099 = round($exchange_rate * $rows['cost'] * 0.069, 2); echo $curr_symbol.$R_099 ;  ?>--></b></h4>
                    <hr />
			<?php } ?>             
            </div>
         </div>
       </div>     

       </div>
      </div>          
    </section>
<?php
}
?>

    <div class="clearfix"></div>