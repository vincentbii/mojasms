<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) { header('location: index.php'); } 

define('DATATABLE', true); 
if(isset($_GET['edit']) && !empty($_GET['edit'])) {
	global $server;
	$ch=mysqli_query($server, "select * from routing where id = '$_GET[edit]' limit 1");
	$rows = mysqli_fetch_assoc($ch);
}
?>
    <section class="content-header">
      <h1>
        Message Routing Rules
        <small>Setup which gateway is used for messages to various destinations</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> System Tools</a></li>
        <li class="active">Routing Settings</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	$sql=mysqli_query($server, "delete from routing where id='$_GET[delete]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected routing rule has been successfully deleted from your system.
              </div>    
     </div>         
    <?php
}   
if(isset($_POST['save'])){
	global $server;
	$user_id = getUser();
	if( ($_POST['is_voice'] > 0 && $_POST['type'] != 'Voice') || ($_POST['is_voice'] < 1 && $_POST['type'] != 'Text')) {
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Error!</h4>
                Sorry but the selected gateway cannot be used for <?=$_POST['type']?> routing. Please choose a gateway that is marked as  <?=$_POST['type']?>
              </div>    
     </div>         
    <?php		
	} else {
		$add = mysqli_query($server, "INSERT INTO routing (`gateway_id`, `destinations`, `type`) 
		VALUES ('$_POST[gateway_id]', '$_POST[destinations]', '$_POST[type]');") or die (mysql_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                Your new routing rule was successfully created.
              </div>    
     </div>         
    <?php
	}
}

if(isset($_POST['update'])){
	global $server;
	if( ($_POST['is_voice'] > 0 && $_POST['type'] != 'Voice') || ($_POST['is_voice'] < 1 && $_POST['type'] != 'Text')) {
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Error!</h4>
                Sorry but the selected gateway cannot be used for <?=$_POST['type']?> routing. Please choose a gateway that is marked as  <?=$_POST['type']?>
              </div>    
     </div>         
    <?php		
	} else {	
	mysqli_query($server, "update routing set `gateway_id` = '$_POST[gateway_id]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update routing set `type` = '$_POST[type]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update routing set `destinations` = '$_POST[destinations]' where id='$_POST[id]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected routing rule has been successfully updated.
              </div>    
     </div>         
    <?php
	}
}
?>     

        <div class="col-md-7">
               
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=routing" method="get">
                <div class="input-group input-group-sm" style="width: 350px;">
                <input type="hidden" name="url" value="routing" />
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Gateway</th>
                  <th>Destinations</th>
                  <th>Type</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$ch=mysqli_query($server, "select * from routing order by id desc limit 1000");
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "destinations LIKE '%$term%' OR type LIKE '%$term%' OR id LIKE '%$term%'";
	    } else {
	         $clauses[] = "type LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$ch=mysqli_query($server, "select * from routing where ".$filter);	
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
?>
                <tr>
                  <td><?php echo gatewayName($row['gateway_id']); ?></td>
                  <td><?php echo $row['destinations']; ?></td>
                  <td><?php echo $row['type']; ?></td>
                  <td >
                  	<div class="btn-group">
                        <a href="index.php?url=routing&edit=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                        <a href="index.php?url=routing&delete=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Gateway</th>
                  <th>Destinations</th>
                  <th>Type</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
        
		<div class="col-md-5">
          <div class="box box-primary">
         <?php if(!isset($_GET['edit'])) { ?>
            <div class="box-header with-border"><h4>Crete Routing Rule</h4></div>
            <?php } else { ?>
            <div class="box-header with-border"><h4>Update Routing Rule</h4></div>
            <?php } ?>

            <form class="form-horizontal" action="index.php?url=routing" method="post" role="form">           
            <div class="box-body" style="width: 90%; margin:0 auto;">
           		<div class="form-group">
                  <label for="type">Apply Rule for For</label>
                  <select name="type" id="type" class="form-control" style="width: 100%;">
                    <option <?php if(@$rows['type']=='Text') echo 'selected' ?>  value="Text">Text Messages</option> 
                    <option <?php if(@$rows['type']=='Voice') echo 'selected' ?>  value="Voice">Voice Messages</option>                    
                  </select>
                </div>
                
                <div class="form-group">
                  <label for="name">Select Route</label>
                  <select name="gateway_id" class="form-control select2" style="width: 100%;">
<?php global $server;
	$ch=mysqli_query($server, "select * from gateways order by id desc");
	while ($rows2 = mysqli_fetch_assoc($ch)) {
		if($rows2['is_voice'] > 0) { $for = 'Voice Gateway'; } else { $for = 'SMS Gateway';}
?>                    
                    <option <?php if(@$rows['gateway_id']==$rows2['id']) echo 'selected' ?> value="<?=$rows2['id']?>"><?=$rows2['name'].' ('.$for.')'?></option>
<?php } ?>                  
                  </select>
                </div>
                
                <div class="form-group">
                  <label for="message">List Destinations</label>
                  <textarea class="form-control" rows="6" id="messageText" name="destinations" placeholder="Type destination prefix here, separating each with a comma. Eg. 23470,44792,92334"><?=@$rows['destinations']?></textarea>                  
                </div>    
                
            </div>

            <div class="box-footer">
            <input type="hidden" name="is_voice" value="<?=$rows2['is_voice']?>" />
              <button type="reset" class="btn btn-danger">Reset Form</button>
               <?php if(!isset($_GET['edit'])) { ?>
              <button type="submit" name="save" class="btn btn-success">Create Routing Rule</button>
              <?php } else { ?>
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update Routing Rule</button>
              <?php }?>
            </div>
            </form>
                          
        </div>
       </div> 
       
       </div>            
    </section>