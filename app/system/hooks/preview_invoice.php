<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

global $server;

$id=$_GET['tid'];
$ch = mysqli_query($server, "SELECT * FROM transactions WHERE id='$id'");
$rows = mysqli_fetch_assoc($ch);
$date = $rows['date'];
$amount = $rows['amount'];
$referrence = $rows['reference'];
$package_id = $rows['package_id'];
$virtual_number_id = $rows['virtual_number_id'];
$cellphone_id = $rows['cellphone_id'];
$gateway = $rows['gateway'];

$chh=mysqli_query($server, "select * from paymentgateways where id = '$gateway' limit 1");
$rows = mysqli_fetch_assoc($chh);
$image = $rows['image'];
$alias = $rows['alias'];
$text = $rows['text'];

?>
 <section class="content-header">
  <h1>
    Invoice
    <small>Transaction Invoice.</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">View Invoice</li>
  </ol>
</section>
<section class="invoice">
    <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Invoice <img src="/logo.jpeg" class="img-responsive" width="150px"/>
            <small class="pull-right">Date: <?= date('d/m/Y', strtotime($date))?></small>
          </h2>
        </div>
      </div>
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong><?=getSetting('businessName')?></strong><br>
            Email: <?=getSetting('emailSender')?><br>
            Mobile: +27 79 479 4811<br>
            Website: www.mojasms.com<br>
            VAT: <?=currencySymbul(getSetting('defaultCurrency')).round($amount)?>
          </address>
        </div>

        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?=userData('name', getUser())?></strong><br>
            Phone: <?=userData('phone', getUser())?><br>
            Email: <?=userData('email', getUser())?>
          </address>
        </div>
        
        <div class="col-sm-4 invoice-col">
          <b>Invoice #<?=$referrence?></b><br>
          <br>
          
          <b>Payment Due:</b> <?= date('d/m/Y', strtotime($date))?><br>
          <b>Amount:</b> <?=currencySymbul(getSetting('defaultCurrency')).number_format($amount,2)?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Item</th>
              <th>Quantity</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>
              <?php 
               if(!empty($package_id)){
                   echo packageName($package_id);
               }else if(!empty($cellphone_id)){
                   echo 'Virtual Number';
               }else if(!empty($virtual_number_id)){
                   echo 'Cell Number';
               }
              ?>
              </td>
              <td>
               <?php 
               if(!empty($package_id)){
                   echo packageQuantity($package_id);
               }else if(!empty($cellphone_id)){
                   echo virtual_numbers($package_id)['number'];
               }else if(!empty($virtual_number_id)){
                   echo cell_numbers($virtual_number_id)['number'];
               }
              ?>
              </td>
              <td><?=currencySymbul(getSetting('defaultCurrency')).number_format($amount, 2)?></td>
            </tr>
            </tbody>
          </table>
        </div>
        <div class="row">
        <div class="col-md-6">
          <p class="lead">Payment Methods:</p>
          
          <img src="system/includes/gateway/<?=$alias?>/<?=$alias?>.png" alt="<?=$rows['name']?>" style="height: 40px;">
          <h4><strong><?=$rows['name']?></strong> </h4>
          
		<?php if(!empty($text)) { ?>	
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            <?=$text?> <br><span style="color:red;"><b> Reference No:</b> <?=$referrence?></span>
          </p>
        <?php } ?>  <br>
        </div>

        <div class="col-md-6">
          <p class="lead">Amount Due <?= date('d/m/Y', strtotime($date))?></p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td><?=currencySymbul(getSetting('defaultCurrency')).round($amount)?></td>
              </tr>
              <tr>
                <th style="width:50%">VAT:</th>
                <td><?=currencySymbul(getSetting('defaultCurrency')).round($amount)?></td>
              </tr>
              <tr>
                <th>Total:</th>
                <td><?=currencySymbul(getSetting('defaultCurrency')).round($amount)?></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      </div>
</section>