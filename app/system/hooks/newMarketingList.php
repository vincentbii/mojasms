<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//if(!isAdmin(getUser())) { header('location: index.php'); } 

define('DATATABLE', true); 
if(isset($_GET['edit']) && !empty($_GET['edit'])) {
	global $server;
	if(!isAdmin(getUser())) {
		$user_id = getUser();
		$ch=mysqli_query($server, "select * from marketinglists where id = '$_GET[edit]' and customer_id = '$user_id' limit 1");
	}
	if(mysqli_num_rows($ch) < 1) {
		header('location: index.php?url=marketingList');	
	}	
	$ch=mysqli_query($server, "select * from marketinglists where id = '$_GET[edit]' limit 1");
	$rows = mysqli_fetch_assoc($ch);
}
if(isset($_GET['edit'])) { 
?>
    <section class="content-header">
      <h1>
        Edit Marketing List
        <small>Edit selected marketing list</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="index.php?url=marketingList"><i class="fa fa-feed"></i> Marketing List</a></li>
        <li class="active">Edit Marketing List</li>
      </ol>
    </section>
<?php } else { ?>
    <section class="content-header">
      <h1>
        Create Marketing List
        <small>Create new marketing list</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="index.php?url=marketingList"><i class="fa fa-feed"></i> Marketing List</a></li>
        <li class="active">New Marketing List</li>
      </ol>
    </section>
<?php } ?>
    <section class="content">
    	<div class="row">
<?php    
if(isset($_POST['save'])){
	foreach ($_POST as $key => $value ){
		${$key} = $value = str_replace('\'', '', $value);
		$rows{$key} = $value = str_replace('\'', '', $value);
	}
	
	$error = 0;
	if(!keywordAvailable($optin_keyword,$short_code)) {
		$error = 1;	
		$warning = 'Sorry but the opt-in keyword <strong>'.$optin_keyword.'</strong> is very similar to a keyword currently being used by another list sharing same short-code.';
	}
	if(!keywordAvailable($help_keyword,$short_code)) {
		$error = 1;	
		$warning = 'Sorry but the help keyword <strong>'.$help_keyword.'</strong> is very similar to a keyword currently being used by another list sharing same short-code.';
	}
	if(!keywordAvailable($optout_keyword,$short_code)) {
		$error = 1;	
		$warning = 'Sorry but the opt-out keyword <strong>'.$optout_keyword.'</strong> is very similar to a keyword currently being used by another list sharing same short-code.';
	}		
	
	if($error < 1) {
		global $server;
		$user_id = getUser();
		$date = date('Y-m-d');
		$add = mysqli_query($server, "INSERT INTO marketinglists (`name`, `date`, `optin_keyword`,`optout_keyword`, `help_keyword`, `optin_response`,`optout_response`, `help_response`, `short_code`, `sender_id`, `customer_id`) 
		VALUES ('$name', '$date', '$optin_keyword','$optout_keyword', '$help_keyword', '$optin_response','$optout_response', '$help_response', '$short_code', '$sender_id', '$customer_id');") or die (mysql_error($server));
		//create activity 
		$activity = userName(getUser()).' created a new list '.$name;	
		$date = date('Y-m-d H:i:s');
		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));		
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                Your new marketing list was successfully created. <a href="index.php?url=viewContact&view=<?=getInsertedID('marketinglists')?>"><strong>Click Here</strong></a> to manage subscribers now.
              </div>    
     </div>         
    <?php
	} else {
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Error!</h4>
                <?=$warning;?>.
              </div>    
     </div>         
    <?php		
	}
}
if(isset($_POST['update'])){
	global $server;
	if(!isAdmin(getUser())) {
		$user_id = getUser();
		$ch=mysqli_query($server, "select * from marketinglists where id = '$_POST[id]' and customer_id = '$user_id' limit 1");
	}
	if(mysqli_num_rows($ch) < 1) {
		header('location: index.php?url=marketingList');	
	}	
	foreach ($_POST as $key => $value ){
		${$key} = $value = str_replace('\'', '', $value);
	}	

	$error = 0;
	if(!keywordAvailable($optin_keyword,$short_code,$id)) {
		$error = 1;	
		$warning = 'Sorry but the opt-in keyword <strong>'.$optin_keyword.'</strong> is very similar to a keyword currently being used by another list sharing same short-code.';
	}
	if(!keywordAvailable($help_keyword,$short_code,$id)) {
		$error = 1;	
		$warning = 'Sorry but the help keyword <strong>'.$help_keyword.'</strong> is very similar to a keyword currently being used by another list sharing same short-code.';
	}
	if(!keywordAvailable($optout_keyword,$short_code,$id)) {
		$error = 1;	
		$warning = 'Sorry but the opt-out keyword <strong>'.$optout_keyword.'</strong> is very similar to a keyword currently being used by another list sharing same short-code.';
	}		
	
	if($error < 1) {	
		mysqli_query($server, "update marketinglists set `name` = '$name' where id='$_POST[id]'")or die(mysqli_error($server));
		mysqli_query($server, "update marketinglists set `short_code` = '$short_code' where id='$_POST[id]'")or die(mysqli_error($server));
		mysqli_query($server, "update marketinglists set `optout_keyword` = '$optout_keyword' where id='$_POST[id]'")or die(mysqli_error($server));
		mysqli_query($server, "update marketinglists set `optin_keyword` = '$optin_keyword' where id='$_POST[id]'")or die(mysqli_error($server));
		mysqli_query($server, "update marketinglists set `help_keyword` = '$help_keyword' where id='$_POST[id]'")or die(mysqli_error($server));
		mysqli_query($server, "update marketinglists set `optout_response` = '$optout_response' where id='$_POST[id]'")or die(mysqli_error($server));
		mysqli_query($server, "update marketinglists set `optin_response` = '$optin_response' where id='$_POST[id]'")or die(mysqli_error($server));
		mysqli_query($server, "update marketinglists set `help_response` = '$help_response' where id='$_POST[id]'")or die(mysqli_error($server));
		mysqli_query($server, "update marketinglists set `sender_id` = '$sender_id' where id='$_POST[id]'")or die(mysqli_error($server));
		mysqli_query($server, "update marketinglists set `customer_id` = '$customer_id' where id='$_POST[id]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected marketing list has been successfully updated. <a href="index.php?url=viewContact&view=<?=$id?>"><strong>Click Here</strong></a> to manage subscribers now.
              </div>    
     </div>         
    <?php
	} else {
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Error!</h4>
                <?=$warning;?>.
              </div>    
     </div>         
    <?php		
	}	
}
?>     

		<div class="col-md-7">
          <div class="box box-primary">
         <?php if(!isset($_GET['edit'])) { ?>
            <div class="box-header with-border"><h4>Crete New Marketing List</h4></div>
            <?php } else { $rows['short_code'] = userData('short_code', getUser()); ?>
            <div class="box-header with-border"><h4>Update <?=$rows['name']?></h4></div>
            <?php } ?>

            <form class="form-horizontal" action="index.php?url=newMarketingList" method="post" role="form">           
            <div class="box-body" style="width: 90%; margin:0 auto;">
           		<div class="form-group">
                  <label for="type">List Type</label>
                  <select name="type" id="type" class="form-control" style="width: 100%;">
                    <option  value="Text">Text & Voice List</option>                    
                  </select>
                </div>
	<?php $user_id = getUser();
	if(isAdmin(getUser())) {
	?>	
                <div class="form-group">
                  <label for="customer_id">Assign to Client</label>
                  <select name="customer_id" class="form-control select2" style="width: 100%;">
                    <option <?php if(@$rows['customer_id']==0) echo 'selected' ?> value="0">Do not assign to a client</option>
<?php global $server;
	$ch=mysqli_query($server, "select * from users where is_customer = 1 order by id desc");
	while ($rows2 = mysqli_fetch_assoc($ch)) {
?>                    
                    <option <?php if(@$rows['customer_id']==$rows2['id']) echo 'selected' ?> value="<?=$rows2['id']?>"><?=$rows2['name']?></option>
<?php } ?>                  
                  </select>
                </div>
    <?php } else { ?>
    			<input type="hidden" name="customer_id" value="<?=$user_id?>" />
    <?php } ?>                            
                <div class="form-group">
                  <label for="name">List Name</label>
                  <input type="text" required class="form-control" id="name" name="name" value="<?=@$rows['name'] ?>" placeholder="Name your new list">
                </div>
	<?php $user_id = getUser();
	if(isAdmin(getUser())) {
	?>
                <div class="form-group">
                  <label for="name">Assign Incoming SMS Short-Code</label>
                  <input type="number" class="form-control" id="short_code" name="short_code" value="<?=@$rows['short_code'] ?>" placeholder="Number to be used for incoming messages. Include country code if applicable">
                </div>
 <?php } else { ?>
    			 <div class="form-group">
                  <label for="name">Assigned Incoming SMS Short-Code</label>
                  <input readonly="readonly" type="number" class="form-control" id="short_code" name="short_code" value="<?=@$rows['short_code'] ?>" placeholder="">
                </div>
    <?php } ?>                 
                <div class="form-group">
                  <label for="name">Default SMS Sender</label>
                  <input type="text" required class="form-control" id="sender_id" name="sender_id" value="<?=@$rows['sender_id'] ?>" placeholder="This is very important for automated responses">
                </div>
                
                <div class="form-group">
                  <label for="name">Opt-In Keyword</label>
                  <input type="text" required class="form-control" id="optin_keyword" name="optin_keyword" value="<?=@$rows['optin_keyword'] ?>" placeholder="Keyword to use in opt-in SMS">
                </div>                
                
                <div class="form-group">
                  <label for="message">Opt-in Response</label>
                  <textarea onBlur="count2(this,this.form.countBox2,1000);" onKeyUp="count2(this,this.form.countBox2,1000);"  class="form-control" rows="6" id="messageText" name="optin_response" placeholder="Type your message text here... You can dynamically insert recipient's phone number in your message using [NUMBER]"><?=@$rows['optin_response']?></textarea>
                  <p class="help-block text-blue" id="countBox2">0 Characters Used</p>

<script>
//count message lenght 
	function count2(field,countfield,maxlimit) {
	if (field.value.lenght > 1000) {
		field.value = field.value.substring(0,1000);
		field.blur();
		return false;
	} else {
		var smslenght = 160;
		if(field.value.length > 160){
			var smslenght = 145;
		}
		var pages = field.value.length /smslenght;
			if(pages < 1) { var page = '1';	}
			if(pages == 1) { var page = '1';}
			if(pages > 1) { var page = '2';	}
			if(pages > 2) {	var page = '3';	}
			if(pages > 3) {	var page = '4';	}
			if(pages > 4) {	var page = '5';	}
			if(pages > 5) {	var page = '6';	}
			if(pages > 6) {	var page = '7';	}
															
		document.getElementById('countBox2').innerHTML = field.value.length + " of 1000 Characters Used ("+page+" SMS)";
		}
	}
</script>                   
                </div>    

                <div class="form-group">
                  <label for="name">Opt-Out Keyword</label>
                  <input type="text" required class="form-control" id="optout_keyword" name="optout_keyword" value="<?=@$rows['optout_keyword'] ?>" placeholder="Keyword to use in opt-out SMS">
                </div>                
                
                <div class="form-group">
                  <label for="message">Opt-Out Response</label>
                  <textarea onBlur="count3(this,this.form.countBox3,1000);" onKeyUp="count3(this,this.form.countBox3,1000);"  class="form-control" rows="6" id="messageText3" name="optout_response" placeholder="Type your message text here... You can dynamically insert recipient's phone number in your message using [NUMBER]"><?=@$rows['optout_response']?></textarea>
                  <p class="help-block text-blue" id="countBox3">0 Characters Used</p>

<script>
//count message lenght 
	function count3(field,countfield,maxlimit) {
	if (field.value.lenght > 1000) {
		field.value = field.value.substring(0,1000);
		field.blur();
		return false;
	} else {
		var smslenght = 160;
		if(field.value.length > 160){
			var smslenght = 145;
		}
		var pages = field.value.length /smslenght;
			if(pages < 1) { var page = '1';	}
			if(pages == 1) { var page = '1';}
			if(pages > 1) { var page = '2';	}
			if(pages > 2) {	var page = '3';	}
			if(pages > 3) {	var page = '4';	}
			if(pages > 4) {	var page = '5';	}
			if(pages > 5) {	var page = '6';	}
			if(pages > 6) {	var page = '7';	}
															
		document.getElementById('countBox3').innerHTML = field.value.length + " of 1000 Characters Used ("+page+" SMS)";
		}
	}
</script>                   
                </div>    


                <div class="form-group">
                  <label for="name">Help Keyword</label>
                  <input type="text" required class="form-control" id="help_keyword" name="help_keyword" value="<?=@$rows['help_keyword'] ?>" placeholder="Keyword to use in help SMS">
                </div>                
                
                <div class="form-group">
                  <label for="message">Help Response</label>
                  <textarea onBlur="count4(this,this.form.countBox4,1000);" onKeyUp="count4(this,this.form.countBox4,1000);"  class="form-control" rows="6" id="messageText4" name="help_response" placeholder="Type your message text here... You can dynamically insert recipient's phone number in your message using [NUMBER]"><?=@$rows['help_response']?></textarea>
                  <p class="help-block text-blue" id="countBox4">0 Characters Used</p>

<script>
//count message lenght 
	function count4(field,countfield,maxlimit) {
	if (field.value.lenght > 1000) {
		field.value = field.value.substring(0,1000);
		field.blur();
		return false;
	} else {
		var smslenght = 160;
		if(field.value.length > 160){
			var smslenght = 145;
		}
		var pages = field.value.length /smslenght;
			if(pages < 1) { var page = '1';	}
			if(pages == 1) { var page = '1';}
			if(pages > 1) { var page = '2';	}
			if(pages > 2) {	var page = '3';	}
			if(pages > 3) {	var page = '4';	}
			if(pages > 4) {	var page = '5';	}
			if(pages > 5) {	var page = '6';	}
			if(pages > 6) {	var page = '7';	}
															
		document.getElementById('countBox4').innerHTML = field.value.length + " of 1000 Characters Used ("+page+" SMS)";
		}
	}
</script>                   
                </div>    
                
            </div>

            <div class="box-footer">
              <button type="reset" class="btn btn-danger">Reset Form</button>
               <?php if(!isset($_GET['edit'])) { ?>
              <button type="submit" name="save" class="btn btn-success">Create List</button>
              <?php } else { ?>
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update List</button>
              <?php }?>
            </div>
            </form>
                          
        </div>
       </div> 


		<div class="col-md-5">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Recent Marketing Lists</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="templates" class="table table-bordered table-hover">
                <tbody>
<?php 
global $server;
$user_id = getUser();
$n = 0;
	$ch=mysqli_query($server, "select * from marketinglists order by id desc limit 20");
	$ch=mysqli_query($server, "select * from marketinglists where customer_id = '$user_id' order by id desc limit 20");
		if(isAdmin(getUser())) { 
		$ch=mysqli_query($server, "select * from marketinglists order by id desc limit 20");
		}
	while ($row = mysqli_fetch_assoc($ch)) {
?>                 
                <tr>
                  <td><?=$row['name'].' &nbsp;&nbsp;&nbsp;<b>('.countSubscribers($row['id']); ?> contacts)</b> </td>
                </tr>          
<?php } ?>                
              </tbody>
             </table>
           </div>
         </div>
             
		</div>    
    </section>