<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) {
	header('location: index.php');
}
define('DATATABLE', true); 
if(isset($_GET['edit']) && !empty($_GET['edit'])) {
	global $server;
	$ch=mysqli_query($server, "select * from cell_numbers where id = '$_GET[edit]' limit 1");
	$rows = mysqli_fetch_assoc($ch);
}
?>
    <section class="content-header">
      <h1>
        Cellphone Number
        <small>View and manage all your cellphone Number</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Cellphone Numbers</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	$sql=mysqli_query($server, "delete from cell_numbers where id='$_GET[delete]'")or die(mysqli_error($server));		
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected Cellphone Number has been successfully deleted from your system.
              </div>    
     </div>         
    <?php
}

if(isset($_POST['save'])){
	global $server;
		$user_id = getUser();
		$add = mysqli_query($server, "INSERT INTO cell_numbers (`number`, `price`, `status`) 
		VALUES ('$_POST[number]', '$_POST[price]', '0');") or die (mysql_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                Your new Cellphone number was successfully created.
              </div>    
     </div>         
    <?php
}

if(isset($_POST['update'])){
	global $server;
	mysqli_query($server, "update cell_numbers set `number` = '$_POST[number]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update cell_numbers set `price` = '$_POST[price]' where id='$_POST[id]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected cellphone number has been successfully updated.
              </div>    
     </div>         
    <?php
}
?>     
        <div class="col-md-7">
               
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=managecellphonenumbers" method="get">
                <div class="input-group input-group-sm" style="width: 350px;">
                <input type="hidden" name="url" value="managecellphonenumbers" />
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Number</th>
                  <th>Price</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$user_id = getUser();

	$ch=mysqli_query($server, "select * from cell_numbers order by id desc limit 1000");
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "number LIKE '%$term%' OR price LIKE '%$term%' OR id LIKE '%$term%'";
	    } else {
	         $clauses[] = "number LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$user_id = getUser();
		$ch=mysqli_query($server, "select * from cell_numbers where ".$filter);	
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
?>
                <tr>
                  <td><?php echo $row['number']; ?></td>
                  <td><?php echo currencySymbul(getSetting('defaultCurrency')).$row['price']; ?></td>
                  <!--<td><?php  $R_099 = round($_SESSION[exchange_rate] * $row['price'] * 0.069,2); echo $_SESSION[curr_symbol].$R_099 ;  ?></td>-->
                  <td >
                  	<div class="btn-group">
                        <a title="Edit" href="index.php?url=managecellphonenumbers&edit=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                        <a title="Delete" href="index.php?url=managecellphonenumbers&delete=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Number</th>
                  <th>Price</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
 

		<div class="col-md-5">
          <div class="box box-primary">
         <?php if(!isset($_GET['edit'])) { ?>
            <div class="box-header with-border"><h4>Create New</h4></div>
            <?php } else { ?>
            <div class="box-header with-border"><h4>Update <?=$rows['number']?></h4></div>
            <?php } ?>

            <form class="form-horizontal" action="index.php?url=managecellphonenumbers" method="post" role="form">           
            <div class="box-body" style="width: 90%; margin:0 auto;">
                
                <div class="form-group">
                  <label for="name">Number Units</label>
                  <input type="text" required class="form-control" id="number" name="number" value="<?=@$rows['number'] ?>" placeholder="Enter your new cellphone  number">
                </div>

                <div class="form-group">
                  <label for="cost">Price (<?=currencySymbul(getSetting('defaultCurrency'))?>)</label>
                  <input type="text" required class="form-control" id="price" name="price" value="<?=@$rows['price'] ?>" placeholder="Price in <?=currencySymbul(getSetting('defaultCurrency'))?>">
                </div>
                
            </div>

            <div class="box-footer">
              <button type="reset" class="btn btn-danger">Reset Form</button>
               <?php if(!isset($_GET['edit'])) { ?>
              <button type="submit" name="save" class="btn btn-success">Create Cellphone Number</button>
              <?php } else { ?>
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update Cellphone Number</button>
              <?php }?>
            </div>
            </form>
                          
        </div>
       </div> 
                     
      </div>      
    
    </section>