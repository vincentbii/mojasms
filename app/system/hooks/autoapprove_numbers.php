<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('DATATABLE', true); 
if(!isAdmin(getUser())) { header('location: index.php'); } 
?>
    <section class="content-header">
      <h1>
        Set Auto Approve Numbers
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Set Auto Approve Numbers</a></li>
        <li class="active">Set Numbers</li>
      </ol>
    </section>

    <section class="content">
    <div class="row">


<style>
/* Dotted red border */
hr {
  border-top: 1px dotted red;
}
</style>
<?php 

global $server;


if(isset($_POST['numbers'])){ $numbers=$_POST['numbers'];

    
    $sql="UPDATE `auto_sms_numbers` SET numbers='$numbers' WHERE id='".$_GET['edit']."'";
		           
if(mysqli_query($server, $sql)){
		          
?>
<div class="col-md-12">
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Cool</h4>
    Numbers updated successfully 
  </div>    
 </div> 
<?php
		           }else{
?>
<div class="col-md-12">
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Error!</h4>
    Try to update once again. 
  </div>    
 </div> 
<?php
	}
}


if(isset($_GET['edit'])){
?>


       <form class="form-vertial" action="index.php?url=autoapprove_numbers&edit=<?php echo $_GET['edit'];?>" method="post" role="form" enctype="multipart/form-data">           
		<div class="col-md-12">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Update Auto Approve Numbers</h4></div>
            
                <div class="box-body">
                    <div class="form-group">
                      <label>Numbers (use comma , to separate the numbers. Do not include spaces e.g 123 456 234)</label>
                      
                      <?php 
                      
                          $chu=mysqli_query($server, "select * from auto_sms_numbers WHERE id='".$_GET['edit']."'");
                          $rower = mysqli_fetch_assoc($chu);
                  	      $numbers=$rower['numbers'];
              	      
                      ?>
                      <textarea class="form-control" name="numbers" required rows="6" placeholder="Enter Numbers"><?php echo $numbers;?></textarea>
                    </div> 
                </div>
                 <div class="box-footer">
                  <button type="submit" name="save" class="btn btn-success">Update</button>
                </div>
                
          </div>
        </div>
       </form>
<?php
}
?>
     </div>
     <div class="row">
         <div class="col-md-12">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Auto Approve Numbers</h4></div>
            
                <div class="box-body">
                          <table id="example1" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>Numbers</th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                   <?php 
                                      $i=1;
                                      
                                      $ch=mysqli_query($server, "select * from auto_sms_numbers");
                                      	while ($row = mysqli_fetch_assoc($ch)) {
                                      ?>
                                      <tr>
                                          <td><?php echo $i++;?></td>
                                          <td><?php echo $row['numbers'];?></td>
                                          <td>
                                              <a href="index.php?url=autoapprove_numbers&edit=<?php echo $row['id'];?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                          </td>
                                      </tr>
                                      <?php
                                      	}
                                      ?>
                              </tbody>
                          </table>
                </div>
          </div>
        </div> 
     </div>
    </section>













