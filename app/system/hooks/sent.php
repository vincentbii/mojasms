<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//if(!isAdmin(getUser())) { header('location: index.php'); } 
define('DATATABLE', true);

?>
    <section class="content-header">
      <h1>
        Sent Message Log
        <small>View all sent message log</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Message Log</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	if(isAdmin(getUser())) {
	$sql=mysqli_query($server, "delete from sentmessages where id='$_GET[delete]'")or die(mysqli_error($server));
	}
	//show mesage
	?>
    <div class="col-xs-12">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected message has been successfully deleted from your message log.
              </div>    
     </div>         
    <?php
}
?>     
        <div class="col-xs-12">
               
          <div class="box"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=sent" method="get">
                <input type="hidden" name="url" value="sent" />
                <div class="input-group input-group-sm" style="width: 350px;">
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th>
                  <th>Date</th>
                  <th>To</th>
                  <th>Message</th>
                  <?php if(isAdmin(getUser())) { ?>
                  <th>Route</th>
                  <?php } ?>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$user_id = getUser();
	$ch=mysqli_query($server, "select * from sentmessages where customer_id = '$user_id' order by id desc limit 1000");
	if(isAdmin(getUser())) {
	$ch=mysqli_query($server, "select * from sentmessages order by id desc limit 1000");	
	}
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']); 
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "recipient LIKE '%$term%' OR message LIKE '%$term%' OR status LIKE '%$term%' OR date LIKE '%$term%'";
	    } else {
	         $clauses[] = "recipient LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$user_id = getUser();
	$ch=mysqli_query($server, "select * from sentmessages where customer_id = '$user_id' and ".$filter);	
	if(isAdmin(getUser())) {
	$ch=mysqli_query($server, "select * from sentmessages where ".$filter);		
	}
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
		if(!empty($row['error'])) {
			$error = '(<span class="text-red">'.$row['error'].'</span>)';
		} else {
			$error = '';	
		}
		$message = $row['message'];
		if(empty($message)) {
			$message = '- Message Text Not Available -';	
		}
?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $row['date']; ?></td>
                  <td><?php echo $row['recipient']; ?></td>
                  <td><?php echo $message; ?></td>
                  <?php if(isAdmin(getUser())) { ?>
                  <td><?php echo gatewayName($row['gateway_id']); ?></td>
                  <?php } ?>
                  <td><?php echo $row['status']; ?> <?=$error?></td>
                  <td>
                  	<div class="btn-group">
                     <?php if(isAdmin(getUser())) { ?>
                        <a href="index.php?url=sent&delete=<?php echo $row['id'];?>" onclick="confirm('Are you sure you want to delete this message?');">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                     <?php } ?>   
                	</div>
                  </td>

<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S/N</th>
                  <th>Date</th>
                  <th>To</th>
                  <th>Message</th>
                  <?php if(isAdmin(getUser())) { ?>
                  <th>Route</th>
                  <?php } ?>
                  <th>Status</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>

      </div>   
    
    </section>