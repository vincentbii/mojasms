<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('DATATABLE', true);
?>
    <section class="content-header">
      <h1>
        Cell Numbers List
        <small>Cell Numbers to downloads</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Cell Numbers List</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
   
        <div class="col-md-12">
               
          <div class="box box-success">
         
            <div class="box-header">Cell Numbers to downloads</div>
           
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                      <th>S/N</th>
                      <th>Date Purchased</th>
                      <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php global $server; $i=1;
                          $ch=mysqli_query($server, "select * from purchased_cell_numbers group by transaction_id order by id asc");
                          while ($rows = mysqli_fetch_assoc($ch)) {
                    ?>  
                    <tr>
                      <td><?php echo 'CELL_NUMBER#00'.$i++;?></td>
                      <td><?php echo $rows['date'];?></td>
                      <td>
                          <a href="index.php?url=transaction_cell_number_download&transid=<?php echo $rows['transaction_id'];?>" class="btn btn-success"><i class="fa fa-download"></i> Download</a>
                      </td>
                    </tr>
                    <?php
                          }
                    ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        
    </section>
