<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) {
	header('location: index.php');
}
define('DATATABLE', true); 
if(isset($_GET['edit']) && !empty($_GET['edit'])) {
	global $server;
	$ch=mysqli_query($server, "select * from paymentgateways where id = '$_GET[edit]' limit 1");
	$rows = mysqli_fetch_assoc($ch);
}
?>
    <section class="content-header">
      <h1>
        Payment Gateways
        <small>View and manage your payment gateways</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Payment Gateway</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	if($_GET['delete'] > 7) {
	$sql=mysqli_query($server, "delete from paymentgateways where id='$_GET[delete]'")or die(mysqli_error($server));		
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected payment gateway has been successfully deleted from your system.
              </div>    
     </div>         
    <?php
	} else {
	?>
    <div class="col-md-12">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Sorry!</h4>
                You cannot delete this payment gateways. 
              </div>    
     </div>         
    <?php		
	}
}

if(isset($_POST['save'])){
	global $server;
	$image = home_base_url().'files/custom.png';
	if(file_exists($_FILES['image']['tmp_name']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
		$upload_path = 'files/';
		$file = $_FILES['image']['name'];		
		$ext = end(explode(".", $_FILES['image']['name']));
		$filename = time().$_FILES['image']['name'];
		if(move_uploaded_file($_FILES['image']['tmp_name'],$upload_path . $filename)) {
			$image = $filename;	
		} else {
			$image = 'custom.png';
		}
	}
	$alias = str_replace(' ','_',$_POST['name']);	
	$user_id = getUser();
	$add = mysqli_query($server, "INSERT INTO `paymentgateways` (`name`, `username`, `alias`, `status`, `custom`, `text`, `image`, `url`, `param1`) VALUES ('$_POST[name]', '', '$alias', 0, 1, '$_POST[text]', '$image', '','');") or die (mysql_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                Your new payment gateway was successfully created.
              </div>    
     </div>         
    <?php
}

if(isset($_POST['update'])){
	global $server;
	mysqli_query($server, "update paymentgateways set `name` = '$_POST[name]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update paymentgateways set `username` = '$_POST[username]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update paymentgateways set `text` = '$_POST[text]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update paymentgateways set `param1` = '$_POST[param1]' where id='$_POST[id]'")or die(mysqli_error($server));
	if(file_exists($_FILES['image']['tmp_name']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
		$upload_path = 'files/';
		$file = $_FILES['image']['name'];		
		$ext = end(explode(".", $_FILES['image']['name']));
		$filename = time().$_FILES['image']['name'];
		if(move_uploaded_file($_FILES['image']['tmp_name'],$upload_path . $filename)) {
			$image = $filename;	
			mysqli_query($server, "update paymentgateways set `image` = '$image' where id='$_POST[id]'")or die(mysqli_error($server));
		} 
	}

	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected payment gateway has been successfully updated.
              </div>    
     </div>         
    <?php
}

if(isset($_GET['activate'])){
	global $server;
	mysqli_query($server, "update paymentgateways set `status` = '1' where id = '$_GET[activate]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected payment gateway has been successfully enabled and will now be available on order page.
              </div>    
     </div>         
    <?php
}

if(isset($_GET['deactivate'])){
	global $server;
	mysqli_query($server, "update paymentgateways set `status` = '0' where id = '$_GET[deactivate]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected payment gateway has been successfully disabled and will no-longer be available on order page.
              </div>    
     </div>         
    <?php
}

?>     
        <div class="col-md-7">
               
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=paymentgateway" method="get">
                <div class="input-group input-group-sm" style="width: 350px;">
                <input type="hidden" name="url" value="paymentgateway" />
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Gateway Name</th>
                  <th>Type</th>
                  <th>Change Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$user_id = getUser();

	$ch=mysqli_query($server, "select * from paymentgateways order by id desc limit 1000");
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "name LIKE '%$term%' OR text LIKE '%$term%' OR id LIKE '%$term%'";
	    } else {
	         $clauses[] = "name LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$user_id = getUser();
		$ch=mysqli_query($server, "select * from paymentgateways where ".$filter);	
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
		if($row['custom'] > 0) {
			$type = 'Custom Gateway';
		} else {
		$type = 'System gateway';
	}
?>
                <tr>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo $type; ?></td>
                  <td>
                        <?php if($row['status'] > 0) { ?>
                        <a title="Disable" href="index.php?url=paymentgateway&deactivate=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-danger btn-sm">&times; Deactivate</button></a>
                        <?php } else {?>
                        <a title="Enable" href="index.php?url=paymentgateway&activate=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Activate</button></a>
                        <?php } ?>                  
                  </td>
                  <td >
                  	<div class="btn-group">
                        <a title="Edit" href="index.php?url=paymentgateway&edit=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                        <?php if($row['id'] > 7) { ?>
                        <a title="Delete" href="index.php?url=paymentgateway&delete=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                        <?php }?>
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Gateway Name</th>
                  <th>Type</th>
                  <th>Change Status</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
 

		<div class="col-md-5">
          <div class="box box-primary">
         <?php if(!isset($_GET['edit'])) { ?>
            <div class="box-header with-border"><h4>Create New Custom Gateway</h4></div>
            <?php } else { ?>
            <div class="box-header with-border"><h4>Update <?=$rows['name']?></h4></div>
            <?php } ?>
<?php
$field1 = 'Gatewau User ID';
$field2 = '';
if($row['id'] == 1) {
	$field1 = 'WebPay Merchant ID';
	$field2 = 'WebPay MAC Key';
}
if($row['id'] == 2) {
	$field1 = 'GTPay Merchant ID';
	$field2 = 'GTPay Hatch Key';
}
if($row['id'] == 3) {
	$field1 = 'Simplepay Public Key';
	$field2 = 'Simplepay Private Key';
}
if($row['id'] == 4) {
	$field1 = 'Paypal Email';
	$field2 = '';
}
if($row['id'] == 5) {
	$field1 = '2Checkout User ID';
	$field2 = '2Checkout Secret Word';
}
if($row['id'] == 6) {
	$field1 = 'Quickteller Payment Code';
	$field2 = 'Quickteller Secrete';
}
if($row['id'] == 7) {
	$field1 = 'Stripe Private Key';
	$field2 = 'Stripe Public Key';
}
?>
            <form class="form-horizontal" action="index.php?url=paymentgateway" method="post" role="form">           
            <div class="box-body" style="width: 90%; margin:0 auto;">
            <?php if(!isset($_GET['edit']) || $rows['custom'] > 0) { ?>    
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" required class="form-control" id="name" name="name" value="<?=@$rows['name'] ?>" placeholder="Name your new package">
                </div>

                <div class="form-group">
                  <label for="text">Payment Instruction</label>
                  <textarea class="form-control" rows="6" id="text" name="text" placeholder="Type some text to be displayed to your clients when to opt to pay with this gateway."><?=$rows['text']?></textarea>
                </div>                

                <div class="form-group">
                  <label for="image">Gateway Image)</label>
                 <input type="file" id="imaage" name="image" accept="image/*">
                  <p class="help-block text-light-blue">Upload an image file.</p>
                </div>
                <input type="hidden" name="param1" value="" />
             <?php } else { ?>
             <p>Need a new merchant account? <a href="<?=$rows['url']?>" target="_blank">Create one here</a></p>
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" required class="form-control" id="name" name="name" value="<?=@$rows['name'] ?>" placeholder="Name your new package">
                </div>
                
                <div class="form-group">
                  <label for="name"><?=$field1?></label>
                  <input type="text" required class="form-control" id="username" name="username" value="<?=@$rows['username'] ?>" placeholder="You can obtain this from the gateway provider">
                </div>
                
                <?php if(!empty($field2)) { ?>
                <div class="form-group">
                  <label for="param1"><?=$field2?></label>
                  <input type="text" required class="form-control" id="param1" name="param1" value="<?=@$rows['param1'] ?>" placeholder="You can obtain this from the gateway provider">
                </div>             
                <?php } ?>
                
             <?php } ?>
                
            </div>

            <div class="box-footer">
              <button type="reset" class="btn btn-danger">Reset Form</button>
               <?php if(!isset($_GET['edit'])) { ?>
              <button type="submit" name="save" class="btn btn-success">Create Gateway</button>
              <?php } else { ?>
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update Gateway</button>
              <?php }?>
            </div>
            </form>
                          
        </div>
       </div> 
                     
      </div>      
    
    </section>