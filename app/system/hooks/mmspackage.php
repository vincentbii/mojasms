<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) {
	header('location: index.php');
}
define('DATATABLE', true); 
if(isset($_GET['edit']) && !empty($_GET['edit'])) {
	global $server;
	$ch=mysqli_query($server, "select * from packages where id = '$_GET[edit]' limit 1");
	$rows = mysqli_fetch_assoc($ch);
}
?>
    <section class="content-header">
      <h1>
        MMS Packages
        <small>View and manage all your sms packages</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">MMS Packages</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	$sql=mysqli_query($server, "delete from packages where id='$_GET[delete]'")or die(mysqli_error($server));		
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected package has been successfully deleted from your system.
              </div>    
     </div>         
    <?php
}

if(isset($_POST['save'])){
	global $server;
		$user_id = getUser();
		$add = mysqli_query($server, "INSERT INTO packages (`quantity`, `cost`, `name`,`type` ) 
		VALUES ('$_POST[quantity]', '$_POST[cost]', '$_POST[name]', '$_POST[type]');") or die (mysql_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                Your new mms package was successfully created.
              </div>    
     </div>         
    <?php
}

if(isset($_POST['update'])){
	global $server;
	mysqli_query($server, "update packages set `name` = '$_POST[name]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update packages set `cost` = '$_POST[cost]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update packages set `quantity` = '$_POST[quantity]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update packages set `type` = '$_POST[type]' where id='$_POST[id]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected mms package has been successfully updated.
              </div>    
     </div>         
    <?php
}
?>     
        <div class="col-md-7">
               
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=package" method="get">
                <div class="input-group input-group-sm" style="width: 350px;">
                <input type="hidden" name="url" value="package" />
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>MMS Credits</th>
                  <th>Type</th>
                  <th>Cost</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$user_id = getUser();

	$ch=mysqli_query($server, "select * from packages where type='mms' order by id desc limit 1000");
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "name LIKE '%$term%' OR cost LIKE '%$term%' OR id LIKE '%$term%'";
	    } else {
	         $clauses[] = "name LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$user_id = getUser();
		$ch=mysqli_query($server, "select * from packages where type='mms' AND ".$filter);	
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
?>
                <tr>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo $row['type']; ?></td>
                  <td><?php echo currencySymbul(getSetting('defaultCurrency')).$row['cost']; ?></td>
                  <!--<td><?php  $R_099 = round($_SESSION[exchange_rate] * $row['cost'] * 0.069,2); echo $_SESSION[curr_symbol].$R_099 ;  ?></td>--->
                  <td >
                  	<div class="btn-group">
                        <a title="Edit" href="index.php?url=mmspackage&edit=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                        <a title="Delete" href="index.php?url=mmspackage&delete=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>MMS Credits</th>
                  <th>Type</th>
                  <th>Cost</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
 

		<div class="col-md-5">
          <div class="box box-primary">
         <?php if(!isset($_GET['edit'])) { ?>
            <div class="box-header with-border"><h4>Create New MMS Package</h4></div>
            <?php } else { ?>
            <div class="box-header with-border"><h4>Update <?=$rows['name']?></h4></div>
            <?php } ?>

            <form class="form-horizontal" action="index.php?url=mmspackage" method="post" role="form">           
            <div class="box-body" style="width: 90%; margin:0 auto;">
                
                <div class="form-group">
                  <label for="name">Package Name</label>
                  <input type="text" required class="form-control" id="name" name="name" value="<?=@$rows['name'] ?>" placeholder="Name your new package">
                </div>
                <div class="form-group">
                  <label for="name">Type</label>
                  <!--<input type="text" required class="form-control" id="type" name="type" value="<?=@$rows['type'] ?>" >-->
                  <select name="type" id="type" class="form-control">
                      <option <?php echo @$rows['type']=='mms' ? 'selected' : ''; ?> value="mms">MMS</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="quantity">Maximum MMS Quantity</label>
                  <input type="number" required class="form-control" id="quantity" name="quantity" value="<?=@$rows['quantity'] ?>" placeholder="">
                </div>                

                <div class="form-group">
                  <label for="cost">Package Cost (<?=currencySymbul(getSetting('defaultCurrency'))?>)</label>
                  <input type="text" required class="form-control" id="cost" name="cost" value="<?=@$rows['cost'] ?>" placeholder="Cost in <?=currencySymbul(getSetting('defaultCurrency'))?>">
                </div>
                
            </div>

            <div class="box-footer">
              <button type="reset" class="btn btn-danger">Reset Form</button>
               <?php if(!isset($_GET['edit'])) { ?>
              <button type="submit" name="save" class="btn btn-success">Create Package</button>
              <?php } else { ?>
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update Package</button>
              <?php }?>
            </div>
            </form>
                          
        </div>
       </div> 
                     
      </div>      
    
    </section>