<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) { header('location: index.php'); } 
define('DATATABLE', true); 
$is_voice = 1;
if(isset($_GET['edit']) && !empty($_GET['edit'])) {
	global $server;
	$ch=mysqli_query($server, "select * from gateways where id = '$_GET[edit]' limit 1");
	$rows = mysqli_fetch_assoc($ch);
}
?>
    <section class="content-header">
      <h1>
        Voice Gateway Settings
        <small>Create and Manage Voice gateways</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> System Tools</a></li>
        <li class="active">Voice Gateway Settings</li>
      </ol>
    </section>

    <section class="content">
    <div class="row">
<?php

if(isset($_GET['delete'])){
	global $server;
	$sql=mysqli_query($server, "delete from gateways where id='$_GET[delete]'")or die(mysqli_error($server));
	$sql=mysqli_query($server, "delete from routing where gateway_id='$_GET[delete]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected Voice gateway has been successfully deleted. All associated routing rules has been removed too.
              </div>    
     </div>         
    <?php
}
 
 if(isset($_GET['default'])){
	global $server;
	
	$sql=mysqli_query($server, "update settings set `value`= '$_GET[default]' where field = 'smsGateway'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected Voice gateway has been successfully set as default for outgoing Voice messages. Note that this will override any dynamic routing rule set for this gateway.
              </div>    
     </div>         
    <?php
}
 
       
if(isset($_POST['save'])){
	global $server;

		foreach ($_POST as $key => $value ){
		${$key} = $value = str_replace('\'', ' ', $value);
		}
		$user_id = getUser();
		$date = date('Y-m-d');
		$error = 0;
		
			$add = mysqli_query($server, "INSERT INTO gateways (`name`, `base_url`, `success_word`, `json_encode`, `request_type`, `sender_field`, `recipient_field`, `message_field`, `param1_field`, `param2_field`, `param3_field`, `param4_field`, `param1_value`, `param2_value`, `param3_value`, `param4_value`, `is_voice`, `language_field`, `audio_field`, `timeout_field`, `speed_field`, `authentication`, `base64_encode`) 
			VALUES ('$name', '$base_url', '$success_word', '$json_encode', '$request_type', '$sender_field', '$recipient_field', '$message_field', '$param1_field', '$param2_field', '$param3_field', '$param4_field', '$param1_value', '$param2_value', '$param3_value', '$param4_value', '$is_voice', '$language_field', '$audio_field', '$timeout_field', '', '$authentication', '$base64_encode');") or die (mysqli_error($server));

		//create activity 
		$activity = userName(getUser()).' created a new Voice gateway '.$_POST['name'];	
		$date = date('Y-m-d H:i:s');
		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));	

	if($error < 1) { ?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The new Voice gateway was successfully created. Click on the [<i class="fa fa-star"></i>] action button to set it as default for Voice message.
              </div>    
     </div>         
    <?php } else { ?>
    
    <div class="col-md-12">
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                <?=$alert?>
              </div>    
     </div>      
    <?php }
}

if(isset($_POST['update'])){
	global $server;
	foreach ($_POST as $key => $value ){
		if($key != 'id' && $key != 'update') {
		$value = str_replace('\'', ' ', $value);
		mysqli_query($server, "update gateways set `$key` = '$value' where id = '$_POST[id]'")or die(mysqli_error($server));
		}
	}
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected Voice gateway has been successfully updated.
              </div>    
     </div>         
    <?php
}
?>    

        <div class="col-md-7">
               
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=voiceGateway" method="get">
                <div class="input-group input-group-sm" style="width: 350px;">
                <input type="hidden" name="url" value="voiceGateway" />
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Base URL</th>
                  <th>Default</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$ch=mysqli_query($server, "select * from gateways where is_voice = '$is_voice' order by id desc limit 1000");
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "base_url LIKE '%$term%' OR id LIKE '%$term%' OR name LIKE '%$term%'";
	    } else {
	         $clauses[] = "name LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$ch=mysqli_query($server, "select * from gateways where ".$filter. " and  is_voice = '$is_voice'");	
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
?>
                <tr>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo shorten($row['base_url'],50); ?></td>
                  <td>
						<?php if(getSetting('voiceGateway') != $row['id']) { ?>
                        <a href="index.php?url=voiceGateway&default=<?php echo $row['id']?>">
                 		<button type="button" class="btn btn-warning btn-sm"><i class="fa fa-star"></i></button></a>
                        <?php } else {
							echo '<button disabled="disabled" type="button" class="btn btn-warning btn-sm"><i class="fa fa-star"></i></button>';
						}?>
               	   </td>
                   <td>   
                  	<div class="btn-group">
                        <a href="index.php?url=voiceGateway&edit=<?php echo $row['id']?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
						<?php if(getSetting('voiceGateway') != $row['id']) { ?>                        
                        <a onclick="confirm('Are you sure you want to delete this gateway?');" href="index.php?url=voiceGateway&delete=<?php echo $row['id']?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
<?php } ?>
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Base URL</th>
                  <th>Default</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
 

		<div class="col-md-5">
          <div class="box box-primary">
         <?php if(!isset($_GET['edit'])) { ?>
            <div class="box-header with-border"><h4>Add New Voice Gateway </h4></div>
            <?php } else { ?>
            <div class="box-header with-border"><h4>Update Gateway</h4></div>
            <?php } ?>

            <form class="form-horizontal" action="index.php?url=voiceGateway" method="post" role="form" enctype="multipart/form-data">           
            <input type="hidden" name="is_voice" value="<?=$is_voice?>" />
            <div class="box-body" style="width: 90%; margin:0 auto;">
                
                <div class="form-group">
                  <label for="name">Name</label>
                  <input <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> required="required" type="text" class="form-control" id="name" name="name" value="<?=@$rows['name'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Base URL</label>
                  <input required type="text" class="form-control" id="base_url" name="base_url" value="<?=@$rows['base_url'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Method</label>
                  <select <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> name="request_type" id="request_type" class="form-control" style="width: 100%;">
                    <option <?php if(@$rows['request_type'] == 'POST') echo 'selected';?> value="POST">POST</option>			
                    <option <?php if(@$rows['request_type'] == 'GET') echo 'selected';?> value="GET">GET</option>                  </select>
                </div>
                
                <div class="form-group">
                  <label for="name">JSON Encode Post</label>
                  <select <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> name="json_encode" id="json_encode" class="form-control" style="width: 100%;">
                    <option <?php if(@$rows['json_encode'] == 1) echo 'selected';?> value="1">Yes</option>			
                    <option <?php if(@$rows['json_encode'] == 0) echo 'selected';?> value="0">No</option>                  
                    </select>
                </div>
                
                <div class="form-group">
                  <label for="name">Success Word</label>
                  <input required type="text" class="form-control" id="success_word" name="success_word" value="<?=@$rows['success_word'] ?>" placeholder="Must be a string contained in your API success response">
                </div>
                
                <div class="form-group">
                  <label for="name">Autrhentication String <small class="text-yellow">if required</small></label>
                  <input type="text" class="form-control" id="authentication" name="authentication" value="<?=@$rows['authentication'] ?>" placeholder="Eg. Username:Password, API_KEY:Token, etc">
                </div>
                                
                <div class="form-group">
                  <label for="name">Base-64 Encode Authentication</label>
                  <select <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> name="base64_encode" id="base64_encode" class="form-control" style="width: 100%;">
                    <option <?php if(@$rows['base64_encode'] == 1) echo 'selected';?> value="1">Yes</option>			
                    <option <?php if(@$rows['base64_encode'] == 0) echo 'selected';?> value="0">No</option>                  
                    </select>
                </div>
                
                <h4>Common API Parameters Definition</h4>
                <div class="form-group">
                  <label for="name">Sender ID Parameter Name</label>
                  <input <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> required type="text" class="form-control" id="sender_field" name="sender_field" value="<?=@$rows['sender_field'] ?>" placeholder="This must be the same as specified on your gateway API">
                </div>
                
                <div class="form-group">
                  <label for="name">Recipient Parameter Name</label>
                  <input <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> required type="text" class="form-control" id="recipient_field" name="recipient_field" value="<?=@$rows['recipient_field'] ?>" placeholder="This must be the same as specified on your gateway API">
                </div>
                
                <div class="form-group">
                  <label for="name">Message Paraameter Name</label>
                  <input <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> required type="text" class="form-control" id="message_field" name="message_field" value="<?=@$rows['message_field'] ?>" placeholder="This must be the same as specified on your gateway API">
                </div>

<?php if($is_voice > 1) { ?>
                <div class="form-group">
                  <label for="name">Language Parameter Name</label>
                  <input <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> required type="text" class="form-control" id="language_field" name="language_field" value="<?=@$rows['language_field'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Voice File Parameter Name</label>
                  <input <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> required type="text" class="form-control" id="audio_field" name="audio_field" value="<?=@$rows['audio_field'] ?>" placeholder="">
                </div>
                                                                
                 <div class="form-group">
                  <label for="name">Call Duration Parameter Name</label>
                  <input <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> required type="text" class="form-control" id="timeout_field" name="timeout_field" value="<?=@$rows['timeout_field'] ?>" placeholder="">
                </div>
<?php  } ?>                
                <h4>Additional Parameters</h4><small class="text-yellow">Specify any other parameters required by your API</small>
                <div class="form-group">
                  <label for="name">Additional Parameter 1 Name</label>
                  <input <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> type="text" class="form-control" id="param1_field" name="param1_field" value="<?=@$rows['param1_field'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Additional Parameter 1 Value</label>
                  <input <?php if(isSystemGateway(@$_GET['edit'])) { echo 'readonly'; }?> required type="text" class="form-control" id="param1_value" name="param1_value" value="<?=@$rows['param1_value'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Additional Parameter 2 Name</label>
                  <input type="text" class="form-control" id="param2_field" name="param2_field" value="<?=@$rows['param2_field'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Additional Parameter 2 Value</label>
                  <input required type="text" class="form-control" id="param2_value" name="param2_value" value="<?=@$rows['param2_value'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Additional Parameter 3 Name</label>
                  <input type="text" class="form-control" id="param3_field" name="param3_field" value="<?=@$rows['param3_field'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Additional Parameter 3 Value</label>
                  <input required type="text" class="form-control" id="param3_value" name="param3_value" value="<?=@$rows['param3_value'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Additional Parameter 4 Name</label>
                  <input type="text" class="form-control" id="param4_field" name="param4_field" value="<?=@$rows['param4_field'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Additional Parameter 4 Value</label>
                  <input required type="text" class="form-control" id="param4_value" name="param4_value" value="<?=@$rows['param4_value'] ?>" placeholder="">
                </div>

              
                
            </div>

            <div class="box-footer">
              <button type="reset" class="btn btn-danger">Reset Form</button>
               <?php if(!isset($_GET['edit'])) { ?>
              <button type="submit" name="save" class="btn btn-success">Save New Gateway</button>
              <?php } else { ?>
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update Gateway</button>
              <?php }?>
            </div>
            </form>
                          
        </div>
       </div> 
    
    </section>