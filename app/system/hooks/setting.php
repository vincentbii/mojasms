<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) { header('location: index.php'); } 

global $configapp_store;
global $configverssion_id;
global $configapp_version;
global $configapp_name;
global $configversion_date;
?>
    <section class="content-header">
      <h1>
        General Settings
        <small>General settings</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> System Tools</a></li>
        <li class="active">General Settings</li>
      </ol>
    </section>

    <section class="content">
    <div class="row">
<?php    
if(isset($_POST['save'])){
	global $server;
	foreach ($_POST as $key => $value ){
		${$key} = $value = str_replace('\'', '', $value);
		mysqli_query($server, "update settings set `value` = '$value' where field = '$key'")or die(mysqli_error($server));
	}	
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                Your new system settings was successfully saved and applied. 
              </div>    
     </div>         
    <?php
	
}
?>     


       <form class="form-vertial" action="index.php?url=setting" method="post" role="form">           
		<div class="col-md-7">
          <div class="box box-success" >
            <div class="box-header with-border"><h4>Update Settings</h4></div>
            
            <div class="box-body">
            <h4>Branding</h4>
                <div class="form-group">
                  <label for="name">Brand Name</label>
                  <input type="text" required class="form-control" id="businessName" name="businessName" value="<?=getSetting('businessName') ?>" placeholder="Your Business / Brand Name">
                </div>
                
                <div class="form-group">
                  <label >Time Zone</label>
                  <select name="timeZone" class="form-control select2" style="width: 100%;">
                <option value="Africa/Johannesburg">(UTC+02:00) Johannesburg South Africa</option>
                <option value="Pacific/Midway">(GMT-11:00) Midway Island</option>
                <option value="US/Samoa">(GMT-11:00) Samoa</option>
                <option value="US/Hawaii">  (GMT-10:00) Hawaii</option>
                <option value="US/Alaska">  (GMT-09:00) Alaska</option>
                <option value="US/Pacific">   (GMT-08:00) Pacific Time (US &amp; Canada)</option>
                <option value="America/Tijuana">  (GMT-08:00) Tijuana</option>
                <option value="US/Arizona"> (GMT-07:00) Arizona</option>
                <option value="US/Mountain">  (GMT-07:00) Mountain Time (US &amp; Canada)</option>
                <option value="America/Chihuahua">    (GMT-07:00) Chihuahua</option>
                <option value="America/Mazatlan">     (GMT-07:00) Mazatlan</option>
                <option value="America/Mexico_City">  (GMT-06:00) Mexico City</option>
                <option value="America/Monterrey">    (GMT-06:00) Monterrey</option>
                <option value="Canada/Saskatchewan">  (GMT-06:00) Saskatchewan</option>
                <option value="US/Central">           (GMT-06:00) Central Time (US &amp; Canada)</option>
                <option value="US/Eastern">           (GMT-05:00) Eastern Time (US &amp; Canada)</option>
                <option value="US/East-Indiana">      (GMT-05:00) Indiana (East)</option>
                <option value="America/Bogota">(GMT-05:00) Bogota</option>
                <option value="America/Lima">         (GMT-05:00) Lima</option>
                <option value="America/Caracas">      (GMT-04:30) Caracas</option>
                <option value="Canada/Atlantic">      (GMT-04:00) Atlantic Time (Canada)</option>
                <option value="America/La_Paz">(GMT-04:00) La Paz</option>
                <option value="America/Santiago">     (GMT-04:00) Santiago</option>
                <option value="Canada/Newfoundland">  (GMT-03:30) Newfoundland</option>
                <option value="America/Buenos_Aires"> (GMT-03:00) Buenos Aires</option>
                <option value="Greenland">            (GMT-03:00) Greenland</option>
                <option value="Atlantic/Stanley">     (GMT-02:00) Stanley</option>
                <option value="Atlantic/Azores">      (GMT-01:00) Azores</option>
                <option value="Atlantic/Cape_Verde">  (GMT-01:00) Cape Verde Is.</option>
                <option value="Africa/Casablanca">    (GMT) Casablanca</option>
                <option value="Europe/Dublin">        (GMT) Dublin</option>
                <option value="Europe/Lisbon">        (GMT) Lisbon</option>
                <option value="Europe/London">        (GMT) London</option>
                <option value="Africa/Monrovia">      (GMT) Monrovia</option>
                <option value="Europe/Amsterdam">     (GMT+01:00) Amsterdam</option>
                <option value="Europe/Belgrade">      (GMT+01:00) Belgrade</option>
                <option value="Europe/Berlin">        (GMT+01:00) Berlin</option>
                <option value="Europe/Bratislava">    (GMT+01:00) Bratislava</option>
                <option value="Europe/Brussels">      (GMT+01:00) Brussels</option>
                <option value="Europe/Budapest">      (GMT+01:00) Budapest</option>
                <option value="Europe/Copenhagen">    (GMT+01:00) Copenhagen</option>
                <option value="Europe/Ljubljana">     (GMT+01:00) Ljubljana</option>
                <option value="Europe/Madrid">        (GMT+01:00) Madrid</option>
                <option value="Europe/Paris">         (GMT+01:00) Paris</option>
                <option value="Europe/Paris">         (GMT+01:00) West/Central Africa</option>
                <option value="Europe/Prague">        (GMT+01:00) Prague</option>
                <option value="Europe/Rome">          (GMT+01:00) Rome</option>
                <option value="Europe/Sarajevo">      (GMT+01:00) Sarajevo</option>
                <option value="Europe/Skopje">        (GMT+01:00) Skopje</option>
                <option value="Europe/Stockholm">     (GMT+01:00) Stockholm</option>
                <option value="Europe/Vienna">        (GMT+01:00) Vienna</option>
                <option value="Europe/Warsaw">        (GMT+01:00) Warsaw</option>
                <option value="Europe/Zagreb">        (GMT+01:00) Zagreb</option>
                <option value="Europe/Athens">        (GMT+02:00) Athens</option>
                <option value="Europe/Bucharest">     (GMT+02:00) Bucharest</option>
                <option value="Africa/Cairo">         (GMT+02:00) Cairo</option>
                <option value="Africa/Harare">        (GMT+02:00) Harare</option>
                <option value="Europe/Helsinki">      (GMT+02:00) Helsinki</option>
                <option value="Europe/Istanbul">      (GMT+02:00) Istanbul</option>
                <option value="Asia/Jerusalem">(GMT+02:00) Jerusalem</option>
                <option value="Europe/Kiev">          (GMT+02:00) Kyiv</option>
                <option value="Europe/Minsk">         (GMT+02:00) Minsk</option>
                <option value="Europe/Riga">          (GMT+02:00) Riga</option>
                <option value="Europe/Sofia">         (GMT+02:00) Sofia</option>
                <option value="Europe/Tallinn">(GMT+02:00) Tallinn</option>
                <option value="Europe/Vilnius">(GMT+02:00) Vilnius</option>
                <option value="Asia/Baghdad">         (GMT+03:00) Baghdad</option>
                <option value="Asia/Kuwait">          (GMT+03:00) Kuwait</option>
                <option value="Africa/Nairobi">(GMT+03:00) Nairobi</option>
                <option value="Asia/Riyadh">          (GMT+03:00) Riyadh</option>
                <option value="Asia/Tehran">          (GMT+03:30) Tehran</option>
                <option value="Europe/Moscow">        (GMT+04:00) Moscow</option>
                <option value="Asia/Baku">            (GMT+04:00) Baku</option>
                <option value="Europe/Volgograd">     (GMT+04:00) Volgograd</option>
                <option value="Asia/Muscat">          (GMT+04:00) Muscat</option>
                <option value="Asia/Tbilisi">         (GMT+04:00) Tbilisi</option>
                <option value="Asia/Yerevan">         (GMT+04:00) Yerevan</option>
                <option value="Asia/Kabul">           (GMT+04:30) Kabul</option>
                <option value="Asia/Karachi">         (GMT+05:00) Karachi</option>
                <option value="Asia/Tashkent">        (GMT+05:00) Tashkent</option>
                <option value="Asia/Kolkata">         (GMT+05:30) Kolkata</option>
                <option value="Asia/Kathmandu">(GMT+05:45) Kathmandu</option>
                <option value="Asia/Yekaterinburg">   (GMT+06:00) Ekaterinburg</option>
                <option value="Asia/Almaty">          (GMT+06:00) Almaty</option>
                <option value="Asia/Dhaka">           (GMT+06:00) Dhaka</option>
                <option value="Asia/Novosibirsk">     (GMT+07:00) Novosibirsk</option>
                <option value="Asia/Bangkok">         (GMT+07:00) Bangkok</option>
                <option value="Asia/Jakarta">         (GMT+07:00) Jakarta</option>
                <option value="Asia/Krasnoyarsk">     (GMT+08:00) Krasnoyarsk</option>
                <option value="Asia/Chongqing">(GMT+08:00) Chongqing</option>
                <option value="Asia/Hong_Kong">(GMT+08:00) Hong Kong</option>
                <option value="Asia/Kuala_Lumpur">    (GMT+08:00) Kuala Lumpur</option>
                <option value="Australia/Perth">      (GMT+08:00) Perth</option>
                <option value="Asia/Singapore">(GMT+08:00) Singapore</option>
                <option value="Asia/Taipei">          (GMT+08:00) Taipei</option>
                <option value="Asia/Ulaanbaatar">     (GMT+08:00) Ulaan Bataar</option>
                <option value="Asia/Urumqi">          (GMT+08:00) Urumqi</option>
                <option value="Asia/Irkutsk">         (GMT+09:00) Irkutsk</option>
                <option value="Asia/Seoul">           (GMT+09:00) Seoul</option>
                <option value="Asia/Tokyo">           (GMT+09:00) Tokyo</option>
                <option value="Australia/Adelaide">   (GMT+09:30) Adelaide</option>
                <option value="Australia/Darwin">     (GMT+09:30) Darwin</option>
                <option value="Asia/Yakutsk">         (GMT+10:00) Yakutsk</option>
                <option value="Australia/Brisbane">   (GMT+10:00) Brisbane</option>
                <option value="Australia/Canberra">   (GMT+10:00) Canberra</option>
                <option value="Pacific/Guam">         (GMT+10:00) Guam</option>
                <option value="Australia/Hobart">     (GMT+10:00) Hobart</option>
                <option value="Australia/Melbourne">  (GMT+10:00) Melbourne</option>
                <option value="Pacific/Port_Moresby"> (GMT+10:00) Port Moresby</option>
                <option value="Australia/Sydney">     (GMT+10:00) Sydney</option>
                <option value="Asia/Vladivostok">     (GMT+11:00) Vladivostok</option>
                <option value="Asia/Magadan">         (GMT+12:00) Magadan</option>
                <option value="Pacific/Auckland">     (GMT+12:00) Auckland</option>
                <option value="Pacific/Fiji">         (GMT+12:00) Fiji</option>
                  <option selected value="<?php echo getSetting('timeZone'); ?>">Current Time Zone is <?php echo getSetting('timeZone'); ?></option>
                           
                  </select>
                </div>
                
                <h4>Cost / Billing</h4>
                <div class="form-group">
                  <label>Default Currency</label>
                  <select name="defaultCurrency" class="form-control select2" style="width: 100%;">
<?php global $server;
	$ch=mysqli_query($server, "select * from currencies order by id desc");
	while ($rows2 = mysqli_fetch_assoc($ch)) {
?>                    
                    <option <?php if(getSetting('defaultCurrency')==$rows2['id']) echo 'selected' ?> value="<?=$rows2['id']?>"><?=$rows2['name']?></option>
<?php } ?>                  
                  </select>
                </div>               

                <div class="form-group">
                  <label for="name">Default SMS Rate (<?=currencySymbul(getSetting('defaultCurrency'))?>)</label>
                  <p class="help-block text-blue">In your default currency</p>
                  <input required type="text" class="form-control" id="smsDefaultCost" name="smsDefaultCost" value="<?=getSetting('smsDefaultCost') ?>" placeholder="Your default per SMS page">
                </div>
                
                <div class="form-group">
                  <label for="smsCost">Dynamic SMS Rate Definition</label>
                  <p class="help-block text-green"><small>Setup cost per SMS page using country & network operator's code. Specify cost of each destination in a new line. Eg. <br />234803=2.1<br />223=2.0<br />1=3.0</small></p>
                  <textarea  class="form-control " rows="6" name="smsCost" placeholder=""><?=getSetting('smsCost')?></textarea>
                </div>
                
                <div class="form-group">
                  <label for="name">Default Voice Rate (<?=currencySymbul(getSetting('defaultCurrency'))?>)</label>
                  <p class="help-block text-blue" id="countBox3">In your default currency</p>
                  <input required="required" type="text" class="form-control" id="voiceDefaultCost" name="voiceDefaultCost" value="<?=getSetting('voiceDefaultCost') ?>" placeholder="Your default rate per 30 seconds of voice message">
                </div>    

                <div class="form-group">
                  <label for="voiceCost">Dynamic Voice Rate Definition</label>
                  <p class="help-block text-green"><small>Setup cost per 30 seconds of Voice message using country & network operator's code. Specify cost of each destination in a new line. Eg. <br />234803=2.1<br />223=2.0<br />1=3.0</small></p>
                  <textarea  class="form-control " rows="6" name="voiceCost" placeholder=""><?=getSetting('voiceCost')?></textarea>
                </div>                            
                
				<h4>Messaging</h4>
                <div class="form-group">
                  <label for="name">Default Short-Code</label>
                  <input type="number" class="form-control" id="defaultShortCode" name="defaultShortCode" value="<?=getSetting('defaultShortCode') ?>" placeholder="Your default incoming SMS number. Include country code if applicable">
                </div>
                
                <div class="form-group">
                  <label for="name">Default SMS Sender ID</label>
                  <input type="text" required class="form-control" id="smsSender" name="smsSender" value="<?=getSetting('smsSender') ?>" placeholder="This will be used for system messages">
                </div>
                
                <div class="form-group">
                  <label for="name">Blacklist Keyword</label>
                  <input type="text" required class="form-control" id="blacklistKeyword" name="blacklistKeyword" value="<?=getSetting('blacklistKeyword') ?>" placeholder="Recipients will send this keyword to your default short-code to get blacklisted">
                </div>                
                
                <div class="form-group">
                  <label for="message">Blacklist SMS Response</label>
                  <textarea onBlur="count2(this,this.form.countBox2,1000);" onKeyUp="count2(this,this.form.countBox2,1000);"  class="form-control" rows="6" id="messageText" name="blacklistResponse" placeholder="Type your message text here... You can dynamically insert recipient's phone number in your message using [NUMBER]"><?=getSetting('blacklistResponse')?></textarea>
                  <p class="help-block text-blue" id="countBox2">0 Characters Used</p>

<script>
//count message lenght 
	function count2(field,countfield,maxlimit) {
	if (field.value.lenght > 1000) {
		field.value = field.value.substring(0,1000);
		field.blur();
		return false;
	} else {
		var smslenght = 160;
		if(field.value.length > 160){
			var smslenght = 145;
		}
		var pages = field.value.length /smslenght;
			if(pages < 1) { var page = '1';	}
			if(pages == 1) { var page = '1';}
			if(pages > 1) { var page = '2';	}
			if(pages > 2) {	var page = '3';	}
			if(pages > 3) {	var page = '4';	}
			if(pages > 4) {	var page = '5';	}
			if(pages > 5) {	var page = '6';	}
			if(pages > 6) {	var page = '7';	}
															
		document.getElementById('countBox2').innerHTML = field.value.length + " of 1000 Characters Used ("+page+" SMS)";
		}
	}
</script>                   
                </div>    

				<h4>Email/SMS Notification Settings</h4>

                <div class="form-group">
                  <label for="name">SMTP Username </label>
                  <input type="email" required class="form-control" id="smtpUsername" name="smtpUsername" value="<?=getSetting('smtpUsername') ?>" placeholder="Your smtp email address">
                </div>                

                <div class="form-group">
                  <label for="name">SMTP Password </label>
                  <input type="text" required class="form-control" id="smtpPassword" name="smtpPassword" value="<?=getSetting('smtpPassword') ?>" placeholder="Your smtp password">
                </div> 

                <div class="form-group">
                  <label for="name">SMTP Server </label>
                  <input type="text" required class="form-control" id="smtpServer" name="smtpServer" value="<?=getSetting('smtpServer') ?>" placeholder="Your smtp server. eg. mail.example.com">
                </div>
                
                <div class="form-group">
                  <label for="name">SMTP Port </label>
                  <input type="number" required class="form-control" id="smtpPort" name="smtpPort" value="<?=getSetting('smtpPort') ?>" placeholder="Your smtp port eg. 587">
                </div> 
                                                
                <div class="form-group">
                  <label for="message">New Client SMS</label>
                  <p class="help-block text-green" id="countBox4">You can insert client's name, username or passwoord using the short-tags [NAME], [USERNAME] and [PASSWORD] respectively.</p>
                  <textarea onBlur="count3(this,this.form.countBox3,1000);" onKeyUp="count3(this,this.form.countBox3,1000);"  class="form-control" rows="6" id="messageText3" name="newClientSMS" placeholder="Type your message text here... "><?=getSetting('newClientSMS')?></textarea>
                  <p class="help-block text-blue" id="countBox3">0 Characters Used</p>

<script>
//count message lenght 
	function count3(field,countfield,maxlimit) {
	if (field.value.lenght > 1000) {
		field.value = field.value.substring(0,1000);
		field.blur();
		return false;
	} else {
		var smslenght = 160;
		if(field.value.length > 160){
			var smslenght = 145;
		}
		var pages = field.value.length /smslenght;
			if(pages < 1) { var page = '1';	}
			if(pages == 1) { var page = '1';}
			if(pages > 1) { var page = '2';	}
			if(pages > 2) {	var page = '3';	}
			if(pages > 3) {	var page = '4';	}
			if(pages > 4) {	var page = '5';	}
			if(pages > 5) {	var page = '6';	}
			if(pages > 6) {	var page = '7';	}
															
		document.getElementById('countBox3').innerHTML = field.value.length + " of 1000 Characters Used ("+page+" SMS)";
		}
	}
</script>                   
                </div>    


                <div class="form-group">
                  <label for="name">New Client Email Subjet</label>
                  <input type="text" required class="form-control" id="newClientEmailSubject" name="newClientEmailSubject" value="<?=getSetting('newClientEmailSubject') ?>" placeholder="">
                </div>                
                
                <div class="form-group">
                  <label for="message">New Client Email Message</label>
                  <p class="help-block text-green">You can insert client's name, username or passwoord using the short-tags [NAME], [USERNAME] and [PASSWORD] respectively.</p>
                  <textarea  class="textarea form-control " rows="6" name="newClientEmail" placeholder="Type your message text here... "><?=getSetting('newClientEmail')?></textarea>
                </div>    
                
                <div class="form-group">
                  <label for="newOrderSMS">New Order SMS</label>
                  <p class="help-block text-green">You can insert client's username, credits added, balance and order package using the short-tags [USERNAME], [UNITS], [BALANCE] and [PACKAGENAME] respectively.</p>
                  <textarea onBlur="count3(this,this.form.countBox5,1000);" onKeyUp="count3(this,this.form.countBox5,1000);"  class="form-control" rows="6" id="messageText5" name="newOrderSMS" placeholder="Type your message text here... "><?=getSetting('newClientSMS')?></textarea>
                  <p class="help-block text-blue" id="countBox5">0 Characters Used</p>

<script>
//count message lenght 
	function count5(field,countfield,maxlimit) {
	if (field.value.lenght > 1000) {
		field.value = field.value.substring(0,1000);
		field.blur();
		return false;
	} else {
		var smslenght = 160;
		if(field.value.length > 160){
			var smslenght = 145;
		}
		var pages = field.value.length /smslenght;
			if(pages < 1) { var page = '1';	}
			if(pages == 1) { var page = '1';}
			if(pages > 1) { var page = '2';	}
			if(pages > 2) {	var page = '3';	}
			if(pages > 3) {	var page = '4';	}
			if(pages > 4) {	var page = '5';	}
			if(pages > 5) {	var page = '6';	}
			if(pages > 6) {	var page = '7';	}
															
		document.getElementById('countBox5').innerHTML = field.value.length + " of 1000 Characters Used ("+page+" SMS)";
		}
	}
</script>                   
                </div>    


                <div class="form-group">
                  <label for="newOrderEmailSubject">Order Approval Email Subjet</label>
                  <input type="text" required class="form-control" id="newOrderEmailSubject" name="newOrderEmailSubject" value="<?=getSetting('newOrderEmailSubject') ?>" placeholder="">
                </div>                
                
                <div class="form-group">
                  <label for="newOrderEmail">Order Approval Email Message</label>
                  <p class="help-block text-green">You can insert client's username, credits added, balance and order package using the short-tags [USERNAME], [UNITS], [BALANCE] and [PACKAGENAME] respectively.</p>
                  <textarea  class="textarea form-control " rows="6" name="newOrderEmail" placeholder="Type your message text here... "><?=getSetting('newOrderEmail')?></textarea>
                </div>                    
            </div>

            <div class="box-footer">
              <button type="submit" name="save" class="btn btn-success">Update Settings</button>
            </div>
                          
        </div>
       </div> 
            </form>

		<div class="col-md-5">
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">System Information</h3>
            </div>

            <div class="box-body">
              <table id="templates" class="table table-bordered table-hover">
                <tbody>                
                <tr>
                  <td>App Name: </td><td><b><?=$configapp_name?></b> </td>
                </tr> 
                <tr>
                  <td>Version: </td><td><b><?=$configapp_version?></b> </td>
                </tr> 
                <tr>
                  <td>Last Updated; </td><td><b><?=$configversion_date?></b> </td>
                </tr> 
                <tr>
                  <td>Built SID: </td><td><b><?=$configverssion_id?></b> </td>
                </tr> 
                <tr>
                  <td><P></P> </td><td></td>
                </tr>
                <tr>
                  <td>Cronjob Status: </td><td><b><?=cronStatus(date('Y-m-d'))?></b> </td>
                </tr> 
                <tr>
                  <td>Server Address: </td><td><b><?=$_SERVER['SERVER_ADDR']?></b> </td>
                </tr>   
                <tr>
                  <td>Host Name: </td><td><b><?=$_SERVER['SERVER_NAME']?></b> </td>
                <tr>
                  <td>Software: </td><td><b><?=$_SERVER['SERVER_SOFTWARE']?></b> </td>
                </tr> 
                <tr>
                  <td>MojaSMS Directory: </td><td><b><?=$_SERVER['DOCUMENT_ROOT']?></b> </td>
                </tr>
                <tr>
                  <td>TIME & DATE NOW: </td><td><b><?php echo date('d-m-Y H:i:s');?></b> </td>
                </tr> 
              </tbody>
             </table>
           </div>
         </div> 
             
		</div>        
    
    </section>