<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('DATATABLE', true); 
?>
    <section class="content-header">
      <h1>
        Withdrawal History
        <small>History</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-money"></i> Transactions</a></li>
        <li class="active">Withdrawal Historys</li>
      </ol>
    </section>

    <section class="content">
     <div class="row">
         <div class="col-md-12">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Withdrawal History</h4></div>
            
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>Bank Details</th>
                              <th>Amount</th>
                              <th>Date</th>
                              <th>Status</th>
                          </tr>
                      </thead>
                      <tbody>
                           <?php  global $server;
                              $i=1; $id=getUser();
                              
                              $ch=mysqli_query($server, "select * from withdrawal_logs WHERE user_id='".$id."'");
                              	while ($row = mysqli_fetch_assoc($ch)) {
                              ?>
                              <tr>
                                  <td><?php echo $i++;?></td>
                                  <td><div class="well"><?php echo $row['bank_details'];?></div></td>
                                  <td>R<?php echo $row['amount'];?></td>
                                  <td><?php echo $row['date'];?></td>
                                  <td><?php echo ($row['status']==0) ? '<span class="label label-primary">Pending Approval</span>' : (($row['status']==3) ? '<span class="label label-danger">Canceled</span>' : '<span class="label label-success">Withdrawal Success</span>');?></td>
                              </tr>
                              <?php
                              	}
                              ?>
                      </tbody>
                  </table>
                </div>
          </div>
        </div> 
     </div>
    </section>

