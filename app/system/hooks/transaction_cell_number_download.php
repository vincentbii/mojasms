<?php

ob_clean();

global $server;

//get records from database
$query = $sql=mysqli_query($server, "select * from purchased_cell_numbers where transaction_id='$_GET[transid]'")or die(mysqli_error($server));

if(mysqli_num_rows($query) > 0){
    $delimiter = ",";
    $filename = "cellnumber_database_" . $_GET[transid] . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Cell Number');
    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = mysqli_fetch_assoc($query)){
        $lineData = array($row['number']);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

?>