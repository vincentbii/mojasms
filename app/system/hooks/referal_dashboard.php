<style>
@media (max-width: 768px) {
    .small-box h3 {
        font-size: 20px!important;
    }
}
</style>
<?php  
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);
    
    
    if(!empty($_POST) && trim($_POST['amount']) > 0){
        
        if(countWithdrawal(getUser()) > 0){
?>
<div class="alert alert-warning">
    You already have a pending withdrawal request!
</div>
<?php
        }else{
        
   $amount=trim($_POST['amount']);
if($amount > (userData('refferal_balance', getUser())/100)){     
?>
<div class="alert alert-warning">
    You can not withdraw more than R<?php echo userData('refferal_balance', getUser())/100;?>
</div>
<?php
}else{
        
        $bank_details=nl2br($_POST['bank_details']);
        $userid=getUser();
        $date=date('Y-m-d H:i:s');
        $amount_in_point=$amount*100;
        $balance=(userData('refferal_balance', getUser())-$amount_in_point) > 0 ? userData('refferal_balance', getUser())-$amount_in_point : 0;
        global $server;
        
        $sql="INSERT INTO `withdrawal_logs`(`user_id`, `amount`, `date`, `status`, `bank_details`) VALUES ('$userid','$amount','$date','0','$bank_details')";
        if(mysqli_query($server,$sql)){
            $sql_update="UPDATE users SET refferal_balance='$balance' WHERE id='$userid'";
            mysqli_query($server,$sql_update)
?>
<div class="alert alert-success">
    Withdrawal in progress, pending approval
</div>
<?php
        }else{
?>
<div class="alert alert-danger">
    Withdrawal Error!
</div>
<?php
        }
    }
 }
}
?>
    <section class="content-header">
      <h1>
        Referal Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> Referal Dashboard</li>
      </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
    	        <div class="panel panel-primary">
    	            <div class="panel-heading">
    	                Refferal Dashboard
    	            </div>
    	            <div class="panel-body">
    	                 <div class="col-lg-3 col-xs-12" >
                          <div class="small-box bg-teal" style="border-radius: 25px;">
                            <div class="inner">
                                <h3><?php echo number_format(userData('refferal_balance', getUser()));?> </h3>   
                                <p>Earning's Points</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-money"></i>
                            </div>
                            <a href="refferals_history" class="small-box-footer">See History <i class="fa fa-arrow-circle-right"></i></a>
                          </div>
                        </div>
                        
                        <div class="col-lg-3 col-xs-12" >
                          <div class="small-box bg-green" style="border-radius: 25px;">
                            <div class="inner">
                                <h3><?php echo countReferals(getUser());?></h3>   
                                <p>Total Refferal's</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-users"></i>
                            </div>
                            <a href="referal_lists" class="small-box-footer">See List <i class="fa fa-arrow-circle-right"></i></a>
                          </div>
                        </div>
                        
                        <div class="col-lg-3 col-xs-12" >
                          <div class="small-box bg-red" style="border-radius: 25px;">
                            <div class="inner">
                                <h3><?php echo countWithdrawal(getUser());?></h3>   
                                <p>Withdrawal Log's</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-bank"></i>
                            </div>
                            <a href="withdrawal_history" class="small-box-footer">See Logs <i class="fa fa-arrow-circle-right"></i></a>
                          </div>
                        </div>
                        
                        <div class="col-lg-3 col-xs-12" >
                          <div  data-toggle="modal" data-target="#myModal" class="small-box bg-blue" style="border-radius: 25px;">
                            <div class="inner">
                                <h3>R<?php echo number_format(userData('refferal_balance', getUser())/100)?></h3>   
                                <p>Withdrawal Now</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-bank"></i>
                            </div>
                            <p  class="small-box-footer">&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                          </div>
                        </div>
                        
    	            </div>
    	            <div class="panel-footer">
    	                <div class="well" style="background-color:#A6C33A;color:white;border-radius:20px;">
    	                    <h3>Refferal Link</h3>
    	                    <p>
        	                    <?php
        	                      echo $ref_link="https://www.mojasms.com/app/index.php?join&&refferal=".userData('username', getUser());
        	                    ?>
    	                    </p>
    	                    <button id="btnlink" class="btn btn-danger" data-clipboard-text="<?php echo $ref_link;?>"><i class="fa fa-copy"></i> Copy</button>
    	                </div>
    	            </div>
    	        </div>
	        </div>
        </div>
    </section>
    
    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Withdraw</h4>
      </div>
      <div class="modal-body">
        <?php
        if((userData('refferal_balance', getUser())/100) < 1000){
        ?>
        <p class="alert alert-danger">You must have minimum of R1000 to be able to withdraw</p>
        <?php
        }elseif(countPWithdrawal(getUser()) > 0){
        ?>
        <p class="alert alert-danger">You already have a pending withdrawal request!</p>
        <?php
        }else{
        ?>
        <form method="post" action="referal_dashboard">
            <div class="form-group">
                <label>Amount</label>
                <input type="number" name="amount" class="form-control" min="0" max="<?php echo (userData('refferal_balance', getUser())/100);?>" placeholder="Enter Amount To Withdraw"/>
            </div>
            <div class="form-group">
                <label>Bank Account Details</label>
                <textarea class="form-control textarea" name="bank_details" id="editor1" rows="3" placeholder="Enter Bank Account Details" style="padding:5%;"></textarea>
            </div>
            <br><hr>
            <button type="submit" class="btn btn-block btn-lg btn-success">Submit</button>
        </form>
        <?php
        }
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    
<script src="/plugin/clipboard/clipboard.js"></script>
<script>
    var btnlink = document.getElementById('btnlink');
    
    var btnlink1 = new ClipboardJS(btnlink);
    
    btnlink1.on('success', function(e) {
      alert('Copied!');
    });
    
    btnlink1.on('error', function(e) {
      alert('Failed!');
    });

</script>
