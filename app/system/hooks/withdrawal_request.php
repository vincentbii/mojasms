<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('DATATABLE', true); global $server;

if(!empty($_REQUEST['approve'])){ $id=filtervar($_REQUEST['approve']);
    mysqli_query($server,"UPDATE withdrawal_logs SET status='1' WHERE id='$id'");
}

if(!empty($_REQUEST['cancel'])){ $id=filtervar($_REQUEST['cancel']);
    mysqli_query($server,"UPDATE withdrawal_logs SET status='3' WHERE id='$id'");
    $chd=mysqli_query($server, "select * from withdrawal_logs WHERE id='$id' LIMIT 1");
    $rowd = mysqli_fetch_assoc($chd);
    $user_id=$rowd['user_id'];
    $amounter=$rowd['amount'];
    
    $rsn = mysqli_query($server,"SELECT * FROM users WHERE id = '$user_id'"); 
	$rn = mysqli_fetch_assoc($rsn);
	$refferal_balance = $rn['refferal_balance']+($amounter*100);
    mysqli_query($server,"UPDATE `users` SET `refferal_balance` =  '$refferal_balance' WHERE `id` = '$user_id'");
}
?>
    <section class="content-header">
      <h1>
        Withdrawal Request
        <small>Request</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-money"></i> Transactions</a></li>
        <li class="active">Withdrawal Requests</li>
      </ol>
    </section>

    <section class="content">
     <div class="row">
         <div class="col-md-12">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Withdrawal Requests</h4></div>
            
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>User</th>
                              <th>Bank Details</th>
                              <th>Amount</th>
                              <th>Date</th>
                              <th>Status</th>
                              <th>&nbsp;</th>
                          </tr>
                      </thead>
                      <tbody>
                           <?php  global $server;
                              $i=1; $id=getUser();
                              
                              $ch=mysqli_query($server, "select * from withdrawal_logs");
                              	while ($row = mysqli_fetch_assoc($ch)) {
                              ?>
                              <tr>
                                  <td><?php echo $i++;?></td>
                                  <td>
                                      <?php
                                      echo userData('username', $row['user_id']);
                                      ?>
                                  </td>
                                  <td><pre><?php echo $row['bank_details'];?></pre></td>
                                  <td>R<?php echo $row['amount'];?></td>
                                  <td><?php echo $row['date'];?></td>
                                  <td><?php echo ($row['status']==0) ? '<span class="label label-primary">Pending Approval</span>' : (($row['status']==3) ? '<span class="label label-danger">Canceled</span>' : '<span class="label label-success">Withdrawal Success</span>');?></td>
                                  <td>
                                      <?php
                                      if($row['status']!=1 && $row['status']!=3):
                                      ?>
                                      
                                      <a href="index.php?url=withdrawal_request&approve=<?php echo $row['id'];?>" class="approvebtn">
                                        <button type="button" class="btn btn-warning btn-sm"><i class="fa fa-check"></i></button>
                                      </a>
                                      
                                      <a href="index.php?url=withdrawal_request&cancel=<?php echo $row['id'];?>" class="cancelbtn">
                                        <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i></button>
                                      </a>
                                      
                                      <?php
                                      endif;
                                      ?>
                                  </td>
                              </tr>
                              <?php
                              	}
                              ?>
                      </tbody>
                  </table>
                </div>
          </div>
        </div> 
     </div>
    </section>

