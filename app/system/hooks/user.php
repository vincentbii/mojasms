<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) { header('location: index.php'); } 
define('DATATABLE', true); 

if(isset($_GET['edit']) && !empty($_GET['edit'])) {
	global $server;
	$ch=mysqli_query($server, "select * from users where id = '$_GET[edit]' limit 1");
	$rows = mysqli_fetch_assoc($ch);
}
?>
    <section class="content-header">
      <h1>
        Manage Users
        <small>Manage administrators</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php

if(isset($_GET['delete'])){
	global $server;
	$sql=mysqli_query($server, "delete from users where id='$_GET[delete]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected user has been successfully deleted.
              </div>    
     </div>         
    <?php
}
       
if(isset($_POST['save'])){
	global $server;
		$user_id = getUser();
		$date = date('Y-m-d');
		$error = 0;
		$password = $_POST['password'];
		$salt = genRandomPassword(32);
		$crypt = getCryptedPassword($password, $salt);
		$password2 = $crypt.':'.$salt;	
		
		if(userAvailable($_POST['username'])) {
			$add = mysqli_query($server, "INSERT INTO users (`name`, `username`, `email`, `password`, `is_admin`) 
			VALUES ('$_POST[name]', '$_POST[username]', '$_POST[email]', '$password2', '$_POST[is_admin]');") or die (mysql_error($server));
			
		//create activity 
		$activity = userName(getUser()).' created a new user '.$_POST['name'];	
		$date = date('Y-m-d H:i:s');
		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));			
		} else {
			$error = 1;
			$alert = 'Sorry but a user already exist with same username "'.$_POST['username'].'". Plese choose another username.';	
		}
	if($error < 1) { ?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The new user was successfully created. The new user will now be able to access your Mobiketa.
              </div>    
     </div>         
    <?php } else { ?>
    
    <div class="col-md-12">
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                <?=$alert?>
              </div>    
     </div>      
    <?php }
}

if(isset($_POST['update'])){
	global $server;
	mysqli_query($server, "update users set `name` = '$_POST[name]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update users set `email` = '$_POST[email]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update users set `username` = '$_POST[username]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update users set `is_admin` = '$_POST[is_admin]' where id='$_POST[id]'")or die(mysqli_error($server));

		//create activity 
		$activity = userName(getUser()).' updated '.$name.' profile';	
		$date = date('Y-m-d H:i:s');
		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));	
			
	if(!empty($_POST['password'])) {
		$password = $_POST['password'];
		$salt = genRandomPassword(32);
		$crypt = getCryptedPassword($password, $salt);
		$password2 = $crypt.':'.$salt;		
		mysqli_query($server, "update users set `password` = '$password2' where id='$_POST[id]'")or die(mysqli_error($server));
	}
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected user account has been successfully updated.
              </div>    
     </div>         
    <?php
}

?>    
        <div class="col-md-7">
               
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=user" method="get">
                <div class="input-group input-group-sm" style="width: 350px;">
                <input type="hidden" name="url" value="user" />
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$ch=mysqli_query($server, "select * from users where is_customer = '0' order by id desc limit 1000");
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "username LIKE '%$term%' OR email LIKE '%$term%' OR name LIKE '%$term%'";
	    } else {
	         $clauses[] = "username LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$ch=mysqli_query($server, "select * from users where ".$filter. " and  is_customer = '0'");	
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
?>
                <tr>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo $row['username']; ?></td>
                  <td><?php echo $row['email']; ?></td>
                  <td>
                  	<div class="btn-group">
                        <a href="index.php?url=user&edit=<?php echo $row['id']?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                        <a onclick="confirm('Are you sure you want to delete this user?');" href="index.php?url=user&delete=<?php echo $row['id']?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
 

		<div class="col-md-5">
          <div class="box box-primary">
         <?php if(!isset($_GET['edit'])) { ?>
            <div class="box-header with-border"><h4>Add New User </h4></div>
            <?php } else { ?>
            <div class="box-header with-border"><h4>Update User</h4></div>
            <?php } ?>

            <form class="form-horizontal" action="index.php?url=user" method="post" role="form" enctype="multipart/form-data">           
            <div class="box-body" style="width: 90%; margin:0 auto;">
           		<div class="form-group">
                  <label for="type">Selected User Role</label>
                  <select name="is_admin" id="is_admin" class="form-control" style="width: 100%;">
                    <option <?php if(@$rows['is_admin'] == 1) echo 'selected';?> value="1">Administrator</option>			
                    <option <?php if(@$rows['is_admin'] == 0) echo 'selected';?> value="0">User</option>                    
                  </select>
                </div>
                
                <div class="form-group">
                  <label for="name">Full Name</label>
                  <input required="required" type="text" class="form-control" id="name" name="name" value="<?=@$rows['name'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Email</label>
                  <input required type="email" class="form-control" id="email" name="email" value="<?=@$rows['email'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Username</label>
                  <input required type="text" class="form-control" id="username" name="username" value="<?=@$rows['username'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Password</label>
                  <input <?php if(!isset($_GET['edit'])) { echo 'required'; }?> type="text" class="form-control" id="password" name="password" value="" placeholder="<?php if(isset($_GET['edit'])) { echo 'Leave empty to keep current password'; }?>">
                </div>
               
                
            </div>

            <div class="box-footer">
              <button type="reset" class="btn btn-danger">Reset Form</button>
               <?php if(!isset($_GET['edit'])) { ?>
              <button type="submit" name="save" class="btn btn-success">Add New User</button>
              <?php } else { ?>
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update User</button>
              <?php }?>
            </div>
            </form>
                          
        </div>
       </div> 

		</div>    
    </section>