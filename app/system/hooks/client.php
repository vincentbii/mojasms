<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('DATATABLE', true); 

if(!isAdmin(getUser())) { 
$user_id = getUser();
	global $server;
	$ch=mysqli_query($server, "select * from users where id = '$user_id' limit 1");
	$rows = mysqli_fetch_assoc($ch);

?>
    <section class="content-header">
      <h1>
        My Profile
        <small>Update my profile</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">My Profile</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php
if(isset($_POST['update'])){
	global $server;
	mysqli_query($server, "update users set `name` = '$_POST[name]' where id='$_POST[id]'")or die(mysqli_error($server));

		//create activity 
		$activity = userName(getUser()).' updated his profile';	
		$date = date('Y-m-d H:i:s');
		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));	
			
	if(!empty($_POST['password'])) {
		$password = $_POST['password'];
		$salt = genRandomPassword(32);
		$crypt = getCryptedPassword($password, $salt);
		$password2 = $crypt.':'.$salt;		
		mysqli_query($server, "update users set `password` = '$password2' where id='$_POST[id]'")or die(mysqli_error($server));
	}
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
               Your user profile information has been successfully updated.
              </div>    
     </div>         
    <?php
}

?>

		<div class="col-md-10">
          <div class="box box-primary">
            <div class="box-header with-border"><h4>Update Profile</h4></div>

            <form class="form-horizontal" action="index.php?url=client" method="post" role="form" enctype="multipart/form-data">           
            <div class="box-body" style="width: 90%; margin:0 auto;">
                
                <div class="form-group">
                  <label for="name">Full Name</label>
                  <input required="required" type="tel" class="form-control" id="name" name="name" value="<?=@$rows['name'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Email</label>
                  <input readonly="readonly" type="email" class="form-control" id="email" name="email" value="<?=@$rows['email'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Phone</label>
                  <input required="required" type="tel" class="form-control" id="phone" name="phone" value="<?=@$rows['phone'] ?>" placeholder="">
                </div>

                <div class="form-group">
                  <label for="name">Default Sender ID</label>
                  <input required type="text" class="form-control" id="smsSender" name="smsSender" value="<?=@$rows['smsSender'] ?>" placeholder="">
                </div> 
                
                <div class="form-group">
                  <label for="name">Assigned Short-code/Number</label>
                  <input readonly="readonly" type="text" class="form-control" id="short_code" name="short_code" value="<?=@$rows['short_code'] ?>" placeholder="">
                </div> 
                                
                <div class="form-group">
                  <label for="name">Username</label>
                  <input readonly="readonly" type="text" class="form-control" id="username" name="username" value="<?=@$rows['username'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Password</label>
                  <input  type="text" class="form-control" id="password" name="password" value="" placeholder="<?php  echo 'Leave empty to keep your current password'; ?>">
                </div>
               
                
            </div>

            <div class="box-footer">
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update Profile</button>
            </div>
            </form>
                          
        </div>
       </div> 

		</div>    
    </section>
<?php
} else {
if(isset($_GET['edit']) && !empty($_GET['edit'])) {
	global $server;
	$ch=mysqli_query($server, "select * from users where id = '$_GET[edit]' limit 1");
	$rows = mysqli_fetch_assoc($ch);
}


?>
    <section class="content-header">
      <h1>
        Manage Clients
        <small>Manage clients</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Clients</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php

if(isset($_GET['delete'])){
	global $server;
	$sql=mysqli_query($server, "delete from users where id='$_GET[delete]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected user has been successfully deleted.
              </div>    
     </div>         
    <?php
}
if(isset($_POST['product_id'])){
    
	global $server;
	
	$smss=get_api_sms_balance();
	
	if($_POST['product_id']=='sms'){
	             
     $blancey=($smss-get_user_sms_total()) >= 0 ? $smss-get_user_sms_total() : 0;
     
     if(!empty($_POST['credit'])){
         
         if($blancey < $_POST['credit']){
?>
<div class="col-md-12">
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Error!</h4>
   You can only credit client number of sms not greater than <?php echo $blancey?>
  </div>    
</div>
<?php
         }else{
             
             	$curBalance = userData('balance', $_POST['customer_id']);
	            $curBalance=($curBalance >=0) ? $curBalance : 0;
	
             $balance = $_POST['credit']+$curBalance;
             
             mysqli_query($server, "update users set `balance` = '$balance' where id='$_POST[customer_id]'")or die(mysqli_error($server));
        		/*//create transaction'
        		$date = date('Y-m-d H:i:s');
        		$description = $desc;
        		$referrence = time();
        		$add = mysqli_query($server, "INSERT INTO transactions (`date`, `description`, `reference`, `amount`, `package_id`, `status`, `customer_id`, `gateway`) 
        	VALUES ('$date', '$description', '$referrence', '0.00', '0', 'Completed', '$_POST[customer_id]', 'Admin');") or die (mysqli_error($server));
        	
        		//create activity 
        		$activity = userName(getUser()).' added '.$_POST['credit'].' credits to '.userName($_POST['customer_id']).' account';	
        		$date = date('Y-m-d H:i:s');
        		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
        		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));	
            	//send messages 
            	$orderApproveSME = getSetting('newOrderSMS');
            	$orderApproveEmail = getSetting('newOrderEmail');
            	$orderApproveEmailSubject = getSetting('newOrderEmailSubject');
            	$smsSender = getSetting('smsSender');
            	$emailSender = getSetting('businessName');
            	$emailFrom = getSetting('emailSender');
            
            	$username = userData('username', $_POST['customer_id']);
            	$phone = userData('phone', $_POST['customer_id']);
            	$email = userData('email', $_POST['customer_id']);
            	$units = $_POST['credit'];
            	
        	
        		if(!empty($orderApproveSMS)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveSMS);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveSMS);
        			$mail = str_replace('[PACKAGENAME]', 'Admin Transfer', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveSMS);	
        			
        			$response1 = sendMessage($smsSender,$phone,$mail,'0','Admin',$IP);		
        		}
        
        		if(!empty($orderApproveEmail)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveEmail);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveEmail);
        			$mail = str_replace('[PACKAGENAME]', 'Admin Transfer', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveEmail);
        
        			sendEmail($emailFrom,$emailSender,$orderApproveEmailSubject,$email,$mail);
        		}*/
    ?>
    
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
               A total of <?=$_POST['credit']?> credits has been updated to <?=userName($_POST['customer_id'])?>'s account.
              </div>    
     </div> 
    
    <?php
         }
         
         
     }elseif(!empty($_POST['debit'])){ 
             
             $curBalance = userData('balance', $_POST['customer_id']);
	         $curBalance=($curBalance >=0) ? $curBalance : 0;
	
             $balance = $curBalance-$_POST['debit'];
             $balance=($balance >= 0) ? $balance : 0;
             
             mysqli_query($server, "update users set `balance` = '$balance' where id='$_POST[customer_id]'")or die(mysqli_error($server));
        		/*//create transaction'
        		$date = date('Y-m-d H:i:s');
        	
        		//create activity 
        		$activity = userName(getUser()).' deducted '.$_POST['debit'].' credits to '.userName($_POST['customer_id']).' account';	
        		$date = date('Y-m-d H:i:s');
        		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
        		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));	
            	//send messages 
            	$orderApproveSME = getSetting('newOrderSMS');
            	$orderApproveEmail = getSetting('newOrderEmail');
            	$orderApproveEmailSubject = getSetting('newOrderEmailSubject');
            	$smsSender = getSetting('smsSender');
            	$emailSender = getSetting('businessName');
            	$emailFrom = getSetting('emailSender');
            
            	$username = userData('username', $_POST['customer_id']);
            	$phone = userData('phone', $_POST['customer_id']);
            	$email = userData('email', $_POST['customer_id']);
            	$units = $_POST['debit'];
            	
        	
        		if(!empty($orderApproveSMS)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveSMS);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveSMS);
        			$mail = str_replace('[PACKAGENAME]', 'Admin Deduction', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveSMS);	
        			
        			$response1 = sendMessage($smsSender,$phone,$mail,'0','Admin',$IP);		
        		}
        
        		if(!empty($orderApproveEmail)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveEmail);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveEmail);
        			$mail = str_replace('[PACKAGENAME]', 'Admin Deduction', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveEmail);
        
        			sendEmail($emailFrom,$emailSender,$orderApproveEmailSubject,$email,$mail);
        		}*/
?>

    
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
               A total of <?php echo ($curBalance-$_POST['debit']) < 0 ? 0 : $_POST['debit'];?> credits has been deducted from <?=userName($_POST['customer_id'])?>'s account.
              </div>    
     </div> 
        
<?php
         
         
     }
	    
	}else if($_POST['product_id']=='mms'){
	    
	   if(!empty($_POST['credit'])){ 
	       
	         $curBalance = userData('mms_balance', $_POST['customer_id']);
	         $curBalance=($curBalance >=0) ? $curBalance : 0;
	
             $balance = $curBalance+$_POST['credit'];
             $balance=($balance >= 0) ? $balance : 0;
             
             mysqli_query($server, "update users set `mms_balance` = '$balance' where id='$_POST[customer_id]'")or die(mysqli_error($server));
        		/*//create transaction'
        		$date = date('Y-m-d H:i:s');
        	
        		//create activity 
        		$activity = userName(getUser()).' added '.$_POST['debit'].' mms credits to '.userName($_POST['customer_id']).' account';	
        		$date = date('Y-m-d H:i:s');
        		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
        		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));	
            	//send messages 
            	$orderApproveSME = getSetting('newOrderSMS');
            	$orderApproveEmail = getSetting('newOrderEmail');
            	$orderApproveEmailSubject = getSetting('newOrderEmailSubject');
            	$smsSender = getSetting('smsSender');
            	$emailSender = getSetting('businessName');
            	$emailFrom = getSetting('emailSender');
            
            	$username = userData('username', $_POST['customer_id']);
            	$phone = userData('phone', $_POST['customer_id']);
            	$email = userData('email', $_POST['customer_id']);
            	$units = $_POST['credit'];
            	
        	
        		if(!empty($orderApproveSMS)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveSMS);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveSMS);
        			$mail = str_replace('[PACKAGENAME]', 'Admin MMS Transfer', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveSMS);	
        			
        			$response1 = sendMessage($smsSender,$phone,$mail,'0','Admin',$IP);		
        		}
        
        		if(!empty($orderApproveEmail)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveEmail);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveEmail);
        			$mail = str_replace('[PACKAGENAME]', 'Admin MMS Transfer', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveEmail);
        
        			sendEmail($emailFrom,$emailSender,$orderApproveEmailSubject,$email,$mail);
        		}*/
?>
<div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
               A total of <?=$_POST['credit']?> mms credits has been added yo <?=userName($_POST['customer_id'])?>'s account.
              </div>    
     </div> 
<?php
	       
	   }elseif(!empty($_POST['debit'])){ 
	       
	         $curBalance = userData('mms_balance', $_POST['customer_id']);
	         $curBalance=($curBalance >=0) ? $curBalance : 0;
	
             $balance = $curBalance-$_POST['debit'];
             $balance=($balance >= 0) ? $balance : 0;
             
             mysqli_query($server, "update users set `mms_balance` = '$balance' where id='$_POST[customer_id]'")or die(mysqli_error($server));
        		/*//create transaction'
        		$date = date('Y-m-d H:i:s');
        	
        		//create activity 
        		$activity = userName(getUser()).' subtracted '.$_POST['credit'].' mms credits to '.userName($_POST['customer_id']).' account';	
        		$date = date('Y-m-d H:i:s');
        		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
        		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));	
            	//send messages 
            	$orderApproveSME = getSetting('newOrderSMS');
            	$orderApproveEmail = getSetting('newOrderEmail');
            	$orderApproveEmailSubject = getSetting('newOrderEmailSubject');
            	$smsSender = getSetting('smsSender');
            	$emailSender = getSetting('businessName');
            	$emailFrom = getSetting('emailSender');
            
            	$username = userData('username', $_POST['customer_id']);
            	$phone = userData('phone', $_POST['customer_id']);
            	$email = userData('email', $_POST['customer_id']);
            	$units = $_POST['debit'];
            	
        	
        		if(!empty($orderApproveSMS)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveSMS);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveSMS);
        			$mail = str_replace('[PACKAGENAME]', 'Admin MMS Deduction', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveSMS);	
        			
        			$response1 = sendMessage($smsSender,$phone,$mail,'0','Admin',$IP);		
        		}
        
        		if(!empty($orderApproveEmail)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveEmail);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveEmail);
        			$mail = str_replace('[PACKAGENAME]', 'Admin MMS Deduction', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveEmail);
        
        			sendEmail($emailFrom,$emailSender,$orderApproveEmailSubject,$email,$mail);
        		}*/
?>
<div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
               A total of <?php echo ($curBalance-$_POST['debit']) < 0 ? 0 : $_POST['debit'];?> mms credits has been deducted from <?=userName($_POST['customer_id'])?>'s account.
              </div>    
     </div> 
<?php
	       
	   }
	    
	}else if($_POST['product_id']=='vms'){
	    
	     if(!empty($_POST['credit'])){ 
	       
	         $curBalance = userData('vms_balance', $_POST['customer_id']);
	         $curBalance=($curBalance >=0) ? $curBalance : 0;
	
             $balance = $curBalance+$_POST['credit'];
             $balance=($balance >= 0) ? $balance : 0;
             
             mysqli_query($server, "update users set `vms_balance` = '$balance' where id='$_POST[customer_id]'")or die(mysqli_error($server));
        		/*//create transaction'
        		$date = date('Y-m-d H:i:s');
        	
        		//create activity 
        		$activity = userName(getUser()).' added '.$_POST['credit'].' mms credits to '.userName($_POST['customer_id']).' account';	
        		$date = date('Y-m-d H:i:s');
        		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
        		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));	
            	//send messages 
            	$orderApproveSME = getSetting('newOrderSMS');
            	$orderApproveEmail = getSetting('newOrderEmail');
            	$orderApproveEmailSubject = getSetting('newOrderEmailSubject');
            	$smsSender = getSetting('smsSender');
            	$emailSender = getSetting('businessName');
            	$emailFrom = getSetting('emailSender');
            
            	$username = userData('username', $_POST['customer_id']);
            	$phone = userData('phone', $_POST['customer_id']);
            	$email = userData('email', $_POST['customer_id']);
            	$units = $_POST['credit'];
            	
        	
        		if(!empty($orderApproveSMS)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveSMS);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveSMS);
        			$mail = str_replace('[PACKAGENAME]', 'Admin VMS Transfer', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveSMS);	
        			
        			$response1 = sendMessage($smsSender,$phone,$mail,'0','Admin',$IP);		
        		}
        
        		if(!empty($orderApproveEmail)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveEmail);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveEmail);
        			$mail = str_replace('[PACKAGENAME]', 'Admin VMS Transfer', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveEmail);
        
        			sendEmail($emailFrom,$emailSender,$orderApproveEmailSubject,$email,$mail);
        		}*/
?>
<div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
               A total of <?=$_POST['credit']?> vms credits has been added to <?=userName($_POST['customer_id'])?>'s account.
              </div>    
     </div> 
<?php
	       
	   }elseif(!empty($_POST['debit'])){ 
	       
	         $curBalance = userData('vms_balance', $_POST['customer_id']);
	         $curBalance=($curBalance >=0) ? $curBalance : 0;
	
             $balance = $curBalance-$_POST['debit'];
             $balance=($balance >= 0) ? $balance : 0;
             
             mysqli_query($server, "update users set `vms_balance` = '$balance' where id='$_POST[customer_id]'")or die(mysqli_error($server));
        		/*create transaction'
        		$date = date('Y-m-d H:i:s');
        	
        		create activity 
        		$activity = userName(getUser()).' subtracted '.$_POST['debit'].' vms credits to '.userName($_POST['customer_id']).' account';	
        		$date = date('Y-m-d H:i:s');
        		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
        		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));	
            	send messages 
            	$orderApproveSME = getSetting('newOrderSMS');
            	$orderApproveEmail = getSetting('newOrderEmail');
            	$orderApproveEmailSubject = getSetting('newOrderEmailSubject');
            	$smsSender = getSetting('smsSender');
            	$emailSender = getSetting('businessName');
            	$emailFrom = getSetting('emailSender');
            
            	$username = userData('username', $_POST['customer_id']);
            	$phone = userData('phone', $_POST['customer_id']);
            	$email = userData('email', $_POST['customer_id']);
            	$units = $_POST['debit'];
            	
        	
        		if(!empty($orderApproveSMS)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveSMS);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveSMS);
        			$mail = str_replace('[PACKAGENAME]', 'Admin VMS Deduction', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveSMS);	
        			
        			$response1 = sendMessage($smsSender,$phone,$mail,'0','Admin',$IP);		
        		}
        
        		if(!empty($orderApproveEmail)) {
        			$mail = str_replace('[USERNAME]', $username, $orderApproveEmail);	
        			$mail = str_replace('[BALANCE]', $balance, $orderApproveEmail);
        			$mail = str_replace('[PACKAGENAME]', 'Admin VMS Deduction', $orderApproveSMS);
        			$mail = str_replace('[UNITS]', $units, $orderApproveEmail);
        
        			sendEmail($emailFrom,$emailSender,$orderApproveEmailSubject,$email,$mail);
        		}*/
?>
<div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
               A total of <?php echo ($curBalance-$_POST['debit']) < 0 ? 0 : $_POST['debit'];?> vms credits has been deducted from <?=userName($_POST['customer_id'])?>'s account.
              </div>    
     </div> 
<?php
	   }
	    
	}
     
}     

if(isset($_POST['save'])){
	global $server;
		$user_id = getUser();
		$date = date('Y-m-d');
		$error = 0;
		$password = $_POST['password'];
		$salt = genRandomPassword(32);
		$crypt = getCryptedPassword($password, $salt);
		$password2 = $crypt.':'.$salt;	
		
		if(userAvailable($_POST['username'])) {
			$add = mysqli_query($server, "INSERT INTO users (`name`, `username`, `email`, `password`, `is_customer`, `phone`, `smsSender`, `short_code`) 
			VALUES ('$_POST[name]', '$_POST[username]', '$_POST[email]', '$password2', '1', '$_POST[phone]', '$_POST[smsSender]', '$_POST[short_code]');") or die (mysql_error($server));

		//create activity 
		$activity = userName(getUser()).' created a new client '.$_POST['name'];	
		$date = date('Y-m-d H:i:s');
		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));	
					
			//send email to client
			$mail = getSetting('newClientEmail');
			$mail = str_replace('[NAME]', $_POST['name'], $mail);
			$mail = str_replace('[USERNAME]', $_POST['username'], $mail);
			$mail = str_replace('[PASSWORD]', $_POST['password'], $mail);
			
			$sender = getSetting('businessName');
			$subject = getSetting('newClientEmailSubject');
			$from = getSetting('smtpUsername');
			
			sendEmail($from,$sender,$subject,$_POST['email'],$mail);
			
			//send sms to client
			
			
		} else {
			$error = 1;
			$alert = 'Sorry but a user or client already exist with same username "'.$_POST['username'].'". Please choose another username.';	
		}
	if($error < 1) { ?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The new client's profile was successfully created. The new client will now be able to view status of campaigns assigned to him.
              </div>    
     </div>         
    <?php } else { ?>
    
    <div class="col-md-12">
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                <?=$alert?>
              </div>    
     </div>      
    <?php }
}

if(isset($_POST['update'])){
	global $server;
	mysqli_query($server, "update users set `name` = '$_POST[name]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update users set `email` = '$_POST[email]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update users set `username` = '$_POST[username]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update users set `phone` = '$_POST[phone]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update users set `smsSender` = '$_POST[smsSender]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update users set `short_code` = '$_POST[short_code]' where id='$_POST[id]'")or die(mysqli_error($server));
	
	if(!empty($_POST['password'])) {
		$password = $_POST['password'];
		$salt = genRandomPassword(32);
		$crypt = getCryptedPassword($password, $salt);
		$password2 = $crypt.':'.$salt;		
		mysqli_query($server, "update users set `password` = '$password2' where id='$_POST[id]'")or die(mysqli_error($server));
	}
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected client account has been successfully updated.
              </div>    
     </div>         
    <?php
}

?>    
        <div class="col-md-7">
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=client" method="get">
                <div class="input-group input-group-sm" style="width: 350px;">
                <input type="hidden" name="url" value="user" />
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Username</th>
                  <th>SMS Balance</th>
                  <th>VMS Balance</th>
                  <th>MMS Balance</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$ch=mysqli_query($server, "select * from users where is_customer = '1' order by id desc limit 1000");
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "username LIKE '%$term%' OR email LIKE '%$term%' OR name LIKE '%$term%'";
	    } else {
	         $clauses[] = "username LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$ch=mysqli_query($server, "select * from users where ".$filter. " and  is_customer = '1'");	
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
?>
                <tr>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo $row['username']; ?></td>
                  <td><?php echo !empty($row['balance']) ? $row['balance'] : 0; ?></td>
                  <td><?php echo !empty($row['vms_balance']) ? $row['vms_balance'] : 0; ?></td>
                  <td><?php echo !empty($row['mms_balance']) ? $row['mms_balance'] : 0; ?></td>
                  <td>
                  	<div class="btn-group">
                        <a href="index.php?url=client&edit=<?php echo $row['id']?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                        <a onclick="confirm('Are you sure you want to delete this client?');" href="index.php?url=client&delete=<?php echo $row['id']?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Username</th>
                  <th>SMS Balance</th>
                  <th>VMS Balance</th>
                  <th>MMS Balance</th>
                  <th>Email</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
 

		<div class="col-md-5">
         <div class="box box-warning">
            <form class="form-horizontal" action="index.php?url=client" method="post" role="form" enctype="multipart/form-data">         
         <?php if(!isset($_GET['edit'])) { ?>
            <div class="box-header with-border"><h4>Update Units to Client's Account </h4></div>
            <?php } else { ?>
            <div class="box-header with-border"><h4>Update Units to <?=userName($_GET['edit'])?>'s Account</h4></div>
            <?php } ?>
				<div class="box-body" style="width: 90%; margin:0 auto;">
                <div class="form-group">
                  <label>Select Client</label>
                  <select name="customer_id" class="form-control select2" style="width: 100%;">
<?php global $server;
	$ch=mysqli_query($server, "select * from users where is_customer = 1 order by id desc");
	while ($rows2 = mysqli_fetch_assoc($ch)) {
?>                    
                    <option <?php if(@$_GET['edit']==$rows2['id']) echo 'selected' ?> value="<?=$rows2['id']?>"><?=$rows2['username']?></option>
<?php } ?>                  
                  </select>
                </div>
                
                <div class="form-group">
                  <label>Select Product</label>
                  <select name="product_id" class="form-control select2" style="width: 100%;">
                    <option value="sms">SMS</option>
                    <option value="vms">VMS</option>
                    <option value="mms">MMS</option>
                  </select>
                </div>
                
                
                <div class="form-group hidden">
                  <label for="name">Total Available Units</label>
                  <input class="form-control" value="0" readonly>
                </div>
                               
                <div class="form-group">
                  <label for="name">Units to add User</label>
                  <input type="number" class="form-control" id="units" name="credit" value="0" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Units to Subtract User</label>
                  <input type="number" min="0" class="form-control" id="units_subtract" name="debit" value="0" placeholder="">
                </div>
                
            </div>
            <div class="box-footer">
              <button type="reset" class="btn btn-danger">Reset Form</button>
              <button type="submit" name="saveCredit" class="btn btn-success">Update Units</button>
            </div>            
           </form>
                
          </div>  
          
          <div class="box box-primary">
         <?php if(!isset($_GET['edit'])) { $rows['short_code'] = getSetting('defaultShortCode');?>
            <div class="box-header with-border"><h4>Add New Client </h4></div>
            <?php } else { ?>
            <div class="box-header with-border"><h4>Update Client</h4></div>
            <?php } ?>

            <form class="form-horizontal" action="index.php?url=client" method="post" role="form" enctype="multipart/form-data">           
            <div class="box-body" style="width: 90%; margin:0 auto;">
                
                <div class="form-group">
                  <label for="name">Full Name</label>
                  <input required="required" type="text" class="form-control" id="name" name="name" value="<?=@$rows['name'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Email</label>
                  <input required type="email" class="form-control" id="email" name="email" value="<?=@$rows['email'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Phone</label>
                  <input required type="text" class="form-control" id="phone" name="phone" value="<?=@$rows['phone'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Assign Sender ID</label>
                  <input type="text" class="form-control" id="smsSender" name="smsSender" value="<?=@$rows['smsSender'] ?>" placeholder="">
                </div> 
                
                <div class="form-group">
                  <label for="name">Assign Short-code/Number</label>
                  <input type="text" class="form-control" id="short_code" name="short_code" value="<?=@$rows['short_code'] ?>" placeholder="">
                </div>             
                
                <div class="form-group">
                  <label for="name">Username</label>
                  <input required type="text" class="form-control" id="username" name="username" value="<?=@$rows['username'] ?>" placeholder="">
                </div>
                
                <div class="form-group">
                  <label for="name">Password</label>
                  <input <?php if(!isset($_GET['edit'])) { echo 'required'; }?> type="text" class="form-control" id="password" name="password" value="" placeholder="<?php if(isset($_GET['edit'])) { echo 'Leave empty to keep current password'; }?>">
                </div>
               
                
            </div>

            <div class="box-footer">
              <button type="reset" class="btn btn-danger">Reset Form</button>
               <?php if(!isset($_GET['edit'])) { ?>
              <button type="submit" name="save" class="btn btn-success">Add New Client</button>
              <?php } else { ?>
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update Client</button>
              <?php }?>
            </div>
            </form>
                          
        </div>
       </div> 

		</div>    
    </section>
    
<?php } ?>    