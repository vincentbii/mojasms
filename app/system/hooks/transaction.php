<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//if(!isAdmin(getUser())) { header('location: index.php'); } 
SESSION_START();
define('DATATABLE', true);
    /**$currency_code1 = file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip);
    echo $res1 = json_decode($currency_code1);
    $exchange_rate1 = $currency_code1->geoplugin_currencyConverter;
    $curr_symbol1 = $currency_code1->geoplugin_currencyCode;**/
?>
    <section class="content-header">
      <h1>
        Transaction Log
        <small>View all transactions</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Transaction Log</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	if(isAdmin(getUser())) {
	$sql=mysqli_query($server, "delete from transactions where id='$_GET[delete]'")or die(mysqli_error($server));
	}
  else{
    $sql=mysqli_query($server, "delete from transactions where id='$_GET[delete]'")or die(mysqli_error($server));
  }
	//show mesage
	?>
    <div class="col-xs-12">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected transactions has been successfully deleted from your transactions log.
              </div>    
     </div>         
    <?php
}
?>
<?php 
if(isset($_GET['approve'])){
	global $server;
	if(isAdmin(getUser())) {
	    
	    $ok=0;
	    
	    $referrence=$_GET['approve'];
	    
	    $result = mysqli_query($server, "SELECT * FROM transactions WHERE reference = '$referrence'"); 
    	$row = mysqli_fetch_assoc($result);
    	$status = $row['status'];	
    	$package_qty = $row['package_qty'];
    		
    	if($status != 'Completed') {
    	    
    	    $package=$row['package_id'];
    	    
    	    if(!empty($package)){
    	        
    	        
    	        $result = mysqli_query($server, "SELECT * FROM packages WHERE id = '$package'"); 
            	$row = mysqli_fetch_assoc($result);
            	$type = $row['type'];
    	        
    	        if($type=='sms'){
    	          
    	          $blancey=(get_api_sms_balance()-get_user_sms_total()) >= 0 ? get_api_sms_balance()-get_user_sms_total() : 0;
    	          
    	          if($package_qty > $blancey){
    	              $ok=1;
?>
<div class="col-xs-12">
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Error!</h4>
    You don't have enough sms to approve this requested <strong><?php echo $package_qty;?></strong>. Available balance is <strong><?php echo $blancey;?></strong>
  </div>    
</div>
<?php
    	          }  
    	            
    	        }
    	        
    	    }
    	    
    	}
	    
	   if($ok==0){
		processTransaction($_GET['approve']);
	   }
		
	}
	//show mesage
	if($ok==0){
	?>
    <div class="col-xs-12">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected transactions has been successfully approved. Purchased product has been added to the client's account.
              </div>    
     </div>         
    <?php
	}
}
?>     
        <div class="col-xs-12">
               
          <div class="box"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=transaction" method="get">
                <input type="hidden" name="url" value="transaction" />
                <div class="input-group input-group-sm" style="width: 350px;">
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Reference No</th>
                  <th>Date</th>
                  <th>Customer</th>
                  <th>Product</th>
                  <th>Amount</th>
                  <th>Gateway</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$user_id = getUser();
	$ch=mysqli_query($server, "select * from transactions where customer_id = '$user_id' order by id desc limit 1000");
	if(isAdmin(getUser())) {
	$ch=mysqli_query($server, "select * from transactions order by id desc limit 1000");	
	}
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "id LIKE '%$term%' OR reference LIKE '%$term%' OR status LIKE '%$term%' OR date LIKE '%$term%' OR description LIKE '%$term%'";
	    } else {
	         $clauses[] = "id LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$user_id = getUser();
	$ch=mysqli_query($server, "select * from transactions where customer_id = '$user_id' and ".$filter);	
	if(isAdmin(getUser())) {
	$ch=mysqli_query($server, "select * from transactions where ".$filter);		
	}
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {

?>
                <tr>
                  <td><?php echo $row['id']; ?></td>
                  <td><?php echo $row['reference']; ?></td>
                  <td><?php echo $row['date']; ?></td>
                  <td><?php echo getUserName($row['customer_id']); ?></td>
                  <td>
                  <?php 
                  
                  if(!empty($row['package_id'])){
                      echo '<strong>'.$row['package_qty']." SMS</strong> (from Package - ".packageName($row['package_id']).')';
                  }elseif(!empty($row['cellphone_id'])){
                      echo "Cellphone: -".cell_numbers($row['cellphone_id'])['number'];
                  }elseif(!empty($row['virtual_number_id'])){
                      echo "Virtual number: -".virtual_numbers($row['virtual_number_id'])['number'];
                  }
                  
                  ?></td>
                  <td><?php echo currencySymbul(getSetting('defaultCurrency')).round($row['amount']); ?></td>
                  <!--<td><?php  $R_099 = round($_SESSION[exchange_rate] * $row['amount'] * 0.069,2); echo $_SESSION[curr_symbol].$R_099 ;  ?></td>-->
                  <td><?php echo paymentGatewayName($row['gateway']); ?></td>
                  <td><?php echo $row['status']; ?> </td>
                  <td style="width:15%">
                  	<div class="btn-group">
                  	    <a href="index.php?url=preview_invoice&tid=<?php echo $row['id'];?>" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                  	   
                     <?php if(isAdmin(getUser())) { ?>
                        
                        <a href="index.php?url=transaction&delete=<?php echo $row['id'];?>" class="deletebtn">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                        <?php if($row['status'] != 'Completed') {?>
                        	<a href="index.php?url=transaction&approve=<?php echo $row['reference'];?>" class="approvebtn">
                 		<button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button></a>
                 		
                        <?php } ?>
                     <?php }else{ ?> 
                     
                          <?php if($row['status'] != 'Completed') {?>
                            <a href="index.php?url=transaction&delete=<?php echo $row['id'];?>" class="deletebtn">
                                <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                         <?php
                         }
                         ?>
                     
                     <?php
                     }
                     ?>
                	</div>
                  </td>

<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Reference No</th>
                  <th>Date</th>
                  <th>Customer</th>
                  <th>Product</th>
                  <th>Amount</th>
                  <th>Gateway</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>

      </div>   
    
    </section>