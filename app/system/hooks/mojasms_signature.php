<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('DATATABLE', true); 
if(!isAdmin(getUser())) { header('location: index.php'); } 
?>
    <section class="content-header">
      <h1>
        Footer Signature
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Footer Signature Tools</a></li>
        <li class="active">signature</li>
      </ol>
    </section>

    <section class="content">
    <div class="row">


<style>
/* Dotted red border */
hr {
  border-top: 1px dotted red;
}
</style>
<?php 

global $server;


if(isset($_POST['signature'])){ $signature=$_POST['signature'];

    
    $sql="UPDATE `sms_signature` SET signature='$signature' WHERE id='".$_GET['edit']."'";
		           
if(mysqli_query($server, $sql)){
		          
?>
<div class="col-md-12">
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Cool</h4>
    Signature updated successfully 
  </div>    
 </div> 
<?php
		           }else{
?>
<div class="col-md-12">
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Error!</h4>
    Try to update once again. 
  </div>    
 </div> 
<?php
		           }
}

if(isset($_GET['delete'])){
	
	$user_id = getUser();
	if(isAdmin(getUser())) {
	    $buju=mysqli_query($server, "select * from sms_signature WHERE id='".$_GET['delete']."' LIMIT 1");
	    while ($rower = mysqli_fetch_assoc($buju)) {
      	    $status=$rower['status'];
      	}
      	
      	if($status==0){
		    $sql=mysqli_query($server, "update sms_signature SET status='1' where id='$_GET[delete]' ")or die(mysqli_error($server));
      	}else{
      	    $sql=mysqli_query($server, "update sms_signature SET status='0' where id='$_GET[delete]' ")or die(mysqli_error($server));
      	}
	}
	//show mesage
	?>
    <div class="col-xs-12">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected signature has been successfully updated.
              </div>    
     </div>         
    <?php
}

if(isset($_GET['edit'])){
?>


       <form class="form-vertial" action="index.php?url=mojasms_signature&edit=<?php echo $_GET['edit'];?>" method="post" role="form" enctype="multipart/form-data">           
		<div class="col-md-12">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Update SMS Signature</h4></div>
            
                <div class="box-body">
                    <div class="form-group">
                      <label>Signature</label>
                      
                      <?php 
                      
                      $chu=mysqli_query($server, "select * from sms_signature WHERE id='".$_GET['edit']."'");
                      $signature="";
                      	while ($rower = mysqli_fetch_assoc($chu)) {
                      	    $signature=$rower['signature'];
                      	}
                      ?>
                      <textarea class="form-control" name="signature" required rows="6" placeholder="Write Signature"><?php echo $signature;?></textarea>
                    </div> 
                </div>
                 <div class="box-footer">
                  <button type="submit" name="save" class="btn btn-success">Update</button>
                </div>
                
          </div>
        </div>
       </form>
<?php
}
?>
     </div>
     <div class="row">
         <div class="col-md-12">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Signatures</h4></div>
            
                <div class="box-body">
                          <table id="example1" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>Signature</th>
                                      <th>Status</th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                   <?php 
                                      $i=1;
                                      
                                      $ch=mysqli_query($server, "select * from sms_signature");
                                      	while ($row = mysqli_fetch_assoc($ch)) {
                                      ?>
                                      <tr>
                                          <td><?php echo $i++;?></td>
                                          <td><?php echo $row['signature'];?></td>
                                          <td><?php echo ($row['status']==1) ? ('<span class="label label-danger">inactive</span>') : ('<span class="label label-success">active</span>');?></td>
                                          <td>
                                              <a href="index.php?url=mojasms_signature&edit=<?php echo $row['id'];?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                              <a onclick="confirm('Are you sure to deactivate?'){return true;}" href="index.php?url=mojasms_signature&delete=<?php echo $row['id'];?>" class="btn btn-danger"><i class="fa fa-ban"></i></a>
                                          </td>
                                      </tr>
                                      <?php
                                      	}
                                      ?>
                              </tbody>
                          </table>
                </div>
          </div>
        </div> 
     </div>
    </section>













