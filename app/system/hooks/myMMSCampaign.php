<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(isset($_GET['view']) && !empty($_GET['view'])) {
	global $server;
	$ch=mysqli_query($server, "select * from mms_campaigns where id = '$_GET[view]' limit 1");
	if(!isAdmin(getUser())) { 
		$user_id = getUser();
		$ch=mysqli_query($server, "select * from mms_campaigns where id = '$_GET[view]' and customer_id = '$user_id' limit 1");
	}
	$rows = mysqli_fetch_assoc($ch);
	
?>	
    <section class="content-header">
      <h1>
        View MMS Campaign
        <small>See details of selected MMS Campaign</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="index.php?url=index.php"><i class="fa fa-dashboard"></i> Manage MMS Campaigns</a></li>
        <li class="active">View MMS Campaign #<?php echo $_GET['view']; ?></li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
 
         <div class="col-md-12">               
          <div class="box">
            <div class="box-header"><h3><?php echo $rows['name']; ?></h3></div>
<div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <td width="30%">Campaign type</td>
                  <td> <?php echo $rows['type']?></td>
                </tr>
                <tr>
                  <td>Created on</td>
                  <td> <?php echo date('F d, Y h:i A', strtotime($rows['start_date']))?><</td>
                </tr>
                <tr>
                  <td>Created by</td>
                  <td> <?php echo userName($rows['user_id']) ?></td>
                </tr>     
<?php if($rows['customer_id'] > 0) {?>
                <tr>
                  <td>Created For</td>
                  <td> <?php echo userName($rows['customer_id']); ?></td>
                </tr>
<?php } ?>                                               
                <tr>
                  <td>Start date</td>
                  <td> <?php echo date('F d, Y h:i A', strtotime($rows['date']))?></td>
                </tr>
                <tr>
                  <td>Cycle</td>
                  <td> <?php echo getCycle($rows['repeats']) ?></td>
                </tr>                
<?php if($rows['intervals'] > 0) { ?>
                <tr>
                  <td>Intervals</td>
                  <td> <?php echo getInterval($rows['intervals'])?></td>
                </tr>
<?php }?>
                <tr>
                  <td>Sender ID</td>
                  <td> <?php echo $rows['sender_id'] ?></td>
                </tr>
<?php if(!empty($rows['message'])) {?>                
                <tr>
                  <td>Message</td>
                  <td> <?php echo $rows['message']?></td>
                </tr>
<?php } if(!empty($rows['file'])) {?>
                <tr>
                  <td>File</td>
                  <td> <?php echo '<a href="/uploads/'.$rows['file'].'">'.$rows['file'].'</a>'; ?></td>
                </tr>
<?php } ?>  
<?php if($rows['marketinglist_id'] > 0) { 
	 ?>
                <tr>
                  <td>Marketing List</td>
                  <td> <?php echo listName($rows['marketinglist_id']); ?></td>
                </tr>
<?php } ?>               
                <tr>
                  <td>Total Recipients</td>
                  <td> <?php echo countCommas($rows['recipient'])?></td>
                </tr>                 
                <tr>
                  <td>Progress</td>
                  <td> 
				<div class="progress-group">
                 <span class="progress-number"><b><?php echo campaignProgress($rows['id']); ?></b>/100</span>
                 <div class="progress sm active">
                    <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="<?php echo campaignProgress($rows['id']); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo campaignProgress($rows['id']); ?>%">
                      <span class="sr-only"><?php echo campaignProgress($rows['id']); ?>% Complete (success)</span>
                    </div>
                  </div>
               </div>				  
                  </td>
                </tr>         
                                                      
              </table><br /><br />
            </div>
           </div>
         </div>  
 
          
          <div class="col-md-7">               
            <div class="box">  
            	<div class="box-body"><h4><i class="fa fa-list"></i> Sent Message Log</h4>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Date</th>
                  <th>Recipient</th>
                  <th>Route</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$ch=mysqli_query($server, "select * from sentmessages where mms_campaign_id = '$rows[id]' order by id desc");
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
		if($row['status'] == 'Sent') {
			$status = '<span class="badge bg-green">Sent</span>';	
		} else {
			$status = '<span class="badge bg-red">Failed</span>';	
		}
		if(!empty($row['error'])) {
			$status .= '&nbsp;&nbsp;<a title="'.$row['error'].'"><i class="fa  fa-exclamation-triangle text-yellow"></i></a>';
		}
?>
                <tr>
                  <td><?php echo $row['date']; ?></td>
                  <td><?php echo $row['recipient']; ?></td>
                  <td><?php echo gatewayName($row['gateway_id']); ?></td>
                  <td><?=$status?></td>
                </tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Date</th>
                  <th>Recipient</th>
                  <th>Route</th>
                  <th>Status</th>
                </tr>
                </tfoot>
              </table>            		
                    
            	</div>           
          </div>
        </div>        
      	
      </div>
    </section>  
    
<?php
} else {
?>
    <section class="content-header">
      <h1>
        Manage Campaigns
        <small>Manage all MMS Campaigns</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Manage Campaign</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	if(!isAdmin(getUser())) { 
	} else {
	$sql=mysqli_query($server, "delete from mms_campaigns where id='$_GET[delete]'")or die(mysqli_error($server));
	$sql=mysqli_query($server, "delete from sentmessages where mms_campaign_id='$_GET[delete]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected mms campaign has been successfully deleted.
              </div>    
     </div>         
    <?php
	}
}
?> 
        <div class="col-md-12">
               
          <div class="box"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=myMMSCampaign" method="get">
                <div class="input-group input-group-sm" style="width: 350px;">
                <input type="hidden" name="url" value="myCampaign" />
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th>
                  <th>Title</th>
                  <th>Type</th>
                  <th>File</th>
                  <th>Created on</th>
                  <th>Start on</th>
                  <th>Progress</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$ch=mysqli_query($server, "select * from mms_campaigns order by id desc limit 1000");
	if(!isAdmin(getUser())) { 
		$user_id = getUser();
		$ch=mysqli_query($server, "select * from mms_campaigns where customer_id = '$user_id' order by id desc limit 1000");
	}
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "name LIKE '%$term%' OR message LIKE '%$term%' OR recipient LIKE '%$term%' OR type LIKE '%$term%'";
	    } else {
	         $clauses[] = "name LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$ch=mysqli_query($server, "select * from mms_campaigns where ".$filter);	
		if(!isAdmin(getUser())) { 
			$user_id = getUser();
			$ch=mysqli_query($server, "select * from mms_campaigns where ".$filter." and customer_id = '$user_id'");	
		}
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
		
		$link=$row['type'];
?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo $row['type']; ?></td>
                  <td><a href="/uploads/<?php echo $row['file']; ?>" download>Download File</a></td>
                  <td><?php echo $row['date']; ?></td>
                  <td><?php echo $row['start_date']; ?></td>
                  <td>
                  	<div class="progress progress-xs progress-striped active">
                      <div class="progress-bar progress-bar-primary" style="width: <?php echo campaignProgress($row['id']); ?>%"></div>
                    </div>
                  </td>
                  <td>
                  	<div class="btn-group">
                   		<a href="index.php?url=myMMSCampaign&view=<?php echo $row['id'];?>" title="Details">
                   		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></button></a>
                        <?php if(isAdmin(getUser())) { ?>
                        <a onclick="confirm('Are you sure you want to delete this campaign?');" href="index.php?url=myMMSCampaign&delete=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                        <?php } ?>
                        <!--<a href="index.php?url=newMMSCampaign&use=<?php echo $row['id'];?>" title="Resend">
                		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-share"></i></button></a>-->
                	</div>
                  </td>
                </tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S/N</th>
                  <th>Title</th>
                  <th>Type</th>
                  <th>File</th>
                  <th>Created on</th>
                  <th>Start on</th>
                  <th>Progress</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>

      </div>
    </section>  
<?php } ?>    