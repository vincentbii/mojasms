<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(isset($_REQUEST['checkUser'])) { 
	$email = $_REQUEST['checkUser'];
	$Config = new CI_Config();
	$server=mysqli_connect($Config->db_host(), $Config->db_username(), $Config->db_password());
	$db=mysqli_select_db($server, $Config->db_name()); 
	
	$query = "SELECT * FROM users WHERE email = '$email'";
	$result = mysqli_query($server, $query) or die(mysqli_error($server));
	$num = mysqli_num_rows($result);	
	if($num < 1) {
		 $found = 0;
	} else {
		$found = 1;
	}
	echo $found;
	die($num);	
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>MojaSMS - Sign in</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Roboto+Mono:300,400,500"> 
    <link rel="stylesheet" href="/fonts/icomoon/style.css">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/css/animate.css">
    
    
    <link rel="stylesheet" href="/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="/css/fl-bigmug-line.css">
  
    <link rel="stylesheet" href="/css/aos.css">

    <link rel="stylesheet" href="/css/style.css">
    
    
    
     <!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Add icon library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script sric="https://cdnjs.cloudflare.com/ajax/libs/jquery-localScroll/1.4.0/jquery.localScroll.min.js"></script>


<!--Social icon Properties -->
<style>

.fa {
  padding:5px;
  font-size: 20px;
  width: 30px;
  height:30px;
  text-align: center;
  text-decoration: none;
  border-radius: 15px;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-youtube {
  background: #bb0000;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

.fa-envelope {
    color:#f1d592;
}

.fa-pinterest {
  background: #cb2027;
  color: white;
}

.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.fa-skype {
  background: #00aff0;
  color: white;
}

.fa-whatsapp {
  background: #25D366;
  color: white;
}
</style>

  </head>
<?php
$Config = new CI_Config();
$server=mysqli_connect($Config->db_host(), $Config->db_username(), $Config->db_password());
$db=mysqli_select_db($server, $Config->db_name()); 

if(isset($_POST['userEmail'])) {
	$email2 = $_POST['userEmail'];	
	$query = "SELECT * FROM users WHERE email = '$email2'";
	$result = mysqli_query($server, $query);
	$num = mysqli_num_rows($result);	
	if($num < 1) {
		 $message = "Oops! This email is not associated with any user here. Please check your email";
	} else {
		$password = rand(199999, 999999);
		$salt = genRandomPassword(32);
		$crypt = getCryptedPassword($password, $salt);
		$password2 = $crypt.':'.$salt;
		mysqli_query($server,"UPDATE  `users` SET `password` =  '$password2'	WHERE  `email` = '$email2';");
								
		$query="SELECT * FROM users WHERE email = '$email2'"; 
		$result = mysqli_query($server, $query);  
		$row = mysqli_fetch_assoc($result); 
		$username = $row['username'];	
		$name = $row['name'];
		$email = $row['email'];
		$phone = $row['phone'];
		
		$mail = 'Dear '.$name.'<br><br>Your '.getSetting('businessName').' login password has been recovered.<br>Your username still remains '.$username.' while your new password is '.$password.'.<br><br><hr><br>Message sent via '.getSetting('businessName').'';
		sendEmail('info@mojasms.com',' MOJA SMS','Your New Login Password',$email,$mail);
		sendMessage(getSetting('smsSender'),$phone,$mail,'0','System','127.0.0.1');
		
		$message = "A new password has been sent to your registered email address.";
	}
}

	
if(isset($_POST['access_login'])) {
define('TIMEOUT_MINUTES', 60);	
$timeout = (TIMEOUT_MINUTES == 0 ? 0 : time() + TIMEOUT_MINUTES * 1200);
	
  $login = $_POST['access_login'];
  $password = $_POST['access_password'];
  	$query="SELECT * FROM users WHERE username = '$login'"; 
	$result = mysqli_query($server,$query);  
	$row = mysqli_fetch_assoc($result); 
	$num = mysqli_num_rows($result);
	if($num < 1) {
    	$message = "Your login detail is not associated with any user here. Please check your details";
	} else {
		$pwsalt = explode( ":",$row["password"]);	
		$pass2 = $row["password"];
		$tracking = $row['tracking'];
		
		if(md5($password . $pwsalt[1]) != $pwsalt[0] && md5($password) != $row["password"]) {	
		   $message = "Oops! Your username or password is incorrect.";
		} elseif ($tracking > 0) {
			 $message = "Oops! Your account is yet to be activated. Please contact admin for activation";
		} else {
			$query="SELECT * FROM users WHERE username = '$login'"; 
			$result = mysqli_query($server,$query);  
			$row = mysqli_fetch_assoc($result); 
			$user_id = $row['id'];
			
			// set cookie if password was validated
			$_SESSION['MOBIKETA'] = md5($login.'%'.$pass2);
			setcookie("MOBIKETA", $user_id, $timeout, '/'); 
			//set time zone 
			
			//Set Last Login Date and Time
			$day = date("Y-m-d H:i:s");
			mysqli_query($server, "UPDATE  `users` SET  `last_login` =  '$day' WHERE `id` ='$user_id'");
			   
		   header('location: index.php');
	  }
  }	
}

//register
if(isset($_GET['joined'])) {
	//redirect to login page with message 
	$message = 'Congratulations. Your '.getSetting('businessName').' account has been created. <br>Your username is "<strong>'.$_SESSION['username'].'</strong>".<br>Other account information has been sent to your email address.';
}

$siteName = getSetting('businessName');
?>
<body>
   <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    
    <div class="site-navbar-wrap bg-white">
      <div class="site-navbar-top">
        <div class="container py-2">
          <div class="row align-items-center">
            
            <div class="col-6 col-md-6 col-lg-2">
              <a href="/index.php" class="d-flex align-items-center site-logo">
                 <img class="img-responsive" src="/logo.jpeg" alt="Mojasms Logo" style="height:50px">
              </a>
            </div>

            <div class="col-6 col-md-6 col-lg-10">
              <ul class="unit-4 ml-auto text-right">

                <li class="text-left">
                  <a href="www.facebook.com/mojasms">
                    <div class="d-flex align-items-center block-unit">
                      <div class="icon mr-0 mr-md-4">
                        <span class="fa fa-facebook fa-2x h3"></span>
                      </div>
                      <div class="d-none d-lg-block">
                        <span class="d-block text-gray-500 text-uppercase">Facebook</span>
                        <span class="h6">Mojasms </span>
                      </div>
                    </div>
                  </a>
                </li>


                <li class="text-left">
                  <a href="#">
                    <div class="d-flex align-items-center block-unit">
                      <div class="icon mr-0 mr-md-4">
                        <span class="fa fa-envelope fa-2x h5"></span>
                      </div>
                      <div class="d-none d-lg-block">
                        <span class="d-block text-gray-500 text-uppercase">Email</span>
                        <span class="h6">info@mojasms.com</span>
                      </div>
                    </div>
                  </a>
                </li>

                <li class="text-left">
                  <a href="#">
                    <div class="d-flex align-items-center block-unit">
                      <div class="icon mr-0 mr-md-4">
                       <a href="https://wa.me/2779479-4811"> <span class="fa fa-whatsapp fa-2x h2" aria-hidden="true"></span></a>
                      </div>
                      <div class="d-none d-lg-block">
                        <span class="d-block text-gray-500 text-uppercase">WhatsApp</span>
                        <span class="h6">Chat With Us Now</span>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </div>

            

          </div>
          
        </div>
      </div>
       <div class="site-navbar bg-dark">
        <div class="container py-1">
          <div class="row align-items-center">

            <div class="col-4 col-md-4 col-lg-8">
              <nav class="site-navigation text-left" role="navigation">
                <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3">
		<a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

                <ul class="site-menu js-clone-nav d-none d-lg-block">
                  <li class="active">
                    <a href="/index.php">Home</a>
                  </li>
                  
                  <li class="has-children">
                    <a href="#">Products/Services</a>
                    <ul class="dropdown arrow-top">
                      <li><a href="/bulk_sms_service.php">Bulk SMS Service</a></li>
                        <li><a href="/bulk_mms_service.php">Bulk MMS Service</a></li>
                        <li><a href="/virtual_number.php">Virtual Number</a></li>
                        <li><a href="/automated_voice_sms.php">Automated Voice SMS</a></li>
                        <li><a href="/cell_number_database.php">Cell Number Database</a></li>
                        <li><a href="/app/api.php">API</a></li>
                    </ul>
                  </li>
                    <li class="has-children">
                        <a href="#">Keywords</a>
                        <ul class="dropdown arrow-top">
                        <li><a href="/sms_keywords.php">SMS Keywords</a></li>
                    </ul>
                      </li>
                  <li><a href="/contact.php">Contact</a></li>
                  <li><a href="/faq.php">FAQ</a></li>
                </ul>
              </nav>
            </div>
            <div class="col-8 col-md-8 col-lg-4 text-right">
              <a href="/app/index.php?join" class="btn btn-primary btn-outline-primary rounded-0 text-white py-2 px-4">Register</a>
              <a href="/app/" class="btn btn-primary btn-primary rounded-0 py-2 px-4">Login</a>
            </div>


          </div>
        </div>
      </div>
    </div>

  
    <div class="unit-5 overlay" style="background-image: url('/images/hero_bg_1.jpg');">
      <div class="container text-center">
        <h2 class="mb-0"><?php if(isset($_GET['join']) || isset($_POST['join'])) { echo "Register"; }else{ echo "Login";}?></h2>
        <p class="mb-0 unit-6"><a href="https://mojasms.com/app/">Home</a> <span class="sep">></span> <span><?php if(isset($_GET['join']) || isset($_POST['join'])) { echo "Register"; }else{ echo "Login";}?></span></p>
      </div>
    </div>


    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-lg-6 mb-5" style="margin:auto;">
          
            <form action="index.php" method="post">
                
                <?php if(isset($_GET['reset'])) { ?>
                
                <?php if(isset($message)) { echo '<div class="alert alert-info">'.$message.'</div>'; } ?>
    
                 <h2 class="mb-4 text-black">Please enter your account email to recover password</h2>
                 <div class="row form-group">
                    <div class="col-md-12">
                      <label class="font-weight-bold" for="email2">Email</label>
                      <input type="email" name="userEmail" class="form-control rounded-0" placeholder="Email">
                    </div>
                 </div>
                 <div class="row form-group">
                    <div class="col-md-12">
                      <input type="submit" value="Login" class="btn btn-primary  py-2 px-4 rounded-0">
                    </div>
                  </div>
                <?php } elseif(isset($_GET['join']) || isset($_POST['join'])) { 
                
                
                    if(isset($_POST['join'])) { 
                
                    foreach ($_POST as $key => $value ){
            			${$key} = $value = str_replace('\'', '', $value);
            		}
            		
            		$query = "SELECT * FROM users WHERE email = '$email'";
            		$result = mysqli_query($server, $query) or die(mysqli_error($server));
            		$num = mysqli_num_rows($result);	
            		if($num < 1) {
            		    $refferal_m=filtervar($_POST['refferal']);
            		    $refer=mysqli_query($server, "SELECT * FROM users WHERE username = '$refferal_m' LIMIT 1");
            		    $reffered_by="";$fr=0;
            		    if(!empty($refferal_m) && mysqli_num_rows($refer) == 0){ $fr++;
            		        echo '<div class="alert alert-danger">Sorry but your referal ID does not exist.</div>';
            		    }else if(!empty($refferal_m) && mysqli_num_rows($refer) > 0){
            		        while ($roww = mysqli_fetch_assoc($refer)) {
            		            $reffered_by=$roww['id'];
            		        }
            		    }
            		    
            		    if($fr == 0){
            		    //refferal
            		    //save info as temp user
            			$_SESSION['username'] = $email;
            			$salt = genRandomPassword(32);
            			$crypt = getCryptedPassword($password, $salt);
            			$password2 = $crypt.':'.$salt;	
            			$track = time();
            			
            			 mysqli_query($server, "INSERT INTO users (`name`, `username`, `email`, `password`, `is_admin`, `is_customer`, `phone`, `tracking`,`reffered_by`) 
            				VALUES ('$name', '$email', '$email', '$password2', '0', '1', '$phone', '0','$reffered_by');") or die (mysql_error($server));
            			
            			//send verification code
            			$mail = 'Use this code to complete your registration at '.getSetting('businessName').': '.$track;
            			//sendMessage(getSetting('smsSender'),$phone,$mail,'0','System','127.0.0.1');
            			
            			//$message = "Successful registration, you can login to your dashboard!";
            			
            			    echo '<div class="alert alert-success">Successful registration, you can login to your dashboard!</div>';
            			
            			    header('location: index.php?joined'); 
            		    }
            		    
            		} else {
            			echo '<div class="alert alert-danger">Sorry but your email address is already registered by another customer.</div>';
            		}
            		
                  }
                
                ?>
               
              <div class="row form-group">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="email2">Full Name</label>
                  <input type="text" name="name" class="form-control rounded-0" placeholder="Full Name" required>
                </div>
              </div>
              <div class="row form-group">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="email2">Email</label>
                  <input type="text" name="email" id="email" class="form-control rounded-0"  placeholder="Your email address" required>
                </div>
              </div>
              <div class="row form-group">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="email2">Phone</label>
                  <input type="tel" name="phone" class="form-control rounded-0" placeholder="Mobile Number (include country code)" required>
                </div>
              </div>
              <div class="row form-group">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="refferal">Refferal ID</label>
                  <input type="text" name="refferal" class="form-control rounded-0" placeholder="Refferal ID" value="<?php echo!empty(isset($_GET['refferal'])) ? filtervar($_GET['refferal']) : 'admin'; ?>">
                </div>
              </div> 
              <div class="row form-group">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="password">Password</label>
                  <input type="password" id="password1" name="password" class="form-control rounded-0" placeholder="Password" required>
                </div>
              </div>
              
              <div class="row form-group">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="password">Re-type Password</label>
                  <input type="password" name="password2" id="password2" class="form-control rounded-0" placeholder="Re-type Password" required>
                </div>
              </div>
             
              <div class="row form-group mb-2">
                <div class="col-md-12">
                  <p>Have account? <a href="index.php">Login</a>. Forgot password? <a href="index.php?reset">click here</a></p>
                </label>
                </div>
              </div>
  
              <div class="row form-group">
                <div class="col-md-12">
                  <input type="hidden" name="join">
                  <input type="submit" value="Sign-up Now" id="submit" class="btn btn-primary  py-2 px-4 rounded-0">
                </div>
              </div>
               
                <?php 
                }else{
                ?>
              <h2 class="mb-4 text-black">Welcome. Sign in to manage your campaigns</h2>
              
              <?php if(isset($message)) { echo '<div class="alert alert-info" style="color: red;">'.$message.'</div>'; } ?>
              
                  <div class="row form-group">
                    <div class="col-md-12">
                      <label class="font-weight-bold" for="email2">Username</label>
                      <input type="text" name="access_login" class="form-control rounded-0" placeholder="Username" required>
                    </div>
                  </div>
                  <div class="row form-group">
                    <div class="col-md-12">
                      <label class="font-weight-bold" for="password">Password</label>
                      <input type="password" name="access_password" class="form-control rounded-0" placeholder="Password" required>
                    </div>
                  </div>
                  
                  <div class="row form-group">
                    <div class="col-md-12">
                      <label class="font-weight-normal" for="remember">
                      <input type="checkbox" id="remember">
                      Remember Me
                    </label>
                    </div>
                  </div>
    
                  <div class="row form-group mb-2">
                    <div class="col-md-12">
                      <p>No account yet? <a href="index.php?join">Register</a>. Forgot password? <a href="index.php?reset">click here</a></p>
                    </label>
                    </div>
                  </div>
      
                  <div class="row form-group">
                    <div class="col-md-12">
                      <input type="submit" value="Login" class="btn btn-primary  py-2 px-4 rounded-0">
                    </div>
                  </div>

                <?php
                }
                ?>
            </form>
          </div>

          
        </div>
      </div>
    </div>



<div class="container" style="margin-bottom:30px;">
    <center>
    <div class="col-sm-8">
        <div class="owl-carousel owl-theme">
            <?php

            $sqler="SELECT * FROM `sliding_marketing`";
        
            $result = mysqli_query($server,$sqler);
        
            if (mysqli_num_rows($result) > 0) { 
            
            $ty='';
            
            while($row = mysqli_fetch_assoc($result)){
                
            ?>
            <div class="item"><img src="/uploads/<?php echo $row['image'];?>" class="image-responsive" style="max-height:400px;" alt="sliding marketing"/></div>
            <?php
            }
            
            }
            ?>
        </div>
    </div>
    </center>
</div>


<footer class="site-footer p-3 justify-content-center text-center bg-primary w-100" style="color:black;padding-top:1px;">
    
    
    <div class="col-sm-12 bg-primary w-100" style="text-align:center;color:white;padding-bottom:1px;">
      <div class="container">
          <div class="col-md-12 justify-content-center text-center">
              <address>
            <strong>Contact Us On</strong><br>
<span class="fa fa-phone" aria-hidden="true"></span>&nbsp;(010) 516-0350<br>      
<span class="fa fa-whatsapp" aria-hidden="true"></span>&nbsp;(079) 479-4811<br>               
<span class="fa fa-envelope" aria-hidden="true"></span>&nbsp;info@mojasms.com
              </address>
                
            </div>

       </div>
    </div>
    <div class="col-sm-12 bg-black w-100" style="background:rgba(0,0,0,1);text-align:center;color:#fff;padding-bottom:1px;font-size:12px;">
        <div class="container">
              <div class="col-md-12">
               <img class="img-responsive" src="<?php echo '';?>/pennypicks-SSL-security-seal2.png" alt="pennypicks-SSL-security-seal2.png" style="height:60px;width:60px">
               <img class="img-responsive rounded" src="<?php echo '';?>/paypal_visa_master.png" alt="paypal_visa_master.png" style="height:50px;width:130px">
              </div>
            
            <div class="col-md-12">
            <p>
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All Rights Reserved | <i class="icon-heart text-danger" aria-hidden="true"></i>
            
            </p>
          </div>
          
        </div>
      </div>
    </footer>
  </div>
  
  <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5cb5ccc2c1fe2560f3ff1626/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

  <script src="/js/jquery-3.3.1.min.js"></script>
  <script src="/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="/js/jquery-ui.js"></script>
  <script src="/js/popper.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/owl.carousel.min.js"></script>
  <script src="/js/jquery.stellar.min.js"></script>
  <script src="/js/jquery.countdown.min.js"></script>
  <script src="/js/jquery.magnific-popup.min.js"></script>
  <script src="/js/bootstrap-datepicker.min.js"></script>
  <script src="/js/aos.js"></script>

  <script src="/js/main.js"></script>
  <script src="<?php echo $base_url;?>/assets/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>
  <script>
     $(document).ready(function(){
         
      $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        autoplay:true,
        items: 1,
        slideBy: 1,
        rtl:true,
        singleItem: true
      });
      
      
      $(".site-menu-toggle").click(function(){
      
          $(".site-nav-wrap").find('li.has-children').each(function(){
              $(this).find('span').removeClass('collapsed');
              $(this).find('span').addClass('active');
              $(this).find('ul').addClass('show');
          });
      
      });
        
    });
  </script>
  
  <script>

$("#email").blur(function(){
  $.ajax({
	  url: 'index.php?join&checkUser='+$(this).val(),
	  type: 'GET'
	})
  .done(function(response){
	  if(response > 0) {
	  	alert('Sorry but your email address is already registered. Please login or use a different one');
		$("#submit").hide();
		$("#reset").show();
		//$("#mesage").html('Please use a different email address to continue');
	  } else {
		$("#submit").show(); 
		$("#reset").hide(); 
	  }
  })
})

$("#password2").blur(function(){ 
	var pass1 = document.getElementById('password1').value;
	var pass2 = document.getElementById('password2').value;
  if (pass1 == pass2) {
	   	$("#submit").show(); 
		$("#reset").hide();
	} else {
  		alert('Sorry but your passwords does not match.');
		$("#submit").hide();
		$("#reset").show();
		//$("#mesage").html('Please type same passwords to continue');
  }
}) 
</script>
  <script>
    $(document).ready(function(){
      
      $(".site-menu-toggle").click(function(){
      
          $(".site-nav-wrap").find('li.has-children').each(function(){
              $(this).find('span').removeClass('collapsed');
              $(this).find('span').addClass('active');
              $(this).find('ul').addClass('show');
          });
      
      });
        
    });
  </script>
</body>
</html>

