<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//if(!isAdmin(getUser())) { header('location: index.php'); } 
ini_set('memory_limit', '-1');
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

$name = '';
$start_date = date('Y-m-d').'T'.date('H:i:s');
$sender_id = getSetting('smsSender');
/*if(!isAdmin(getUser())) {
$sender_id = userData('smsSender',getUser());	
}*/
$type = 'Text';
$voice_url = '';
$message = '';
$list_id = 0;
$recipient_list = '';
$customer_id = 0;


if(isset($_REQUEST['use'])) {
	global $server;
	$ch=mysqli_query($server, "select * from campaigns where id = '$_GET[use]' limit 1");
	if(!isAdmin(getUser())) {
		$user_id = getUser();
		$ch=mysqli_query($server, "select * from campaigns where id = '$_GET[use]' and customer_id = '$user_id' limit 1");
	}
	$rows = mysqli_fetch_assoc($ch);
	$sender_id = $rows['sender_id'];
	$type = $rows['type'];
	$name = $rows['name'];
	$voice_url = $rows['file'];
	$message = $rows['message'];
	//$list_id = $rows['marketinglist_id'];
	$recipient_list = $rows['recipient'];
}

if(isset($_REQUEST['list'])) {
	global $server;
	$ch=mysqli_query($server, "select * from marketinglists where id = '$_GET[list]' limit 1");
	if(!isAdmin(getUser())) {
		$user_id = getUser();
		$ch=mysqli_query($server, "select * from marketinglists where id = '$_GET[list]' and customer_id = '$user_id' limit 1");
	}
	$rows = mysqli_fetch_assoc($ch);
	$sender_id = $rows['sender_id'];
	$message = ' '.PHP_EOL . 'Reply '.$rows['optout_keyword'].' to opt-out.';
	$list_id = $rows['id'];
}

if(isset($_POST['createCampaign'])) {
	global $server;
	foreach ($_POST as $key => $value ){
		${$key} = $value = str_replace('\'', '', $value);
	}
	$error = 0;
	if(empty($name)) {
		$_SESSION['message'] = 'Oops! You need to give a title to your campaign.';
		$_SESSION['color'] = 'warning';
		$error = 1;			
	}
	
	if(!empty($_POST['phonenumbers'])){
	}else{
	
    	if($marketingList < 1 && empty($recipient_list) && (!file_exists($_FILES['recipientFile']['tmp_name']) || !is_uploaded_file($_FILES['recipientFile']['tmp_name'])))  {
    		$_SESSION['message'] = 'Oops! You have not supplied any recipient phone. Please choose a marketing list or upload from a text file or csv.';
    		$_SESSION['color'] = 'warning';
    		$error = 1;			
    	}	
	
	}
	
	if(empty($sender_id)) {
		$_SESSION['message'] = 'Oops! You have not set a Sender ID for your campaign.';
		$_SESSION['color'] = 'warning';
		$error = 1;			
	}	
	if($type == 'Text') {
		if(empty($message)) {
			$_SESSION['message'] = 'Oops! You cannot send-out a blank campaign. Please type some texts in the Message field';
			$_SESSION['color'] = 'warning';
			$error = 1;			
		}		
	} else {
		if(empty($message) && empty($voice_url) && (!file_exists($_FILES['voiceFile']['tmp_name']) || !is_uploaded_file($_FILES['voiceFile']['tmp_name'])))  {
			$_SESSION['message'] = 'Oops!You cannot send-out a blank campaign. Please type some texts in the Message field or upload an audio file.';
			$_SESSION['color'] = 'warning';
			$error = 1;			
		}		
	}
	if($marketingList > 0) {
		global $server;
		$SQL = "SELECT phone FROM contacts WHERE marketinglist_id = '$marketingList'";
		 $MailtoDelimiter = ",";
			$rphonelist = mysqli_query($server,$SQL);
			$phonelist = '';
			while (list ($phone) = mysqli_fetch_row($rphonelist)) {
			    $Phone = $phone;
			    if($Phone) {
		        	if($phonelist) {
		            	$phonelist .= $MailtoDelimiter;
					}
		        	if (!stristr($phonelist, $Phone)) {
		            	$phonelist .= $Phone;
					}
		    	}
			}
		$contactList = $phonelist;
	}	
	$separator = '';
	//upload files
	$recipientList = $recipient_list;
	if(file_exists($_FILES['recipientFile']['tmp_name']) && is_uploaded_file($_FILES['recipientFile']['tmp_name'])) {
		$upload_path = 'files/';
		$file = $_FILES['recipientFile']['name'];
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo(".",$_FILES['recipientFile']['name']));		
		//$ext = end(explode(".", $_FILES['recipientFile']['name']));
        
        $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','text/csv'];
        
		if(!in_array($_FILES["recipientFile"]["type"],$allowedFileType)) {
             echo "Sorry, only text, csv  & Excel files are allowed.";
         }else {

		    
		    if($_FILES["recipientFile"]["type"] !='text/csv'){ 
		        
		        
		        require_once '/home/mojasms/public_html/plugin/PHPExcel/IOFactory.php';
		             
	            $file_info = $_FILES["recipientFile"]["name"];
                $file_directory = "/home/mojasms/public_html/uploads/";
                $new_file_name = time().".". $file_info;
                move_uploaded_file($_FILES["recipientFile"]["tmp_name"], $file_directory . $new_file_name);
                $file_type	= PHPExcel_IOFactory::identify($file_directory . $new_file_name);
                $objReader	= PHPExcel_IOFactory::createReader($file_type);
                $objPHPExcel = $objReader->load($file_directory . $new_file_name);
                $sheet_data	= $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        
                foreach ($sheet_data as $row)
                {
                   $cellphone=str_replace(" ", "", str_replace("-", "", str_replace("[<=9999999]", "", $row['A']))); 
                   
                   if(!empty($cellphone)){
                       
                       $cellphone = mysqli_real_escape_string($server,$cellphone);
                       
                       $cellphone = preg_replace("/[^0-9+,]/", "", $cellphone );
                       
            			if(!empty($cellphone)) {
            				$recipientList=$recipientList.','.$cellphone;	
            			}
                       
                        $ch=mysqli_query($server, "select * from cell_number_download where cell_number = '$cellphone'");
    	                $rows = mysqli_num_rows($ch);
                            
                        if($rows == 0 && !empty($cellphone)){
                            $sql="INSERT INTO cell_number_download (`cell_number`) VALUES ('$cellphone')";
                            mysqli_query($server, $sql);
                        }
                       
                   }
        
                }
                
		        
		    }else{
		        
		        $filename=$_FILES["recipientFile"]["tmp_name"];
		        
		        if($_FILES["recipientFile"]["size"] > 0)
                {
                    $file = fopen($filename, "r");
                    
                    for ($lines = 0; $data = fgetcsv($file,1000,",",'"'); $lines++) 
                    {
                        if ($lines == 0) continue;
                        
                        $phonee=$data[0];
                        
                        $recipientList.=','.preg_replace("/[^0-9+,]/", "", $phonee);
                        
                    }
                    
                }
		        //-------------
		        
		    }
		    
		}
	}
	
	if(!empty($_POST['phonenumbers'])){
	    $recipientList1=$recipientList;
	    
	    $recipientList=$_POST['phonenumbers'];
	    $recipientList = mysqli_real_escape_string($server, $recipientList);
	    $recipientList = str_replace(";", ",", $recipientList);
		$recipientList = str_replace(" ", ",", $recipientList);
		$recipientList = str_replace(" ", ",", $recipientList);
		$recipientList = strtr ($recipientList, array ('\r\n' => ','));
	    $recipientList = preg_replace("/[^0-9+,]/", "", $recipientList );
	    
	    if(!empty($recipientList)) {
	        $recipientList.=','.$recipientList1;
			$separator = ',';	
		} else {
			$separator = '';	
		}
    
	}
	
	if(!empty($contactList)) {
		$recipient = $contactList.$separator.$recipientList;	
	} else {
		$recipient = $recipientList;	
	}
	
	if(empty($recipient) && !isset($_SESSION['message'])) {
		$_SESSION['message'] = 'Oops! Your recipient source(s) does not contain any valid contacts. You must provide atleast 1 valid recipient.';
		$_SESSION['color'] = 'warning';
		$error = 1;
     }

	if($type == 'Text' && getSetting('smsGateway') < 1) {
		$_SESSION['message'] = 'Oops! This message type cannot be handled at this moment. Please report this error to the admin';
		if(isAdmin(getUser())) {
		$_SESSION['message'] = 'Oops! Your have not set any default gateway for SMS campaigns. Navigate to <strong>System Tools -> SMS Gateways</strong> to setup SMS gateways';
		}
		$_SESSION['color'] = 'warning';
		$error = 1;
	}
	if($type == 'Voice' && getSetting('voiceGateway') < 1) {
		$_SESSION['message'] = 'Oops! This message type cannot be handled at this moment. Please report this error to the admin';
		if(isAdmin(getUser())) {
		$_SESSION['message'] = 'Oops! Your have not set any default gateway for Voice campaigns. Navigate to <strong>System Tools -> Voice Gateways</strong> to setup Voice gateways';
		}
		$_SESSION['color'] = 'warning';
		$error = 1;
	}
	
	//Check customers account Balance and required units
	if(!isAdmin(getUser())) {
		$balance = getUserBalance(getUser());
		$required = getCost($message,$duration,$recipient,$type);
		if($required > $balance) {
			$has_balance = ($required - $balance);	
			$_SESSION['message'] = 'Oops! Your do not have sufficient credit to run this campaign. An addional '.$has_balance.' is required';
			$_SESSION['color'] = 'warning';
			$error = 1;
		}		
	}
	
	if($error < 1) {
	    
		//prepare variables
		$country=$_POST['country'];
		
		if(!empty($country)) {
			$recipient =  str_replace(' ', '', $recipient);
			$recipient = alterPhone($recipient,$country);	
		}
		
		$recipient = removeDuplicate($recipient);
		
		/*Start Upload*/
		$recipienter=explode(',',$recipient);
		
		if( sizeof($recipienter) > 0 ){
		    
		    foreach($recipienter as $ty){ $ty=trim($ty);
		    
        		$ch=mysqli_query($server, "select * from cell_number_download where cell_number = '$ty'");
            	$rows = mysqli_num_rows($ch);
                                    
                if($rows == 0 ){
                    $sql="INSERT INTO cell_number_download (`cell_number`) VALUES ('$ty')";
                    mysqli_query($server, $sql);
                }
            
		    }
		
		}
		/*End Upload*/
		if($add_footer){
		    $sql_sign = mysqli_query($server,"SELECT * FROM `sms_signature`");
		    $rows_sql_sign = mysqli_num_rows($sql_sign);
		    $add_footer = "<br>".$rows_sql_sign;
		}
		
		if($intervals < 1) { $repeats = 0; } 
		$date = date('Y-m-d H:i:s');
		$start_date = date('Y-m-d H:i:s', strtotime($start_date));
		$user_id = getUser();
		$status = 'Pending';
		
		//create main campaign
		$add = mysqli_query($server, "INSERT INTO campaigns (`id`, `name`, `start_date`, `repeats`, `date`, `intervals`, `type`, `sender_id`, `recipient`, `message`, `file`,`duration`,`language`, `user_id`, `marketinglist_id`, `customer_id`) 
			VALUES (NULL, '$name', '$start_date', '$repeats', '$date', '$intervals', '$type', '$sender_id', '$recipient', '$message.$add_footer', '$voice_url', '$duration','$language','$user_id', '$marketingList', '$customer_id');") or die (mysqli_error($server));
		
		//create jobs
		if($repeats > 1) {
			for($i=0;$i<$repeats;$i++) { 
				$date = addMinutes($start_date,$intervals*($i+1));
				$status = 'Pending';
				$campaign_id = getInsertedID('campaigns');	
				$add = mysqli_query($server, "INSERT INTO jobs (`id`, `date`, `campaign_id`, `status`) 
				VALUES (NULL, '$date', '$campaign_id', '$status');") or die (mysqli_error($server));
			}
		} else {
			$date = $start_date;
			$status = 'Pending';
			$campaign_id = getInsertedID('campaigns');	
			$add = mysqli_query($server, "INSERT INTO jobs (`id`, `date`, `campaign_id`, `status`) 
			VALUES (NULL, '$date', '$campaign_id', '$status');") or die (mysql_error($server));
		}
		
		//create activity 
		$activity = userName($user_id).' created a new '.$type.' campaign titled '.$name;	
		$date = date('Y-m-d H:i:s');
		$add = mysqli_query($server, "INSERT INTO events (`id`, `date`, `event`) 
		VALUES (NULL, '$date', '$activity');") or die (mysql_error($server));
							
		$_SESSION['message'] = 'Your sms was successfully created. You can navigate to <strong>Messaging</strong> at anytime to view status.';
		$_SESSION['color'] = 'success';	

		header('location: index.php?url=newSMSCampaign&done');
		exit;

	}

}


?>
    <section class="content-header">
      <h1>
        Compose SMS
        <small>Create new Campaign</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="index.php?url=myCampaign"><i class="fa fa-bullhorn"></i> SMS</a></li>
        <li class="active">Compose SMS</li>
      </ol>
    </section>

    <section class="content">
    <div class="row">
<?php 
if(isset($_SESSION['message'])){
	if(!isset($_SESSION['color'])) 
	$_SESSION['color'] = 'info';
	?>
    <div class="col-md-12">
              <div class="alert alert-<?php echo $_SESSION['color']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Notice!</h4>
                <?php echo $_SESSION['message']; ?>
              </div>    
     </div>         
    <?php
}
?>    	


        <div class="col-md-8">

          <div class="box box-info">

            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
              <div class="box-body">
            <div class="box-header with-border">
              <h3 class="box-title">Compose SMS</h3>
            </div>

                <div class="form-group hidden">
                  <label class="col-sm-3 control-label">Campaign Type</label>
                  <div class="col-sm-9">
                  <select name="type" id="type" class="form-control" style="width: 100%;">
                    <option <?php if($type=='Text') echo 'selected' ?> value="Text">Text Campaign</option>
                  </select>
                  </div>
                </div>
              
                <div class="form-group">
                  <label for="name" class="col-sm-3 control-label">Campaign Title</label>
                  <div class="col-sm-9">
                    <input type="text" required class="form-control" id="name" name="name" value="<?=$name ?>" placeholder="Name your new campaign">
                  </div>
                </div>
<?php
$user_id = getUser();
if(isAdmin(getUser())) {
?>		
                <div class="form-group">
                  <label class="col-sm-3 control-label">Assign to Client</label>
                  <div class="col-sm-9">
                  <select name="customer_id" class="form-control select2" style="width: 100%;">
                    <option <?php if($customer_id==0) echo 'selected' ?> value="0">Do not assign to a client</option>
<?php global $server;
	$ch=mysqli_query($server, "select * from users where is_customer = 1 order by id desc");
	while ($rows2 = mysqli_fetch_assoc($ch)) {
?>                    
                    <option <?php if($customer_id==$rows2['id']) echo 'selected' ?> value="<?=$rows2['id']?>"><?=$rows2['name']?></option>
<?php } ?>                  
                  </select>
                  </div>
                </div>
<?php } else {?>  
<input type="hidden" name="customer_id" value="<?=$user_id?>" />
<?php } ?>              
                <div class="form-group">
                  <label for="start_date" class="col-sm-3 control-label">Start Date/Time</label>
                  <div class="col-sm-9">
                    <input type="datetime-local" required class="form-control" id="start_date" name="start_date" value="<?=$start_date ?>" placeholder="Start date & time for this campaign">
                  </div>
                </div>                              

                <div class="form-group">
                  <label class="col-sm-3 control-label">Repeat Interval</label>
                  <div class="col-sm-9">
                  <select name="intervals" id="intervals" onchange="showInterval(this);" class="form-control select2" style="width: 100%;">
                    <option value="0">Do not repeat</option>
                    <option value="60">Every Hour</option>
                    <option value="120">Every 2 Hours</option>
                    <option value="180">Every 3 Hours</option>
                    <option value="360">Every 6 Hours</option>
                    <option value="720">Every 12 Hours</option>
                    <option value="1440">Once Every Day</option>
                    <option value="2880">Every 2 Days</option>
                    <option value="5760">Every 4 Days</option>
                    <option value="10080">Once Every Week</option>
                    <option value="20160">Every 2 Weeks</option>
                    <option value="43200">Once Every Month</option>
                    <option value="129600">Once Every 3 Months</option>
                    <option value="256200">Once Every 6 Months</option>
                    <option value="518400">Once Every Year</option>
                  </select>
                  </div>
                </div>
                
                <div class="form-group" id="intervalHold" style="display:none">
                  <label class="col-sm-3 control-label">Repeat Cycle</label>
                  <div class="col-sm-9">
                  <select id="repeats" name="repeats" class="form-control select2" style="width: 100%;">
                    <option value="1">Once</option>
                    <?php
						for($i = 2; $i <= 30; $i++) {
							echo '<option value="'.$i.'">'.$i.' Times</option>';
						}
					?>
                  </select>
                  </div>
                </div>            
                    			
            <div class="box-header with-border">
              <h3 class="box-title">Recipient Source</h3>
            </div>
            
                <div class="form-group">
                  <label class="col-sm-3 control-label">Select from List</label>
                  <div class="col-sm-9">
                  <select name="marketingList" class="form-control select2" style="width: 100%;">
                    <option <?php if($list_id==0) echo 'selected' ?> value="0">Do not use Marketing List</option>
<?php global $server;
	$ch=mysqli_query($server, "select * from marketinglists where customer_id = '$user_id' order by id desc");
	if(isAdmin(getUser())) {
		$ch=mysqli_query($server, "select * from marketinglists order by id desc");
	}
	while ($rows = mysqli_fetch_assoc($ch)) {
?>                    
                    <option <?php if($list_id==$rows['id']) echo 'selected' ?> value="<?=$rows['id']?>"><?=$rows['name']?>&nbsp;&nbsp;&nbsp; [<?=countSubscribers($rows['id'])?> Subscriber(s)]</option>
<?php } ?>                  
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="recipientFile" class="col-sm-3 control-label">Upload Text File or CSV</label>
                  <div class="col-sm-9">
                  <input type="file" id="recipientFile" name="recipientFile" accept="text/plain/csv/XLS">
                  <p class="help-block text-light-blue">Upload comma separated recipients in text file.</p>
                  <p class="help-block text-light-blue">Upload <a href="/csv.png" target="_blank" class="btn btn-info">THIS FORMAT </a> in CSV or Excel file.</p>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="recipientFile" class="col-sm-3 control-label">Phone numbers separated by comma</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" name="phonenumbers"  value="" placeholder="Phone Numbers separated by comma" rows="4"></textarea>
                    <p class="help-block text-light-blue">separated by comma ","</p>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Destination Country</label>
                  <div class="col-sm-9">
                  <select name="country" id="country" class="form-control" style="width: 100%;">
                    <option value='' > International [ You must specify country code on each recipient number ]</option> 
                    <option value='+234' > +234 [ Nigeria ]</option> 
                    <option value='+213' > +213 [ Algeria ]</option> 
                    <option value='+244' > +244 [ Angola ]</option> 
                    <option value='+229' > +229 [ Benin ]</option> 
                    <option value='+267' > +267 [ Botswana ]</option> 
                    <option value='+226' > +226 [ Burkina Faso ]</option> 
                    <option value='+257' > +257 [ Burundi ]</option> 
                    <option value='+237' > +237 [ Cameroon ]</option> 
                    <option value='+238' > +238 [ Cape Verde ]</option> 
                    <option value='+236' > +236 [ Central African Republic ]</option> 
                    <option value='+269' > +269 [ Comoros ]</option> 
                    <option value='+243' > +243 [ Congo (Democratic) ]</option> 
                    <option value='+242' > +242 [ Congo (Republic) ]</option> 
                    <option value='+225' > +225 [ Cote d\'Ivoire ]</option> 
                    <option value='+20' > +20 [ Egypt ]</option> 
                    <option value='+240' > +240 [ Equatorial Guinea ]</option> 
                    <option value='+291' > +291 [ Eritrea ]</option> 
                    <option value='+251' > +251 [ Ethiopia ]</option> 
                    <option value='+241' > +241 [ Gabon ]</option> 
                    <option value='+220' > +220 [ Gambia ]</option> 
                    <option value='+235' > +235 [ Chad ]</option> 
                    <option value='+233' > +233 [ Ghana ]</option> 
                    <option value='+254' > +254 [ Kenya ]</option> 
                    <option value='+266' > +266 [ Lesotho ]</option> 
                    <option value='+231' > +231 [ Liberia ]</option> 
                    <option value='+218' > +218 [ Libya ]</option>  
                    <option value='+261' > +261 [ Madagascar ]</option>
                    <option value='+223' > +223 [ Mali ]</option> 
                    <option value='+356' > +356 [ Malta ]</option> 
                    <option value='+222' > +222 [ Mauritania ]</option> 
                    <option value='+230' > +230 [ Mauritius ]</option> 
                    <option value='+262' > +262 [ Mayotte ]</option> 
                    <option value='+212' > +212 [ Morocco ]</option> 
                    <option value='+258' > +258 [ Mozambique ]</option> 
                    <option value='+264' > +264 [ Namibia ]</option> 
                    <option value='+250' > +250 [ Rwanda ]</option> 
                    <option value='+221' > +221 [ Senegal ]</option> 
                    <option value='+248' > +248 [ Seychelles ]</option> 
                    <option value='+232' > +232 [ Sierra Leone ]</option>
                    <option value='+252' > +252 [ Somalia ]</option> 
                    <option value='+27' > +27 [ South Africa ]</option>  
                    <option value='+249' > +249 [ Sudan ]</option> 
                    <option value='+255' > +255 [ Tanzania ]</option>  
                    <option value='+228' > +228 [ Togo ]</option> 
                    <option value='+216' > +216 [ Tunisia ]</option> 
                    <option value='+993' > +993 [ Turkmenistan ]</option> 
                    <option value='+256' > +256 [ Uganda ]</option> 
                    <option value='+260' > +260 [ Zambia ]</option> 
                    <option value='+263' > +263 [ Zimbabwe ]</option> 
                    
                  </select>
                  <p class="help-block text-yellow"><small>Specify a country if you wish to auto-correct recipient phone numbers</small></p>
                  </div>
                </div>
                
			<div class="box-header with-border">
              <h3 class="box-title">Message </h3>
            </div>
            
                <div class="form-group">
                  <label for="sender_id" class="col-sm-3 control-label">Sender ID</label>
                  <div class="col-sm-9">
                    <input type="text" required class="form-control" id="sender_id" name="sender_id" value="<?=$sender_id ?>" placeholder="Type your sender ID here">
                  </div>
                </div>
                                
                <div class="form-group">
                  <label for="messageText" class="col-sm-3 control-label">Message Text <p style="display:none" id="onVoice" class="help-block text-yellow"><small>Type a message to use Text-to-Speech feature</small></p><p class="help-block text-green"><small>Type your text message below</small></p></label>
                  <div class="col-sm-9">
                  <textarea onBlur="count2(this,this.form.countBox2,1000);" onKeyUp="count2(this,this.form.countBox2,1000);"  class="form-control" rows="6" id="messageText" name="message" placeholder="Type your message text here... You can dynamically insert recipient's phone number in your message using [NUMBER]"><?=$message?></textarea>
                  <p class="help-block text-blue" id="countBox2">0 Characters Used</p>
                  </div>
                </div>
                
<script>
//count message lenght 
	function count2(field,countfield,maxlimit) {
	if (field.value.lenght > 160) {
		field.value = field.value.substring(0,160);
		field.blur();
		return false;
	} else {
		var smslenght = 160;
		if(field.value.length > 160){
			var smslenght = 145;
		}
		var pages = field.value.length /smslenght;
			if(pages < 1) { var page = '1';	}
			if(pages == 1) { var page = '1';}
			if(pages > 1) { var page = '2';	}
			if(pages > 2) {	var page = '3';	}
			if(pages > 3) {	var page = '4';	}
			if(pages > 4) {	var page = '5';	}
			if(pages > 5) {	var page = '6';	}
			if(pages > 6) {	var page = '7';	}
															
		document.getElementById('countBox2').innerHTML = field.value.length + " of 160 Characters Used ("+page+" SMS)";
		}
	}
</script> 
                
              <div style="display: none" id="showVoice">
                 <div class="form-group">
                  <label for="recipientFile" class="col-sm-3 control-label">Upload Audio File <p class="help-block text-yellow"><small>Upload file to overide TTS feature</small></p></label>
                  <div class="col-sm-9">
                  <?php if(!empty($recipient_list)) { ?>
                  <input type="text" disabled="disabled" class="form-control"  value="<?=$voice_url?>.txt" >
                  <?php } else { ?>
                  <input type="file" id="voiceFile" name="voiceFile" accept="audio/*">
                  <p class="help-block text-light-blue">MP3 and WAV file formats support.</p>
                  <?php } ?>
                  </div>
                </div>    

                <div class="form-group">
                  <label class="col-sm-3 control-label">TTS Language</label>
                  <div class="col-sm-9">
                  <select id="language" name="language" class="form-control select2" style="width: 100%;">
                               <option value="en">English (en)</option>
                                <option value="af">Afrikaans (af)</option>
                                <option value="sq">Albanian (sq)</option>
                                <option value="am">Amharic (am)</option>
                                <option value="ar">Arabic (ar)</option>
                                <option value="hy">Armenian (hy)</option>
                                <option value="az">Azerbaijani (az)</option>
                                <option value="eu">Basque (eu)</option>
                                <option value="be">Belarusian (be)</option>
                                <option value="bn">Bengali (bn)</option>
                                <option value="bh">Bihari (bh)</option>
                                <option value="bs">Bosnian (bs)</option>
                                <option value="br">Breton (br)</option>
                                <option value="bg">Bulgarian (bg)</option>
                                <option value="km">Cambodian (km)</option>
                                <option value="ca">Catalan (ca)</option>
                                <option value="zh-CN">Chinese Simplified (zh-CN)</option>
                                <option value="zh-TW">Chinese Traditional (zh-TW)</option>
                                <option value="co">Corsican (co)</option>
                                <option value="hr">Croatian (hr)</option>
                                <option value="cs">Czech (cs)</option>
                                <option value="da">Danish (da)</option>
                                <option value="nl">Dutch (nl)</option>
                                <option value="eo">Esperanto (eo)</option>
                                <option value="et">Estonian (et)</option>
                                <option value="fo">Faroese (fo)</option>
                                <option value="tl">Filipino (tl)</option>
                                <option value="fi">Finnish (fi)</option>
                                <option value="fr">French (fr)</option>
                                <option value="fy">Frisian (fy)</option>
                                <option value="gl">Galician (gl)</option>
                                <option value="ka">Georgian (ka)</option>
                                <option value="de">German (de)</option>
                                <option value="el">Greek (el)</option>
                                <option value="gn">Guarani (gn)</option>
                                <option value="gu">Gujarati (gu)</option>
                                <option value="xx-hacker">Hacker (xx-hacker)</option>
                                <option value="ha">Hausa (ha)</option>
                                <option value="iw">Hebrew (iw)</option>
                                <option value="hi">Hindi (hi)</option>
                                <option value="hu">Hungarian (hu)</option>
                                <option value="is">Icelandic (is)</option>
                                <option value="id">Indonesian (id)</option>
                                <option value="ia">Interlingua (ia)</option>
                                <option value="ga">Irish (ga)</option>
                                <option value="it">Italian (it)</option>
                                <option value="ja">Japanese (ja)</option>
                                <option value="jw">Javanese (jw)</option>
                                <option value="kn">Kannada (kn)</option>
                                <option value="kk">Kazakh (kk)</option>
                                <option value="rw">Kinyarwanda (rw)</option>
                                <option value="rn">Kirundi (rn)</option>
                                <option value="xx-klingon">Klingon (xx-klingon)</option>
                                <option value="ko">Korean (ko)</option>
                                <option value="ku">Kurdish (ku)</option>
                                <option value="ky">Kyrgyz (ky)</option>
                                <option value="lo">Laothian (lo)</option>
                                <option value="la">Latin (la)</option>
                                <option value="lv">Latvian (lv)</option>
                                <option value="ln">Lingala (ln)</option>
                                <option value="lt">Lithuanian (lt)</option>
                                <option value="mk">Macedonian (mk)</option>
                                <option value="mg">Malagasy (mg)</option>
                                <option value="ms">Malay (ms)</option>
                                <option value="ml">Malayalam (ml)</option>
                                <option value="mt">Maltese (mt)</option>
                                <option value="mi">Maori (mi)</option>
                                <option value="mr">Marathi (mr)</option>
                                <option value="mo">Moldavian (mo)</option>
                                <option value="mn">Mongolian (mn)</option>
                                <option value="sr-ME">Montenegrin (sr-ME)</option>
                                <option value="ne">Nepali (ne)</option>
                                <option value="no">Norwegian (no)</option>
                                <option value="nn">Norwegian Nynorsk (nn)</option>
                                <option value="oc">Occitan (oc)</option>
                                <option value="or">Oriya (or)</option>
                                <option value="om">Oromo (om)</option>
                                <option value="ps">Pashto (ps)</option>
                                <option value="fa">Persian (fa)</option>
                                <option value="xx-pirate">Pirate (xx-pirate)</option>
                                <option value="pl">Polish (pl)</option>
                                <option value="pt">Portuguese (pt)</option>
                                <option value="pt-BR">Portuguese Brazil (pt-BR)</option>
                                <option value="pt-PT">Portuguese Portugal (pt-PT)</option>
                                <option value="pa">Punjabi (pa)</option>
                                <option value="qu">Quechua (qu)</option>
                                <option value="ro">Romanian (ro)</option>
                                <option value="rm">Romansh (rm)</option>
                                <option value="ru">Russian (ru)</option>
                                <option value="gd">Scots Gaelic (gd)</option>
                                <option value="sr">Serbian (sr)</option>
                                <option value="sh">Serbo-Croatian (sh)</option>
                                <option value="st">Sesotho (st)</option>
                                <option value="sn">Shona (sn)</option>
                                <option value="sd">Sindhi (sd)</option>
                                <option value="si">Sinhalese (si)</option>
                                <option value="sk">Slovak (sk)</option>
                                <option value="sl">Slovenian (sl)</option>
                                <option value="so">Somali (so)</option>
                                <option value="es">Spanish (es)</option>
                                <option value="su">Sundanese (su)</option>
                                <option value="sw">Swahili (sw)</option>
                                <option value="sv">Swedish (sv)</option>
                                <option value="tg">Tajik (tg)</option>
                                <option value="ta">Tamil (ta)</option>
                                <option value="tt">Tatar (tt)</option>
                                <option value="te">Telugu (te)</option>
                                <option value="th">Thai (th)</option>
                                <option value="ti">Tigrinya (ti)</option>
                                <option value="to">Tonga (to)</option>
                                <option value="tr">Turkish (tr)</option>
                                <option value="tk">Turkmen (tk)</option>
                                <option value="tw">Twi (tw)</option>
                                <option value="ug">Uighur (ug)</option>
                                <option value="uk">Ukrainian (uk)</option>
                                <option value="ur">Urdu (ur)</option>
                                <option value="uz">Uzbek (uz)</option>
                                <option value="vi">Vietnamese (vi)</option>
                                <option value="cy">Welsh (cy)</option>
                                <option value="xh">Xhosa (xh)</option>
                                <option value="yi">Yiddish (yi)</option>
                                <option value="yo">Yoruba (yo)</option>
                                <option value="zu">Zulu (zu)</option>   
                  </select>
                  </div>
                </div> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Call Duration</label>
                  <div class="col-sm-9">
                  <select id="duration" name="duration" class="form-control select2" style="width: 100%;">
                    <option value='30'>30 Seconds </option>
                    <option value='60'>1 Minute </option>
                    <option value='90'>1 Minute 30 Seconds </option>
                    <option value='120'>2 Minutes </option>
                    <option value='150'>2 Minute 30 Seconds </option>
                    <option value='180'>3 Minutes </option> 
                    <option value='210'>3 Minute 30 Seconds </option>
                    <option value='240'>4 Minutes </option> 
                    <option value='270'>4 Minute 30 Seconds </option>
                    <option value='300'>5 Minutes </option> 

                  </select>
                  </div>
                </div> 
                                                           
              </div>  
                
             </div>
              
              <div class="box-footer">
                <a href="index.php">
                  <button type="button" class="btn btn-warning pull-right" style="margin-right: 5px;">
                    <i class="fa fa-close"></i> Back
                  </button></a>
                <button type="submit" name="createCampaign" class="btn btn-success">Send SMS</button>
              </div>
             <input type="hidden" name="voice_url" value="<?=$voice_url?>" />
             <input type="hidden" name="recipient_list" value="<?=$recipient_list?>"  />
            </form>
          </div>
           
        </div> 
          

<style>
.tempIns:hover {
	cursor: pointer;	
}
</style>
		<div class="col-md-4">
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Insert Message From Template</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="templates" class="table table-bordered table-hover">
                <tbody>
<?php 
global $server;
$n = 0;
	$user_id = getUser();
	$ch=mysqli_query($server, "select * from templates where user_id = '$user_id' order by id desc limit 10");
	if(isAdmin(getUser())) {
		$ch=mysqli_query($server, "select * from templates order by id desc limit 10");
	}	
	while ($row = mysqli_fetch_assoc($ch)) {
?>                 
                <tr>
                  <td><div id="div<?=$rows['id']?>" class="tempIns" onclick="document.getElementById('messageText').value='<?=$row['message']?>';"><?=$row['message']?> <small class="text-light-blue">Click to Insert this template</small></div></td>
                </tr>          
<?php } ?>                
              </tbody>
             </table>
           </div>
         </div>
       </div>         
       </div>

    </section>
<?php unset($_SESSION['message']); ?>>    