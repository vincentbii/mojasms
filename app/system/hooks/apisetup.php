<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) { header('location: index.php'); } 
?>
    <section class="content-header">
      <h1>
        General Settings
        <small>API Setup</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> System Tools</a></li>
        <li class="active">API Setup</li>
      </ol>
    </section>

    <section class="content">
    <div class="row">
<?php    
if(isset($_POST['save'])){
	global $server;
		$api = $_POST['api_setup'];
		mysqli_query($server, "update apis set `api_name` = '$api', status='1' ")or die(mysqli_error($server));
	
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                Your new system settings was successfully saved and applied. 
              </div>    
     </div>         
    <?php
	
}
?>     

<style>
    
    /* Dotted red border */
hr {
  border-top: 1px dotted red;
}
    
</style>
       <form class="form-vertial" action="index.php?url=apisetup" method="post" role="form">           
		<div class="col-md-6">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Update API to Use</h4></div>
            
                <div class="box-body">
                    <div class="form-group">
                      <label>Choose The Reseller</label>
                      <select name="api_setup" class="form-control select2" style="width: 100%;">
                         <option <?php echo activeApi()=='global' ? 'selected' : '';?> value="global">Global SMS</option>
                         <option <?php echo activeApi()=='bulk' ? 'selected' : '';?> value="bulk">Bulk SMS</option>
                         <option <?php echo activeApi()=='zoom' ? 'selected' : '';?> value="zoom">Zoom Connect</option>
                      </select>
                    </div> 
                </div>
                 <div class="box-footer">
                  <button type="submit" name="save" class="btn btn-success">Update API</button>
                </div>
          </div>
          
            <div class="box box-success" >
               <div class="box-header with-border"><h4>Total Assigned to Users</h4></div>
                <div class="box-body">
                    
                     <h4>SMS: <span class="label label-default"><?php echo get_user_sms_total();?></span></h4>
                     <h4>VMS: <span class="label label-default"><?php echo get_user_vms_total();?></span></h4>
                     <h4>MMS: <span class="label label-default"><?php echo get_user_mms_total();?></span></h4>
                    
                </div>
            </div>
          
        </div>
        
        <div class="col-md-6">
            <div class="box box-success" >
                <div class="box-header" style="margin-bottom:0;padding-bottom:0;"><h4 style="margin-bottom:0;padding-bottom:0;">API Balances</h4></div>
                    <div class="box-body">
                        <h2>Global SMS <span class="label label-warning"><?php echo activeApi()=='global' ? 'Active' : '';?></span></h2>
                        
                        <?php 
                      
                             $smss=round(url_get_contents("http://148.251.196.36/app/miscapi/1XfUGDAM/getBalance/true/"));
                             $server_output=$zoom_connect=url_get_contents("https://www.zoomconnect.com:443/app/api/rest/v1/account/balance?token=d7923de2-b46e-41a8-8885-679a9d42ea0f&email=admin%40mojasms.com");
                             
                             $xml = simplexml_load_string($server_output);
                             $zoom = (string) $xml->creditBalance;
                             
                             $ssmh=send_message_bulk_sms_balance();

                        ?>
                        
                         <h4>SMS: <span class="label label-default"><?php echo $smss;?></span></h4>
                        <hr>
                        <h2>Bulk SMS <span class="label label-warning"><?php echo activeApi()=='bulk' ? 'Active' : '';?></span></h2>
                        <h4>SMS: <span class="label label-default"><?php echo floor((float)json_decode($ssmh)->credits->balance);?></span></h4>
                        <hr>
                        <h2>Zoom Connect <span class="label label-warning"><?php echo activeApi()=='zoom' ? 'Active' : '';?></span></h2>
                        <h4>SMS: <span class="label label-default"><?php echo round($zoom);?></span></h4>
                    </div>
                </div>
            </div>
            
        </div>
        
       </form>
     </div>
    </section>