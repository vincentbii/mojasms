<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) { header('location: index.php'); } 

define('DATATABLE', true);


?>
    <section class="content-header">
      <h1>
        Database Backup Manager
        <small>Backup/restore mobiketa database</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> System Tools</a></li>
        <li class="active">Database Backups</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	$ch=mysqli_query($server, "select * from backup where id = '$_GET[delete]' limit 1");
	$rows = mysqli_fetch_assoc($ch);	
	$sql=mysqli_query($server, "delete from backup where id='$_GET[delete]'")or die(mysqli_error($server));
	$sql=unlink('backups/'.$rows['url']);
	//show mesage
	?>
    <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected database backup has been deleted from the system.
              </div>    
     </div>         
    <?php
}
?>

<?php 
if(isset($_GET['restore'])){
	global $server;
	$ch=mysqli_query($server, "select * from backup where id = '$_GET[restore]' limit 1");
	$rows = mysqli_fetch_assoc($ch);	
	$filename = 'backups/'.$rows['file'];
	$date = $rows['date'];
	
	$done = restore_tables($filename);

	//show mesage
	?>
    <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                Your system database has been successfully restored to the selected backup of <?=$date?>.
              </div>    
     </div>         
    <?php
}
?>

<?php 
if(isset($_GET['new'])){
	global $server;	

	$type = 'Manual Backup';
	$file = backup_tables($type);

	//show mesage
	?>
    <div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                You have successfully taken a backup of your system database as of <?=date('Y-m-d H:i')?>.
              </div>    
     </div>         
    <?php
}
?>     
        <div class="col-xs-12">
               
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=backup" method="get">
                <input type="hidden" name="url" value="backup" />
                <div class="input-group input-group-sm" style="width: 350px;">
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>

              </div>
                <a href="index.php?url=backup&new" onclick="confirm('Are you sure you want to take new backup?');">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Take Backup</button></a>              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th>
                  <th>File</th>
                  <th>Date</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$ch=mysqli_query($server, "select * from backup order by id desc limit 1000");
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "date LIKE '%$term%' OR url LIKE '%$term%' OR type LIKE '%$term%'";
	    } else {
	         $clauses[] = "date LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$ch=mysqli_query($server, "select * from backup where ".$filter);	
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><a>/backups/<?php echo $row['url']; ?></a></td>
                  <td><?php echo $row['date']; ?></td>
                  <td>
                  	<div class="btn-group">
                        <a onclick="confirm('Are you sure you want to restore this backup?');" href="index.php?url=backup&restore=<?php echo $row['id'];?>" title="Restore">
                 		<button type="button" class="btn btn-success btn-sm"><i class="fa fa-undo"></i></button></a>
                        <a title="Download" href="backups/<?php echo $row['url'];?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-floppy-o"></i></button></a>
                        <a onclick="confirm('Are you sure you want to delete this backup?');" href="index.php?url=backup&delete=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>S/N</th>
                  <th>Fule</th>
                  <th>Date</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
    
    
    </section>