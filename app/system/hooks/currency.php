<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) {
	header('location: index.php');
}
define('DATATABLE', true); 
if(isset($_GET['edit']) && !empty($_GET['edit'])) {
	global $server;
	$ch=mysqli_query($server, "select * from currencies where id = '$_GET[edit]' limit 1");
	$rows = mysqli_fetch_assoc($ch);
}
?>
    <section class="content-header">
      <h1>
        Currencies
        <small>View and manage currencies</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Currencies</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	if(getSetting('defaultCurrency') != $_GET['delete'] && $_GET['delete'] > 3) {
	$sql=mysqli_query($server, "delete from currencies where id='$_GET[delete]'")or die(mysqli_error($server));		
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected currency has been successfully deleted from your system.
              </div>    
     </div>         
    <?php
	} else {
	?>
    <div class="col-md-12">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Sorry!</h4>
                You cannot delete this currency. This currency is being used by a system feature.
              </div>    
     </div>         
    <?php		
	}
}

if(isset($_POST['save'])){
	global $server;
		$user_id = getUser();
		$add = mysqli_query($server, "INSERT INTO currencies (`rate`, `symbul`, `name`) 
		VALUES ('$_POST[rate]', '$_POST[symbul]', '$_POST[name]');") or die (mysql_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                Your new currency was successfully created.
              </div>    
     </div>         
    <?php
}

if(isset($_POST['update'])){
	global $server;
	mysqli_query($server, "update currencies set `name` = '$_POST[name]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update currencies set `rate` = '$_POST[rate]' where id='$_POST[id]'")or die(mysqli_error($server));
	mysqli_query($server, "update currencies set `symbul` = '$_POST[symbul]' where id='$_POST[id]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected currency has been successfully updated.
              </div>    
     </div>         
    <?php
}

if(isset($_GET['default'])){
	global $server;
	mysqli_query($server, "update settings set `value` = '$_GET[default]' where field = 'defaultCurrency'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected currency has been successfully set as system default.
              </div>    
     </div>         
    <?php
}

?>     
        <div class="col-md-7">
               
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=currency" method="get">
                <div class="input-group input-group-sm" style="width: 350px;">
                <input type="hidden" name="url" value="currency" />
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Symbul</th>
                  <th>Exchange Rate</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$user_id = getUser();

	$ch=mysqli_query($server, "select * from currencies order by id desc limit 1000");
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "name LIKE '%$term%' OR rate LIKE '%$term%' OR id LIKE '%$term%'";
	    } else {
	         $clauses[] = "name LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$user_id = getUser();
		$ch=mysqli_query($server, "select * from currencies where ".$filter);	
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
?>
                <tr>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo $row['rate']; ?></td>
                  <td><?php echo $row['symbul']; ?></td>
                  <td >
                  	<div class="btn-group">
                        <a title="Edit" href="index.php?url=currency&edit=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                        <?php if(getSetting('defaultCurrency') != $row['id'] ) { ?>
                        <a title="Default" href="index.php?url=currency&default=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-success btn-sm"><i class="fa fa-star"></i></button></a>
                        <?php }?>
                        <?php if(getSetting('defaultCurrency') != $row['id'] && $row['id'] > 3) { ?>
                        <a title="Delete" href="index.php?url=currency&delete=<?php echo $row['id'];?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                        <?php }?>
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Symbul</th>
                  <th>Exchange Rate</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
 

		<div class="col-md-5">
          <div class="box box-primary">
         <?php if(!isset($_GET['edit'])) { ?>
            <div class="box-header with-border"><h4>Create New Currency</h4></div>
            <?php } else { ?>
            <div class="box-header with-border"><h4>Update <?=$rows['name']?></h4></div>
            <?php } ?>

            <form class="form-horizontal" action="index.php?url=currency" method="post" role="form">           
            <div class="box-body" style="width: 90%; margin:0 auto;">
                
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" required class="form-control" id="name" name="name" value="<?=@$rows['name'] ?>" placeholder="Name your new package">
                </div>

                <div class="form-group">
                  <label for="quantity">Symbul</label>
                  <input type="text" required class="form-control" id="symbul" name="symbul" value="<?=@$rows['symbul'] ?>" placeholder="">
                </div>                

                <div class="form-group">
                  <label for="cost">Exchange Rate (Relating to <?=currencySymbul(getSetting('defaultCurrency'))?>)</label>
                  <input type="text" required class="form-control" id="rate" name="rate" value="<?=@$rows['rate'] ?>" placeholder="">
                </div>
                
            </div>

            <div class="box-footer">
              <button type="reset" class="btn btn-danger">Reset Form</button>
               <?php if(!isset($_GET['edit'])) { ?>
              <button type="submit" name="save" class="btn btn-success">Create Currency</button>
              <?php } else { ?>
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update Currency</button>
              <?php }?>
            </div>
            </form>
                          
        </div>
       </div> 
                     
      </div>      
    
    </section>