<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('DATATABLE', true); 
if(!isAdmin(getUser())) { header('location: index.php'); } 
?>
    <section class="content-header">
      <h1>
        Upload Sliding Marketing Images
        <small>Upload</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> System Tools</a></li>
        <li class="active">Upload Cell Number</li>
      </ol>
    </section>

    <section class="content">
    <div class="row">


<style>
/* Dotted red border */
hr {
  border-top: 1px dotted red;
}
</style>
<?php 
if(isset($_GET['delete'])){
	global $server;
	$user_id = getUser();
	if(isAdmin(getUser())) {
		$sql=mysqli_query($server, "delete from sliding_marketing where id='$_GET[delete]'")or die(mysqli_error($server));	
	}
	//show mesage
	?>
    <div class="col-xs-12">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected Image has been successfully deleted from the list.
              </div>    
     </div>         
    <?php
}

?>

<?php    
if(isset($_FILES['image'])){
	global $server;
	
	if($_FILES['image']['tmp_name']){
        if(!$_FILES['image']['error'])
        {
            
            $allowedFileType = ['image/gif','image/png','image/jpeg','image/JPEG','image/PNG','image/GIF'];
		       
		       if(!in_array($_FILES["image"]["type"],$allowedFileType)) {
?>
<div class="col-md-12">
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Error!</h4>
    Please upload image only. 
  </div>    
 </div> 
<?php
		       }else{
		           
		            $file_info = $_FILES["image"]["name"];
                    $file_directory = "/home/mojasms/public_html/uploads/";
                    $new_file_name = time().".". $file_info;
                    move_uploaded_file($_FILES["image"]["tmp_name"], $file_directory . $new_file_name);
		           
		           $sql="INSERT INTO `sliding_marketing`(`image`) VALUES ('$new_file_name')";
		           
		           if(mysqli_query($server, $sql)){
		          
?>
<div class="col-md-12">
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Cool</h4>
    Image Uploaded successfully 
  </div>    
 </div> 
<?php
		           }else{
?>
<div class="col-md-12">
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Error!</h4>
    Try to upload once again. 
  </div>    
 </div> 
<?php
		           }
		           
		       }
            
        }
	}
	
}

?>  
    

       <form class="form-vertial" action="index.php?url=sliding_marketing" method="post" role="form" enctype="multipart/form-data">           
		<div class="col-md-12">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Upload Sliding Marketing</h4></div>
            
                <div class="box-body">
                    <div class="form-group">
                      <label>Upload Image</label>
                      <input type="file" name="image" class="form-group" required/>
                    </div> 
                </div>
                 <div class="box-footer">
                  <button type="submit" name="save" class="btn btn-success">Upload</button>
                </div>
                
          </div>
        </div>
       </form>
       
     </div>
     <div class="row">
         <div class="col-md-12">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Upload Sliding Marketing</h4></div>
            
                <div class="box-body">
                          <table id="example1" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>Image</th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                   <?php 
                                      global $server;
                                      $i=1;
                                      
                                      $ch=mysqli_query($server, "select * from sliding_marketing");
                                      	while ($row = mysqli_fetch_assoc($ch)) {
                                      ?>
                                      <tr>
                                          <td><?php echo $i++;?></td>
                                          <td><img src="/uploads/<?php echo $row['image'];?>" class="image-responsive" style="max-width:200px;" alt="sliding marketing"/></td>
                                          <td>
                                             <a href="index.php?url=sliding_marketing&delete=<?php echo $row['id'];?>" onclick="confirm('Are you sure you want to delete this image?');" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                          </td>
                                      </tr>
                                      <?php
                                      	}
                                      ?>
                              </tbody>
                          </table>
                </div>
          </div>
        </div> 
     </div>
    </section>
