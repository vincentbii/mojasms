<style>
@media (max-width: 768px) {
    .small-box h3 {
        font-size: 20px!important;
    }
    .col-xs-6{
        padding-left: 5px!important;
    }
}
</style>
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
global $configverssion_id;
global $configapp_version;

if(!DEMO_MODE) {
if(getSetting('businessName') == 'Mobiketa') {
	if(isAdmin(getUser())) {
		header('location: index.php?url=setting');	
	}
}
}
?>
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <section class="content">
    <!-- dashboard boes --->
      <div class="row">
 <?php //if(!DEMO_MODE) {if(isAdmin(getUser())) { isOutdated(); systemInformation();autoUpdate(); } } ?>  
        <div class="col-lg-3 col-xs-6" >
          <div class="small-box bg-maroon" style="border-radius: 25px;">
            <div class="inner">
            <?php if(isAdmin(getUser())) { ?>
             <h3><?php echo countCampaigns(); ?></h3>
            <?php } else { ?>
              <h3><?php echo countCampaigns(getUser()); ?></h3>
             <?php } ?>   
              <p>Campaign</p>
            </div>
            <div class="icon">
              <i class="fa fa-bullhorn"></i>
            </div>
            <a href="index.php?url=myCampaign" class="small-box-footer">View Campaigns <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6" >
          <div class="small-box bg-teal" style="border-radius: 25px;">
            <div class="inner">
            <?php if(isAdmin(getUser())) { ?>
             <h3><?php echo countCampaigns(); ?></h3>
            <?php } else { ?>
              <h3><?php echo countMarketingList(getUser()); ?></h3>
             <?php } ?>   
              <p>Marketing Lists</p>
            </div>
            <div class="icon">
              <i class="fa fa-navicon"></i>
            </div>
            <a href="index.php:url=marketingList" class="small-box-footer">See Lists <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-purple" style="border-radius: 25px;">
            <div class="inner">
            <?php if(isAdmin(getUser())) { ?>
            <h3><?php echo countSubscribers(0); ?></h3>
            <?php } else { ?>
              <h3><?php echo countSubscribers(0,getUser()); ?></h3>
            <?php } ?>  
              <p>Unique Subscribers</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="#" class="small-box-footer">&nbsp;</a>
          </div>
        </div>
        
        <?php if(isAdmin(getUser())) { ?>
        <div class="col-lg-3 col-xs-6" >
          <div class="small-box bg-green" style="border-radius: 25px;">
            <div class="inner">
              <h3><?php echo successRate(); ?>%</h3>
              
              <p>Success Rate</p>
            </div>
            <div class="icon">
              <i class="fa fa-check-circle"></i>
            </div>
            <a href="#" class="small-box-footer">&nbsp;</a>
          </div>
        </div>
        <?php } else { ?>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box" style="border-radius: 25px;background-color:#CAFAFE;">
            <div class="inner">
              <h3><?php echo !empty(getUserBalance(getUser())) ? getUserBalance(getUser()) : 0; ?></h3>
              
              <p>Current SMS Balance</p>
            </div>
            <div class="icon">
              <i class="fa fa-envelope"></i>
            </div>
            <a href="#" class="small-box-footer">&nbsp;</a>
          </div>
        </div>   
        <div class="col-lg-3 col-xs-6">
          <div class="small-box" style="border-radius: 25px;background-color:brown;color:white;">
            <div class="inner">
              <h3><?php echo !empty(getUserBalanceAll(getUser())['mms_balance']) ? getUserBalanceAll(getUser())['mms_balance'] : 0; ?></h3>
              
              <p>Current MMS Balance</p>
            </div>
            <div class="icon">
              <i class="fa fa-envelope"></i>
            </div>
            <a href="#" class="small-box-footer">&nbsp;</a>
          </div>
        </div>  
        
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green" style="border-radius: 25px;">
            <div class="inner">
              <h3><?php echo !empty(getUserBalanceAll(getUser())['vms_balance']) ? getUserBalanceAll(getUser())['vms_balance'] : 0; ?></h3>
              
              <p>Current VMS Balance</p>
            </div>
            <div class="icon">
              <i class="fa fa-envelope"></i>
            </div>
            <a href="#" class="small-box-footer">&nbsp;</a>
          </div>
        </div>  
        
        <div  class="col-lg-3 col-xs-6 align-items-center">
          <div class="small-box bg-orange" style="border-radius: 25px;">
            <div class="inner">
              <h3><i class="fa fa-download"></i></h3>
              
              <p>Cell Number Database..</p>
            </div>
            <div class="icon">
              <i class="fa fa-download"></i>
            </div>
            <a <?php if(check_cell_number_downloadable(getUser()) == 0){ ?> onclick="alert('You have not purchased any cell number.'); return false;"<?php  }?> href="download_cell_numbers" class="small-box-footer">Click Here To Download</a>
          </div>
        </div> 
        
        <div class="col-lg-3 col-xs-6" >
          <div class="small-box bg-teal" style="border-radius: 25px;">
            <div class="inner">
                <h3><?php echo number_format(userData('refferal_balance', getUser()));?> </h3>   
                <p>Earning's Points</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="refferals_history" class="small-box-footer">See History <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <?php
        
        } ?>
      </div>    
<!-- end of big dashboad boxed -->  

	  <div class="row">
        <section class="col-lg-7 connectedSortable">
		<!-- recent activity   -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Recent Activities</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <tbody>
<?php 
global $server;
	$ch=mysqli_query($server, "select * from events order by id desc limit 10");
	if(!isAdmin(getUser())) { 
		$user_name = userName(getUser());
		$ch=mysqli_query($server, "select * from events where event like '%$user_name%' order by id desc limit 10");
	}
	while ($row = mysqli_fetch_assoc($ch)) {
?>
                  <tr>
                    <td><a href=""><?php echo date('F d Y h:i A', strtotime($row['date'])); ?></a></td>
                    <td><?php echo $row['event']; ?></td>
                  </tr>
<?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Events</a>
            </div>
          </div>      
        
        </section>
        
        <section class="col-lg-5 connectedSortable">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Recent Campaign</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            
            <div class="box-body">
<?php 
global $server;
	$ch=mysqli_query($server, "select * from campaigns order by id desc limit 5");
	if(!isAdmin(getUser())) { 
		$user_id = getUser();
		$ch=mysqli_query($server, "select * from campaigns where customer_id = '$user_id' order by id desc limit 10");
	}
	while ($row = mysqli_fetch_assoc($ch)) {
?>
               <div class="progress-group">
                 <span class="progress-text"><?php echo $row['name']; ?></span>
                 <span class="progress-number"><b><?php echo campaignProgress($row['id']); ?></b>/100</span>
                 <div class="progress sm active">
                    <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="<?php echo campaignProgress($row['id']); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo campaignProgress($row['id']); ?>%">
                      <span class="sr-only"><?php echo campaignProgress($row['id']); ?>% Complete (success)</span>
                    </div>
                  </div>
               </div>
<?php } ?>
           	</div>                
          </div>
        </section>
     </div>   
<?php if(!isAdmin(getUser())) {  ?>
     <div class="row">
     <section class="col-lg-12 connectedSortable">
         <!-- recent campaign -->
          <div class="box box-solid bg-blueactive">
            <div class="box-header">
              <i class="fa fa-th"></i>
              <h3 class="box-title"><?php echo date('F'); ?> Subscription Graph</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body border-radius-none">
              <!-- <div class="chart" id="line-chart" style="height: 250px;"></div> -->
              <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
            </div>
            
          </div>
     </section>
<?php

} else { ?>
	  
     <div class="row">
     <section class="col-lg-12 connectedSortable">
         <!-- recent campaign -->
          <div class="box box-solid bg-navy-active">
            <div class="box-header">
              <i class="fa fa-th"></i>
              <h3 class="box-title"><?php echo date('F'); ?> Subscription Graph</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body border-radius-none">
              <!-- <div class="chart" id="line-chart" style="height: 250px;"></div> -->
              <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
            </div>
            <div class="box-footer no-border">
              <div class="row">
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <input type="text" readonly="readonly" class="knob" data-readonly="true" value="<?php echo countDateSubscribers(date('Y-m-d')); ?>" data-width="60" data-height="60" data-fgColor="#441F3F">
                  <div class="knob-label"><strong>Today so far</strong></div>
                </div>
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <input type="text" readonly="readonly" class="knob" data-readonly="true" value="<?php echo countPeriodSubscribers('7'); ?>" data-width="60" data-height="60" data-fgColor="#221F3F">
                  <div class="knob-label"><strong>Past 7 Days</strong></div>
                </div>
                <div class="col-xs-4 text-center">
                  <input type="text" class="knob" readonly="readonly" data-readonly="true" value="<?php echo countPeriodSubscribers('30'); ?>" data-width="60" data-height="60" data-fgColor="#001F3F">
                  <div class="knob-label"><strong>Past 30 Days</strong></div>
                </div>
              </div>
            </div>
          </div>
     </section>
     <?php } ?>
     </div>
    </section>
