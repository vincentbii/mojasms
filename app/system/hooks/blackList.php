<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isAdmin(getUser())) { header('location: index.php'); } 

define('DATATABLE', true); 

?>
    <section class="content-header">
      <h1>
        Manage Blacklist
        <small>Manage black-listed numbers</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Blacklist</li>
      </ol>
    </section>

    <section class="content">
       <div class="row">
<?php 
if(isset($_GET['delete'])){
	global $server;
	$sql=mysqli_query($server, "delete from blacklists where id='$_GET[delete]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected phone number has been successfully removed from your blacklist.
              </div>    
     </div>         
    <?php
}    

if(isset($_POST['save'])){
	global $server;
		$user_id = getUser();
		$date = date('Y-m-d');
		$error = 0;
		$phone = '';
		if(!empty($_POST['phone'])) {
			$phone = $_POST['phone'];
			$add = mysqli_query($server, "INSERT INTO blacklists (`phone`, `date`) 
			VALUES ('$_POST[phone]', '$date');") or die (mysql_error($server));
		} 
 ?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The new phone number was successfully added to your blacklist.
              </div>    
 
    <?php 
}

if(isset($_POST['update'])){
	global $server;
	mysqli_query($server, "update blacklists set `phone` = '$_POST[phone]' where id='$_POST[id]'")or die(mysqli_error($server));
	//show mesage
	?>
    <div class="col-md-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected phone number has been successfully updated.
              </div>    
     </div>         
    <?php
}
?>     
        <div class="col-md-7">
               
          <div class="box box-success"><br /><p></p>
         
            <div class="box-header">
              <div class="box-tools">
                <form action="index.php?url=blackList" method="get">
                <input type="hidden" name="url" value="blackList" />
                <div class="input-group input-group-sm" style="width: 350px;">
                  <input type="search" name="keyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
              
            </div>
            <!-- /.box-header -->
            <p></p>
            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Phone Number</th>
                  <th>Added on</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
<?php 
global $server;
	$ch=mysqli_query($server, "select * from blacklists  order by id desc limit 1000");
	if(isset($_GET['keyword'])) {
	$term = mysql_real_escape_string($_GET['keyword']);
	$ser = explode(' ', $term);
	$clauses = array();
	foreach($ser as $look)
	{
	    $term = trim(preg_replace('/[^a-z0-9]/i', '', $look));   
	    if (!empty($term)) {
	         $clauses[] = "phone LIKE '%$term%' OR date LIKE '%$term%'";
	    } else {
	         $clauses[] = "phone LIKE '%%'";
	    }
	}

	$filter = '('.implode(' OR ', $clauses).')';	
	$ch=mysqli_query($server, "select * from blacklists where ".$filter);	
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($ch)) {
?>
                <tr>
                  <td><?php echo $row['phone']; ?></td>
                  <td><?php echo $row['date']; ?></td>
                  <td>
                  	<div class="btn-group">
                        <a href="index.php?url=blackList&delete=<?php echo $row['id']?>">
                 		<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                	</div>
                  </td>
				</tr>
<?php $i++; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Phone Number</th>
                  <th>Added on</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
 

		<div class="col-md-5">
          <div class="box box-primary">
         <?php if(!isset($_GET['edit'])) { ?>
            <div class="box-header with-border"><h4>Add Phone Number </h4></div>
            <?php } else { ?>
            <div class="box-header with-border"><h4>Update Phone Number</h4></div>
            <?php } ?>

            <form class="form-horizontal" action="index.php?url=blackList" method="post" role="form" enctype="multipart/form-data">           
            <div class="box-body" style="width: 90%; margin:0 auto;">
           		<div class="form-group">
                  <label for="type">Selected List</label>
                  <select name="type" id="type" class="form-control" style="width: 100%;">
                    <option  value="">Blacklist</option>                    
                  </select>
                </div>
                
                <div class="form-group">
                  <label for="name">Phone Number</label>
                  <input required type="tel" class="form-control" id="phone" name="phone" value="" placeholder="Enter Phone Number (include country code)">
                </div>                
                
            </div>

            <div class="box-footer">
              <button type="reset" class="btn btn-danger">Reset Form</button>
               <?php if(!isset($_GET['edit'])) { ?>
              <button type="submit" name="save" class="btn btn-success">Add Number</button>
              <?php } else { ?>
              <input type="hidden" name="id" value="<?=$rows['id']?>" />
              <button type="submit" name="update" class="btn btn-success">Update Number</button>
              <?php }?>
            </div>
            </form>
                          
        </div>
       </div> 
    
    </section>