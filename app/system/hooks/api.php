<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	global $server;$userID=getUser();
	$ch=mysqli_query($server, "select * from users where id = '$userID' limit 1");
	$rows = mysqli_fetch_assoc($ch);
?>
    <section class="content-header">
      <h1>
        API
        <small>API and integration guide</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">API</li>
      </ol>
    </section>

    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
	          <div class="box box-info">         
    	        <div class="box-header"><h4>General Information</h4></div>
                <div class="box-body table-responsive">
                	<table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>Parameter</th>
                          <th>Value</th>
                        </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Base URL</td>
                            <td><div class="well"><?=home_base_url()?>API/index.php?key=<?=$rows['api_key']?>&phone=...&&text=..&&senderid=MOJASMS</div></td>
                          </tr>
                          <tr>
                            <td>Request Method</td>
                            <td>HTTP GET</td>
                          </tr>
                          <tr>
                            <td>API Key</td>
                            <td><?=$rows['api_key']?></td>
                          </tr>
                        </tbody>
                     </table>  
                </div>
              </div> 
    	</div>
    </section>