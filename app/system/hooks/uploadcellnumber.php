<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('DATATABLE', true); 
if(!isAdmin(getUser())) { header('location: index.php'); } 
?>
    <section class="content-header">
      <h1>
        Upload Cell Number Database
        <small>Upload Cell Number</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> System Tools</a></li>
        <li class="active">Upload Cell Number</li>
      </ol>
    </section>
<style>
    .dataTables_paginate{
        display:none!important;
    }
</style>
    <section class="content">
    <div class="row">
<?php    
if(isset($_FILES['uploadfile'])){
	global $server;
		$api = $_FILES['uploadfile'];
		
		if($_FILES['uploadfile']['tmp_name']){
            if(!$_FILES['uploadfile']['error'])
            {
                 $filename=$_FILES["uploadfile"]["tmp_name"];
                 
		       //$ext = end(explode(".", $_FILES['uploadfile']['name']));
           
               $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','text/csv'];
		       
		       if(!in_array($_FILES["uploadfile"]["type"],$allowedFileType)) {
?>
<div class="col-md-12">
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-info"></i> Error!</h4>
    Please upload csv or excel. 
  </div>    
 </div> 
<?php
		       }else{
		           
		         if($_FILES["uploadfile"]["type"] == 'text/csv'){ 
		        
    		        if($_FILES["uploadfile"]["size"] > 0)
                    {
                        $file = fopen($filename, "r");
                        
                        for ($lines = 0; $data = fgetcsv($file,1000,",",'"')||$data = fgetxlsx($file,1000,",",'"'); $lines++ ) 
                        {
                            if ($lines == 0) continue;
                            
                            $cellno=$data[0];
                            
                            $ch=mysqli_query($server, "select * from cell_number_download where cell_number = '$cellno'");
    	                    $rows = mysqli_num_rows($ch);
                            
                            if($rows == 0 ){
                                $sql="INSERT INTO cell_number_download (`cell_number`) VALUES ('$cellno')";
                                mysqli_query($server, $sql);
                            }
                            
                        }
                        
                  
    	?>
            <div class="col-md-12">
                  <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-info"></i> Done!</h4>
                    Your new database has been uploaded and updated successfully! 
                  </div>    
            </div>         
        <?php
                    }
                
		         }else{
		             
		            require_once '/home/mojasms/public_html/plugin/PHPExcel/IOFactory.php';
		             
		            $file_info = $_FILES["uploadfile"]["name"];
                    $file_directory = "/home/mojasms/public_html/uploads/";
                    $new_file_name = time().".". $file_info;
                    move_uploaded_file($_FILES["uploadfile"]["tmp_name"], $file_directory . $new_file_name);
                    $file_type	= PHPExcel_IOFactory::identify($file_directory . $new_file_name);
                    $objReader	= PHPExcel_IOFactory::createReader($file_type);
                    $objPHPExcel = $objReader->load($file_directory . $new_file_name);
                    $sheet_data	= $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            
                    foreach ($sheet_data as $row)
                    {
                       $cellphone=str_replace(" ", "", str_replace("-", "", str_replace("[<=9999999]", "", $row['A']))); 
                       
                       if(!empty($cellphone)){
                           
                           $cellphone = mysqli_real_escape_string($server,$cellphone);
                           
                            $ch=mysqli_query($server, "select * from cell_number_download where cell_number = '$cellphone'");
        	                $rows = mysqli_num_rows($ch);
                                
                            if($rows == 0 && !empty($cellphone)){
                                $sql="INSERT INTO cell_number_download (`cell_number`) VALUES ('$cellphone')";
                                mysqli_query($server, $sql);
                            }
                           
                       }
            
                    }
                    
		             
		      ?>
		      
		      <?php
		             
		         }
                
		       }
                	
            }
        }
	
}
?>     


<?php 
if(isset($_GET['delete'])){
	global $server;
	$user_id = getUser();
	if(isAdmin(getUser())) {
		$sql=mysqli_query($server, "delete from cell_number_download where id='$_GET[delete]'")or die(mysqli_error($server));	
	}
	//show mesage
	?>
    <div class="col-xs-12">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Done!</h4>
                The selected cell number has been successfully deleted from the list.
              </div>    
     </div>         
    <?php
}
?>  

<style>
/* Dotted red border */
hr {
  border-top: 1px dotted red;
}
    
</style>
       <form class="form-vertial" action="index.php?url=uploadcellnumber" method="post" role="form" enctype="multipart/form-data">           
		<div class="col-md-12">
          <div class="box box-success" >
                <div class="box-header with-border"><h4>Update Cell Number Database</h4></div>
            
                <div class="box-body">
                    <div class="form-group">
                      <label>Upload File CSV or Excel</label>
                      <input type="file" name="uploadfile" class="form-group" required/>
                    </div> 
                    <div class="form-group">
                        <a href="/uploads/1557167871MADARA.xlsx" class="btn btn-info btn-sm btn-lg" download>Download Example Excel</a>
                    </div>
                </div>
                 <div class="box-footer">
                  <button type="submit" name="save" class="btn btn-success">Upload</button>
                </div>
                
          </div>
        </div>
       </form>
       
     </div>
     <div class="row">
         <div class="col-md-12">
          <div class="box box-success" >
              <?php global $server;
              $sql = "SELECT * FROM cell_number_download"; 
              $rs_result = mysqli_query($server,$sql);
              ?>
                <div class="box-header with-border"><h4>(<?php echo number_format(mysqli_num_rows($rs_result),0);?>) Uploaded Cell Number Database</h4></div>
            
                <div class="box-body">
                        <a href="allcellnumber_download" class="btn btn-primary btn-lg pull-right" download><i class="fa fa-download"></i> Download All Cell Numbers</a>
                        <br>
                          <table id="example1" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>Cell Number</th>
                                      <th>Status</th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                   <?php 
                                      
                                      $i=1;
                                      $num_rec_per_page=10;
                                      
                                      if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
                                        $start_from = ($page-1) * $num_rec_per_page; 
                                      
                                      $ch=mysqli_query($server, "select * from cell_number_download LIMIT $start_from,$num_rec_per_page");
                                      	while ($row = mysqli_fetch_assoc($ch)) {
                                      ?>
                                      <tr>
                                          <td><?php echo $row['id'];?></td>
                                          <td><?php echo $row['cell_number'];?></td>
                                          <td>
                                            <?php
                                            
                                            echo ($row['bought']==NULL) ? '<span class="label label-success">Available</span>' : '<span class="label label-danger">Purchased</span>';
                                            
                                            ?>
                                          </td>
                                          <td>
                                             <a href="index.php?url=uploadcellnumber&delete=<?php echo $row['id'];?>" onclick="confirm('Are you sure you want to delete this cell number?');" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                          </td>
                                      </tr>
                                      <?php
                                      	}
                                      ?>
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th>#</th>
                                  <th>Cell Number</th>
                                  <th>Status</th>
                                  <th></th>
                                </tr>
                              </tfoot>
                          </table>
                          
                          <?php 
                            $sql = "SELECT * FROM cell_number_download"; 
                            $rs_result = mysqli_query($server,$sql); //run the query
                            $total_records = mysqli_num_rows($rs_result);  //count number of records
                            $total_pages = ceil($total_records / $num_rec_per_page); 
                            
                            $page1=isset($_GET['page']) && (int)$_GET['page'] > 1 ? ((int)$_GET['page']-1) : 1;
                            
                            $page2=isset($_GET['page']) && ((int)$_GET['page']+1) <= $total_pages ? ((int)$_GET['page']+1) : 2;
                          ?>
                          
                        <nav>
                        <ul class="pager">
                        <li class="previous"><a href="?url=uploadcellnumber&page=<?php echo $page1;?>" class="btn btn-success">Previous</a></li>
                        <li class="next"><a href="?url=uploadcellnumber&page=<?php echo $page2;?>" class="btn btn-success">Next</a></li>
                        </ul>
                        </nav>
                          

                </div>
          </div>
        </div> 
     </div>
    </section>