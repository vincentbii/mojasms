<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(getUser() > 0) {
	//load header
	if(isAdmin(getUser())) {
?>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="<?php if($URL == 'dashboard') echo 'active'; ?>">
          <a href="index.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
          </a>
        </li>
        
        <li class="treeview <?php if($URL == 'newSMSCampaign' || $URL == 'newVoiceCampaign' || $URL == 'myCampaign') echo 'active'; ?>">
          <a href="#">
            <i class="fa fa-tag"></i>
            <span>Messaging</span>
            <i class="fa fa-chevron-down pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="newSMSCampaign"><i class="fa fa-circle-o"></i> Create New SMS</a></li>
            <li><a href="myCampaign"><i class="fa fa-circle-o"></i> Manage SMS</a></li>
            <li><a href="newMMSCampaign"><i class="fa fa-circle-o"></i> Create New MMS</a></li>
            <li><a href="myMMSCampaign"><i class="fa fa-circle-o"></i> Manage MMS</a></li>
            <li><a href="newVMSCampaign"><i class="fa fa-circle-o"></i> Create New VMS</a></li>
            <li><a href="myVMSCampaign"><i class="fa fa-circle-o"></i> Manage VMS</a></li>
          </ul>
        </li>
 
        <li class="<?php if($URL == 'inbox') echo 'active'; ?>">
          <a href="inbox">
            <i class="fa fa-inbox"></i> <span>Inbox</span>
            <?php if(countInbox() > 0) { ?>
            <small class="label pull-right bg-yellow"><?php echo countInbox(); ?></small>
            <?php } ?>
          </a>
        </li>
        
        <li class="<?php if($URL == 'sent') echo 'active'; ?>">
          <a href="sent">
            <i class="fa fa-envelope"></i> <span>Sent Messages</span>
          </a>
        </li>
        
        <li class="<?php if($URL == 'template') echo 'active'; ?>">
          <a href="template">
            <i class="fa fa-file-word-o"></i> <span>SMS Templates</span>
          </a>
        </li>
        
        <li class="<?php if($URL == 'marketingList' || $URL == 'newMarketingList' || $URL == 'viewContact' ) echo 'active'; ?>">
          <a href="marketingList">
            <i class="fa fa-feed"></i> <span>Marketing List</span>
          </a>
        </li>
        
        <li class="<?php if($URL == 'blackList') echo 'active'; ?>">
          <a href="blackList">
            <i class="fa fa-ban"></i> <span>Blacklist Manager</span>
          </a>
        </li>
        
        <li class="<?php if($URL == 'withdrawal_request') echo 'active'; ?>">
          <a href="withdrawal_request">
            <i class="fa fa-bank"></i> <span>Withdrawal Request</span>
          </a>
        </li>
        
        <li class="<?php if($URL == 'transaction') echo 'active'; ?>">
          <a href="transaction">
            <i class="fa fa-money"></i> <span>Transactions</span>
          </a>
        </li>
        
      <?php if(getRoleName(getUser()) == 'Administrator') { ?>  
        <li class="<?php if($URL == 'user') echo 'active'; ?>">
          <a href="user">
            <i class="fa fa-users"></i> <span>Manage Users</span>
          </a>
        </li> 

        <li class="<?php if($URL == 'client') echo 'active'; ?>">
          <a href="client">
            <i class="fa fa-users"></i> <span>Manage Clients</span>
          </a>
        </li>         
        <?php } ?>                      
        
        <li class="<?php if($URL == 'api') echo 'active'; ?>">
          <a href="api">
            <i class="fa fa-cubes"></i> <span>SEND SMS API</span>
          </a>
        </li>        

	<?php if(getRoleName(getUser()) == 'Administrator') { ?>
        <li class="treeview <?php if($URL == 'setting' || $URL=='managevirtualnumbers' || $URL=='uploadcellnumber' || $URL=='managecellphonenumbers' || $URL=='vmspackage' || $URL=='mmspackage' || $URL=='apisetup' || $URL=='sliding_marketing' || $URL=='mojasms_signature' || $URL=='popup_note' || $URL=='autoapprove_numbers' || $URL == 'smsGateway' || $URL == 'voiceGateway' || $URL == 'routing' || $URL == 'incoming' || $URL == 'backup') echo 'active'; ?>">
          <a href="#">
            <i class="fa fa-cogs"></i>
            <span>System Tools</span>
            <i class="fa fa-chevron-down pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="sliding_marketing"><i class="fa fa-circle-o"></i> Manage Slider Marketings</a></li>
             <li><a href="popup_note"><i class="fa fa-circle-o"></i>Manage Login Messages</a></li>
             <li><a href="mojasms_signature"><i class="fa fa-circle-o"></i>Mojasms Signature</a></li>
             <li><a href="autoapprove_numbers"><i class="fa fa-circle-o"></i>Set Auto Approve Numbers</a></li>
            <li><a href="apisetup"><i class="fa fa-circle-o"></i> API Setup</a></li>
            <li><a href="setting"><i class="fa fa-circle-o"></i> General Settings</a></li>
            <li><a href="smsGateway"><i class="fa fa-circle-o"></i> SMS Gateway Setup</a></li>
            <li><a href="routing"><i class="fa fa-circle-o"></i> Message Routing Rules</a></li>
            <li><a href="incoming"><i class="fa fa-circle-o"></i> Incoming SMS Setup</a></li>
            <li><a href="package"><i class="fa fa-circle-o"></i> Manage SMS Packages</a></li>
            <li><a href="mmspackage"><i class="fa fa-circle-o"></i> Manage MMS Packages</a></li>
            <li><a href="vmspackage"><i class="fa fa-circle-o"></i> Manage VMS Packages</a></li>
            
            <li><a href="managevirtualnumbers"><i class="fa fa-circle-o"></i> Manage Virtual Numbers</a></li>
            <li><a href="managecellphonenumbers"><i class="fa fa-circle-o"></i> Manage Cellphone Numbers</a></li>
            <li><a href="uploadcellnumber"><i class="fa fa-circle-o"></i> Upload Cellphone Numbers</a></li>
            
            <li><a href="paymentgateway"><i class="fa fa-circle-o"></i> Manage Payment Gateways</a></li>
            <li><a href="currency"><i class="fa fa-circle-o"></i> Manage Currencies</a></li>
            <li><a href="backup"><i class="fa fa-circle-o"></i> Backup Manager</a></li>
          </ul>
        </li>   
    <?php } ?>    
        <li>
          <a href="index.php?logout">
            <i class="fa fa-power-off"></i> <span>Logout</span>
          </a>
        </li>             
      </ul>
    </section>
  </aside>

  <div class="content-wrapper">
<style>
.icon i.fa {
	font-size: 66px;	
	margin-top: 30px;
}
</style>
<?php	
	} else {
?>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="<?php if($URL == 'dashboard') echo 'active'; ?>">
          <a href="index.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
          </a>
        </li>
        <li class="treeview <?php if($URL == 'transactions') echo 'active'; ?>">
          <a href="#">
            <i class="fa fa-credit-card"></i>
            <span>Buy Credits</span>
            <i class="fa fa-chevron-down pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="cellphone"><i class="fa fa-shopping-cart"></i> <span>Buy Cell Number Database</span></a></li>
            <li><a href="virtual"><i class="fa fa-shopping-cart"></i> <span>Buy Virtual Number</span></a></li>
            <li><a href="ordermms"><i class="fa fa-shopping-cart"></i> <span>Buy MMS Credit</span></a></li>
            <li><a href="ordervms"><i class="fa fa-shopping-cart"></i> <span>Buy VMS Credit</span></a></li>
            <li><a href="order"> <i class="fa fa-shopping-cart"></i> <span>Buy SMS Credit</span></a></li>
          </ul>
        </li>
        
        <li class="treeview <?php if($URL == 'newSMSCampaign' || $URL == 'newVoiceCampaign' || $URL == 'myCampaign') echo 'active'; ?>">
          <a href="#">
            <i class="fa fa-bullhorn"></i>
            <span>Messaging</span>
            <i class="fa fa-chevron-down pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="newSMSCampaign"><i class="fa fa-circle-o"></i> Create New SMS</a></li>
            <li><a href="myCampaign"><i class="fa fa-circle-o"></i> Manage SMS</a></li>
            <li><a href="newMMSCampaign"><i class="fa fa-circle-o"></i> Create New MMS</a></li>
            <li><a href="myMMSCampaign"><i class="fa fa-circle-o"></i> Manage MMS</a></li>
            <li><a href="newVMSCampaign"><i class="fa fa-circle-o"></i> Create New VMS</a></li>
            <li><a href="myVMSCampaign"><i class="fa fa-circle-o"></i> Manage VMS</a></li>
          </ul>
        </li>
 
        <li class="<?php if($URL == 'inbox') echo 'active'; ?>">
          <a href="inbox">
            <i class="fa fa-inbox"></i> <span>Inbox</span>
          </a>
        </li>
        
        <li class="<?php if($URL == 'sent') echo 'active'; ?>">
          <a href="sent">
            <i class="fa fa-envelope"></i> <span>Sent Messages</span>
          </a>
        </li>
        
        <li class="<?php if($URL == 'template') echo 'active'; ?>">
          <a href="template">
            <i class="fa fa-file-word-o"></i> <span>SMS Templates</span>
          </a>
        </li>
        
        <li class="<?php if($URL == 'marketingList' || $URL == 'newMarketingList' || $URL == 'viewContact' ) echo 'active'; ?>">
          <a href="marketingList">
            <i class="fa fa-feed"></i> <span>Marketing List</span>
          </a>
        </li>
        
        <li class="<?php if($URL == 'api') echo 'active'; ?>">
          <a href="api">
            <i class="fa fa-cubes"></i> <span>SEND SMS API</span>
          </a>
        </li>  
        
        <li class="<?php if($URL == 'referal_dashboard' || $URL == 'refferals_history' || $URL == 'withdrawal_history' || $URL == 'referal_lists') echo 'active'; ?>">
          <a href="referal_dashboard">
            <i class="fa fa-money"></i> <span>Referal Dashboard</span>
          </a>
        </li>  

        <!-- <li class="<?php if($URL == 'marketingList' || $URL == 'viewContact' ) echo 'active'; ?>">
          <a href="marketingList">
            <i class="fa fa-feed"></i> <span>My Subscriber List</span>
          </a>
        </li> -->

        <li class="<?php if($URL == 'transaction') echo 'active'; ?>">
          <a href="transaction">
            <i class="fa fa-money"></i> <span>Transactions</span>
          </a>
        </li>
                
        <li class="<?php if($URL == 'client') echo 'active'; ?>">
          <a href="client">
            <i class="fa fa-users"></i> <span>Manage Profile</span>
          </a>
        </li>                        
        
        <li>
          <a href="index.php?logout">
            <?php
            $cookie_name = 'popup';
              unset($_COOKIE[$cookie_name]);
            // empty value and expiration one hour before
            
            ?>
            <i class="fa fa-power-off"></i> <span>Logout</span>
          </a>
        </li>             
      </ul>
    </section>
  </aside>

  <div class="content-wrapper">
<style>
.icon i.fa {
	font-size: 66px;	
	margin-top: 30px;
}
</style>

<?php }		
}