<?php

//check if gateay requires currency conversion
$gatewayCurrency = '1'; //naira = 1, USD = 2, etc see currency tale
$defaultCurrency = getSetting('defaultCurrency');
global $server;
$result = mysqli_query($server,"SELECT * FROM currencies WHERE id = '$gatewayCurrency'"); 
$row = mysqli_fetch_assoc($server,$result);
$currency_rate = $row['rate'];	
$currencyRate = $currency_rate;
	if($gatewayCurrency != $defaultCurrency) {
		//set exchange rate
		define('RATE',$currencyRate);	
	} else {
		define('RATE',1);	
	}

$amount = round($amount*RATE*100, 2);
$curr_code = '566';
$payment_memo = $description;
$notify_url = $payment_params['transaction_history_url'];
$tran_ref = $referrence;

$interswitch_product_id=$payment_params['interswitch_product_id'];
$interswitch_mac_key=$payment_params['interswitch_mac_key'];
				
if($payment_params['interswitch_demo']==1){					
	$trans_id=userData('email',getUser());
	$form_action='https://stageserv.interswitchng.com/test_paydirect/pay';
} else {					
	$trans_id=userData('email',getUser());
	$form_action='https://webpay.interswitchng.com/paydirect/webpay/pay.aspx';				
}
				
$inter_hash_string = trim($tran_ref.$interswitch_product_id.$trans_id.$amount.$notify_url.$interswitch_mac_key);
$interswitch_hash = hash('sha512',$inter_hash_string, true);

?>
<form action="<?php echo $form_action; ?>" method="POST">
 	<input type='hidden' value='<?php echo $inter_hash_string; ?>'/>
	<input name='product_id' type='hidden' value='<?php echo $interswitch_product_id; ?>'/>
	<input name='pay_item_id' type='hidden' value='<?php echo $trans_id; ?>'/>
	<input name='amount' type='hidden' value='<?php echo $amount; ?>'/>
	<input name='currency' type='hidden' value='<?php echo $curr_code; ?>'/>
	<input name='cust_name' type='hidden' value="<?php echo userData('name',getUser()); ?>"/>
	<input name='cust_id' type='hidden' value='<?php echo userData('email',getUser()); ?>' />
	<input name='site_redirect_url' type='hidden' value='<?php echo $notify_url; ?>'/>
	<input name='txn_ref' type='hidden' value='<?php echo $tran_ref; ?>'/>
	<input name='hash' type='hidden' value='<?php echo $interswitch_hash; ?>'/>             
