<?php

//check if gateay requires currency conversion
$gatewayCurrency = '2'; //naira = 1, USD = 2, etc see currency tale
$defaultCurrency = getSetting('defaultCurrency');
global $server;
$result = mysqli_query($server,"SELECT * FROM currencies WHERE id = '$gatewayCurrency'"); 
$row = mysqli_fetch_assoc($server,$result);
$currency_rate = $row['rate'];	
$currencyRate = $currency_rate;
	if($gatewayCurrency != $defaultCurrency) {
		//set exchange rate
		define('RATE',$currencyRate);	
	} else {
		define('RATE',1);	
	}


$amount = round($amount*RATE*115/100, 2);
$curr_code = 'USD';
$payment_memo = $description;
$tran_ref = $referrence;
$notify_url = $payment_params['transaction_history_url']."?trans_ref=".$tran_ref;
?>
	<form action="https://www.paypal.com/cgi-bin/webscr" method="POST" >
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="business" value="<?php echo $payment_params['paypal_email']; ?>">
        <input type="hidden" name="item_name" value="<?php echo $payment_memo; ?>">
        <input type="hidden" name="item_number" value="<?php echo $tran_ref; ?>">
        <input type="hidden" name="amount" value="<?php echo $amount; ?>">
        <input type="hidden" name="cancel_return" value="<?php echo $notify_url; ?>">
        <input type="hidden" name="custom" value="<?php echo $tran_ref; ?>">
        <input type="hidden" name="currency_code" value="USD">
        <input type='hidden' name='no_shipping' value='1'>
		<input type='hidden' name="no_note" value='1'>
		<input type='hidden' value='2' name='rm'>               
        <input type="hidden" name="lc" value="US">
        <input type="hidden" name="return" value="<?php echo $notify_url; ?>">
        <input type="hidden" name="notify_url" value="<?php echo $notify_url; ?>">
