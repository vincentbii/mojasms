<?php

//check if gateay requires currency conversion
$gatewayCurrency = '1'; //naira = 1, USD = 2, etc see currency tale
$defaultCurrency = getSetting('defaultCurrency');
global $server;
$result = mysqli_query($server,"SELECT * FROM currencies WHERE id = '$gatewayCurrency'"); 
$row = mysqli_fetch_assoc($server,$result);
$currency_rate = $row['rate'];	
$currencyRate = $currency_rate;
	if($gatewayCurrency != $defaultCurrency) {
		//set exchange rate
		define('RATE',$currencyRate);	
	} else {
		define('RATE',1);	
	}
	
//get students

$amount = round($amount*RATE*100, 2);
$curr_code = 'USD';
$payment_memo = $description;
$tran_ref = $referrence;
$notify_url = $payment_params['transaction_history_url']."?trans_ref=".$tran_ref;
	
?>
<form action="https://paywith.quickteller.com/" method="post" >
  <input type="hidden" name="amount" value="<?php echo $amount; ?>">
  <input type="hidden" name="customerId" value="<?php echo $tran_ref; ?>">
  <input type="hidden" name="redirectUrl" value="<?php echo $notify_url; ?>">
  <input type="hidden" name="paymentCode" value="<?php echo $payment_params['quickteller_payment_code']; ?>"> 
  <input type="hidden" name="mobileNumber" value="<?php echo userData('phone',getUser()); ?>"> 
  <input type="hidden" name="emailAddress" value="<?php echo userData('email',getUser()); ?>">              
