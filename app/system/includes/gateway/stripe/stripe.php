<?php

//check if gateay requires currency conversion
$gatewayCurrency = '2'; //naira = 1, USD = 2, etc see currency tale
$defaultCurrency = getSetting('defaultCurrency');
global $server;
$result = mysqli_query($server,"SELECT * FROM currencies WHERE id = '$gatewayCurrency'"); 
$row = mysqli_fetch_assoc($server,$result);
$currency_rate = $row['rate'];	
$currencyRate = $currency_rate;
	if($gatewayCurrency != $defaultCurrency) {
		//set exchange rate
		define('RATE',$currencyRate);	
	} else {
		define('RATE',1);	
	}


$amount = round($amount*RATE, 2);
$payment_memo = $description;
$tran_ref = $referrence;
$notify_url = $payment_params['transaction_history_url']."?trans_ref=".$tran_ref;
?>

<!-- ---- The fllowing codes goes to the main stripe HTML ---->
        <script type="text/javascript" src="https://js.stripe.com/v1/"></script>
        <!-- jQuery is used only for this example; it isn't required to use Stripe -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <script type="text/javascript">
            // this identifies your website in the createToken call below
            Stripe.setPublishableKey('<?php echo $payment_params["stripe_publishable_api_key"]; ?>');
            function stripeResponseHandler(status, response) {
                if (response.error) {
                    // re-enable the submit button
                    $('.submit').removeAttr("disabled");
                    // show the errors on the form
                    //$(".payment-errors").html(response.error.message);
					alert(response.error.message+'. Please report this error to your admin');
                } else {
                    var form$ = $("#payment-form");
                    // token contains id, last4, and card type
                    var token = response['id'];
                    // insert the token into the form so it gets submitted to the server
                    form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                    // and submit
                    form$.get(0).submit();
                }
            }
            $(document).ready(function() {
                $("#payment-form").submit(function(event) {
                    // disable the submit button to prevent repeated clicks
                    $('.submit').attr("disabled", "disabled");
                    // createToken returns immediately - the supplied callback submits the form if there are no errors
                    Stripe.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val()
                    }, stripeResponseHandler);
                    return false; // submit from callback
                });
            });
        </script>
	<div class="panel-body2">
        <span class="payment-errors"><?= $error ?></span> <span class="payment-success"><?= $success ?></span>
        <form action="" method="POST" id="payment-form">
        	<table width="90%" border="0" cellspacing="0" cellpadding="0">         
              <tr class="cont">
                <td align="right"><strong>Card Number</strong></td>
                <td width="2%"></td>
                <td><input type="text" style="width: 90%" size="20" autocomplete="off" class="card-number" /></td>
              </tr>
              <tr class="cont">
                <td align="right"> <strong>CVC</strong></td>
                <td></td>
                <td><input type="text" style="width: 40%" size="4" autocomplete="off" class="card-cvc" /></td>
              </tr>
              <tr class="cont">
                <td align="right"><strong>Expiration (MM/YYYY)</strong></td>
                <td></td>
                <td>
                <input type="text" style="width: 30%" size="2" placeholder="MM" class="card-expiry-month"/>
                <span> / </span>
                <input type="text" style="width: 30%" size="4" placeholder="YYYY" class="card-expiry-year"/></td>
              </tr>
              <tr>
              <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
              </tr>              
            </table>         
            <input type="hidden" name="transaction_id" value="<?php echo $tran_ref; ?>">
            <input type="hidden" name="amount" value="<?php echo $amount; ?>">
    </div>        