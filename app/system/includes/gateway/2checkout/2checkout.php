<?php

//check if gateay requires currency conversion
$gatewayCurrency = '2'; //naira = 1, USD = 2, etc see currency tale
$defaultCurrency = getSetting('defaultCurrency');
global $server;
$result = mysqli_query($server,"SELECT * FROM currencies WHERE id = '$gatewayCurrency'"); 
$row = mysqli_fetch_assoc($server,$result);
$currency_rate = $row['rate'];	
$currencyRate = $currency_rate;
	if($gatewayCurrency != $defaultCurrency) {
		define('RATE',$currencyRate);	
	} else {
		define('RATE',1);	
	}
	
//get this gateway data
$amount = round($amount*RATE, 2);
$curr_code = 'USD';
$payment_memo = $description;
$notify_url = $payment_params['transaction_history_url'];

?>
    <form action='https://www.2checkout.com/checkout/purchase' method='post'>
        <input type='hidden' name='sid' value="<?php echo $payment_params['2checkout_seller_id'];?>" />
        <input type='hidden' name='mode' value='2CO' />
        <input type='hidden' name='li_0_type' value='product' />
        <input type='hidden' name='li_0_name' value='<?php echo $payment_memo; ?>' />
        <input type='hidden' name='li_0_price' value='<?php echo $amount; ?>' />
        <input type='hidden' name='li_0_quantity' value='<?php echo '1'; ?>' >
        <input type='hidden' name='card_holder_name' value="<?php echo userData('name', getUser()); ?>" />
        <input type='hidden' name='street_address' value="<?php echo ''; ?>" />
        <input type='hidden' name='street_address2' value="" />
        <input type='hidden' name='city' value="<?php echo ''; ?>" />
        <input type='hidden' name='state' value="<?php echo ''; ?>" />
        <input type='hidden' name='zip' value="" />
        <input type='hidden' name='country' value="<?php echo ''; ?>" />
        <input type='hidden' name='email' value="<?php echo userData('email', getUser()); ?>" />
        <input type='hidden' name='phone' value="<?php echo userData('phone', getUser()); ?>" />
        <input type="hidden" name="x_receipt_link_url" value="<?php echo $notify_url; ?>">
