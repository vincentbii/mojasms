<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if(getUser() > 0) {
	//load header
	global $server;
	
	// check API key and create if not exist
	$userid=getUser();
	
	$sqler="SELECT * FROM `users` WHERE id='$userid' AND api_key is NULL or api_key='' LIMIT 1";
	
    $res = mysqli_query($server,$sqler);
    
    if(mysqli_num_rows($res) >0){ 
        
        $apikey=implode('-',str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 30), 6));
        $sqlerr="UPDATE `users` SET `api_key`='$apikey' WHERE id='$userid'";
    
        mysqli_query($server,$sqlerr);
    }
    
    
    $ip=get_client_ip();
    
                        
    
	$sqler="SELECT * FROM `visitors` WHERE ip='$ip' AND date='".date('Y-m-d')."'";
    
    $result = mysqli_query($server,$sqler);
    
    if(mysqli_num_rows($result) == 0){
        
        $sqlerr="INSERT INTO `visitors`(`ip`, `date`) VALUES ('$ip','".date('Y-m-d')."')";
    
        mysqli_query($server,$sqlerr);
    }
	
	$chu=mysqli_query($server, "select * from welcome_popup LIMIT 2");
      $welcome="";$message="";
      	while ($rower = mysqli_fetch_assoc($chu)) {
      	    
      	    if($rower['status']==1){
      	        $welcome=$rower['note'];
      	    }else{
      	        $message=$rower['note'];
      	    }
      	    
      	}


if(!isset($_COOKIE['popup']))
{
    setcookie('popup', time(3600));
    echo '<script>alert("'.$welcome.'");</script>';
}
else
{
    if((time() - $_COOKIE['popup']) > (3600))
    {
        setcookie('popup', time());
        echo '<script>alert("'.$message.'");</script>';        
    }
//     $cookie_name = 'popup';
//   unset($_COOKIE[$cookie_name]);
// // empty value and expiration one hour before
// $res = setcookie($cookie_name, '', time() - 360);
 }
  
 
  
  ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $siteName = getSetting('businessName');?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="plugins/select2/select2.min.css">
  <link rel="stylesheet" href="plugins/datatables/jquery.dataTables.css">
  
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  
       .skin-blue .main-sidebar
      {
          background: #002C5A;
      }
      
      .skin-blue .main-header .navbar, .skin-blue .main-header .logo {
         background: #A6C33A;
      }
      
      .skin-blue .sidebar-menu>li.active>a {
        background: #D7005E;
      }
      .skin-blue .main-header li.user-header
      {
          background: #A6C33A;
      }
      
      .btn{
          border-radius: 20px!important;
      }
      
      .btn-success {
            background: #310064 !important;
            border-color: #310064 !important;
      }
      .btn-danger {
            background: #D7005D !important;
            border-color: #D7005D !important;
      }
      
      input, select, textarea{
          border-radius: 25px!important;
      }
      
      input[type="file"]{
          border-radius:0px!important;
      }
      
      .input-group input[type="search"]{
         width:40%;
      }
      
      @media only screen and (max-width: 600px) {
          .input-group input[type="search"]{
             width:100%;
          }
        }
    /* Layout */
    .wrapper {
      /*min-height: 100%;
      position: relative;
      overflow: hidden;*/
      width: 100%;
      min-height: 100%;
      height: auto !important;
      position: absolute;
    }
  </style>
  <script>
      function pos_print(content) {
          var myWindow = window.open("", "POS", "width=700,height=600");
          myWindow.document.write('');
          myWindow.document.write(content);
          myWindow.document.close();
          
          setTimeout(function () {
        			myWindow.print(); 
        		}, 500);
          
          
          myWindow.onfocus = function () { 
        		setTimeout(function () { 
        			myWindow.close();
        		}, 500); 
        	}
        	
        }
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>M</b>S</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>MOJA</b>SMS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/avatar.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo userName(getUser()); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="dist/img/avatar.png" class="img-circle" alt="User Image">
                <p>
                  <?php echo userName(getUser()). ' - '.getRoleName(getUser()); ?>
                  <small>Last Login: <?php echo date('d F, Y', strtotime(getLastLogin(getUser()))); ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <?php if(!isAdmin(getUser())) { ?>
                  <a href="client" class="btn btn-default btn-flat">Update Profile</a>
                <?php } else { ?>
                  <a href="user" class="btn btn-default btn-flat">Update Profile</a>
                <?php } ?>
                </div>
                <div class="pull-right">
                  <a href="index.php?logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>

      </div>
    </nav>
  </header>


<?php	
}
if(DEMO_MODE) {
	if(isset($_REQUEST['delete']) || isset($_REQUEST['edit']) || isset($_REQUEST['update'])) {
		echo "<script>";
		echo "alert('Sorry but you can not Delete or Edit a record in Demo Mode\nPlease consider creating a fresh record if you are trying to update an existing record');";
		echo "</script>";header('location: index.php');
	}
}
