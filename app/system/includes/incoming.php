<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
global $server;
if(getSetting('incomingXML') > 0) {
   header("content-type: text/xml");
   echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
}
/*
 This script handles connection/communication with your incming message server.
 Read your 2-way SMS provider's instruction/documentation for the appropriate way of linking this script with their server.
 Do not modify this page if you do not have sufficent tecnical background. Hre a techy instead.
*/

//Define your Incoming SMS Gateway parameters

$source = getSetting('incomingFrom');
					
$recipient = getSetting('incomingTo');
					
$message = getSetting('incomingBody'); 	




/* ===============================================================================================================
    Do Not Edit Beyond This Line. 
	Altering any part of the following codes may affect normal system performace
 ================================================================================================================*/	

					
if(isset($_REQUEST[$message])) {
        $message = $_REQUEST[$message];
        $sender = $_REQUEST[$source];
		$to = $_REQUEST[$recipient];

		//remove + sign 
		$sender = str_ireplace('+', '', $sender);
		$to = str_ireplace('+', '', $to);
		
		$date = date('Y-m-d H:i:s');
		$campaign_id = 0;
		$customer = getInboxClient($message, $to);
		$marketinglist = checkKeyword('id',$message,$to);
		
         mysqli_query($server,"INSERT INTO inbox (`from`, `message`,`to`,`date`,`campaign_id`, `customer_id`) 
         VALUES ('$sender', '$message', '$to','$date', '$campaign_id', '$customer')");
		 $inbox_id = getInsertedID('inbox');
		 
		 if($marketinglist > 0) { 
			$sql = "SELECT * FROM marketinglists WHERE id = '$marketinglist' LIMIT 1";
			$result = mysqli_query($server,$sql);
			$num = mysqli_num_rows($result);	
			$row = mysqli_fetch_assoc($result);
			$optin_keyword = strtoupper($row['optin_keyword']);
			$optin_response = $row['optin_response'];
			$optout_keyword = strtoupper($row['optout_keyword']);
			$optout_response = $row['optout_response'];
			$help_keyword = strtoupper($row['help_keyword']);
			$help_response = $row['help_response'];
			$sender_id = $row['sender_id'];
			$client = $row['customer_id'];
			if(empty($sender_id)) {
				$sender_id = getSetting('smsSender');	
			}
			mysqli_query($server,"UPDATE `inbox` SET `customer_id` = '$client' WHERE `id` = '$inbox_id'");
			//unsubscribe
			if((stripos(strtoupper($message),$optout_keyword) !== false)) {
			 	mysqli_query($server,"DELETE FROM contacts WHERE phone LIKE '%$sender%' AND marketinglist_id = '$marketinglist'") or die(mysqli_error($server));
			 	if(!empty($optout_response)) {
					sendMessage($sender_id,$sender,$optout_response,0,0,'','','Text',0,$client);
				}
			} 
			//subscribe 
			if((stripos(strtoupper($message),$optin_keyword) !== false)) {
				$date = date('Y-m-d');
				$add = mysqli_query($server, "INSERT INTO contacts (`marketinglist_id`, `phone`, `date`) 
				VALUES ('$marketinglist', '$sender', '$date');") or die (mysqli_error($server));
				if(!empty($optin_response)) {
					sendMessage($sender_id,$sender,$optin_response,0,0,'','','Text',0,$client);
				}
			}
			//help 
			if((stripos(strtoupper($message),$help_keyword) !== false)) { 
				if(!empty($optout_response)) { 
					sendMessage(getSetting('smsSender'),$sender,$help_response,0,0,'','','Text',0,$client);
				}
			}
		 }
		 
		 //do blacklist
		 $blackListWord = strtoupper(getSetting('blacklistKeyword'));
		 if((stripos(strtoupper($message),$blackListWord) !== false)) {
			 $date = date('Y-m-d');
			 $blacklistResponse = getSetting('blacklistResponse');
			 mysqli_query($server,"INSERT INTO blacklists (`phone`, `date`) VALUES ('$sender', '$date')");
			 if(!empty($blacklistResponse)) {
					sendMessage(getSetting('smsSender'),$sender,$blacklistResponse,0,0,'','','Text',0,$client);
			}
		 }
         mysqli_close($server);
}					