<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
@define('DEMO_MODE',false);


if(getUser() > 0) {
	//load footer
?>


  </div> <!-- this is to end the main body section --->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Powered By <a href="https://thecodeshop.co.za/" target="_blank">TheCodeShop</a></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
    </ul>
        <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
<?php 
if(isAdmin(getUser())) { 
global $server;
	$ch=mysqli_query($server, "select * from events order by id desc limit 10");
	while ($row = mysqli_fetch_assoc($ch)) {
?>        
          <li>
            <a href="javascript:void(0)">
            <i class="menu-icon fa fa-file-code-o bg-green"></i> 

              <div class="menu-info">
                <h4 class="control-sidebar-subheading"><?php echo $row['event']; ?></h4>
                <p><?php echo $row['date']; ?></p>
              </div>
            </a>
          </li>
<?php } ?>          
        </ul>
        <!-- /.control-sidebar-menu -->
<?php } ?>
        <h3 class="control-sidebar-heading">Recent Campaign Progress</h3>
        <ul class="control-sidebar-menu">
<?php 
global $server;
	$ch=mysqli_query($server, "select * from campaigns order by id desc limit 5");
if(!isAdmin(getUser())) { 
$user_id = getUser();
	$ch=mysqli_query($server, "select * from campaigns where customer_id = '$user_id' order by id desc limit 5");
}
	while ($row = mysqli_fetch_assoc($ch)) {
?>          
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">

                <?php echo $row['name']; ?>
                <span class="label label-primary pull-right"><?php echo campaignProgress($row['id']); ?>%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: <?php echo campaignProgress($row['id']); ?>%"></div>
              </div>
            </a>
          </li>
<?php } ?>          
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">

          <!-- /.form-group -->
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<?php if($URL == 'myCampaign' || $URL == 'newVoiceCampaign' || $URL == 'newSMSCampaign' || defined('DATATABLE')) {?>
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>
<script>
    $(document).on('click','.approvebtn',function(e){
      e.preventDefault();
        if(confirm('Are you sure you want to approve this withdrawal?')){ 
            window.location.href=$(this).attr('href');
        }else{
            return false; 
        }
    });
    
    $(document).on('click','.cancelbtn',function(e){
      e.preventDefault();
        if(confirm('Are you sure you want to cancel this withdrawal?')){ 
            window.location.href=$(this).attr('href');
        }else{
            return false; 
        }
    });
    
    $(document).on('click','.deletebtn',function(e){
      e.preventDefault();
        if(confirm('Are you sure you want to delete this withdrawal?')){ 
            window.location.href=$(this).attr('href');
        }else{
            return false; 
        }
    });
    
    
</script>
<script>
  $(function () {
      
    $("table").parent().addClass("table-responsive");
      
    $('#example1').DataTable({
		//"ordering": true,
    	"searching": false
    });
  });
  $(function () {
    $('#templates').DataTable({
		//"ordering": true,
    	"searching": false
    });
  });
</script>
<?php } else {?>
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>


<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/pages/dashboard.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>
<script>

  var area = new Morris.Area({
    element: 'revenue-chart',
    resize: true,
    data: [
<?php
$month = date('m');
$year = date('Y');
$maxDay = getMonthDays($month);
for($d = 1; $d <= $maxDay; $d++) {
	$date = $year.'-'.$month.'-'.sprintf('%02d',$d);
	$subscribers = countDateSubscribers($date);
	if(!isAdmin(getUser())) { 
		$subscribers = countDateSubscribers($date, getUser());
	}
	$echo = " {m: '".$date."', Subscribers: ".$subscribers."},";
	if($d == $maxDay) {
		$echo = " {m: '".$date."', Subscribers: ".$subscribers."}";
	}
	echo $echo;
}
?>	
    ],
    xkey: 'm',
    ykeys: ['Subscribers'],
    labels: ['New Subscriptions'],
    lineColors: ['#a0d0e0'],
    hideHover: 'auto'
  });
  
  
</script>   
<?php } ?>
<!-- FLOT CHARTS -->
<script src="plugins/flot/jquery.flot.min.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="plugins/flot/jquery.flot.resize.min.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="plugins/flot/jquery.flot.pie.min.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="plugins/flot/jquery.flot.categories.min.js"></script>

<?php if($URL == 'myCampaign' && $_GET['view'] > 0) {?>
<script>
  $(function () {
	var donutData = [
	<?php if(countSent($_GET['view']) > 0 || countFailed($_GET['view']) > 0) { ?>
      {label: "Success", data: <?php echo countSent($_GET['view']); ?>, color: "#11aa77"},
      {label: "Failed", data: <?php echo countFailed($_GET['view']); ?>, color: "#ff1111"},
      {label: "Unknown", data: 0, color: "#00c0ef"}
	  <?php } else { ?>
	  {label: "Unknown", data: 1, color: "#00c0ef"}
	  <?php } ?>
    ];
    $.plot("#donut-chart", donutData, {
      series: {
        pie: {
          show: true,
          radius: 1,
          innerRadius: 0.5,
          label: {
            show: true,
            radius: 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    });
  });
  function labelFormatter(label, series) {
    return '<div style="font-size:13px; text-align:center; padding:3px; color: #fff; font-weight: 600;">'
        + label
        + "<br>"
        + Math.round(series.percent) + "%</div>";
  }  
</script>
<?php } ?>
<?php	
if($URL == 'setting') { ?>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    CKEDITOR.replace('editor1');
    $(".textarea").wysihtml5();
  });
</script>
<?php
}
}
?>
<script src="plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-rane-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap datetabled -->
<script src="plugins/datatables/jquery.dataTables.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>

<script>
  $(function () {
      
    $('#example321').DataTable();
         
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });	

function createCookies(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookies(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookies(name) {
    createCookie(name,"",-1);
}  
function hideUpgrade(value) {
	createCookies('hideUpgrade',value,90);	
}

/*document.getElementById('intervals').addEventListener('change', function () {
    var style = this.value > 0 ? 'block' : 'none';
    document.getElementById('intervalHold').style.display = style;
});
*/
/*document.getElementById('type').addEventListener('change', function () {
    var style = this.value == 'Voice' ? 'block' : 'none';
    document.getElementById('showVoice').style.display = style;
	
	var style = this.value == 'Voice' ? 'block' : 'none';
    document.getElementById('onVoice').style.display = style;
    
    var style = this.value == 'MMS' ? 'block' : 'none';
    document.getElementById('showMMS').style.display = style;
});*/


</script>
<script src="plugins/morris/morris.min.js"></script>	
