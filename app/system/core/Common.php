<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
* Determines if the current version of PHP is greater then the supplied value
*
* Since there are a few places where we conditionally test for PHP > 5
* we'll set a static variable.
*
* @access	public
* @param	string
* @return	bool	TRUE if the current version is $version or higher
*/
$Config = new CI_Config();
$server=mysqli_connect($Config->db_host(), $Config->db_username(), $Config->db_password());
$db=mysqli_select_db($server, $Config->db_name());
global $server;

if ( ! function_exists('is_php'))
{
	function is_php($version = '5.0.0')
	{
		static $_is_php;
		$version = (string)$version;

		if ( ! isset($_is_php[$version]))
		{
			$_is_php[$version] = (version_compare(PHP_VERSION, $version) < 0) ? FALSE : TRUE;
		}

		return $_is_php[$version];
	}
}

// ------------------------------------------------------------------------

/**
 * Tests for file writability
 *
 * is_writable() returns TRUE on Windows servers when you really can't write to
 * the file, based on the read-only attribute.  is_writable() is also unreliable
 * on Unix servers if safe_mode is on.
 *
 * @access	private
 * @return	void
 */
if ( ! function_exists('is_really_writable'))
{
	function is_really_writable($file)
	{
		// If we're on a Unix server with safe_mode off we call is_writable
		if (DIRECTORY_SEPARATOR == '/' AND @ini_get("safe_mode") == FALSE)
		{
			return is_writable($file);
		}

		// For windows servers and safe_mode "on" installations we'll actually
		// write a file then read it.  Bah...
		if (is_dir($file))
		{
			$file = rtrim($file, '/').'/'.md5(mt_rand(1,100).mt_rand(1,100));

			if (($fp = @fopen($file, FOPEN_WRITE_CREATE)) === FALSE)
			{
				return FALSE;
			}

			fclose($fp);
			@chmod($file, DIR_WRITE_MODE);
			@unlink($file);
			return TRUE;
		}
		elseif ( ! is_file($file) OR ($fp = @fopen($file, FOPEN_WRITE_CREATE)) === FALSE)
		{
			return FALSE;
		}

		fclose($fp);
		return TRUE;
	}
}

// ------------------------------------------------------------------------

/**
* Class registry
*
* This function acts as a singleton.  If the requested class does not
* exist it is instantiated and set to a static variable.  If it has
* previously been instantiated the variable is returned.
*
* @access	public
* @param	string	the class name being requested
* @param	string	the directory where the class should be found
* @param	string	the class name prefix
* @return	object
*/
if ( ! function_exists('load_class'))
{
	function &load_class($class, $directory = 'libraries', $prefix = 'CI_')
	{
		static $_classes = array();

		// Does the class exist?  If so, we're done...
		if (isset($_classes[$class]))
		{
			return $_classes[$class];
		}

		$name = FALSE;

		// Look for the class first in the local application/libraries folder
		// then in the native system/libraries folder
		foreach (array(APPPATH, BASEPATH) as $path)
		{
			if (file_exists($path.$directory.'/'.$class.'.php'))
			{
				$name = $prefix.$class;

				if (class_exists($name) === FALSE)
				{
					require($path.$directory.'/'.$class.'.php');
				}

				break;
			}
		}

		// Is the request a class extension?  If so we load it too
		if (file_exists(APPPATH.$directory.'/'.config_item('subclass_prefix').$class.'.php'))
		{
			$name = config_item('subclass_prefix').$class;

			if (class_exists($name) === FALSE)
			{
				require(APPPATH.$directory.'/'.config_item('subclass_prefix').$class.'.php');
			}
		}

		// Did we find the class?
		if ($name === FALSE)
		{
			// Note: We use exit() rather then w_error() in order to avoid a
			// self-referencing loop with the Excptions class
			exit('Unable to locate the specified class: '.$class.'.php');
		}

		// Keep track of what we just loaded
		is_loaded($class);

		$_classes[$class] = new $name();
		return $_classes[$class];
	}
}

// --------------------------------------------------------------------

/**
* Keeps track of which libraries have been loaded.  This function is
* called by the load_class() function above
*
* @access	public
* @return	array
*/
if ( ! function_exists('is_loaded'))
{
	function &is_loaded($class = '')
	{
		static $_is_loaded = array();

		if ($class != '')
		{
			$_is_loaded[strtolower($class)] = $class;
		}

		return $_is_loaded;
	}
}

// ------------------------------------------------------------------------

/**
* Loads the main config.php file
*
* This function lets us grab the config file even if the Config class
* hasn't been instantiated yet
*
* @access	private
* @return	array
*/
if ( ! function_exists('get_config'))
{
	function &get_config($replace = array())
	{
		static $_config;

		if (isset($_config))
		{
			return $_config[0];
		}

		// Is the config file in the environment folder?
		if ( ! defined('ENVIRONMENT') OR ! file_exists($file_path = APPPATH.'config/'.ENVIRONMENT.'/config.php'))
		{
			$file_path = APPPATH.'config/config.php';
		}

		// Fetch the config file
		if ( ! file_exists($file_path))
		{
			exit('The configuration file does not exist.');
		}

		require($file_path);

		// Does the $config array exist in the file?
		if ( ! isset($config) OR ! is_array($config))
		{
			exit('Your config file does not appear to be formatted correctly.');
		}

		// Are any values being dynamically replaced?
		if (count($replace) > 0)
		{
			foreach ($replace as $key => $val)
			{
				if (isset($config[$key]))
				{
					$config[$key] = $val;
				}
			}
		}

		return $_config[0] =& $config;
	}
}

// ------------------------------------------------------------------------

/**
* Returns the specified config item
*
* @access	public
* @return	mixed
*/
if ( ! function_exists('config_item'))
{
	function config_item($item)
	{
		static $_config_item = array();

		if ( ! isset($_config_item[$item]))
		{
			$config =& get_config();

			if ( ! isset($config[$item]))
			{
				return FALSE;
			}
			$_config_item[$item] = $config[$item];
		}

		return $_config_item[$item];
	}
}

// ------------------------------------------------------------------------

/**
* Error Handler
*
* This function lets us invoke the exception class and
* display errors using the standard error template located
* in application/errors/errors.php
* This function will send the error page directly to the
* browser and exit.
*
* @access	public
* @return	void
*/
if ( ! function_exists('show_error'))
{
	function show_error($message, $status_code = 500, $heading = 'An Error Was Encountered')
	{
		$_error =& load_class('Exceptions', 'core');
		echo $_error->show_error($heading, $message, 'error_general', $status_code);
		exit;
	}
}

// ------------------------------------------------------------------------

/**
* 404 Page Handler
*
* This function is similar to the show_error() function above
* However, instead of the standard error template it displays
* 404 errors.
*
* @access	public
* @return	void
*/
if ( ! function_exists('show_404'))
{
	function show_404($page = '', $log_error = TRUE)
	{
		$_error =& load_class('Exceptions', 'core');
		$_error->show_404($page, $log_error);
		exit;
	}
}

// ------------------------------------------------------------------------

/**
* Error Logging Interface
*
* We use this as a simple mechanism to access the logging
* class and send messages to be logged.
*
* @access	public
* @return	void
*/
if ( ! function_exists('log_message'))
{
	function log_message($level = 'error', $message, $php_error = FALSE)
	{
		static $_log;

		if (config_item('log_threshold') == 0)
		{
			return;
		}

		$_log =& load_class('Log');
		$_log->write_log($level, $message, $php_error);
	}
}

// ------------------------------------------------------------------------

/**
 * Set HTTP Status Header
 *
 * @access	public
 * @param	int		the status code
 * @param	string
 * @return	void
 */
if ( ! function_exists('set_status_header'))
{
	function set_status_header($code = 200, $text = '')
	{
		$stati = array(
							200	=> 'OK',
							201	=> 'Created',
							202	=> 'Accepted',
							203	=> 'Non-Authoritative Information',
							204	=> 'No Content',
							205	=> 'Reset Content',
							206	=> 'Partial Content',

							300	=> 'Multiple Choices',
							301	=> 'Moved Permanently',
							302	=> 'Found',
							304	=> 'Not Modified',
							305	=> 'Use Proxy',
							307	=> 'Temporary Redirect',

							400	=> 'Bad Request',
							401	=> 'Unauthorized',
							403	=> 'Forbidden',
							404	=> 'Not Found',
							405	=> 'Method Not Allowed',
							406	=> 'Not Acceptable',
							407	=> 'Proxy Authentication Required',
							408	=> 'Request Timeout',
							409	=> 'Conflict',
							410	=> 'Gone',
							411	=> 'Length Required',
							412	=> 'Precondition Failed',
							413	=> 'Request Entity Too Large',
							414	=> 'Request-URI Too Long',
							415	=> 'Unsupported Media Type',
							416	=> 'Requested Range Not Satisfiable',
							417	=> 'Expectation Failed',

							500	=> 'Internal Server Error',
							501	=> 'Not Implemented',
							502	=> 'Bad Gateway',
							503	=> 'Service Unavailable',
							504	=> 'Gateway Timeout',
							505	=> 'HTTP Version Not Supported'
						);

		if ($code == '' OR ! is_numeric($code))
		{
			show_error('Status codes must be numeric', 500);
		}

		if (isset($stati[$code]) AND $text == '')
		{
			$text = $stati[$code];
		}

		if ($text == '')
		{
			show_error('No status text available.  Please check your status code number or supply your own message text.', 500);
		}

		$server_protocol = (isset($_SERVER['SERVER_PROTOCOL'])) ? $_SERVER['SERVER_PROTOCOL'] : FALSE;

		if (substr(php_sapi_name(), 0, 3) == 'cgi')
		{
			header("Status: {$code} {$text}", TRUE);
		}
		elseif ($server_protocol == 'HTTP/1.1' OR $server_protocol == 'HTTP/1.0')
		{
			header($server_protocol." {$code} {$text}", TRUE, $code);
		}
		else
		{
			header("HTTP/1.1 {$code} {$text}", TRUE, $code);
		}
	}
}

// --------------------------------------------------------------------

/**
* Exception Handler
*
* This is the custom exception handler that is declaired at the top
* of Codeigniter.php.  The main reason we use this is to permit
* PHP errors to be logged in our own log files since the user may
* not have access to server logs. Since this function
* effectively intercepts PHP errors, however, we also need
* to display errors based on the current error_reporting level.
* We do that with the use of a PHP error template.
*
* @access	private
* @return	void
*/
if ( ! function_exists('_exception_handler'))
{
	function _exception_handler($severity, $message, $filepath, $line)
	{
		 // We don't bother with "strict" notices since they tend to fill up
		 // the log file with excess information that isn't normally very helpful.
		 // For example, if you are running PHP 5 and you use version 4 style
		 // class functions (without prefixes like "public", "private", etc.)
		 // you'll get notices telling you that these have been deprecated.
		if ($severity == E_STRICT)
		{
			return;
		}

		$_error =& load_class('Exceptions', 'core');

		// Should we display the error? We'll get the current error_reporting
		// level and add its bits with the severity bits to find out.
		if (($severity & error_reporting()) == $severity)
		{
			$_error->show_php_error($severity, $message, $filepath, $line);
		}

		// Should we log the error?  No?  We're done...
		if (config_item('log_threshold') == 0)
		{
			return;
		}

		$_error->log_exception($severity, $message, $filepath, $line);
	}
}

// --------------------------------------------------------------------

/**
 * Remove Invisible Characters
 *
 * This prevents sandwiching null characters
 * between ascii characters, like Java\0script.
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('remove_invisible_characters'))
{
	function remove_invisible_characters($str, $url_encoded = TRUE)
	{
		$non_displayables = array();
		
		// every control character except newline (dec 10)
		// carriage return (dec 13), and horizontal tab (dec 09)
		
		if ($url_encoded)
		{
			$non_displayables[] = '/%0[0-8bcef]/';	// url encoded 00-08, 11, 12, 14, 15
			$non_displayables[] = '/%1[0-9a-f]/';	// url encoded 16-31
		}
		
		$non_displayables[] = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';	// 00-08, 11, 12, 14-31, 127

		do
		{
			$str = preg_replace($non_displayables, '', $str, -1, $count);
		}
		while ($count);

		return $str;
	}
}

// ------------------------------------------------------------------------

/**
* Returns HTML escaped variable
*
* @access	public
* @param	mixed
* @return	mixed
*/
if ( ! function_exists('html_escape'))
{
	function html_escape($var)
	{
		if (is_array($var))
		{
			return array_map('html_escape', $var);
		}
		else
		{
			return htmlspecialchars($var, ENT_QUOTES, config_item('charset'));
		}
	}
}

// --------------------------------------------------------------------
/**
* @access	public
* @return	array
*/
if ( ! function_exists('getUser'))
{
	function getUser() {
		if(!isset($_SESSION['MOBIKETA'])) {
		return 0;
		} else {
		return @$_COOKIE['MOBIKETA']; 
		}
	}
}

if ( ! function_exists('which_app'))
{
	function which_app($url = '')
	{
	global $configinstalled;
	global $payment_params;
		if(empty($url)) {
			$url = 'dashboard';	
		}
		if(!file_exists(APPPATH.$url.'.php')) {
			$url = 'dashboard';	
		}
		if(getUser() < 1) {
			$url = 'login';	
		}
		if($configinstalled == 'INSTALLED') {
		include(APPPATH.$url.'.php');
		} else {
			include(APPPATH.$url.'.php');
		//	header('location: install/');	
		}
	}
}


if ( ! function_exists('sendEmail'))
{
    
    function sendEmail($from,$sender,$subject,$to,$message) {
		$email_from = $sender.'<'.$from.'>';
		$email_subject = $subject;
		$to = $to;
		//Build HTML Message
		$emessage = '<html><body>';
		$emessage .= "<img src='https://www.mojasms.com/logo.jpeg' style='text-align:center;' /><br><br>";
		$emessage .= $message;
		$emessage .= '</body></html>';
		$body = $emessage;
		
		//Send Mail using PHP Mailer
		$headers = "From: info@mojasms.com\r\n";
		$headers .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
		//Send the email!
		mail($to,$email_subject,$emessage,$headers);	
		
	}
	
	
	function sendEmail123($from,$sender,$subject,$to,$message) {
		$email_from = $sender.'<'.$from.'>';
		$email_subject = $subject;
		$to = $to;
		//Build HTML Message
		$emessage = '<html><body>';
		$emessage .= $message;
		$emessage .= '</body></html>';
		$body = $emessage;
		$smtpfrom = getSetting('smtpUsername');
		
		if(!empty($smtpfrom)) {
			require BASEPATH.'Mailer/PHPMailerAutoload.php';
			$mail = new PHPMailer;

			//$mail->SMTPDebug = 3;                               // Enable verbose debug output			
			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host =  getSetting('smtpServer');  			// Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = getSetting('smtpUsername');         // SMTP username
			$mail->Password = getSetting('smtpPassword');          // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = getSetting('smtpPort');                  // TCP port to connect to
			
			$mail->setFrom($smtpfrom, $sender);
			//set recipients
			$emailList = explode(',', $to);
			if(count($emailList) > 1) {
				foreach($emailList as $emailAdd) {
					$mail->addAddress($emailAdd);   
					$mail->isHTML(true);                                  // Set email format to HTML
					
					$mail->Subject = $subject;
					$mail->Body    = $body;
					$mail->AltBody = 'This email contain HTML that could not be displayed by your email reader. Please use a HTML email reader instead.';
					
					if(!$mail->send()) {
						$return = 'Error: ' . $mail->ErrorInfo;
					} else {
						$return = 'Sent';
					}				
				}
			} else {
				$mail->addAddress($to); 
				$mail->isHTML(true);                                  // Set email format to HTML
				
				$mail->Subject = $subject;
				$mail->Body    = $body;
				$mail->AltBody = 'This email contain HTML that could not be displayed by your email reader. Please use a HTML email reader instead.';
				
				if(!$mail->send()) {
					$return = 'Error: ' . $mail->ErrorInfo;
				} else {
					$return = 'Sent';
				}				
			} 
		} else {
		//Send Mail using PHP Mailer
			$headers = "From: " . getSetting('smtpUsername'). "\r\n";
			$headers .= 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
			//Send the email!
			mail($to,$email_subject,$emessage,$headers);	
		}
	}
	
}
function genRandomPassword($length = 8)
	{
		$salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$base = strlen($salt);
		$makepass = '';

		$random = genRandomBytes($length + 1);
		$shift = ord($random[0]);
		for ($i = 1; $i <= $length; ++$i)
		{
			$makepass .= $salt[($shift + ord($random[$i])) % $base];
			$shift += ord($random[$i]);
		}

		return $makepass;
	}

	function _toAPRMD5($value, $count)
	{
		/* 64 characters that are valid for APRMD5 passwords. */
		$APRMD5 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

		$aprmd5 = '';
		$count = abs($count);
		while (--$count)
		{
			$aprmd5 .= $APRMD5[$value & 0x3f];
			$value >>= 6;
		}
		return $aprmd5;
	}
	
function genRandomBytes($length = 16)
	{
		$sslStr = '';

		if (
			function_exists('openssl_random_pseudo_bytes')
			&& (version_compare(PHP_VERSION, '5.3.4') >= 0
				|| substr(PHP_OS, 0, 3) !== 'WIN'
			)
		)
		{
			$sslStr = openssl_random_pseudo_bytes($length, $strong);
			if ($strong)
			{
				return $sslStr;
			}
		}

		$bitsPerRound = 2;
		$maxTimeMicro = 400;
		$shaHashLength = 20;
		$randomStr = '';
		$total = $length;

		$urandom = false;
		$handle = null;
		if (function_exists('stream_set_read_buffer') && @is_readable('/dev/urandom'))
		{
			$handle = @fopen('/dev/urandom', 'rb');
			if ($handle)
			{
				$urandom = true;
			}
		}

		while ($length > strlen($randomStr))
		{
			$bytes = ($total > $shaHashLength)? $shaHashLength : $total;
			$total -= $bytes;

			$entropy = rand() . uniqid(mt_rand(), true) . $sslStr;
			$entropy .= implode('', @fstat(fopen( __FILE__, 'r')));
			$entropy .= memory_get_usage();
			$sslStr = '';
			if ($urandom)
			{
				stream_set_read_buffer($handle, 0);
				$entropy .= @fread($handle, $bytes);
			}
			else
			{

				$samples = 3;
				$duration = 0;
				for ($pass = 0; $pass < $samples; ++$pass)
				{
					$microStart = microtime(true) * 1000000;
					$hash = sha1(mt_rand(), true);
					for ($count = 0; $count < 50; ++$count)
					{
						$hash = sha1($hash, true);
					}
					$microEnd = microtime(true) * 1000000;
					$entropy .= $microStart . $microEnd;
					if ($microStart > $microEnd) {
						$microEnd += 1000000;
					}
					$duration += $microEnd - $microStart;
				}
				$duration = $duration / $samples;

				$rounds = (int)(($maxTimeMicro / $duration) * 50);

				$iter = $bytes * (int) ceil(8 / $bitsPerRound);
				for ($pass = 0; $pass < $iter; ++$pass)
				{
					$microStart = microtime(true);
					$hash = sha1(mt_rand(), true);
					for ($count = 0; $count < $rounds; ++$count)
					{
						$hash = sha1($hash, true);
					}
					$entropy .= $microStart . microtime(true);
				}
			}

			$randomStr .= sha1($entropy, true);
		}

		if ($urandom)
		{
			@fclose($handle);
		}

		return substr($randomStr, 0, $length);
	}
function getSalt($encryption = 'md5-hex', $seed = '', $plaintext = '')
	{
		// Encrypt the password.
		switch ($encryption)
		{
			case 'crypt':
			case 'crypt-des':
				if ($seed)
				{
					return substr(preg_replace('|^{crypt}|i', '', $seed), 0, 2);
				}
				else
				{
					return substr(md5(mt_rand()), 0, 2);
				}
				break;

			case 'crypt-md5':
				if ($seed)
				{
					return substr(preg_replace('|^{crypt}|i', '', $seed), 0, 12);
				}
				else
				{
					return '$1$' . substr(md5(mt_rand()), 0, 8) . '$';
				}
				break;

			case 'crypt-blowfish':
				if ($seed)
				{
					return substr(preg_replace('|^{crypt}|i', '', $seed), 0, 16);
				}
				else
				{
					return '$2$' . substr(md5(mt_rand()), 0, 12) . '$';
				}
				break;

			case 'ssha':
				if ($seed)
				{
					return substr(preg_replace('|^{SSHA}|', '', $seed), -20);
				}
				else
				{
					return mhash_keygen_s2k(MHASH_SHA1, $plaintext, substr(pack('h*', md5(mt_rand())), 0, 8), 4);
				}
				break;

			case 'smd5':
				if ($seed)
				{
					return substr(preg_replace('|^{SMD5}|', '', $seed), -16);
				}
				else
				{
					return mhash_keygen_s2k(MHASH_MD5, $plaintext, substr(pack('h*', md5(mt_rand())), 0, 8), 4);
				}
				break;

			case 'aprmd5': /* 64 characters that are valid for APRMD5 passwords. */
				$APRMD5 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

				if ($seed)
				{
					return substr(preg_replace('/^\$apr1\$(.{8}).*/', '\\1', $seed), 0, 8);
				}
				else
				{
					$salt = '';
					for ($i = 0; $i < 8; $i++)
					{
						$salt .= $APRMD5{rand(0, 63)};
					}
					return $salt;
				}
				break;

			default:
				$salt = '';
				if ($seed)
				{
					$salt = $seed;
				}
				return $salt;
				break;
		}
	}
	
	
function getCryptedPassword($plaintext, $salt = '', $encryption = 'md5-hex', $show_encrypt = false)
	{
		// Get the salt to use.
		$salt = getSalt($encryption, $salt, $plaintext);

		// Encrypt the password.
		switch ($encryption)
		{
			case 'plain':
				return $plaintext;

			case 'sha':
				$encrypted = base64_encode(mhash(MHASH_SHA1, $plaintext));
				return ($show_encrypt) ? '{SHA}' . $encrypted : $encrypted;

			case 'crypt':
			case 'crypt-des':
			case 'crypt-md5':
			case 'crypt-blowfish':
				return ($show_encrypt ? '{crypt}' : '') . crypt($plaintext, $salt);

			case 'md5-base64':
				$encrypted = base64_encode(mhash(MHASH_MD5, $plaintext));
				return ($show_encrypt) ? '{MD5}' . $encrypted : $encrypted;

			case 'ssha':
				$encrypted = base64_encode(mhash(MHASH_SHA1, $plaintext . $salt) . $salt);
				return ($show_encrypt) ? '{SSHA}' . $encrypted : $encrypted;

			case 'smd5':
				$encrypted = base64_encode(mhash(MHASH_MD5, $plaintext . $salt) . $salt);
				return ($show_encrypt) ? '{SMD5}' . $encrypted : $encrypted;

			case 'aprmd5':
				$length = strlen($plaintext);
				$context = $plaintext . '$apr1$' . $salt;
				$binary = _bin(md5($plaintext . $salt . $plaintext));

				for ($i = $length; $i > 0; $i -= 16)
				{
					$context .= substr($binary, 0, ($i > 16 ? 16 : $i));
				}
				for ($i = $length; $i > 0; $i >>= 1)
				{
					$context .= ($i & 1) ? chr(0) : $plaintext[0];
				}

				$binary = _bin(md5($context));

				for ($i = 0; $i < 1000; $i++)
				{
					$new = ($i & 1) ? $plaintext : substr($binary, 0, 16);
					if ($i % 3)
					{
						$new .= $salt;
					}
					if ($i % 7)
					{
						$new .= $plaintext;
					}
					$new .= ($i & 1) ? substr($binary, 0, 16) : $plaintext;
					$binary = _bin(md5($new));
				}

				$p = array();
				for ($i = 0; $i < 5; $i++)
				{
					$k = $i + 6;
					$j = $i + 12;
					if ($j == 16)
					{
						$j = 5;
					}
					$p[] = _toAPRMD5((ord($binary[$i]) << 16) | (ord($binary[$k]) << 8) | (ord($binary[$j])), 5);
				}

				return '$apr1$' . $salt . '$' . implode('', $p) . _toAPRMD5(ord($binary[11]), 3);

			case 'md5-hex':
			default:
				$encrypted = ($salt) ? md5($plaintext . $salt) : md5($plaintext);
				return ($show_encrypt) ? '{MD5}' . $encrypted : $encrypted;
		}
	}
	
function shorten($intro, $len) { 
	$string = $intro;
	if (strlen($string) > $len) 
	{
    // truncate string
    $string = $stringCut = substr($string, 0, $len);
    // make sure it ends in a word so assassinate doesn't become ass...
    //$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... '; 
	}
	return $string.'... ';
}

function getUserName($id) {
	global $server;
	
	$result=mysqli_query($server, "select name as value from users WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result); 
	return $row['value'];			
}

function userName($id) {
	global $server;
	
	$result=mysqli_query($server, "select name as value from users WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result); 
	return $row['value'];			
}

function gatewayName($id) {
	global $server;
	
	$result=mysqli_query($server, "select name as value from gateways WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result); 
	return $row['value'];			
}

function paymentGatewayName($id) {
	global $server;
	
	$result=mysqli_query($server, "select name as value from paymentgateways WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result); 
	return $row['value'];			
}

function gatewayData($id,$field) {
	global $server;
	
	$result=mysqli_query($server, "select ".$field." as value from paymentgateways WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result); 
	return $row['value'];			
}

function getLastLogin($id) {
	global $server;
	
	$result=mysqli_query($server, "select last_login as value from users WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result); 
	return $row['value'];			
}

function getRoleName($id) {
	global $server;
	$result=mysqli_query($server, "select is_admin as value from users WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result); 
	$role = $row['value'];	
	if($role > 0) {
		return 'Administrator';
	} else {
		return 'User';	
	}
}

function isAdmin($id) {
	global $server;
	$result=mysqli_query($server, "select is_customer as value from users WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result); 
	$role = $row['value'];	
	if($role > 0) {
		return false;
	} else {
		return true;	
	}
}

function activeApi() {
    
	global $server;
	
	$result=mysqli_query($server, "select * from apis WHERE status='1'");
	$row = mysqli_fetch_assoc($result); 
	$api = $row['api_name'];	
	
	return $api;
}

function listName($id) {
	global $server;
	$result=mysqli_query($server, "select name as value from marketinglists WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result); 
	return $role = $row['value'];	
}

function countInbox() {
	global $server;
	
	$ch=mysqli_query($server, "select * from inbox where is_read = '0'");
	return mysqli_num_rows($ch);		
}

function countWithdrawal($id) {
	global $server;
	
	$ch=mysqli_query($server, "select * from withdrawal_logs where user_id = '$id'");
	return mysqli_num_rows($ch);		
}

function countPWithdrawal($id) {
	global $server;
	
	$ch=mysqli_query($server, "select * from withdrawal_logs where user_id = '$id' AND status='0' OR status='2'");
	return mysqli_num_rows($ch);		
}

function countCampaigns($customer=0) {
	global $server;
	
	$ch=mysqli_query($server, "select * from campaigns");
	if($customer > 0) {
		$ch=mysqli_query($server, "select * from campaigns where customer_id = '$customer'");
	}
	return mysqli_num_rows($ch);		
}
function countMarketingList($customer=0) {
	global $server;
	
	$ch=mysqli_query($server, "select * from marketinglists");
	if($customer > 0) {
		$ch=mysqli_query($server, "select * from marketinglists where customer_id = '$customer'");
	}
	return mysqli_num_rows($ch);		
}
function countSubscribers($contact=0,$customer=0) {
	global $server;
	
	$ch=mysqli_query($server, "select distinct(phone) from contacts");
	if($contact > 0) {	
		$ch=mysqli_query($server, "select * from contacts where marketinglist_id = '$contact'");
	}
	if($customer > 0) {	
		$ch=mysqli_query($server, "select distinct(phone) from contacts c join marketinglists m on c.marketinglist_id = m.id where m.customer_id = '$customer'");
	}
	
	return mysqli_num_rows($ch);		
}
function getSetting($field) {
	global $server;
	$query="SELECT * FROM settings WHERE field = '$field'"; 
	$result = mysqli_query($server,$query) or die(mysql_error($server));  
	$row = mysqli_fetch_assoc($result); 
	$value = $row['value'];	
	if(empty($value)) {
	return false;
	} else {
	return $value;
	}
}

function countDateSubscribers($date,$client=0) {
	global $server;
	
	$ch=mysqli_query($server, "select * from contacts where date like '%$date%'");
	if($client > 0) {
		$ch=mysqli_query($server, "select * from contacts c join marketinglists m on c.marketinglist_id = m.id where c.date like '%$date%' and m.customer_id = '$client'") ;
	}
	return mysqli_num_rows($ch);		
}
function reduceDate($date,$number) {
	if($number < 1) {
		$number = 1;	
	}
	$seconds = $number*60*60*24;
	$date = strtotime($date)-$seconds;
	return date('Y-m-d', $date);
}
function increaseDate($date,$number) {
	if($number < 1) {
		$number = 1;	
	}
	$seconds = $number*60*60*24;
	$date = strtotime($date)+$seconds;
	return date('Y-m-d', $date);
}
function addMinutes($date,$number) {
	 $minutes_to_add = $number; 
	 $newtimestamp = strtotime($date) + ($number*60);
	date('Y-m-d H:i:s', $newtimestamp); 
	
return 	date('Y-m-d H:i:s', $newtimestamp);
}
function countPeriodSubscribers($period) {
	global $server;
	$begin = date('Y-m-d');
	 $end = reduceDate($begin,$period); 
	$ch=mysqli_query($server, "select count(id) as totalh from contacts where (date BETWEEN '$end' AND '$begin')");
	$rows = mysqli_fetch_assoc($ch);
	return $rows['totalh'];
}
function keywordAvailable($keyword,$code,$list=0) {
	global $server;
	if(empty($code)) {
		$code = getSetting('defaultShortCode');	
	}
	$ch=mysqli_query($server, "select * from marketinglists where (optin_keyword = '$keyword' OR optout_keyword = '$keyword' OR help_keyword = '$keyword') AND short_code = '$code'");
	$rows = mysqli_fetch_assoc($ch);
	$found = $rows['id'];
	if($found > 0 || $found != $list) {
		return false;
	} else {
		return true;
	}
}
function userAvailable($username,$id=0) {
	global $server;
	$ch=mysqli_query($server, "select * from users where username = '$username'");
	$rows = mysqli_fetch_assoc($ch);
	$found = $rows['id'];
	if($found > 0 || $found != $id) {
		return false;
	} else {
		return true;
	}
}
function phoneBlacklisted($phone) {
	global $server;
	$ch=mysqli_query($server, "select * from blacklists where phone = '$phone'");
	$rows = mysqli_fetch_assoc($ch);
	$found = $rows['id'];
	if($found > 0) {
		return true;
	} else {
		return false;
	}
}

function successRate($campaign=0,$customer=0) {
	global $server;
	
	$condition = '';
	if($campaign > 0) {
		$condition = " and campaign_id = '$campaign'";	
	}
	$ch=mysqli_query($server, "select * from sentmessages where status like '%%' ".$condition);
	$total = mysqli_num_rows($ch);
	$ch=mysqli_query($server, "select * from sentmessages where status = 'Failed' ".$condition);
	$totalFailed = mysqli_num_rows($ch);		
	$success = $total - $totalFailed;
	return @$rate = round(($success/$total)*100, 2);
}

function countSent($campaign=0) {
	global $server;
	
	$ch=mysqli_query($server, "select * from sentmessages where status = 'Sent'");
	if($campaign > 0) {
	$ch=mysqli_query($server, "select * from sentmessages where status = 'Sent' and campaign_id = '$campaign'");
	}
	$total = mysqli_num_rows($ch);
	return $total;
}

function countFailed($campaign=0) {
	global $server;
	
	$ch=mysqli_query($server, "select * from sentmessages where status = 'Failed'");
	if($campaign > 0) {
	$ch=mysqli_query($server, "select * from sentmessages where status = 'Failed' and campaign_id = '$campaign'");
	}
	$total = mysqli_num_rows($ch);
	return $total;
}

function campaignProgress($campaign) {
	global $server;
	
	$ch=mysqli_query($server, "select * from jobs where campaign_id = '$campaign'");
	$total = mysqli_num_rows($ch);
	$ch=mysqli_query($server, "select * from jobs where campaign_id = '$campaign' and status = 'Completed'");
	$totalSuccess = mysqli_num_rows($ch);		
	return $rate = @round(($totalSuccess/$total)*100);
}
function campaignStatus($campaign) {
	$progress = campaignProgress($campaign);	
	if($progress < 1) {
		$status = 'Pending';
	}
	if($progress > 0) {
		$status = 'In-Progress';	
	}
	if($progress >= 100) {
		$status = 'Completed';	
	}
}
function getMonthDays($month) {
	$days = 31;
	if($month == '02') {
		$days = 29;	
	}
	if($month == '04' || $month == '06' || $month == '09' || $month == '11') {
		$days = 30;	
	}
	return $days;
}
function getInterval($minutes) {
	$days = 'Every Hour';
	if($minutes > '60') {
		$days = $minutes/60;
		$days = 'Every '.$days.' Hours';	
	}
	if($minutes == '1440') {
		$days = $minutes/(60*24);
		$days = 'Once Daily';	
	}
	if($minutes >= '2880') {
		$days = $minutes/(60*24);
		$days = 'Every '.$days.' Days';	
	}	
	return $days;
}

function getCycle($minutes) {
	$days = 'Once';
	if($minutes > '1') {
		$days = $minutes.' Times';	
	}
	return $days;
}function countCommas($value) {
	$value = explode(',', $value);
	return count($value);
}

function alterPhone($gsm,$code) {
	$code = str_replace('+', '', $code);
	$array = is_array($gsm);
	$gsm = ($array) ? $gsm : explode(",",$gsm);
	$homeCountry = $code;
	$outArray = array();
	foreach($gsm as $item)
	{
		if(!empty($item)){
			$item1 = (string)$item;
			$q = substr($item1,0,1);
			$w = substr($item1,0,3);
			$item1 = (substr($item1,0,1) == "+") ? substr($item1,1) : $item1;
			$item1 = (substr($item1,0,3) == "009") ? $homeCountry.substr($item1,3): $item1;
			$item1 = (substr($item1,0,1) == "0") ? $homeCountry.substr($item1,1): $item1;
			$item1 = strlen($item1) == 9 ? $homeCountry.$item1: $item1;
		$item1 = (substr($item1,0,strlen($homeCountry)) == $homeCountry) ? $homeCountry.substr($item1,strlen($homeCountry)): $item1;
		$outArray[] = $item1;
		}
	}
	return ($array) ? $outArray : implode(",",$outArray);
}

function removeDuplicate($myArray) {
	$array = is_array($myArray);
	$myArray = ($array) ? $myArray: explode(",",$myArray);
	$myArray = array_flip(array_flip(array_reverse($myArray,true)));
	return ($array) ? $myArray : implode(',',$myArray);
}
function getNumberRoute($phone,$type) {
global $server;	
	$phone = strtr ($phone, array ('+' => ''));	
	$sql = "SELECT * FROM routing WHERE type = '$type'";
	$result = mysqli_query($server, $sql) or die(mysqli_error($server));
	$num = mysqli_num_rows($result);	
	$gateway = 0;
	while ($row = mysqli_fetch_assoc($result)) {
		$id = $row['gateway_id'];
	    $destinations = explode(",", $row['destinations']);
		for($j=0; $j < count($destinations); $j++){
			$len = strlen($destinations[$j]); 
			if(substr($phone,0,$len) == $destinations[$j] && $len > 0 && !empty($destinations[$j])){
				$gateway = $id; 
				break;
			} else {
			$gateway  = 0;	
			}

		} 	
		if($gateway > 0) { break; }
	}
	if($gateway < 1) {
		if($type == 'Text' || $type == 'SMS') {
			$gateway  = getSetting('smsGateway');	
		} else {
			$gateway  = getSetting('voiceGateway');
		}
	}	
	return $gateway;	
}
function setGateway($id,$value) {
	global $server;
	$query="SELECT * FROM gateways WHERE id = '$id'"; 
	$result = mysqli_query($server, $query) or die(mysqli_error($server));  
	$row = mysqli_fetch_assoc($result); 
	$value = $row[$value];	
	if(empty($value)) {
	return '';
	} else {
	return $value;
	}
}

function isSystemGateway($id) {
	if($id == 1 || $id == 2) {
	return true;
	} else {
	return false;
	}
}

function checkKeyword($field='id',$message,$to='') {
global $server;	
$return = false;
$message = strtoupper($message);

	$sql = "SELECT * FROM marketinglists ORDER BY id DESC";
	$result = mysqli_query($server,$sql);
	$num = mysqli_num_rows($result);	
	while ($row = mysqli_fetch_assoc($result)) {
		$id = $row['id'];
		$keyword1 = strtoupper($row['optin_keyword']);
		$keyword2 = strtoupper($row['optout_keyword']);
		$keyword3 = strtoupper($row['help_keyword']);
		$code = $row['short_code'];
		if(empty($code)) {
			$code = getSetting('defaultShortCode');	
		}
		//$field = $row[$field];
		if( ($code == $to) && (strpos($message,$keyword1) !== false || strpos($message,$keyword2) !== false || strpos($message,$keyword3) !== false) ) {
			$return = $id;
		}
	}
	
return $return;	
}
function getInboxClient($message, $sender='') {
	global $server;
	$customer = 0;
	$sql = "SELECT * FROM users WHERE short_code LIKE '%$sender%'";
	$result = mysqli_query($server,$sql);
	$num = mysqli_num_rows($result);	
	if(!empty($sender) && $num == 1) { 
		$row = mysqli_fetch_assoc($result);
		$customer = $row['id'];
	} else {
		$keyword = '';
		$keyword1 = checkKeyword('optin_keyword',$message);
		$keyword2 = checkKeyword('optout_keyword',$message);
		$keyword3 = checkKeyword('help_keyword',$message);
		$keyword = $keyword1.$keyword2.$keyword3;
		$sql = "SELECT * FROM marketinglists WHERE optin_keyword = '$keyword1' OR 'optout_keyword' = '$keyword2' OR 'help_keyword' = '$keyword3'";
		$result = mysqli_query($server,$sql);
		$num = mysqli_num_rows($result);	
		if(!empty($keyword) && $num > 0) { 
			$row = mysql_fetch_assoc($result);
			$customer = $row['customer_id'];
		}else { 
			$sql = "SELECT * FROM marketinglists WHERE short_code LIKE '%$sender%' LIMIT 1";
			$result = mysqli_query($server,$sql);
			$num = mysqli_num_rows($result);	
			if(!empty($sender) && $num > 0) { 
				$row = mysqli_fetch_assoc($result);
				$customer = $row['customer_id'];
			} 		
		}
	}
return $customer;	
}
function cronStatus($date) {
	$last = getSetting('lastCron');
	$last = date('Y-m-d', strtotime($last));
	if(strtotime($date) > strtotime($last)) {
		return '<span class="badge bg-red">Not Running</span>';
	} else {
		return '<span class="badge bg-green">Runing OK</span>';	
	}
}

function getInsertedID($table) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM $table ORDER BY id DESC LIMIT 1"); 
	$row = mysqli_fetch_assoc($result);
	$id = $row['id'];	
	return $id;	
}

function getUserBalance($id) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM users where id = '$id'"); 
	$row = mysqli_fetch_assoc($result);
	$id = $row['balance'];	
	return $id;		
}

function getUserBalanceAll($id) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM users where id = '$id'"); 
	$row = mysqli_fetch_assoc($result);
	return $row;		
}

function packageName($id) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM packages where id = '$id'"); 
	$row = mysqli_fetch_assoc($result);
	$id = $row['name'];	
	return $id;		
}

function virtual_numbers($id) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM virtual_numbers where id = '$id'"); 
	$row = mysqli_fetch_assoc($result);
	$id = $row;	
	return $id;		
}

function cell_numbers($id) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM cell_numbers where id = '$id'"); 
	$row = mysqli_fetch_assoc($result);
	$id = $row;	
	return $id;		
}

function packageQuantity($id) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM packages where id = '$id'"); 
	$row = mysqli_fetch_assoc($result);
	$id = $row['quantity'];	
	return $id;		
}
function packageCost($id) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM packages where id = '$id'"); 
	$row = mysqli_fetch_assoc($result);
	$id = $row['cost'];	
	return $id;		
}
function currencyName($id) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM currencies where id = '$id'"); 
	$row = mysqli_fetch_assoc($result);
	$id = $row['name'];	
	return $id;		
}
function currencySymbul($id) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM currencies where id = '$id'"); 
	$row = mysqli_fetch_assoc($result);
	$id = $row['symbul'];	
	return $id;		
}
function currencyRate($id) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM currencies where id = '$id'"); 
	$row = mysqli_fetch_assoc($result);
	$id = $row['rate'];	
	return $id;		
}

function countReferals($id){
    global $server;
    $ch=mysqli_query($server, "select * from users  WHERE reffered_by='".$id."'");
	return mysqli_num_rows($ch);
}

function userData($field, $user) {
	global $server;
	$result = mysqli_query($server, "SELECT * FROM users where id = '$user'"); 
	$row = mysqli_fetch_assoc($result);
	$id = $row[$field];	
	return $id;		
}
function mceil($number) {
	$array = explode(".",$number);
	$deci = ((int)$array[1] > 0) ? 1 : 0;
	return (int)$array[0] + $deci;
}
function messageLength($textMessage) {
	$test_Len = strlen($textMessage);
	$lines = substr_count( $textMessage, "\n" );
	$messageLength = $test_Len - $lines;
return $messageLength;	
}
function removeAllSpace($text) {
	return $string = preg_replace('/\s+/', '', $text);;	
}
function stripNonNumeric($test) {
	return preg_replace("/[^0-9,.]/", "",$test);		
}
function getCost($textMessage,$duration,$recipientList,$type,$customer=0) {
	global $server;
	if($type == 'Voice'){
		$requiredCredit = 0;
		$messageLenght = $duration;
		$smsLength = "30";		
		$pricelist =  getSetting('voiceCost');
		if($customer > 0) {
			$userPricelist  = userData('custom_voice_rate',$customer);
			if(!empty($userPricelist)) {
				$pricelist = $userPricelist;	
			}
		}
		$searchKeys = array("\r\n","<br>","\n","\r");
		$replaceKeys = array(",",",",",",",");
		$pricelist = str_replace( $searchKeys , $replaceKeys , $pricelist);
		$pricelist = explode(',',$pricelist);
		$countryPrices = array();
		for($i=0; $i<count($pricelist); $i++) { 
			$countryPrices[] = explode('=',$pricelist[$i]);
		}		
		$priceList = $countryPrices; 		
		$credit = array(); 		$credito = 0;      $recipientList = explode(",", $recipientList);
		
		for($i=0; $i < count($recipientList); $i++){			
			$unitcost=getSetting('voiceDefaultCost');
			for($j=0; $j < count($priceList); $j++){
				$len = strlen($priceList[$j][0]); 
								
				if(substr(stripNonNumeric($recipientList[$i]),0,$len) == $priceList[$j][0] && $len > 0 && !empty($priceList[$j][0])){
					$unitcost=$priceList[$j][1]; 
					break;
				}
			} 
		
			$requiredCredit2 = ($messageLenght <= $smsLength) ? mceil($messageLenght/$smsLength)*$unitcost : mceil($messageLenght/$smsLength)*$unitcost ;   
			$requiredCredit = $requiredCredit + $requiredCredit2;
		} //end for si
		return $requiredCredit;		
	} else {
		$requiredCredit = 0;
		$messageLenght = messageLength($textMessage);
		$smsLength = "160";
		if(messageLength($textMessage) > 160){
			$smsLength = 145;
		}
	
		$pricelist =  getSetting('smsCost');
		if($customer > 0) {
			$userPricelist  = userData('custom_sms_rate',$customer);
			if(!empty($userPricelist)) {
				$pricelist = $userPricelist;	
			}
		}
		$searchKeys = array("\r\n","<br>","\n","\r");
		$replaceKeys = array(",",",",",",",");
		$pricelist = str_replace( $searchKeys , $replaceKeys , $pricelist);
		$pricelist = explode(',',$pricelist);
		$countryPrices = array();
		for($i=0; $i<count($pricelist); $i++) { 
			$countryPrices[] = explode('=',$pricelist[$i]);
		}
		
		$priceList = $countryPrices; 
		
		$credit = array();      
		$credito = 0;		
		$recipientList = explode(",", $recipientList);
		
		for($i=0; $i < count($recipientList); $i++){			
			//get the sms unit to use for each number
			$unitcost=getSetting('smsDefaultCost');
			for($j=0; $j < count($priceList); $j++){
				$len = strlen($priceList[$j][0]); 
							
				if(substr(stripNonNumeric($recipientList[$i]),0,$len) == $priceList[$j][0] && $len > 0 && !empty($priceList[$j][0])){
					$unitcost=$priceList[$j][1]; 
					break;
				}
			} 
							
			$requiredCredit2 = ($messageLenght <= $smsLength) ? mceil($messageLenght/$smsLength)*$unitcost : mceil($messageLenght/$smsLength)*$unitcost ;   
			$requiredCredit = $requiredCredit + $requiredCredit2;
		} //end for si
		return $requiredCredit;		
	}
}

function sendMessage($senderID,$recipientList,$textMessage,$unicode=0,$duration=0,$media='',$language='',$type='Text',$saved=0,$client=0) {
	global $server; 
	//replace dynamic firlds 
	$textMessage = str_replace("[", '%', $textMessage);
	$textMessage = str_replace("]", '%', $textMessage);
	$textMessage = str_replace("%NUMBER%", $recipientList, $textMessage);
	$recipientList = str_replace('+', '', $recipientList);
	
	$active=activeApi();
	
	     $textMessage=urlencode($textMessage.' '.sms_signature());
        if($active=='global'){
            //global
            
            $global="http://148.251.196.36/app/smsapi/index.php?key=1XfUGDAM&type=text&contacts=$recipientList&senderid=$senderID&msg=$textMessage";
            
            $redffd=url_get_contents($global);
            $samunjo=!empty(explode(',',$redffd)[1]) ? explode(',',$redffd)[1] : '';
            
            $saved=1;
            $return = 'Message Sent Successfully';
            
        }else/*if($active=='bulk')*/{
            //bulk
            
            $messages = array(
              array('to'=>$recipientList, 'body'=>$textMessage)
            );
    
            $result = send_message_bulk_sms( json_encode($messages) );
    
            if ($result['http_status'] != 201) {
                $return = 'Message sending failed';
            } else {
              $return = 'Message Sent Successfully';
            }
    
        } 
        /*}else{
            //zoom
        }*/
      
	 
	if($saved != 1) { 	
		$date = date('Y-m-d H:i:s');
		$error = str_replace("'",'',$error);
		$samunjo=!empty($samunjo) ? $samunjo : '';
		$add = mysqli_query($server,"INSERT INTO sentmessages (`id`, `campaign_id`, `job_id`, `recipient`, `status`, `gateway_id`, `error`, `date`,`message`, `customer_id`,`m_sent_id`) 
		VALUES (NULL, '0', '0', '$recipientList', '$status', '$activeGateway', '$error', '$date', '$textMessage', '$client','$samunjo');") or die(mysqli_error($server));	
	}
	return $return; 
}

function sms_signature(){
    global $server;
    $ch=mysqli_query($server, "select * from sms_signature");
    $status=0;$signature="";
  	while ($row = mysqli_fetch_assoc($ch)) {
  	    $status=$row['status'];
  	    $signature=$row['signature'];
  	}
      	
    if($status==0){
      return ' \n'.$signature;
    }else{
        return ' ';
    }
}

function processJob($id) {
	global $server;
	//fetch message
	$sql = "SELECT * FROM jobs WHERE id = '$id'";
	$result = mysqli_query($server, $sql) or die(mysqli_error($server));
	$row = mysqli_fetch_assoc($result);
	
	$campaign_id = $row['campaign_id'];
	
	$error = 0;
	$now = date("Y-m-d H:i:s");
	mysqli_query($server,"UPDATE `jobs` SET 
	`status` = 'Processing'
	 WHERE `id` = '$id'");

	$sql = "SELECT * FROM campaigns WHERE id = '$campaign_id'";
	$result = mysqli_query($server, $sql) or die(mysqli_error($server));
	$row = mysqli_fetch_assoc($result);
	$type = $row['type'];		 
	$senderID = $row['sender_id'];
	$textMessage = $row['message'];
	$toNumber = $recipientList = removeDuplicate($row['recipient']);
	$duration = $row['duration'];
	$language = $row['language'];
	$media = $row['file'];
	$client = $row['customer_id'];
	
	$batchSize = 1;
	$currentList = $recipientcount = explode(',',$recipientList);
	
	
	$sendingResponse2 = 'Failed';
	if($error < 1) {	
		if(count($recipientcount) > $batchSize){			
			foreach($currentList as $to) {
			    
			    if(empty($to)) { continue; }
			    
				$status = 'Sending';
				$campaign_id = $campaign_id;
				$job_id = $id;
				$gateway = getNumberRoute($to,$type);
				$nedOn = date('Y-m-d H:i:s');
				$textMessage = str_replace("[", '%', $textMessage);
				$textMessage = str_replace("]", '%', $textMessage);
				$textMessage = str_replace("%NUMBER%", $to, $textMessage);		
				$required = getCost($textMessage,$duration,$to,$type);		
				
				$add = mysqli_query($server,"INSERT INTO sentmessages (`id`, `campaign_id`, `job_id`, `recipient`, `status`, `gateway_id`, `error`, `date`,`message`, `customer_id`,`cost`) 
				VALUES (NULL, '$campaign_id', '$job_id', '$to', '$status', '$gateway', '', '$nedOn', '$textMessage', '$client','$required');");
				$mesd_id  =  getInsertedID('sentmessages');
	
				//Check Balance
				$has_balance = 1;
				if($client > 0) {
					$balance = getUserBalance($client);
					$required = getCost($textMessage,$duration,$to,$type);
					if($required > $balance) {
						$has_balance = 0;	
					}
				}	
				
				if($has_balance < 1) {
					$sendingResponse = 'Insufficient Credit';
					$status = 'Failed';
				} elseif(phoneBlacklisted($to)) {
					$sendingResponse = 'Recipient Blacklisted';
					$status = 'Failed';
				} else {
					$sendingResponse = sendMessage($senderID,$to,$textMessage,0,$duration,$media,$language,$type,1);
				}
				if(strpos($sendingResponse,'Message Sent Successfully') !== false) {
					mysqli_query($server,"UPDATE `sentmessages` SET `error` = '' WHERE `id` = '$mesd_id'");
					mysqli_query($server,"UPDATE `sentmessages` SET `status` = 'Sent' WHERE `id` = '$mesd_id'");
					//bill user
					if($client > 0) {
						$balance = getUserBalance($client);
						$newBalance = ($balance - $required) > 0 ? ($balance - $required) : 0;
						mysqli_query($server,"UPDATE `users` SET `balance` = '$newBalance' WHERE `id` = '$client'");	
					}
				} else {
					$partial_sent = 1;	
					$sendingResponse2 = $sendingResponse = str_replace('\'',',',$sendingResponse);; 
					mysqli_query($server,"UPDATE `sentmessages` SET `status` = 'Failed' WHERE `id` = '$mesd_id'");
					mysqli_query($server,"UPDATE `sentmessages` SET `error` = '$sendingResponse' WHERE `id` = '$mesd_id'");
				}
			} 
		} else { 
			$status = 'Sending';
			$campaign_id = $campaign_id;
			$job_id = $id;
			$gateway = getNumberRoute($toNumber,$type);
			$nedOn = date('Y-m-d H:i:s');
			$textMessage = str_replace("[", '%', $textMessage);
			$textMessage = str_replace("]", '%', $textMessage);
			$textMessage = str_replace("%NUMBER%", $toNumber, $textMessage);
			//$required = getCost($textMessage,$duration,$toNumber,$type);		
			$add = mysqli_query($server,"INSERT INTO sentmessages (`id`, `campaign_id`, `job_id`, `recipient`, `status`, `gateway_id`, `error`, `date`,`message`, `customer_id`,`cost`) 	VALUES (NULL, '$campaign_id', '$job_id', '$toNumber', '$status', '$gateway', '', '$nedOn', '$textMessage', '$client','$required');");
			$mesd_id  =  getInsertedID('sentmessages');
			//Check Balance
			$has_balance = 1;
			if($client > 0) {
				$balance = getUserBalance($client);
				$required = getCost($textMessage,$duration,$toNumber,$type);
				if($balance < 0) {
					$has_balance = 0;	
				}					
			}	
				
			if($has_balance <= 0) {
				$sendingResponse = 'Insufficient Credit';
				$status = 'Failed';
			} elseif(phoneBlacklisted($toNumber)) {
				$sendingResponse = 'Recipient Blacklisted';
				$status = 'Failed';
			} else {
				$sendingResponse = sendMessage($senderID,$toNumber,$textMessage,0,$duration,$media,$language,$type,1);
			}
			
			if(strpos($sendingResponse,'Message Sent Successfully') !== false) {
				mysqli_query($server,"UPDATE `sentmessages` SET `error` = '' WHERE `id` = '$mesd_id'");
				mysqli_query($server,"UPDATE `sentmessages` SET `status` = 'Sent' WHERE `id` = '$mesd_id'");
				//bill user
				if($client > 0) {
					$balance = getUserBalance($client);
					$newBalance = ($balance - $required) > 0 ? ($balance - $required) : 0;
					mysqli_query($server,"UPDATE `users` SET `balance` = '$newBalance' WHERE `id` = '$client'");	
				}
			} else {
				$partial_sent = 1;	
				$sendingResponse2 = $sendingResponse = str_replace('\'',',',$sendingResponse);
				mysqli_query($server,"UPDATE `sentmessages` SET `status` = 'Failed' WHERE `id` = '$mesd_id'");
				mysqli_query($server,"UPDATE `sentmessages` SET `error` = '$sendingResponse' WHERE `id` = '$mesd_id'");
			}
		}
		
		if($partial_sent > 0) {
			$error = 'Notice: Some messages were not sent. See Sent Message detail for more';
		} else {
			$error = 'Message Sent Successfully';
		}	
		//update status
		mysqli_query($server,"UPDATE `jobs` SET 
		`status` = 'Completed'
		 WHERE `id` = '$id'");
	}	
if($partial_sent > 0) {
	$error = $sendingResponse2;	
}

return $error;	
}

function setTimeZone() {
global $server;
	$serverTimeZone = getSetting('timeZone');
	date_default_timezone_set($serverTimeZone);
	$timzn = date_default_timezone_get();	
}

function backup_tables($type,$tables = '*'){
	global $server;
	$return = '';
	
	//get all of the tables
	if($tables == '*')
	{
		$tables = array();
		$result = mysqli_query($server,'SHOW TABLES');
		while($row = mysqli_fetch_row($result))
		{
			$tables[] = $row[0];
		}
	}
	else
	{
		$tables = is_array($tables) ? $tables : explode(',',$tables);
	}
	
	//cycle through
	foreach($tables as $table)
	{
		$result = mysqli_query($server,'SELECT * FROM '.$table);
		$num_fields = mysqli_num_fields($result);
		
		$return.= 'DROP TABLE '.$table.';';
		$row2 = mysqli_fetch_row(mysqli_query($server,'SHOW CREATE TABLE '.$table));
		$return.= "\n\n".$row2[1].";\n\n";
		
		for ($i = 0; $i < $num_fields; $i++) 
		{
			while($row = mysqli_fetch_row($result))
			{
				$return.= 'INSERT INTO '.$table.' VALUES(';
				for($j=0; $j<$num_fields; $j++) 
				{
					$row[$j] = addslashes($row[$j]);
					$row[$j] = @ereg_replace("\n","\\n",$row[$j]);
					if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
					if ($j<($num_fields-1)) { $return.= ','; }
				}
				$return.= ");\n";
			}
		}
		$return.="\n\n\n";
	}
	
	//save file
	$database_backup = 'backups/mojasms_db_backup'.time().'.sql';
	$handle = fopen($database_backup,'w+');
	fwrite($handle,$return);
	fclose($handle);
	
	//insert to record
	$filename = 'mojasms_db_backup'.time().'.sql';
	$date = date('Y-m-d H:i:s');

	$add = mysqli_query($server,"INSERT INTO backup (`id`, `date`, `type`, `url`) 
			VALUES (NULL, '$date', '$type', '$filename');") or die(mysql_error());
	return $filename;
}


function restore_tables($filename){
	global $server;

	$templine = '';
	// Read in entire file
	$lines = file($filename);
	// Loop through each line
	foreach ($lines as $line){
		if (substr($line, 0, 2) == '--' || $line == '')
			continue;
	
	$templine .= $line;
		if (substr(trim($line), -1, 1) == ';')	{
			mysqli_query($server,$templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysqli_error($server) . '<br /><br />');
			$templine = '';
		}
	}
}

function processTransaction($referrence) {
	global $server;
	
	$type="";
	
	
	$result = mysqli_query($server, "SELECT * FROM transactions WHERE reference = '$referrence'"); 
	$row = mysqli_fetch_assoc($result);
	$status = $row['status'];	
		
	if($status != 'Completed') {
	mysqli_query($server,"UPDATE `transactions` SET `status` =  'Completed' WHERE  `reference` = '$referrence';");	
	
	$result = mysqli_query($server,"SELECT * FROM transactions WHERE reference = '$referrence'"); 
	$row = mysqli_fetch_assoc($result);
	$customer = $row['customer_id'];
	$package = $row['package_id'];
	
	$type=!empty($row['virtual_number_id']) ? 'virtual' : '';
	$type=!empty($row['cellphone_id']) ? 'cell' : '';
	$amount=$row['amount'];
	// earned Ipoint = 1/100R 1R =100c 
	//$point=$amount/100;
	$rowwww=$row;
	
    
    if(empty($type)){

	$result = mysqli_query($server,"SELECT * FROM packages WHERE id = '$package'"); 
	$row = mysqli_fetch_assoc($result);
	$units = $rowwww['package_qty'];
	
	$result = mysqli_query($server,"SELECT * FROM users WHERE id = '$customer'"); 
	$row = mysqli_fetch_assoc($result);
	$smsBalance = $row['balance'];
	$username = $row['username'];
	$email = $row['email'];
	$phone = $row['phone'];	
	$newBalance = 0;
	
	if($rowwww['package_type']=='sms'){
    	$newBalance = $units+$row['balance'];$typp="SMS";
    	mysqli_query($server,"UPDATE `users` SET `balance` =  '$newBalance' WHERE  `id` = '$customer';");
	}else if($rowwww['package_type']=='mms'){
	    $newBalance = $units+$row['mms_balance'];$typp="MMS";
    	mysqli_query($server,"UPDATE `users` SET `mms_balance` =  '$newBalance' WHERE  `id` = '$customer';");
	}else if($rowwww['package_type']=='vms'){
	    $newBalance = $units+$row['vms_balance'];$typp="VMS";
    	mysqli_query($server,"UPDATE `users` SET `vms_balance` =  '$newBalance' WHERE  `id` = '$customer';");
	}
	
	$referrer_by=userData('reffered_by', $customer);
    if(!empty($referrer_by)){
        // 	save to logs
        $date=date('Y-m-d H:i:s');
        $sql="INSERT INTO `refferal_logs`(`user_id`, `refferal_id`, `earned`, `date`) VALUES ('$referrer_by','$customer','$units','$date')";
        mysqli_query($server,$sql);
        $rsn = mysqli_query($server,"SELECT * FROM users WHERE id = '$referrer_by'"); 
    	$rn = mysqli_fetch_assoc($rsn);
    	$refferal_balance = $rn['refferal_balance']+$units;
        mysqli_query($server,"UPDATE `users` SET `refferal_balance` =  '$refferal_balance' WHERE `id` = '$referrer_by'");
    }

	$orderApproveSME = getSetting('newOrderSMS');
	$orderApproveEmail = getSetting('newOrderEmail');
	$orderApproveEmailSubject = getSetting('newOrderEmailSubject');
	$smsSender = getSetting('smsSender');
	$emailSender = getSetting('businessName');
	$emailFrom = getSetting('emailSender');

		if(!empty($typp)) {
// 			$mail = str_replace('[USERNAME]', $username, $orderApproveSMS);	
// 			$mail = str_replace('[BALANCE]', $newBalance, $orderApproveSMS);
// 			$mail = str_replace('[PACKAGENAME]', $package, $orderApproveSMS);
// 			$mail = str_replace('[UNITS]', $units, $orderApproveSMS);	
// 			$mail = strtr ($orderApproveSMS, array ('[UNITS]' => $units,'[BALANCE]' => $newBalance,'[USERNAME]' => $username));

            $mail="Hello ".$username;
            $mail.="New ".$typp." Balance is ".$newBalance;
            $mail.=" After purchase of ".$units." units on package ".$package;
            
			$response1 = sendMessage($smsSender,$phone,$mail,'0','Admin',$IP);		
		}

		if(!empty($typp)) {
// 			$mail = str_replace('[USERNAME]', $username, $orderApproveEmail);	
// 			$mail = str_replace('[BALANCE]', $newBalance, $orderApproveEmail);
// 			$mail = str_replace('[PACKAGENAME]', $package, $orderApproveSMS);
// 			$mail = str_replace('[UNITS]', $units, $orderApproveEmail);
// 			$mail = strtr ($orderApproveEmail, array ('[UNITS]' => $units,'[BALANCE]' => $newBalance,'[USERNAME]' => $username));													


            $mail="Hello ".$username;
            $mail.="New ".$typp." Balance is ".$newBalance;
            $mail.=" After purchase of ".$units." units on package ".$package;
            
			sendEmail($emailFrom,$emailSender,'Purchased MOJASMS Package',$email,$mail);
		}
		
      }else if($type=='cell'){ // cell numbers
        
          $ch=mysqli_query($server, "select * from cell_number_download WHERE bought is NULL order by id asc");
          
          $gh=mysqli_query($server, "select * from cell_numbers WHERE id='".$rowwww['cellphone_id']."' order by id asc");
          $rowm = mysqli_fetch_assoc($gh);
          
          if( mysqli_num_rows($ch) < $rowm['number'] ){
              mysqli_query($server, "Update cell_number_download SET bought=NULL");
          }
          
          $ch1=mysqli_query($server, "select * from cell_number_download WHERE bought is NULL order by id asc LIMIT ".$rowm['number']);
          
          while ($rows = mysqli_fetch_assoc($ch1)) {
          
            $date=date('Y-m-d');
            mysqli_query($server, "Update cell_number_download SET bought='1' WHERE cell_number='".$rows['cell_number']."'");
            mysqli_query($server,"INSERT INTO `purchased_cell_numbers` (`id`, `number`, `transaction_id`, `date`) VALUES (NULL, '".$rows['cell_number']."', '".$rowwww['id']."', '".$date."')");
          
          }
          
          
      }else{
          
          // virtual numbers
          
      }
     
      
	}
}


/* End of file Common.php */
/* Location: ./system/core/Common.php */ 

function url_get_contents ($Url) {
    if (!function_exists('curl_init')){ 
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

/*$messages = array(
  array('to'=>'+1233454567', 'body'=>'Hello World!'),
  array('to'=>'+1233454568', 'body'=>'Hello World!')
);*/

//$result = send_message( json_encode($messages), '', $username, $password );

/*if ($result['http_status'] != 201) {
  print "Error sending: " . ($result['error'] ? $result['error'] : "HTTP status ".$result['http_status']."; Response was " .$result['server_response']);
} else {
  print "Response " . $result['server_response'];
}*/

function send_message_bulk_sms( $post_body ) {
    
  $url='https://api.bulksms.com/v1/messages';
  $username = 'Mojasms';
  $password = 'Sithole@14';
    
  $ch = curl_init( );
  $headers = array(
  'Content-Type:application/json',
  'Authorization:Basic '. base64_encode("$username:$password")
  );
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt ( $ch, CURLOPT_URL, $url );
  curl_setopt ( $ch, CURLOPT_POST, 1 );
  curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
  curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_body );
  // Allow cUrl functions 20 seconds to execute
  curl_setopt ( $ch, CURLOPT_TIMEOUT, 20 );
  // Wait 10 seconds while trying to connect
  curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
  $output = array();
  $output['server_response'] = curl_exec( $ch );
  $curl_info = curl_getinfo( $ch );
  $output['http_status'] = $curl_info[ 'http_code' ];
  $output['error'] = curl_error($ch);
  curl_close( $ch );
  return $output;
  
} 
     

function send_message_bulk_sms_balance() {
    
  $url='https://api.bulksms.com/v1/profile';
  $username = 'Mojasms';
  $password = 'Sithole@14';
    
  $ch = curl_init( );
  $headers = array(
  'Content-Type:application/json',
  'Authorization:Basic '. base64_encode("$username:$password")
  );
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt ( $ch, CURLOPT_URL, $url );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $output = curl_exec($ch);
  curl_close( $ch );
  return $output;
  
} 

function get_active_reseller(){
    
    global $server;
    $ch=mysqli_query($server, "select * from users WHERE mms_balance > 0");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt=$tt+$rows['mms_balance'];
    }
    
    return $tt;
    
}


function get_user_sms_total(){
    
    global $server;
    $ch=mysqli_query($server, "select * from users WHERE balance > 0");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt=$tt+$rows['balance'];
    }
    
    return $tt;
    
}

function get_user_vms_total(){
    
    global $server;
    $ch=mysqli_query($server, "select * from users WHERE vms_balance > 0");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt=$tt+$rows['vms_balance'];
    }
    
    return $tt;
    
}

function get_user_mms_total(){
    
    global $server;
    $ch=mysqli_query($server, "select * from users WHERE mms_balance > 0");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt=$tt+$rows['mms_balance'];
    }
    
    return $tt;
    
}

function cell_number_download_quantity($id){
    
    global $server;
    $ch=mysqli_query($server, "select * from transactions WHERE customer_id='$id' AND status='Completed'");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt=$rows['cellphone_id'];
    }
    
    
    $chh=mysqli_query($server, "select * from cell_numbers WHERE id='$tt'");
    
    $ttt=0;
    
    while ($rowss = mysqli_fetch_assoc($chh)) {
        $ttt=$rowss['number'];
    }
    
    $haiya=time();
    
    $donwload='SELECT * INTO OUTFILE "/uploads/'.$haiya.'.csv" FIELDS TERMINATED BY \',\' OPTIONALLY ENCLOSED BY \'"\' LINES TERMINATED BY "\n" FROM cell_number_download LIMIT '.$ttt;
    
    mysqli_query($server,$donwload);
    
    return $haiya.'.csv';
    
}

function check_cell_number_downloadable($id){
    
    global $server;
    $ch=mysqli_query($server, "select * from transactions WHERE customer_id='$id' AND status='Completed' AND cellphone_id!='NULL'");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt++;
    }
    
    return $tt;
    
}

function check_cell_number_pending_due($id){
    
    global $server;
    $ch=mysqli_query($server, "select * from transactions WHERE customer_id='$id' AND status!='Completed' AND cellphone_id!='NULL'");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt++;
    }
    
    return $tt;
    
}

function check_virtual_number_pending_due($id){
    
    global $server;
    $ch=mysqli_query($server, "select * from transactions WHERE customer_id='$id' AND status!='Completed' AND virtual_number_id!='NULL'");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt++;
    }
    
    return $tt;
    
}

function check_cell_number_pending_due_order($id){
    
    global $server;
    $ch=mysqli_query($server, "select * from transactions WHERE customer_id='$id' AND status!='Completed'");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt++;
    }
    
    return $tt;
    
}

function check_cell_number_pending_due_ordermms($id){
    
    global $server;
    $ch=mysqli_query($server, "select * from transactions WHERE customer_id='$id' AND status!='Completed' AND package_type='mms'");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt++;
    }
    
    return $tt;
    
}
function check_cell_number_pending_due_ordersms($id){
    
    global $server;
    $ch=mysqli_query($server, "select * from transactions WHERE customer_id='$id' AND status!='Completed' AND package_type='sms'");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt++;
    }
    
    return $tt;
    
}

function filtervar($input){
    return filter_var(trim($input), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
}

function check_cell_number_pending_due_ordervms($id){
    
    global $server;
    $ch=mysqli_query($server, "select * from transactions WHERE customer_id='$id' AND status!='Completed' AND package_type='vms'");
    $tt=0;
    
    while ($rows = mysqli_fetch_assoc($ch)) {
        $tt++;
    }
    
    return $tt;
    
}

function get_api_sms_balance(){
    $active=activeApi();
    
    if($active=='global'){
        //global
        $smss=round(url_get_contents("http://148.251.196.36/app/miscapi/1XfUGDAM/getBalance/true/"));
    }elseif($active=='bulk'){
        //bulk
        $smss=send_message_bulk_sms_balance();
    }else{
        //zoom
         $server_output=$zoom_connect=url_get_contents("https://www.zoomconnect.com:443/app/api/rest/v1/account/balance?token=d7923de2-b46e-41a8-8885-679a9d42ea0f&email=admin%40mojasms.com");
                             
         $xml = simplexml_load_string($server_output);
         $smss = (string) $xml->creditBalance;
    }
    
    return $smss;
    
}

  // Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

?>
