<?php 

define('ENVIRONMENT', 'production');

ob_start();
session_start();

$URL = 'dashboard';

if(isset($_GET['url']))
{ 
	$URL = $_GET['url'];
}
echo '<span style="display: none;">'.@$_SESSION['MOBIKETA'].'</span>';
if(isset($_GET['logout'])) {
	setcookie("MOBIKETA", '', $timeout, '/'); // clear password;
	unset($_COOKIE['MOBIKETA']);
	unset($_SESSION['MOBIKETA']);
	
  header("location: index.php");
  exit;
}


if (defined('ENVIRONMENT'))
{
	switch (ENVIRONMENT)
	{
		case 'development':
			error_reporting(E_ALL);
		break;
	
		case 'testing':
		case 'production':
			error_reporting(0);
		break;

		default:
			exit('The application environment is not set correctly.');
	}
}


//$timezone = "Asia/Bangkok";
//if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone); 
$system_path = 'system';
$application_folder = 'hooks';

	// Set the current directory correctly for CLI requests
	if (defined('STDIN'))
	{
		chdir(dirname(__FILE__));
	}

	if (realpath($system_path) !== FALSE)
	{
		$system_path = realpath($system_path).'/';
	}

	// ensure there's a trailing slash
	$system_path = rtrim($system_path, '/').'/';

	// Is the system path correct?
	if ( ! is_dir($system_path))
	{
		exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
	}
	// The name of THIS file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
	// The PHP file extension
	// this global constant is deprecated.
	define('EXT', '.php');
	// Path to the system folder
	define('BASEPATH', str_replace("\\", "/", $system_path));
	// Path to the front controller (this file)
	define('FCPATH', str_replace(SELF, '', __FILE__));
	// Name of the "system folder"
	define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));

	// The path to the "application" folder
	if (is_dir($application_folder))
	{
		define('APPPATH', $application_folder.'/');
	}
	else
	{
		if ( ! is_dir(BASEPATH.$application_folder.'/'))
		{
			exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
		}

		define('APPPATH', BASEPATH.$application_folder.'/');
	}


define('COR', BASEPATH.'core/');	
define('LIB', BASEPATH.'libraries/');	
define('HELP', BASEPATH.'helpers/');	
require_once 'root.php';
require_once BASEPATH.'core/Config.php';
require_once BASEPATH.'Services/Twilio.php';
foreach (glob(COR."/*.php") as $filename) { include $filename; }
foreach (glob(HELP."/*.php") as $filename) { include $filename; }
foreach (glob(LIB."/*.php") as $filename) { include $filename; }
$Config = new CI_Config();
setTimeZone();
//include payment stuffs
require_once BASEPATH.'includes/parameters.php';
require_once BASEPATH.'includes/Stripe/lib/ini.php';
url_get_contents('http://mojasms.com/app/crons.php');
require_once BASEPATH.'includes/header.php';
require_once BASEPATH.'includes/menu.php';
which_app($URL);
require_once BASEPATH.'includes/footer.php';