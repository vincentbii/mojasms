<?php
include('includes/header.php');
?>
<style>
 @import url(https://fonts.googleapis.com/css?family=Lato:400,700|Montserrat:400,700);
@import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css);


    .border {
    width:100%; 
	margin-top: 10px;
	padding-left:10px;
	padding-right:10px
	border-width: thick;
    }



/*/ start count stats /*/

section#counter-stats {
	display: flex;
	justify-content: center;
	margin-top: 50px;
}

.stats {
  text-align: center;
  font-size: 15px;
  font-weight: 700;
  font-family: 'Montserrat', sans-serif;
}

.stats .fa {
  color: #A6C33A;
  font-size: 30px;
}

/*/ end count stats /*/
</style>

<div class="slide-one-item home-slider owl-carousel">
      <div class="site-blocks-cover overlay" style="background-image: url(<?php echo $base_url;?>/images/hero_bg_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-10">
              <h1 class="mb-2">We Globalize Your Business Cost<br>Effectly and Instantly</h1>
              <p class="mb-5"></p>
              <p>
                <a href="<?php echo $base_url;?>app/index.php?join" class="btn btn-primary py-3 px-5 rounded-0">Sign Up</a>
                <a href="<?php echo $base_url;?>contact.php" class="btn btn-white btn-outline-white py-3 px-5 rounded-0">Get In Touch</a>
              </p>
            </div>
          </div>
        </div>
      </div>  

      <div class="site-blocks-cover overlay" style="background-image: url(<?php echo $base_url;?>/images/hero_bg_2.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-10">
              <h1 class="mb-5">Bulk SMS Message To Cellphone Anywhere Worldwide</h1>
              <p>
                <a href="<?php echo $base_url;?>/app/index.php?join" class="btn btn-primary py-3 px-5 rounded-0">Register</a>
                <a href="<?php echo $base_url;?>/app/" class="btn btn-white btn-outline-white py-3 px-5 rounded-0">Login</a>
              </p>
            </div>
          </div>
        </div>
      </div> 
        <div class="site-blocks-cover overlay" style="background-image: url(<?php echo $base_url;?>/images/hero_bg_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-10">
                <h1>Free SMS Account Registration. Send SMS Message Immediately</h1>
                <p class="mb-5">Free SMS Software</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section bg-light">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade">
            <h2>MOJASMS – BULK SMS MESSAGES</h2>
          </div>
        </div>
        <div class="row hosting">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">

            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <span class="icon fl-bigmug-line-paper122 text-primary"></span>
              <h3 class="h4 text-black">Lowest Prices</h3>
              <p><a href="#" class="btn btn-primary py-2 px-4 rounded-0">View Details</a></p>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
            
            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <span class="icon fl-bigmug-line-airplane86 text-primary"></span>
              <h3 class="h4 text-black">Free Software</h3>
              <p><a href="#" class="btn btn-primary py-2 px-4 rounded-0">View Details</a></p>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <span class="icon fl-bigmug-line-hot67 text-primary"></span>
              <h3 class="h4 text-black">Automated Database Creation</h3>
              <p><a href="#" class="btn btn-primary py-2 px-4 rounded-0">View Details</a></p>
            </div>

          </div>
        </div>
      
      </div>
    </div>

    <div class="site-blocks-cover overlay" style="background-image: url(<?php echo $base_url;?>/images/hero_bg_3.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center text-left">
            <div class="col-md-7">
              <h1 class="mb-2">Satisfied Clients</h1>
              <p class="mb-5">We are rated 4.99 / 5.00 (based on 4443 Reviews)</p>
              <p>
                <a href="<?php echo $base_url;?>/app/index.php?join" class="btn btn-primary py-3 px-5 rounded-0">Register Now</a>
              </p>
            </div>
          </div>
        </div>
      </div>  

    <div class="site-section">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade" >
            <h2>It's time to save on SMS</h2>
             <p class="mb-5">Price Guarantee:<br> Find it cheaper & we'll beat the price!</p>
              <p> 
          </div>
        </div>
        <div class="row hosting">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="100">

            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-equalization3"></span>
                </div>
                <h2 class="h5">Business In Countries Worldwide</h2>
              </div>
              <div class="unit-3-body">
                <p>MojaSMS provides Bulk SMS services to business in countries worldwide</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="200">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-cube29"></span>
                </div>
                <h2 class="h5">Most Delivered</h2>
              </div>
              <div class="unit-3-body">
                <p>MojaSMS has delivered more than 10 million SMS messages globally the past few years</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-airplane86"></span>
                </div>
                <h2 class="h5">Outstanding Confirmations</h2>
              </div>
              <div class="unit-3-body">
                <p>97% of customers prefer to buy from a business that provides bulk SMS confirmations</p>
              </div>
            </div>

          </div>

          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="400">

            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-hot67"></span>
                </div>
                <h2 class="h5">Amazing Uptime</h2>
              </div>
              <div class="unit-3-body">
                <p>SMS is accessible on almost 100% of handsets</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="500">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-headphones32"></span>
                </div>
                <h2 class="h5">Direct SMS Campaigns</h2>
              </div>
              <div class="unit-3-body">
                <p>Direct SMS campaigns yield a 30% response rate on average.(Direct Mail is 2.6%, Pamphlets is 0.5% and Published Media is 8.5%)</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="600">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-user143"></span>
                </div>
                <h2 class="h5">Mobile Marketing</h2>
              </div>
              <div class="unit-3-body">
                <p>Since the introduction FAX machines, mobile marketing via SMS has become and still is the most popular form of business communication</p>
              </div>
            </div>

          </div>

        </div>
      
      </div>
    </div>
 <div class="site-section" style="padding-bottom:10px;">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade" >
            <h2>WHY IS SMS STILL THE NO.1 MARKETING CHANNEL?</h2>
             The cell phone is the most pervasive media on the planet and 80% of cell phone subscribers use SMS regularly 
          </div>
        </div>
        <div class="row hosting">
            
    <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="100">

            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-equalization3"></span>
                </div>
                <h2 class="h5">Sustainable</h2>
              </div>
              <div class="unit-3-body">
                <p>SMS messages are far more widely available, more affordable and almost all cell phone users know how to use SMS. Sms messages haven’t decreased in any way as a result of other mobile media, including mobile Web, email, applications, IM, multimedia messaging or mobile social networking such as Facebook or Twitter. For the foreseeable future it firmly remains the second most used mobile utility after voice calling</p>
              </div>
            </div>

          </div>        

          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="200">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-cube29"></span>
                </div>
                <h2 class="h5">SMS Marketing</h2>
              </div>
              <div class="unit-3-body">
                <p>SMS messages are widely accepted as a channel for services such as news alerts, customer-interaction, complaints, CRM, surveys and many more. This channel also proved an increase in customer loyalty and willingness to be added to opt-in databases. Companies that realised this trend early have reaped the rewards, beyond expectation</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-airplane86"></span>
                </div>
                <h2 class="h5">Time/Availibility</h2>
              </div>
              <div class="unit-3-body">
                <p>SMS doesn’t require both parties to be available at the same time. The marketing message can still be sent even when the recipient’s phone is switched off or there is no network signal. It will be delivered when the phone becomes available and sits in the inbox to be read when the recipient is free – and remains there until the message is deleted</p>
              </div>
            </div>

          </div>

          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="400">

            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-hot67"></span>
                </div>
                <h2 class="h5">Versatility</h2>
              </div>
              <div class="unit-3-body">
                <p>There’s much more to SMS than 160 characters. SMS often underpins many mobile services – it’s an excellent platform for exchanging information between applications</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="500">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-headphones32"></span>
                </div>
                <h2 class="h5">Premium Services</h2>
              </div>
              <div class="unit-3-body">
                <p>Media companies, particularly, encourage viewers/readers to send in feedback, tips for stories, pictures and join in promotions using SMS. Marketing campaigns will often encourage customers to participate in selection issuess.</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="600">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-user143"></span>
                </div>
                <h2 class="h5">Interactivity</h2>
              </div>
              <div class="unit-3-body">
                <p>Interactivity is imminent since it’s easy for customers to reply, to act and/or click-to-browse links in the message, making the mobile a great channel for lead generation</p>
              </div>
            </div>

          </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="600">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-cube29"></span>
                </div>
                <h2 class="h5">Cheaper than Mail/Post/Voice calls</h2>
              </div>
              <div class="unit-3-body">
                <p>Comparing the cost of sending bulk SMS as opposed to the cost of direct mail and/or letters/stamps, SMS is between 70-85% more cost effective</p>
              </div>
            </div>

          </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-4" data-aos="fade" data-aos-delay="600">
            
            <div class="unit-3 h-100 bg-white">
              
              <div class="d-flex align-items-center mb-3 unit-3-heading">
                <div class="unit-3-icon-wrap mr-4">
                  <svg class="unit-3-svg" xmlns="http://www.w3.org/2000/svg" width="59px" height="68px">
                    <path fill-rule="evenodd" stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M29.000,66.000 L1.012,49.750 L1.012,17.250 L29.000,1.000 L56.988,17.250 L56.988,49.750 L29.000,66.000 Z"></path>
                  </svg><span class="unit-3-icon icon fl-bigmug-line-user143"></span>
                </div>
                <h2 class="h5">Our Clients</h2>
              </div>
              <div class="unit-3-body">
                <p>We provide SMS solutions to ALL industries; From Schools, Doctors, Plumbers, Artists, Debt Collectors, Banks, Restaurants to Political Parties with our user-friendly business tools to: do cost effective marketing, save money, increase sales and improve communication[/fusion_text]</p>
              </div>
            </div>

          </div>
          
          
        </div>
        
        <?php include('live_stats.php');?>

      </div>
    </div>

    
<?php
include('includes/footer.php');
?>