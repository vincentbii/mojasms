<?php
include('includes/header.php');
?>

    <div class="unit-5 overlay" style="background-image: url('<?php echo $base_url;?>/images/hero_bg_1.jpg');">
      <div class="container text-center">
        <h2 class="mb-0">Bulk SMS Service</h2>
        <p class="mb-0 unit-6"><a href="<?php echo $base_url;?>">Products</a> <span class="sep">></span> <span>Bulk SMS Service</span></p>
      </div>
    </div>
      
    <div class="py-5 bg-primary">
      <div class="container">
        <div class="row align-items-center">
         <div class="col-lg-6 mb-5 mb-lg-0">
            <div>
             <h2><strong class="d-block text-white">FREE SMS PROGRAM PROVIDED</strong></h2>
            <h3>(SALES, MARKETING, CUSTOMER BIRTHDAYS, CUSTOMER CARE &amp; INFORMATION SMS’s)</h3>    
              <div class="border-right pr-5">
              <p class="text-white h6">Join thousands of businesses and organizations, from all industry sectors, using us to find new prospects, increase sales and develop exceptional brand loyalty</p><br>
              
              
              <p class="text-white h6">Your next SMS campaign could be delivered straight into the hands of your prospects and customers (Online SMS Program – No Installation required)</p><br>
            
             
              <p class="text-white h6">Sending via <strong>SMSS’s</strong> App is completely <strong>free</strong>. You pay only for the SMS messages you send</p>
              </div>
                
            <p>To <strong>start</strong> now, click <a href="<?php echo $base_url;?>/app/index.php?join" class="btn btn-danger btn-lg">HERE</a></p>    
                             
            </div>
          </div> 
  
            
        
          <div class="col-lg-6 ml-md-auto">
            <div>
                
                
        <h2 class="text-white">START SENDING VIA ANY OF THE METHODS BELOW:</h2>        
                
        <ol class="text-black">
            <li>SMS using our FREE <strong>online</strong> SMS Program</li>
            <li>SMS using your <strong>email</strong> to any <strong>cell</strong> number</li>
            <li>SMS using your <strong><i>own</i> system/program/user interface</strong> or <strong>software</strong> via <strong>API</strong> (HTTP &amp; <strong>SMPP</strong> Connectivity)</li>
            <li>SMS using our <strong>SMSS Managed System</strong> (You provide numbers &amp; messages, we do the rest)</li>
            <li>SMS using our unique <strong>Remote Bulk SMS Program</strong> (No internet needed)</li>
        </ol> 
              
              
            </div>
          </div>
        </div>
      </div>
    </div>
 <div class="site-section bg-light">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade">
            <h2>5 Easy steps to send SMS messages within a few seconds</h2>
          </div>
        </div>
        <div class="row hosting">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">

            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <img src="<?php echo $base_url;?>images/orange 1.png">
              <h3 class="h4 text-black">Register a free account</h3>
              <p><a href="#" class="btn btn-primary py-2 px-4 rounded-0">Free Registration</a></p>
            </div>

          </div>

          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <img src="<?php echo $base_url;?>images/orange 2.png">
              <h3 class="h4 text-black">Login account and create your sms</h3>
              <p><a href="#" class="btn btn-primary py-2 px-4 rounded-0">See Example</a></p>
            </div>

          </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <img src="<?php echo $base_url;?>images/orange 3.png">
              <h3 class="h4 text-black">Send your SMS, View Replies</h3>
              <p><a href="#" class="btn btn-primary py-2 px-4 rounded-0">See Example</a></p>
            </div>

          </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <img src="<?php echo $base_url;?>images/orange 4.png">
              <h3 class="h4 text-black">View SMS delivery report</h3>
              <p><a href="#" class="btn btn-primary py-2 px-4 rounded-0">See Example</a></p>
            </div>

          </div>
        </div>
      
      </div>
    </div>
 <div class="row justify-content-center">   
    <picture class="thumbnail">        
    <img class="img-fluid" src="<?php echo $base_url;?>Bulksms%20Table-01-01.jpg">  </picture>      
      </div> 
<div class="site-section bg-danger">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade">
            <h2 class="text-white">Option.2 – We send SMS messages on your behalf</h2>
          </div>
        </div>
        <div class="row hosting">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">

            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <img src="<?php echo $base_url;?>images/orange 1.png">
              <h3 class="h4 text-black">Create your SMS &amp; a list of cell number to receive your SMS</h3>
              <p><a href="#" class="btn btn-danger py-2 px-4 rounded-0">See Example</a></p>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
            
            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <img src="<?php echo $base_url;?>images/orange 2.png">
              <h3 class="h4 text-black">Email the list and the SMS message to info@mojasms.co.za</h3>
              <p><a href="#" class="btn btn-danger py-2 px-4 rounded-0">See Example</a></p>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <img src="<?php echo $base_url;?>images/orange 3.png">
              <h3 class="h4 text-black">Make an EFT payment or buy online &amp; attach POP to email</h3>
              <p><a href="#" class="btn btn-danger py-2 px-4 rounded-0">See SMS Bundels</a></p>
            </div>

          </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <img src="<?php echo $base_url;?>images/orange 4.png">
              <h3 class="h4 text-black">We INSTANTLY send the SMS messages on your behalf</h3>
              <p><a href="#" class="btn btn-danger py-2 px-4 rounded-0">See Example</a></p>
            </div>

          </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
            
            <div class="unit-2 text-center border py-5 h-100 bg-white">
              <img src="<?php echo $base_url;?>images/orange 5.png">
              <h3 class="h4 text-black">You’ll receive a Receipt &amp; a Report of delivered messages</h3>
              <p><a href="#" class="btn btn-danger py-2 px-4 rounded-0">See Example</a></p>
            </div>

          </div>
        </div>
      
      </div>
    </div>      


<?php
include('includes/footer.php');
?>