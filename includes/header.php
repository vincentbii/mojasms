<?php
  $base_url = "https://www.mojasms.com/";
  
  // Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
//function get_country($ipaddress) {
    
//}


include('con.php');

$ip=get_client_ip();


$sqler="SELECT * FROM `visitors` WHERE ip='$ip' AND date='".date('Y-m-d')."'";

$result = $conn->query($sqler);

if($result->num_rows == 0){
    
    $sqlerr="INSERT INTO `visitors`(`ip`, `date`) VALUES ('$ip','".date('Y-m-d')."')";

    $conn->query($sqlerr);
}
  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>MojaSMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Roboto+Mono:300,400,500"> 
    <link rel="stylesheet" href="<?php echo $base_url;?>/fonts/icomoon/style.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/css/animate.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/css/fl-bigmug-line.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/css/aos.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $base_url;?>/assets/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
    
        <!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Add icon library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script sric="https://cdnjs.cloudflare.com/ajax/libs/jquery-localScroll/1.4.0/jquery.localScroll.min.js"></script>
  
<style>

.fa {
  padding:5px;
  font-size: 20px;
  width: 30px;
  height:30px;
  text-align: center;
  text-decoration: none;
  border-radius: 15px;
}





.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-envelope {
    color:#f1d592;
}



.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-youtube {
  background: #bb0000;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

.fa-pinterest {
  background: #cb2027;
  color: white;
}

.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.fa-skype {
  background: #00aff0;
  color: white;
}

.fa-whatsapp {
  background: #25D366;
  color: white;
}
</style>

    
    </head>
  

    
    
    
    
    
  <body>
 <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    <div class="site-navbar-wrap bg-white">
      <div class="site-navbar-top">
        <div class="container py-2">
          <div class="row align-items-center">
            
            <div class="col-6 col-md-6 col-lg-2">
              <a href="<?php echo $base_url;?>index.php" class="d-flex align-items-center site-logo">
                 <img class="img-responsive" src="<?php echo $base_url;?>/logo.jpeg" alt="Mojasms Logo" style="height:50px">
             </a>
            </div>

            <div class="col-6 col-md-6 col-lg-10">
              <ul class="unit-4 ml-auto text-right">

                <li class="text-left">
                  <a href="#">
                    <div class="d-flex align-items-center block-unit">
                      <div class="icon mr-0 mr-md-4">
                        <span class="fa fa-facebook fa-2x h3"></span>
                      </div>
                      <div class="d-none d-lg-block">
                        <span class="d-block text-gray-500 text-uppercase">Facebook</span>
                        <span class="h6">Mojasms </span>
                      </div>
                    </div>
                  </a>
                </li>


                <li class="text-left">
                  <a href="#">
                    <div class="d-flex align-items-center block-unit">
                      <div class="icon mr-0 mr-md-4">
                        <span class="fa fa-envelope fa-2x h5"></span>
                      </div>
                      <div class="d-none d-lg-block">
                        <span class="d-block text-gray-500 text-uppercase">Email</span>
                        <span class="h6">info@mojasms.com</span>
                      </div>
                    </div>
                  </a>
                </li>

                <li class="text-left">
                  <a href="#">
                    <div class="d-flex align-items-center block-unit">
                      <div class="icon mr-0 mr-md-4">
                       <a href="https://wa.me/27"> <span class="fa fa-whatsapp fa-2x h2" aria-hidden="true"></span></a>
                      </div>
                      <div class="d-none d-lg-block">
                        <span class="d-block text-gray-500 text-uppercase">WhatsApp</span>
                        <span class="h6">Chat With Us Now</span>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          
        </div>
      </div>
      <div class="site-navbar bg-dark">
        <div class="container py-1">
          <div class="row align-items-center">

            <div class="col-4 col-md-4 col-lg-8">
              <nav class="site-navigation text-left" role="navigation">
                <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3">
		<a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

                <ul class="site-menu js-clone-nav d-none d-lg-block">
                  <li class="active">
                    <a href="<?php echo $base_url;?>index.php">Home</a>
                  </li>
                  
                  <li class="has-children">
                    <a href="#">Products/Services</a>
                    <ul class="dropdown arrow-top">
                      <li><a href="<?php echo $base_url;?>bulk_sms_service.php">Bulk SMS Service</a></li>
                        <li><a href="<?php echo $base_url;?>bulk_mms_service.php">Bulk MMS Service</a></li>
                        <li><a href="<?php echo $base_url;?>virtual_number.php">Virtual Number</a></li>
                        <li><a href="<?php echo $base_url;?>automated_voice_sms.php">Automated Voice SMS</a></li>
                        <li><a href="<?php echo $base_url;?>cell_number_database.php">Cell Number Database</a></li>
                        <li><a href="<?php echo $base_url;?>api.php">API</a></li>
                    </ul>
                  </li>
                    <li class="has-children">
                        <a href="#">Keywords</a>
                        <ul class="dropdown arrow-top">
                        <li><a href="<?php echo $base_url;?>sms_keywords.php">SMS Keywords</a></li>
                    </ul>
                      </li>
                  <li><a href="<?php echo $base_url;?>contact.php">Contact</a></li>
                  <li><a href="<?php echo $base_url;?>faq.php">FAQ</a></li>
                </ul>
              </nav>
            </div>
            <div class="col-8 col-md-8 col-lg-4 text-right">

              <a href="<?php echo $base_url;?>app/index.php?join" class="btn btn-primary btn-outline-primary rounded-0 text-white py-2 px-4">Register</a>
              <a href="<?php echo $base_url;?>app/" class="btn btn-primary btn-primary rounded-0 py-2 px-4">Login</a>
            </div>


          </div>
        </div>
      </div>
    </div>
    


