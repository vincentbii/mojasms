<?php include('slider.php');?>
<footer class="site-footer p-3 justify-content-center text-center bg-primary w-100" style="color:black;padding-top:1px;">
    
    
    <div class="col-sm-12 bg-primary w-100" style="text-align:center;color:white;padding-bottom:1px;">
      <div class="container">
          <div class="col-md-12 justify-content-center text-center">
              <address>
            <strong>Contact Us On</strong><br>
<span class="fa fa-phone" aria-hidden="true"></span>&nbsp;(010) 516-03509<br>      
<span class="fa fa-whatsapp" aria-hidden="true"></span>&nbsp;(079) 479-4811<br>               
<span class="fa fa-envelope" aria-hidden="true"></span>&nbsp;info@mojasms.com
              </address>
                
            </div>

       </div>
    </div>
    <div class="col-sm-12 bg-black w-100" style="background:rgba(0,0,0,1);text-align:center;color:#fff;padding-bottom:1px;font-size:12px;">
        <div class="container">
              <div class="col-md-12">
               <img class="img-responsive" src="<?php echo $base_url;?>/pennypicks-SSL-security-seal2.png" alt="pennypicks-SSL-security-seal2.png" style="height:60px;width:60px">
               <img class="img-responsive rounded" src="<?php echo $base_url;?>/paypal_visa_master.png" alt="paypal_visa_master.png" style="height:50px;width:130px">
              </div>
            
            <div class="col-md-12">
            <p>
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All Rights Reserved | <i class="icon-heart text-danger" aria-hidden="true"></i>
            
            </p>
          </div>
          
        </div>
      </div>
    </footer>
  </div>
  
  <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5cb5ccc2c1fe2560f3ff1626/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
  
  <script src="<?php echo $base_url;?>/js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo $base_url;?>/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?php echo $base_url;?>/js/jquery-ui.js"></script>
  <script src="<?php echo $base_url;?>/js/popper.min.js"></script>
  <script src="<?php echo $base_url;?>/js/bootstrap.min.js"></script>
  <script src="<?php echo $base_url;?>/js/owl.carousel.min.js"></script>
  <script src="<?php echo $base_url;?>/js/jquery.stellar.min.js"></script>
  <script src="<?php echo $base_url;?>/js/jquery.countdown.min.js"></script>
  <script src="<?php echo $base_url;?>/js/jquery.magnific-popup.min.js"></script>
  <script src="<?php echo $base_url;?>/js/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo $base_url;?>/js/aos.js"></script>
  <script src="<?php echo $base_url;?>/js/main.js"></script>
  <script src="<?php echo $base_url;?>/assets/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>
  <script>
     $(document).ready(function(){
         
      $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        autoplay:true,
        items: 1,
        slideBy: 1,
        rtl:true,
        singleItem: true
      });
      
      $(".site-menu-toggle").click(function(){
      
          $(".site-nav-wrap").find('li.has-children').each(function(){
              $(this).find('span').removeClass('collapsed');
              $(this).find('span').addClass('active');
              $(this).find('ul').addClass('show');
          });
      
      });
        
    });
  </script>
  </body>
</html>
