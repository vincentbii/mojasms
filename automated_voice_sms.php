<?php
include('includes/header.php');
?>


    <div class="unit-5 overlay" style="background-image: url('<?php echo $base_url;?>images/hero_bg_1.jpg');">
      <div class="container text-center">
        <h2 class="mb-0">Automated Voice SMS</h2>
        <p class="mb-0 unit-6"><a href="<?php echo $base_url;?>">Products</a> <span class="sep">></span> <span>Automated Voice SMS</span></p>
      </div>
    </div>
      
    <div class="site-section bg-light">
      <div class="container">
        
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-2 col-sm-12">
            <center><img src="<?php echo $base_url;?>images-old/automated voice message.png" class="img img-responsive"></center>
        </div>
        
        <div class="col-md-10 col-sm-12">
            <p>The <strong>Voice SMS</strong> (Automated Voice Message) is a personalized <strong>pre-recorded</strong> message that is broadcasted to thousands of cell phones or landlines in a short period of time.<br>
            Included is the possibility for the recipient of the call to interact or to be diverted to a call center by using the phone’s keypad.</p>
            
            <p>Easy to implement, the voice message can quickly be integrated in your communication strategy.<br>
            <strong>Be proactive, don’t wait for customers to contact you.</strong></p>
        </div>
    </div>  
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <br><strong>Also known as:</strong>
        <div class="padding">
            <ul>
                <li>Bulk Voice Messages</li>
                <li>Automated Voice Messages</li>
                <li>Voice SMS</li>
                <li>Voice Text Messages</li>
                <li>Voice Drops</li>
            </ul>
        </div>
    </div>
    <div class="row">
        
<h3><a href="<?php echo $base_url;?>/app/index.php?join">Send a <strong>FREE</strong> test <strong>AVM</strong> now. Click <strong>HERE</strong></a></h3>

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
<a class="btn btn-lg btn-primary" href="<?php echo $base_url;?>/app/index.php?join"><strong>Register Now Completely Free</strong></a>     
            </div>       
          </div>
        </div>
      </div>
      
      
    <div class="site-section bg-primary">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade">
            <h2 class="text-white">Benefits</h2>
          </div>
        </div>
        <div class="row">
     <ul class="text-white">
        <li>No set up fee, no subscription fee: the service is usage based</li>
        <li>State of the art voicemail detection</li>
        <li>Pay only for recipients reached</li>
        <li>High outbound capacity</li>
        <li>Warm and personalized technology</li>
        <li>Message can be done in any language</li>
        <li>Reach cell phone and landline: people receive the call like a regular phone call</li>
        <li>Go green using electronic media</li>
        <li>The service is 100% hosted and you do not require any hardware of software installation</li>
        <li>Improve your response rates using voice messaging as a subsitiute for, or a supplement to, your traditional customer contact methods</li>
        <li>Cost-effectively increase revenue, enhance customer service and retention, and secure payments in a more timely fashion</li>
        <li>Reduce inbound calls to your contact center by proactively communicating with customers</li>
        <li>Increase agent productivity by automating routine tasks, freeing agents to spend time talking to valued customers</li>
    </ul>
      <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade">
            <audio controls="">
            <source src="<?php echo $base_url;?>images/Casino Natasha 2 Survey.mp3" type="audio/mpeg" title="Casino Natasha 2 Survey">
            Your browser does not support the audio element.
        </audio>

    <h2 class="text-white"><a href="<?php echo $base_url;?>app/index.php?join">Send a <strong>FREE</strong> test <strong>AVM</strong> now. Click <strong>HERE</strong></a></h2>
              
              <p class="text-white">An alternative method to reach thousands of customers, members, or new contacts like never before with our easy, efficient and cost-effective software.</p>  
          </div>
        </div>      
          
          </div>
        </div>
      </div>
      
      
<div class="site-section bg-light">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade">
            <h2 >THE CONCEPT</h2>
          </div>
        </div>
          
  <div class="row">
      
       <ul>
        <li><strong>Hot Key Transfer:</strong> At any time during the message the call can be transferred to your call center or reception desk by pressing a key on the phone’s keypad</li>
        <li><strong>Interaction:</strong> Interact with your customers by the use of the phone’s key pad. Recipients can answer questions, give confirmations, and register for events…</li>
        <li><strong>Text to Speech:</strong> Personalize your message by inserting variable such as name, dates, amounts… thanks to our text to speech technology</li>
        <li><strong>Data Collections:</strong> Evaluate the impact and performance of your campaigns using our reporting feature. After each campaign you will get a detailed report giving you valuable information on who received the call, how long the recipients listened to the message and which key was pressed…, enabling you to take appropriate actions.</li>
        <li><strong>Dual Delivery:</strong> Our technology can accurately detect Voicemails and answering Machines enabling you to serve only live calls or to leave a different message on voicemails.</li>
        <li><strong>Right Party Authentications:</strong> Confirm recipients’ identities before proceeding with the call.</li>
        <li><strong>Automatic retries:</strong> Unreached and busy calls will be tried again automatically to maximize the impact of your campaign.</li>
        <li><strong>Delivery schedule:</strong> Customize date and time of broadcast and retries.</li>
        <li><strong>Campaign Pacing:</strong> Our system will adapt the flow of the outbound calls according to you call center capacity making sure that none of your agent are idle but also that the call center is not overwhelmed.</li>
    </ul>            
          </div>          
    </div>
      </div>
   

    <div class="site-section">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-6" data-aos="fade" >
            <p>SMSS’s Platform allows organizations to seamlessly communicate with their customers across multiple communication channels and mobile devices.<br>
    Our industry- leading automated voice messaging can be used as part of a multi-channel Proactive Customer Communications strategy or as a stand-alone communications channel.<br>
    Organizations rely on SMSS voice messaging to proactively communicate with their customers through all stages of the customer lifecycle.<br>
    Our voice messaging Solutions provide organizations with a wide variety of customer self- service and agent assisted proactive customer communication solutions.</p>
          </div>
        </div>
        

        <div class="row justify-content-center" data-aos="fade" data-aos-delay="100">
          <div class="col-md-8">
            <div class="accordion unit-8" id="accordion">
            <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseOne" role="button" aria-expanded="true" aria-controls="collapseOne">SMS Voice Messaging Options<span class="icon"></span></a>
              </h3>
              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>SMS voice messaging delivers relevant and personalized communications to large volumes of customers around the world.
    Depending on your business objectives, these high quality messages can reduce inbound call volumes, increase customer self-service, or when warranted, connect customers to customer service representatives in your contact center.
    Our flexible, on-demand solution offers a variety of options to accommodate your evolving business needs:</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->
            
            <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseTwo" role="button" aria-expanded="false" aria-controls="collapseTwo">Proactive Notifications<span class="icon"></span></a>
              </h3>
              <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>One-way alerts are an effective way to reach your customers with time-sensitive and personally relevant Information.
    Professionally scripted messages influence customer behavior, driving traffic to your storefront, website, or other self-service channel.</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->

                 <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">Interactive Dialog<span class="icon"></span></a>
              </h3>
              <div id="collapseThree" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>Fully-automated customer interactions provide a consistent, branded customer experience.
    Our flexible scripting and reporting allows you to capture valuable customer insights in a cost-effective manner.</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->
                
                <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">Direct Connect<span class="icon"></span></a>
              </h3>
              <div id="collapseThree" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>For occasions that require a live agent, SMSS can route the right party and their important account information directly to appropriate customer service representatives.</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->
                
                
                 <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">Create a Personal Dialog with Your Customers<span class="icon"></span></a>
              </h3>
              <div id="collapseThree" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>SMSS combines personalizing and interactive scripting options to deliver the most meaningful and productive voice messages to your customers.
    Personalize each voice message with your customer’s name, address or other unique information using text-to-speech (TTS) technology available in a variety of languages, dialects, and male and female voices.
    Choose from a wide array of professional voice talent to match your brand and the tone of your message.</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->
                
                
                
                <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">Maximize Contact Center Productivity<span class="icon"></span></a>
              </h3>
              <div id="collapseThree" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>SMSS provides robust campaign management tools that contact centers can leverage to maximize agent utilization and contact center productivity.
    Contact center managers can create personalized views using SMSS’s customization management console to display real-time contact center activity and drive immediate improvement.
    The easy-to- use console streamlines advanced tools such as skill group administration, call pacing, and campaign management.</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->
                
            <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">Increase Right Party Contacts <span class="icon"></span></a>
              </h3>
              <div id="collapseThree" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>SMSS’s voice messaging provides an automated, interactive verification process to confirm the intended recipient is reached.
    SMSS can automate the entire customer interaction or connect the right party to a customer service representative, increasing agent productivity and saving you money.
    For those customers not available, SMSS’s industry leading answering machine detection ensures that your party receives a clear and complete message every time.</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->
                
                
            <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">Expand Customer Reach<span class="icon"></span></a>
              </h3>
              <div id="collapseThree" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>SMSS Insures maximum reach by simplifying when to call, who to call, and permissible calling windows  built directly into your contact strategy.
    We automatically manage your calling campaign based on enterprise and regulatory compliance rules, individual opt-outs, and wireless phone numbers.
    With SMSS’s Local Connect we can reach more of your customers by presenting them with familiar caller identification numbers allowing them to instantly recognize an important call.
    And we continue to expand our call distribution network by building relationships with multiple telecommunications carriers.</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->    

            <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseFour" role="button" aria-expanded="false" aria-controls="collapseFour">ABOUT SMSS &amp; AVM<span class="icon"></span></a>
              </h3>
              <div id="collapseFour" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>SMSS provides top of the range innovative communication solutions to small, medium and large organizations throughout South Africa, Europe and Africa. Thousands of companies already rely on SMSS’s expertise, flexibility and reliability to take care of their communication campaigns in Marketing, Sales, customer care and Debt collection using Automated Voice Message, Email, Bulk SMS, Fax and Conferencing solutions.</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->

          </div>
          </div>
        </div>
      
      </div>
    </div>




<?php
include('includes/footer.php');
?>